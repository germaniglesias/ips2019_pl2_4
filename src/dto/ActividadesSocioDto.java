package dto;
/**
 * 
 * Dto para realizasr la lista de Actividades por Socio
 * @author German
 *
 */

public class ActividadesSocioDto {

    private String nombre;
    private String horaFin;
    private String horaInicio;
    private String fecha;
    private boolean limitePlazas;
    private String numLimitePlazas;

    
    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getHoraFin() {
	return horaFin;
    }

    public void setHoraFin(String horaFin) {
	this.horaFin = horaFin;
    }

    public String getHoraInicio() {
	return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
	this.horaInicio = horaInicio;
    }

    public String getFecha() {
	return fecha;
    }

    public void setFecha(String fecha) {
	this.fecha = fecha;
    }

    /**
     * Ponemos una condición para mostrar los que si y no tienen limite de
     * plazas
     * 
     * @return "Si" o "No"
     */
    public String isTieneLimitePlazas() {
	if (limitePlazas == true) {
	    return "Si";
	} else {
	    return "No";
	}

    }

    public void setTieneLimitePlazas(boolean tieneLimitePlazas) {
	this.limitePlazas = tieneLimitePlazas;
    }

    public String getNumLimitePlazas() {
	return numLimitePlazas;
    }

    public void setNumLimitePlazas(String numLimitePlazas) {
	this.numLimitePlazas = numLimitePlazas;
    }

}