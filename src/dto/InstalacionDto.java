package dto;

public class InstalacionDto {

    private Long id;
    private String nombre;
    private int precioPorHora;

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public String toString() {
	return getNombre();
    }

    public void setPrecioHora(int precioPorHora) {
	this.precioPorHora = precioPorHora;
    }

    public int getPrecioPorHora() {
	return precioPorHora;
    }
}
