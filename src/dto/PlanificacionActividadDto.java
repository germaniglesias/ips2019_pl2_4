package dto;

public class PlanificacionActividadDto {

    private Long id;
    private Long id_Actividad;
    private String nombre; //De la actividad
    private Long id_Instalacion;
    private Long id_Monitor;
    private String horaFin;
    private String horaInicio;
    private String fecha;
    private boolean limitePlazas;
    private int numLimitePlazas;
    private int numPlazasDisponibles;
    private String nombreInstalacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_Actividad() {
	return id_Actividad;
    }

    public void setId_Actividad(Long id_Actividad) {
	this.id_Actividad = id_Actividad;
    }

    public Long getId_Instalacion() {
	return id_Instalacion;
    }

    public void setId_Instalacion(Long id_Instalacion) {
	this.id_Instalacion = id_Instalacion;
    }

    public String getHoraFin() {
	return horaFin;
    }

    public void setHoraFin(String horaFin) {
	this.horaFin = horaFin;
    }

    public String getHoraInicio() {
	return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
	this.horaInicio = horaInicio;
    }

    public String getFecha() {
	return fecha;
    }

    public void setFecha(String fecha) {
	this.fecha = fecha;
    }

    public Long getId_Monitor() {
	return id_Monitor;
    }

    public void setId_Monitor(Long id_Monitor) {
	this.id_Monitor = id_Monitor;
    }

    public boolean isLimitePlazas() {
	return limitePlazas;
    }

    public void setLimitePlazas(boolean limitePlazas) {
	this.limitePlazas = limitePlazas;
    }

    public int getNumLimitePlazas() {
	return numLimitePlazas;
    }

    public void setNumLimitePlazas(int numLimitePlazas) {
	this.numLimitePlazas = numLimitePlazas;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public int getNumPlazasDisponibles() {
        return numPlazasDisponibles;
    }

    public void setNumPlazasDisponibles(int numPlazasDisponibles) {
        this.numPlazasDisponibles = numPlazasDisponibles;
    }

    public String getNombreInstalacion() {
        return nombreInstalacion;
    }

    public void setNombreInstalacion(String nombreInstalacion) {
        this.nombreInstalacion = nombreInstalacion;
    }
    
    @Override
    public String toString() {
	return nombre;
    }

    public String datos() {
	String str = "La actividad " + nombre + " con instalacion " + nombreInstalacion + "colisiona el día " + fecha.toString() + " de " + horaInicio + " a " + horaFin + " horas.";
	return str;
    }
}