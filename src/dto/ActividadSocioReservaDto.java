package dto;

public class ActividadSocioReservaDto {
    
    public String id_Actividad;
    public String id_Socio;
    public String nombre_Actividad;
    public String nombre_Nombre;
    public String dni_Socio;
    public String fecha;
    public String horario_Fin;
    public String horario_Inicio;
    public int numero_Plazas;
    public boolean is_LimitePlazas;
    public String estado;
    public int id_Instalacion;
    public String id_SocioReservaActividad;
    public String nombre_Instalacion;
    

}
