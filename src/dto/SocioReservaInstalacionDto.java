package dto;

public class SocioReservaInstalacionDto {

    private Long id;
    private Long id_Socio;
    private Long id_Instalacion;
    private String fecha;
    private String horaInicio;

    // Datos externos
    // Instalacion
    private String nombreInstalacion;
    private int precioPorHora;

    // Socio
    private String nombreCompleto;
    private String dni;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Long getId_Socio() {
	return id_Socio;
    }

    public void setId_Socio(Long id_Socio) {
	this.id_Socio = id_Socio;
    }

    public Long getId_Instalacion() {
	return id_Instalacion;
    }

    public void setId_Instalacion(Long id_Instalacion) {
	this.id_Instalacion = id_Instalacion;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getFecha() {
	return fecha;
    }

    public void setFecha(String fecha) {
	this.fecha = fecha;
    }

    public String getHoraInicio() {
	return horaInicio;
    }

    public void setHoraInicio(String string) {
	this.horaInicio = string;
    }

    public String getNombreInstalacion() {
	return nombreInstalacion;
    }

    public String getDni() {
        return dni;
    }

    public void setNombreInstalacion(String nombreInstalacion) {
	this.nombreInstalacion = nombreInstalacion;
    }

    public int getPrecioPorHora() {
	return precioPorHora;
    }

    public void setPrecioPorHora(int precioPorHora) {
	this.precioPorHora = precioPorHora;
    }

    public String getNombreCompleto() {
	return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
	this.nombreCompleto = nombreCompleto;
    }

    @Override
    public String toString() {
	return nombreCompleto;
    }

    public String datos() {
	return "- " + nombreInstalacion + ", " + fecha + ", a las " + horaInicio
		+ "\n\t\tPrecio a pagar: " + precioPorHora + "€";
    }
    
    // Formato: Gimnasio: 12345678A, 2019-11-28 a las 10:00:00
    public String otrosDatos() {
	return nombreInstalacion + ": " + dni + ", " + fecha + " a las " + horaInicio;
    }

}
