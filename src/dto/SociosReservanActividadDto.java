package dto;

public class SociosReservanActividadDto {

    private Long id_Socio;
    private Long id_Planificacion;
    
    public Long getId_Socio() {
        return id_Socio;
    }
    public void setId_Socio(Long id_Socio) {
        this.id_Socio = id_Socio;
    }
    public Long getId_Actividad() {
        return id_Planificacion;
    }
    public void setId_Actividad(Long id_Planificacion) {
        this.id_Planificacion = id_Planificacion;
    }

    
}
