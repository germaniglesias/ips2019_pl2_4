package dto;

public class ActividadDto {

    private Long id;
    private String nombre;
    private String intensidad;
    private boolean usaRecursos;

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getIntensidad() {
	return intensidad;
    }

    public void setIntensidad(String intensidad) {
	this.intensidad = intensidad;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public String toString() {
	String str = nombre + " ";
	str += usaRecursos ? "- Usa Recursos" : "- Sin Recursos";
	return str;

    }

    public String imprimir() {
	return nombre;
    }

    public boolean isUsaRecursos() {
	return usaRecursos;
    }

    public void setUsaRecursos(boolean usaRecursos) {
	this.usaRecursos = usaRecursos;
    }
}