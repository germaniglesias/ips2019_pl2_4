package dto;

public class InstalacionSocioReservaDto {

    public String id_Instalacion;
    public String id_Socio;
    public String nombre_Instalacion;
    public String nombre_Socio;
    public String dni_Socio;
    public String fecha;
    public String horaInicio;
    public int precio;
    public String id_Reserva;
}
