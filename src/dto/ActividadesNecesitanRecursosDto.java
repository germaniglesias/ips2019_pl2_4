package dto;

public class ActividadesNecesitanRecursosDto {

    private Long id_Actividad;
    private Long id_Recurso;

    public Long getId_Actividad() {
	return id_Actividad;
    }

    public void setId_Actividad(Long id_Actividad) {
	this.id_Actividad = id_Actividad;
    }

    public Long getId_Recurso() {
	return id_Recurso;
    }

    public void setId_Recurso(Long id_Recurso) {
	this.id_Recurso = id_Recurso;
    }
}
