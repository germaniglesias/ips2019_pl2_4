package dto;

public class InstalacionesTienenRecursosDto {

    private Long id_Instalacion;
    private Long id_Recurso;
    private String nombre_Instalacion;
    private int cantidadRecurso;

    public Long getId_Instalacion() {
	return id_Instalacion;
    }

    public void setId_Instalacion(Long id_Instalacion) {
	this.id_Instalacion = id_Instalacion;
    }

    public Long getId_Recurso() {
	return id_Recurso;
    }

    public void setId_Recurso(Long id_Recurso) {
	this.id_Recurso = id_Recurso;
    }

    public String getNombre_Instalacion() {
	return nombre_Instalacion;
    }

    public void setNombre_Instalacion(String nombre_Instalacion) {
	this.nombre_Instalacion = nombre_Instalacion;
    }
    
    public String toString() {
	return nombre_Instalacion;
    }

    public int getCantidadRecurso() {
	return cantidadRecurso;
    }

    public void setCantidadRecurso(int numrecursodisponible) {
	this.cantidadRecurso = numrecursodisponible;
    }
    
    public String imprimir() {
	return "Id Inst: " + id_Instalacion + " Id Rec: " + id_Recurso + "numRecursoDisponible: " + cantidadRecurso;
    }
}
