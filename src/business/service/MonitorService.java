package business.service;

import java.util.List;

import dto.ActividadDto;
import dto.MonitorDto;
import dto.PlanificacionActividadDto;
import dto.SocioDto;

public interface MonitorService {
    List<PlanificacionActividadDto> listActivitiesByMonitor(int id);

    boolean isIdCorrecto(int id);

    List<SocioDto> listSociosByActivity(int idActividad);

    boolean isListActivityAvailable(String dia, String horaInicio);

    List<PlanificacionActividadDto> listActivitiesLimited(int idMonitor);

    SocioDto getSocioByDni(String dni);

    int getNumeroPlazasById(int idActividad);

    void cambiarNumeroPlazasLibres(int sociosNoPresentes, int idActividad);

    void eliminarSocioEnActividad(int idSocio, int idActividad);

    int getCantidadPersonasApuntadas(int idActividad);

    boolean isDniCorrecto(String dni);

    void registrarSocioEnActividad(int idSocio, int idActividad);

    boolean isSocioInActivity(String dni, int idActividad);

    List<PlanificacionActividadDto> listFinishedActivitiesByMonitor(
	    int id_monitor);
    
    ActividadDto findActivityByPlanificacionId(int id);

    PlanificacionActividadDto findPlanificacionActivityById(Long idPlanificacionActividad);

    void generarParteIncidencias(Long id, int numAproxAsistentes,
	    String incidencias);

    void actualizarEstadoSocioActividad(int idSocio, int idPActividad);

    boolean isReservaSocioCancelada(int idSocio, int idActividad);

    boolean isSocioInAnotherActivity(Long idSocio, String horaInicio);

    MonitorDto buscarMonitor(int idMonitor);

}
