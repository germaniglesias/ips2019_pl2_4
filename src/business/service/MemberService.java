package business.service;

import java.sql.Connection;
import java.util.List;

import dto.ActividadesSocioDto;
import dto.SocioReservaInstalacionDto;

public interface MemberService {
    
    public List<ActividadesSocioDto> listaHorariosActividades();

    public List<SocioReservaInstalacionDto> listReservasSocio(Long id,
	    Connection conn);

}