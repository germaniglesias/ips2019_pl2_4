package business.service.implementation;

import java.sql.Connection;
import java.util.List;

import business.service.MemberService;
import business.service.implementation.member.ListReservasSocio;
import controller.member.ActivitiesScheduleController;
import dto.ActividadesSocioDto;
import dto.SocioReservaInstalacionDto;

public class MemberServiceImpl implements MemberService{
    
    @Override
    public List<ActividadesSocioDto> listaHorariosActividades() {
	return new ActivitiesScheduleController().execute();
    }

    @Override
    public List<SocioReservaInstalacionDto> listReservasSocio(Long id,
	    Connection conn) {
	return new ListReservasSocio(id, conn).execute();
    }

}
