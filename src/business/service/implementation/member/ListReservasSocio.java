package business.service.implementation.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.SocioReservaInstalacionDto;
import utils.Settings;

public class ListReservasSocio {

    private Long id;
    private Connection conn;

    public ListReservasSocio(Long id, Connection conn) {
	this.id = id;
	this.conn = conn;
    }

    public List<SocioReservaInstalacionDto> execute() {
	List<SocioReservaInstalacionDto> list = new ArrayList<SocioReservaInstalacionDto>();

	PreparedStatement ps = null;
	ResultSet rs = null;

	try {
	    String consulta = Settings.get("SQL_LIST_RESERVAS_CUOTA_BY_SOCIO_ID");
	    ps = conn.prepareStatement(consulta);

	    ps.setLong(1, id);

	    rs = ps.executeQuery();

	    while (rs.next()) {
		SocioReservaInstalacionDto r = new SocioReservaInstalacionDto();
		r.setId(rs.getLong("id"));
		r.setId_Instalacion(rs.getLong("id_Instalacion"));
		r.setId_Socio(rs.getLong("id_Socio"));
		r.setNombreCompleto(rs.getString("nombresocio") + " " + rs.getString("apellidos"));
		r.setNombreInstalacion(rs.getString("nombreinstalacion"));
		r.setPrecioPorHora(rs.getInt("precioPorHora"));
		r.setFecha(rs.getString("fecha"));
		r.setHoraInicio(rs.getString("horainicio"));

		list.add(r);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	}

	return list;
    }

}
