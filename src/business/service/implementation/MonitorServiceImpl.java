package business.service.implementation;


import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import business.service.MonitorService;
import controller.monitor.AssistControlActivityController;
import controller.monitor.IncidentActivityController;
import controller.monitor.ListMembersActivityController;
import controller.monitor.MonitorController;
import controller.monitor.RegisterMemberController;
import database.gestorDDBB;
import dto.ActividadDto;
import dto.MonitorDto;
import dto.PlanificacionActividadDto;
import dto.SocioDto;

public class MonitorServiceImpl implements MonitorService {
    private MonitorController mc = new MonitorController();
    private ListMembersActivityController lmac = new ListMembersActivityController();
    private AssistControlActivityController acac = new AssistControlActivityController();
    private RegisterMemberController rmc = new RegisterMemberController();
    private IncidentActivityController ic = new IncidentActivityController();

    @Override
    public List<PlanificacionActividadDto> listActivitiesByMonitor(int id) {
	// Pedimos actividades al controller
	List<PlanificacionActividadDto> actividades = lmac
		.listOfActivitiesByMonitor(id);
	// Se las pasamos a la vista
	return actividades;
    }

    @Override
    public boolean isIdCorrecto(int id) {
	// Comprobamos que el id existe en la base de datos
	return lmac.isMonitorInDatabase(id);
    }

    @Override
    public List<SocioDto> listSociosByActivity(int idActividad) {
	return lmac.listSociosByActivity(idActividad);
    }

    @Override
    public boolean isListActivityAvailable(String fecha, String horaInicio) {
	
	// Convertimos la fecha de la actividad a un array de enteros
	int[] fechaActividad = Arrays.stream(fecha.split("-")).mapToInt(Integer::parseInt).toArray();
	// Convertimos la hora de la actividad en un array de enteros
	int[] horaActividad = Arrays.stream(horaInicio.split(":")).mapToInt(Integer::parseInt).toArray();
	
	Calendar calActual = Calendar.getInstance();
	Calendar calActividad = Calendar.getInstance();	
	
	// El mes empieza a contarse en 0 en java, por eso se le resta 1
	calActividad.set(fechaActividad[0], fechaActividad[1]-1, fechaActividad[2],
	horaActividad[0], horaActividad[1], horaActividad[2]);
	
	long diferencia = calcularDiferenciaHorasEnMinutos(calActividad, calActual);
	if  (diferencia <= 60 && diferencia >= 0 &&
		(calActividad.get(Calendar.DAY_OF_YEAR) == calActual.get(Calendar.DAY_OF_YEAR))){
	    return true;
	}
	return false;

    }

    private long calcularDiferenciaHorasEnMinutos(Calendar calActividad, Calendar calActual) {
	return Duration.between(calActual.toInstant(), calActividad.toInstant()).toMinutes();
    }

    @Override
    public List<PlanificacionActividadDto> listActivitiesLimited(int idMonitor) {
	return acac.listActivitiesLimited(idMonitor);
	
    }

    @Override
    public SocioDto getSocioByDni(String dni) {
	return acac.socioByDni(dni);
    }

    @Override
    public int getNumeroPlazasById(int idActividad) {
	return acac.numeroPlazasById(idActividad);
    }

    @Override
    public void cambiarNumeroPlazasLibres(int sociosNoPresentes, int idActividad) {
	acac.disminuirPlazasLibres(sociosNoPresentes, idActividad);		
    }

    @Override
    public void eliminarSocioEnActividad(int idSocio, int idActividad) {
	acac.eliminarSocioEnActividad(idSocio, idActividad);
	
    }

    @Override
    public int getCantidadPersonasApuntadas(int idActividad) {
	return rmc.getCantidadPersonasApuntadas(idActividad);
    }

    @Override
    public boolean isDniCorrecto(String dni) {
	return rmc.isDniCorrecto(dni);
    }

    @Override
    public void registrarSocioEnActividad(int idSocio, int idActividad) {
	rmc.registrarSocioEnActividad(idSocio, idActividad);	
    }

    @Override
    public boolean isSocioInActivity(String dni, int idActividad) {
	return rmc.isSocioInActivity(dni, idActividad);
    }
    
    @Override
    public void actualizarEstadoSocioActividad(int idSocio, int idPActividad) {
	rmc.actualizarEstadoSocioActividad(idSocio, idPActividad);
	
    }

    @Override
    public boolean isReservaSocioCancelada(int idSocio, int idActividad) {
	if (rmc.getEstadoSocioActividad(idSocio, idActividad).equalsIgnoreCase("CANCELADA")) 
	    return true;
	return false;
    }

    @Override
    public List<PlanificacionActividadDto> listFinishedActivitiesByMonitor(
	    int id_monitor) {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
	Date date = new Date();
	return ic.listFinishedActivities(id_monitor, sdf.format(date), sdf2.format(date));
    }

    @Override
    public ActividadDto findActivityByPlanificacionId(int idPlanificacionId) {
	return ic.findActivityByPlanificacionId(idPlanificacionId);
    }

    @Override
    public PlanificacionActividadDto findPlanificacionActivityById(
	    Long idPlanificacionActividad) {
	return ic.findPlanificacionActivityById(idPlanificacionActividad);
    }

    @Override
    public void generarParteIncidencias(Long id, int numAproxAsistentes,
	    String incidencias) {
	ic.generarParteIncidencias(id, numAproxAsistentes, incidencias);	
    }

    @Override
    public boolean isSocioInAnotherActivity(Long idSocio, String horaInicio) {
	return rmc.isSocioInAnotherActivity(idSocio, horaInicio);
    }

    @Override
    public MonitorDto buscarMonitor(int idMonitor) {
	try (Connection conn = gestorDDBB.getConnection();) {
	    mc.setConnection(conn);
	    return mc.buscarMonitor(idMonitor);
	} catch (SQLException e) {
	    e.printStackTrace();
	}
	
	return null;
    }

}
