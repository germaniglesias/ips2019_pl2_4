package business.service.implementation;

import dto.ActividadDto;
import dto.ActividadesNecesitanRecursosDto;
import dto.InstalacionDto;
import dto.InstalacionesTienenRecursosDto;
import dto.MonitorDto;
import dto.RecursoDto;
import dto.SocioDto;
import dto.SocioReservaInstalacionDto;
import dto.PlanificacionActividadDto;
import utils.Fecha;
import utils.Settings;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import business.service.AdminService;
import database.gestorDDBB;
import business.service.implementation.admin.ActivityToAssignMonitor;
import business.service.implementation.admin.CheckMemberSimultaneousActivities;
import business.service.implementation.admin.CheckMemberSimultaneousReservations;
import business.service.implementation.admin.CheckSimultaneousActivities;
import business.service.implementation.admin.CheckSimultaneousReservations;
import business.service.implementation.admin.CheckValidMember;
import business.service.implementation.admin.FindMember;
import business.service.implementation.admin.FindSocioById;
import business.service.implementation.admin.InsertMemberActivitiesReservation;
import business.service.implementation.admin.InsertMemberFacilitiesReservation;
import business.service.implementation.admin.InsertarReservaEnEfectivo;
import business.service.implementation.admin.InsertarReservaImpagada;
import business.service.implementation.admin.IsReservaPagada;
import business.service.implementation.admin.ListActivitiesWithLimitSeat;
import business.service.implementation.admin.ListActivitiesWithoutMonitor;
import business.service.implementation.admin.ListAvailableMonitors;
import business.service.implementation.admin.ListFacilities;
import business.service.implementation.admin.ListFacilitiesReservations;
import business.service.implementation.admin.ListReservasInstalacionSocio;
import business.service.implementation.admin.UpdateActivityAssignMonitor;
import controller.admin.PlanActivity;

public class AdminServiceImpl implements AdminService {

    @Override
    public void createActividad(ActividadDto actividad) {
	Connection conn = gestorDDBB.getConnection();

	PreparedStatement ps = null;
	try {
	    String consulta = Settings.get("SQL_INSERT_ACTIVITY");
	    ps = conn.prepareStatement(consulta);

	    ps.setString(1, actividad.getNombre());
	    ps.setString(2, actividad.getIntensidad());
	    ps.setBoolean(3, actividad.isUsaRecursos());

	    ps.executeUpdate();

	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}

    }

    public List<ActividadDto> getListaActividades(boolean tieneRecursos) {
	List<ActividadDto> lista = new ArrayList<ActividadDto>();
	Connection conn = gestorDDBB.getConnection();

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String consulta = Settings.get("SQL_LIST_ACTIVITIES");
	    ps = conn.prepareStatement(consulta);
	    ps.setBoolean(1, tieneRecursos);
	    rs = ps.executeQuery();

	    while (rs.next()) {
		ActividadDto act = new ActividadDto();
		act.setId(rs.getLong("id"));
		act.setNombre(rs.getString("nombre"));
		act.setUsaRecursos(rs.getBoolean("usorecurso"));

		lista.add(act);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}

	return lista;
    }

    public List<InstalacionDto> getListaInstalaciones() {
	List<InstalacionDto> lista = new ArrayList<InstalacionDto>();

	Connection conn = gestorDDBB.getConnection();

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String consulta = Settings.get("SQL_LIST_FACILITIES");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();

	    while (rs.next()) {
		InstalacionDto inst = new InstalacionDto();
		inst.setId(rs.getLong("id"));
		inst.setNombre(rs.getString("nombre"));

		lista.add(inst);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return lista;
    }

    public void insertarPlanificacionActividad(
	    PlanificacionActividadDto reserva) {
	Connection conn = gestorDDBB.getConnection();

	PreparedStatement ps = null;
	try {
	    String consulta = Settings.get("SQL_INSERT_ARI");
	    ps = conn.prepareStatement(consulta);

	    ps.setLong(1, reserva.getId_Actividad());
	    ps.setLong(2, reserva.getId_Instalacion());
	    ps.setString(3, Fecha.fromApptoDDBB(reserva.getFecha()));
	    ps.setString(4, reserva.getHoraInicio() + ":00:00");
	    ps.setString(5, reserva.getHoraFin() + ":00:00");
	    ps.setBoolean(6, reserva.isLimitePlazas());
	    ps.setInt(7, reserva.getNumLimitePlazas());

	    ps.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
    }

    public List<RecursoDto> getListaRecursos() {
	List<RecursoDto> lista = new ArrayList<RecursoDto>();

	Connection conn = gestorDDBB.getConnection();

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String consulta = Settings.get("SQL_LIST_RECURSOS");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();

	    while (rs.next()) {
		RecursoDto recurso = new RecursoDto();
		recurso.setId(rs.getLong("id"));
		recurso.setNombre(rs.getString("nombre"));

		lista.add(recurso);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return lista;
    }

    public List<InstalacionesTienenRecursosDto> getListaInstalacionesQueContenganRecursos(
	    List<RecursoDto> listaRecursosAniadidos) {
	List<InstalacionesTienenRecursosDto> lista = new ArrayList<InstalacionesTienenRecursosDto>();

	Connection conn = gestorDDBB.getConnection();

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    for (RecursoDto recurso : listaRecursosAniadidos) {
		String consulta = Settings
			.get("SQL_LIST_INSTALACIONES_CON_RECURSOS");
		ps = conn.prepareStatement(consulta);

		ps.setLong(1, recurso.getId());

		rs = ps.executeQuery();
		while (rs.next()) {
		    InstalacionesTienenRecursosDto instalacion = new InstalacionesTienenRecursosDto();
		    instalacion.setId_Instalacion(rs.getLong("id"));
		    instalacion.setNombre_Instalacion(rs.getString("nombre"));
		    instalacion.setId_Recurso(recurso.getId());
		    instalacion
			    .setCantidadRecurso(rs.getInt("cantidadRecurso"));

		    lista.add(instalacion);
		}
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return lista;
    }

    public void insertarActividadNecesitaRecursoS(
	    List<ActividadesNecesitanRecursosDto> listaActNecRec) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	try {

	    String consulta = Settings
		    .get("SQL_INSERT_ACTIVIDAD_NECESITA_RECURSO");
	    for (ActividadesNecesitanRecursosDto ari : listaActNecRec) {
		ps = conn.prepareStatement(consulta);

		ps.setLong(1, ari.getId_Actividad());
		ps.setLong(2, ari.getId_Recurso());

		ps.executeUpdate();
	    }
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(ps);
	    gestorDDBB.close(conn);
	}
    }

    public long getIdActividad(ActividadDto actividad) {
	long id = -1;

	Connection conn = gestorDDBB.getConnection();

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {

	    String consulta = Settings.get("SQL_SELECT_ID_ACTIVITY");
	    ps = conn.prepareStatement(consulta);

	    // nombre, intensidad, usoRecurso
	    ps.setString(1, actividad.getNombre());
	    ps.setString(2, actividad.getIntensidad());
	    ps.setBoolean(3, actividad.isUsaRecursos());

	    rs = ps.executeQuery();
	    while (rs.next()) {
		id = rs.getLong(1);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return id;
    }

    public List<RecursoDto> getRecursosQueTieneActividad(Long id_actividad) {
	List<RecursoDto> lista = new ArrayList<RecursoDto>();

	Connection conn = gestorDDBB.getConnection();

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String consulta = Settings
		    .get("SQL_LIST_RESOURCES_NEEDED_BY_ACTIVITY");
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, id_actividad);
	    rs = ps.executeQuery();

	    while (rs.next()) {
		RecursoDto recurso = new RecursoDto();
		recurso.setId(rs.getLong("id"));
		recurso.setNombre(rs.getString("nombre"));

		lista.add(recurso);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return lista;
    }

    public List<ActividadDto> getListaActividades() {
	List<ActividadDto> lista = new ArrayList<ActividadDto>();

	Connection conn = gestorDDBB.getConnection();

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String consulta = Settings.get("SQL_LIST_ALL_ACTIVITIES");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();

	    while (rs.next()) {
		ActividadDto act = new ActividadDto();
		act.setId(rs.getLong("id"));
		act.setNombre(rs.getString("nombre"));
		act.setUsaRecursos(rs.getBoolean("usorecurso"));

		lista.add(act);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}

	return lista;
    }

    @Override
    public List<InstalacionDto> listFacilities() {
	ListFacilities lf = new ListFacilities();
	return lf.execute();
    }

    @Override

    public List<PlanificacionActividadDto> listFacilitiesReservations(
	    Long index, String startWeek, String endWeek) {
	ListFacilitiesReservations lfr = new ListFacilitiesReservations(index,
		startWeek, endWeek);
	return lfr.execute();
    }

    @Override
    public void updateActivityAssignMonitor(Long monitor_id, Long activity_id) {
	UpdateActivityAssignMonitor uasm = new UpdateActivityAssignMonitor(
		monitor_id, activity_id);
	uasm.execute();
    }

    @Override
    public List<PlanificacionActividadDto> listActivitiesWithLimitSeat(Long id,
	    String date, String startTime) {
	ListActivitiesWithLimitSeat lawls = new ListActivitiesWithLimitSeat(
		id,date, startTime);
	return lawls.execute();
    }

    @Override
    public String checkMemberSimultaneousActivities(String dni,
	    String date, String startTime, String endTime) {
	CheckMemberSimultaneousActivities cmsa = new CheckMemberSimultaneousActivities(
		dni, date, startTime, endTime);
	return cmsa.execute();
    }
    
    @Override
    public String checkMemberSimultaneousReservations(String dni,
	    String date, String startTime) {
	CheckMemberSimultaneousReservations cmsr = new CheckMemberSimultaneousReservations(
		dni, date, startTime);
	return cmsr.execute();
    }

    @Override
    public void InsertMemberActivitiesReservations(Long member_id,
	    Long plan_id) {
	InsertMemberActivitiesReservation imar = new InsertMemberActivitiesReservation(
		member_id, plan_id);
	imar.execute();
    }

    @Override
    public boolean checkValidMember(String dni) {
	CheckValidMember cvm = new CheckValidMember(dni);
	return cvm.execute();
    }

    @Override
    public void planActivity(PlanificacionActividadDto reserva, Connection conn)
	    throws SQLException {
	PlanActivity pa = new PlanActivity(reserva, conn);
	pa.execute();
    }

    @Override
    public List<PlanificacionActividadDto> listActivitiesWithoutMonitor(Long id,
	    String endWeek, String today, String startHour) {
	ListActivitiesWithoutMonitor lawm = new ListActivitiesWithoutMonitor(id,
		endWeek, today, startHour);
	return lawm.execute();
    }

    @Override
    public Long getIdToAssignMonitor(String date, Long id, String startTime,
	    String endTime) {
	ActivityToAssignMonitor atam = new ActivityToAssignMonitor(date, id,
		startTime, endTime);
	return atam.execute();
    }

    @Override
    public List<MonitorDto> listAvailableMonitors(List<String> dias,
	    String startTime, String endTime) {
	ListAvailableMonitors lam = new ListAvailableMonitors(dias, startTime,
		endTime);
	return lam.execute();
    }

    @Override
    public List<PlanificacionActividadDto> listPlanificationsOnDateAndHour(
	    PlanificacionActividadDto reserva, Connection conn)
	    throws SQLException {
	List<PlanificacionActividadDto> lista = new ArrayList<PlanificacionActividadDto>();

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String consulta = Settings.get("SQL_LIST_PLANIFICATIONS_ON_DATE");
	    ps = conn.prepareStatement(consulta);

	    ps.setString(1, Fecha.fromApptoDDBB(reserva.getFecha()));
	    ps.setLong(2, reserva.getId_Instalacion());

	    rs = ps.executeQuery();

	    while (rs.next()) {
		PlanificacionActividadDto p = new PlanificacionActividadDto();
		p.setId_Actividad(rs.getLong("id_actividad"));
		p.setId_Instalacion(rs.getLong("id_instalacion"));
		p.setFecha(Fecha.fromDDBBtoApp(rs.getString("fecha")));
		p.setHoraInicio(rs.getInt("horainicio") + "");
		p.setHoraFin(rs.getInt("horafin") + "");

		lista.add(p);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	}

	return lista;
    }

    @Override
    public SocioDto findSocioById(Long id_Socio, Connection conn) {
	return new FindSocioById(id_Socio, conn).execute();
    }

    @Override
    public void insertarReservaImpagada(Long id, Connection conn) {
	new InsertarReservaImpagada(id, conn).execute();
    }

    @Override
    public void insertarReservaEnEfectivo(Long id, Connection conn) {
	new InsertarReservaEnEfectivo(id, conn).execute();
    }

    @Override
    public List<SocioReservaInstalacionDto> listReservasInstalacionSocio(
	    Long id, String startWeek, String endWeek) {
	return new ListReservasInstalacionSocio(id, startWeek, endWeek)
		.execute();
    }

    @Override
    public boolean isPagado(Long id, Connection conn) {
	return new IsReservaPagada(id, conn).execute();
    }

    @Override
    public void InsertMemberFacilitiesReservations(Long member_id, Long id,
	    String date, String startHour) {
	InsertMemberFacilitiesReservation imfr = new InsertMemberFacilitiesReservation(
		member_id, id,date,startHour);
	imfr.execute();
	
    }

    @Override
    public SocioDto findMember(String dni) {
	FindMember fm = new FindMember(dni);
	return fm.execute();
    }

    @Override
    public boolean checkSimultaneousActivity(Long id, String date,
	    String startHour) {
	CheckSimultaneousActivities csa = new CheckSimultaneousActivities(id,date,startHour);
	return csa.execute();
    }

    @Override
    public boolean checkSimultaneousReservations(Long id, String date,
	    String startHour) {
	CheckSimultaneousReservations csr = new CheckSimultaneousReservations(id, date, startHour);
	return csr.execute();
    }

}
