package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import database.gestorDDBB;
import dto.PlanificacionActividadDto;
import exception.ApplicationException;
import utils.Fecha;
import utils.Settings;

public class ListFacilitiesReservations {
    List<PlanificacionActividadDto> list = new ArrayList<PlanificacionActividadDto>();
    private PlanificacionActividadDto reservation;
    private Long index;
    private String startWeek;
    private String endWeek;

    public ListFacilitiesReservations(Long index, String startWeek,
	    String endWeek) {
	this.index = index;
	this.startWeek = startWeek;
	this.endWeek = endWeek;
    }

    public List<PlanificacionActividadDto> execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(Settings
			.get("SQL_LIST_FACILITIES_RESERVATIONS_DATES"));) {
	    pst.setLong(1, index);
	    pst.setString(2, startWeek);
	    pst.setString(3, endWeek);
	    try (ResultSet rs = pst.executeQuery();) {
		while (rs.next()) {
		    reservation = new PlanificacionActividadDto();
		    reservation.setNombre(rs.getString("nombre"));
		    reservation.setId_Monitor(rs.getLong("id_Monitor"));
		    reservation.setFecha(Fecha.fromDDBBtoApp(rs.getString("fecha")));
		    reservation.setHoraInicio(rs.getString("horaInicio"));
		    reservation.setHoraFin(rs.getString("horaFin"));
		    list.add(reservation);
		}
	    }

	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD esté activada.");
	}

	return list;
    }

}
