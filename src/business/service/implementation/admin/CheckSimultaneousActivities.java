package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.gestorDDBB;
import exception.ApplicationException;
import utils.Settings;

public class CheckSimultaneousActivities {

    private Long id;
    private String date;
    private String startHour;
    
    public CheckSimultaneousActivities(Long id, String date, String startHour) {
	this.id=id;
	this.date=date;
	this.startHour=startHour;
    }

    public boolean execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			Settings.get("SQL_CHECK_SIMULTANEOUS_ACTIVITIES"));) {

	    pst.setLong(1, id);
	    pst.setString(2, date);
	    pst.setString(3, startHour);
	    
	    try (ResultSet rs = pst.executeQuery();) {
		return rs.next();
	    }

	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD esté activada.");
	}
    }

}
