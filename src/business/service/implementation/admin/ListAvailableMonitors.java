package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.MonitorDto;
import exception.ApplicationException;

public class ListAvailableMonitors {

    private String startTime;
    private String endTime;
    private List<String> days;

    private List<MonitorDto> list = new ArrayList<MonitorDto>();
    private MonitorDto monitor;

    public ListAvailableMonitors(List<String> dias, String startTime,
	    String endTime) {
	this.days = dias;
	this.startTime = startTime;
	this.endTime = endTime;
    }

    public List<MonitorDto> execute() {
	String s = "";
	for (int i = 0; i < days.size(); i++) {
	    s= s+((i > 0) ? ", " : "") +"'" + days.get(i) + "'";
	}
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			//Settings.get("SQL_LIST_AVAILABLE_MONITORS")
"select id, nombre,apellidos from monitor where id not in (select id_monitor from PlanificacionActividad where fecha in ("+s+") and (horaInicio is ? or horaFin is ?)and id_monitor is not null)");
		) {
	    pst.setString(1, startTime);
	    pst.setString(2, endTime);

	    try (ResultSet rs = pst.executeQuery();) {
		while (rs.next()) {
		    monitor = new MonitorDto();
		    monitor.setId(rs.getLong("id"));
		    monitor.setNombre(rs.getString("nombre"));
		    monitor.setApellidos(rs.getString("apellidos"));
		    list.add(monitor);
		}
	    }

	} catch (SQLException e) {
	    System.out.println(e.getMessage());
	    throw new ApplicationException(
		    "Es necesario que la BBDD este activada.");
	}

	return list;
    }

}
