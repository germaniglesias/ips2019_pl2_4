package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.gestorDDBB;
import utils.Settings;

public class IsReservaPagada {

    private Long id;
    private Connection conn;
    
    public IsReservaPagada(Long id2, Connection conn2) {
	id=id2;
	conn = conn2;
    }

    public boolean execute() {
	boolean resultado = false;

	PreparedStatement ps = null;
	ResultSet rs = null;
	
	try {
	    String consulta = Settings.get("SQL_FIND_PAYED_SOCIO_RESERVA_INSTALACION");
	    ps = conn.prepareStatement(consulta);

	    ps.setLong(1, id);
	    ps.setLong(2, id);

	    rs = ps.executeQuery();

	    if (rs.next()) {
		resultado = true;
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    try { ps.close(); rs.close(); gestorDDBB.close(conn); } catch (SQLException e) {}
	}
	
	return resultado;
    }

}
