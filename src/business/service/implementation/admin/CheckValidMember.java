package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.gestorDDBB;
import exception.ApplicationException;
import utils.Settings;

public class CheckValidMember {

    private String dni;

    public CheckValidMember(String dni) {
	this.dni = dni;
    }

    public boolean execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			Settings.get("SQL_CHECK_VALID_MEMBER"));) {

	    pst.setString(1, dni);
	    try (ResultSet rs = pst.executeQuery();) {
		return rs.next();
	    }

	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD esté activada.");
	}

    }

}
