package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.gestorDDBB;
import dto.ActividadDto;
import utils.Settings;

public class ActivityInDate {

    private Connection conn;

    public void setConnection(Connection con) {
	this.conn = con;

    }

    public boolean isFechaValida(ActividadDto actividad, String fecha) {
	ResultSet rs = null;
	PreparedStatement ps = null;

	String consulta = Settings.get("SQL_ACTIVITY_IN_DATE");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, actividad.getId());
	    ps.setString(2, fecha);
	    
	    rs = ps.executeQuery();
	    
	    if (rs.next())
		return true;

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}
	return false;
    }

}
