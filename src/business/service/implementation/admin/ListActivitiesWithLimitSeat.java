package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.PlanificacionActividadDto;
import exception.ApplicationException;
import utils.Fecha;
import utils.Settings;

public class ListActivitiesWithLimitSeat {
    private List<PlanificacionActividadDto> list = new ArrayList<PlanificacionActividadDto>();
    private PlanificacionActividadDto limit_activity;
    private String date;
    private String startTime;
    private Long id;

    public ListActivitiesWithLimitSeat(Long id, String date, String startTime) {
	this.id=id;
	this.date = date;
	this.startTime = startTime;
    }

    public List<PlanificacionActividadDto> execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			Settings.get("SQL_LIST_ACTIVITIES_WITH_LIMIT_SEAT"));) {
	    pst.setLong(1, id);
	    pst.setString(2, date);
	    pst.setString(3, startTime);
	    try (ResultSet rs = pst.executeQuery();) {
		
		while (rs.next()) {
		    limit_activity = new PlanificacionActividadDto();
		    limit_activity.setId(rs.getLong("id"));
		    limit_activity.setNombre(rs.getString("nombre"));
		    limit_activity.setFecha(Fecha.fromDDBBtoApp(rs.getString("fecha")));
		    limit_activity.setHoraInicio(rs.getString("horaInicio"));
		    limit_activity.setHoraFin(rs.getString("horaFin"));
		    limit_activity
			    .setNumLimitePlazas(rs.getInt("numLimitePlazas"));
		    limit_activity.setNumPlazasDisponibles(
			    rs.getInt("numPlazasDisponibles"));
		    list.add(limit_activity);
		}
	    }

	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD est� activada.");
	}

	return list;
    }

}

