package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.gestorDDBB;
import exception.ApplicationException;
import utils.Settings;

public class CheckMemberSimultaneousActivities {

    private String dni;
    private String date;
    private String startTime;
    private String endTime;
    
    private String activity="";
   
    public CheckMemberSimultaneousActivities(String dni,String date, String startTime,
	    String endTime) {
	this.dni=dni;
	this.date= date;
	this.startTime= startTime;
	this.endTime = endTime;
    }

    public String execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(Settings
			.get("SQL_CHECK_MEMBER_SIMULTANEOUS_ACTIVITIES"));) {
	    pst.setString(1, dni);
	    pst.setString(2, date);
	    pst.setString(3, startTime);
	    pst.setString(4, endTime);
	    
	    try (ResultSet rs = pst.executeQuery();) {
		while(rs.next()) {
		    activity = rs.getString(1);
		}
		
	    }

	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD este activada.");
	}
	return activity;
    }
}
