package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.gestorDDBB;
import dto.SocioDto;
import utils.Settings;

public class FindMember {

    private String dni;
    private SocioDto member = null;
    
    public FindMember(String dni) {
	this.dni = dni;
    }

    public SocioDto execute() {

	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			Settings.get("SQL_FIND_MEMBER_BY_DNI"));) {
	    pst.setString(1, dni);
	    try (ResultSet rs = pst.executeQuery();) {
		while (rs.next()) {
		    member = new SocioDto();
		    member.setId(rs.getLong("id"));
		    member.setNombre(rs.getString("nombre"));
		    member.setApellidos(rs.getString("apellidos"));
		    member.setDni(rs.getString("dni"));
		    member.setCorreo(rs.getString("correo"));
		    member.setClave(rs.getString("clave"));
		}
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	}

	return member;
    }
}
