package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.gestorDDBB;
import exception.ApplicationException;
import utils.Settings;

public class ActivityToAssignMonitor {

    private String date;
    private Long id;
    private String startTime;
    private String endTime;

    public ActivityToAssignMonitor(String date, Long id, String startTime,
	    String endTime) {
	this.date = date;
	this.id = id;
	this.startTime = startTime;
	this.endTime = endTime;
    }

    public Long execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			Settings.get("SQL_ACTIVITY_TO_ASSIGN_MONITOR"));) {
	    pst.setString(1, date);
	    pst.setLong(2, id);
	    pst.setString(3, startTime);
	    pst.setString(4, endTime);

	    try (ResultSet rs = pst.executeQuery();) {
		while (rs.next()) {
		    return rs.getLong("id");
		}
	    }

	} catch (SQLException e) {
	    System.out.println(e.getMessage());
	    throw new ApplicationException(
		    "Es necesario que la BBDD esté activada.");
	}

	return null;
    }

}
