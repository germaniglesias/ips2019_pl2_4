package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import utils.Settings;

public class InsertarReservaEnEfectivo {

    private Long id;
    private Connection conn;

    public InsertarReservaEnEfectivo(Long id, Connection conn2) {
	this.id = id;
	this.conn = conn2;
    }

    public void execute() {
	PreparedStatement ps = null;
	try {
	    String consulta = Settings.get("SQL_INSERT_EN_EFECTIVO");
	    ps = conn.prepareStatement(consulta);

	    ps.setLong(1, id);

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	}
    }

}
