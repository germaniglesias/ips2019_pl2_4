package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.SocioReservaInstalacionDto;
import utils.Fecha;
import utils.Settings;

public class ListReservasInstalacionSocio {
    
    private Long id;
    private String startWeek;
    private String endWeek;
    private Connection conn = gestorDDBB.getConnection();

    public ListReservasInstalacionSocio(Long id, String startWeek,
	    String endWeek) {
	this.id = id;
	this.startWeek = startWeek;
	this.endWeek = endWeek;
    }

    public List<SocioReservaInstalacionDto> execute() {
	List<SocioReservaInstalacionDto> reservas = new ArrayList<SocioReservaInstalacionDto>();
	
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	//SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	
	try {
	    String consulta = Settings.get("SQL_LIST_RESERVA_SOCIO_OF_WEEK");
	    ps = conn.prepareStatement(consulta);

	    ps.setLong(1, id);
	    ps.setString(2, startWeek);
	    ps.setString(3, endWeek);

	    rs = ps.executeQuery();

	    while (rs.next()) {
		SocioReservaInstalacionDto r = new SocioReservaInstalacionDto();
		r.setId(rs.getLong("id"));
		r.setId_Instalacion(rs.getLong("id_Instalacion"));
		r.setId_Socio(rs.getLong("id_Socio"));
		r.setHoraInicio(rs.getString("horaInicio"));
		r.setFecha(Fecha.fromDDBBtoApp(rs.getString("fecha")));
		r.setNombreCompleto(rs.getString("nombreSocio") + " " + rs.getString("apellidos"));
		r.setNombreInstalacion(rs.getString("nombreInstalacion"));
		r.setPrecioPorHora(rs.getInt("precioPorHora"));
		
		reservas.add(r);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	}
	
	return reservas;
    }

}
