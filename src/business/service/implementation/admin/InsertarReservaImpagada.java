package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import utils.Settings;

public class InsertarReservaImpagada {

    private Long id;
    private Connection conn;

    public InsertarReservaImpagada(Long id, Connection conn2) {
	this.id = id;
	this.conn = conn2;
    }

    public void execute() {
	PreparedStatement ps = null;
	try {
	    String consulta = Settings.get("SQL_INSERT_CUOTA");
	    ps = conn.prepareStatement(consulta);

	    ps.setLong(1, id);

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	}
    }

}
