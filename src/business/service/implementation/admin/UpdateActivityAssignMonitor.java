package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import database.gestorDDBB;
import exception.ApplicationException;
import utils.Settings;

public class UpdateActivityAssignMonitor {

    private Long monitor_id;
    private Long activity_id;

    public UpdateActivityAssignMonitor(Long monitor_id, Long activity_id) {
	this.monitor_id = monitor_id;
	this.activity_id = activity_id;
    }

    public void execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			Settings.get("SQL_UPDATE_ACTIVITY_ASSIGN_MONITOR"));) {
	    pst.setLong(1, monitor_id);
	    pst.setLong(2, activity_id);

	    pst.executeUpdate();

	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD esté activada.");
	}

    }

}
