package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.ActividadDto;
import dto.PlanificacionActividadDto;
import dto.SocioDto;
import utils.Settings;

public class CancelActivitySelectedDay {
    private Connection conn;

    public void setConnection(Connection conn) {
	this.conn = conn;
    }

    public List<ActividadDto> getListActivities() {
	List<ActividadDto> actividades = new ArrayList<ActividadDto>();
	ResultSet rsActivities = null;
	PreparedStatement ps = null;

	String consulta = Settings
		.get("SQL_LIST_ALL_ACTIVITIES");

	try {

	    ps = conn.prepareStatement(consulta);
	    rsActivities = ps.executeQuery();

	    while (rsActivities.next()) {
		ActividadDto act = new ActividadDto();
		act.setId(rsActivities.getLong("id"));
		act.setNombre(rsActivities.getString("nombre"));
		act.setIntensidad(rsActivities.getString("intensidad"));
		act.setUsaRecursos(rsActivities.getBoolean("usoRecurso"));

		actividades.add(act);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rsActivities, ps, conn);
	}
	return actividades;
    }

    public List<String> getHorasParaActividadDia(ActividadDto act, String dia) {
	List<String> horas = new ArrayList<String>();
	ResultSet rs = null;
	PreparedStatement ps = null;

	String consulta = Settings
		.get("SQL_GET_HORAS_ACTIVIDAD");

	try {

	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, act.getId());
	    ps.setString(2, dia);
	    
	    rs = ps.executeQuery();

	    while (rs.next()) {
		horas.add(rs.getString("horaInicio") + "-" + rs.getString("horaFin"));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}
	return horas;
    }

    public void eliminarActividad(Long actividadId, String dia, String horaInicio, String horaFin) {
	PreparedStatement ps = null;

	String consulta = Settings
		.get("SQL_DELETE_ACTIVITY");

	try {

	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, actividadId);
	    ps.setString(2, dia);
	    ps.setString(3, horaInicio);
	    ps.setString(4, horaFin);
	    
	    ps.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(ps);
	    gestorDDBB.close(conn);
	}
	
    }

    /**
     * Planificacion de actividades en un rango de horas específico
     * @param principioRango
     * @param finRango
     * @return
     */
    public List<PlanificacionActividadDto> getListPlanificacionActividadPorRango(String principioRango, String finRango, String fechaActual) {
	List<PlanificacionActividadDto> actividades = new ArrayList<PlanificacionActividadDto>();
	ResultSet rsActivities = null;
	PreparedStatement ps = null;

	String consulta = Settings
		.get("SQL_LIST_ALL_PLANNING_BY_RANGE");

	try {

	    ps = conn.prepareStatement(consulta);
	    ps.setString(1, principioRango);
	    ps.setString(2, finRango);
	    ps.setString(3, principioRango);
	    ps.setString(4, finRango);
	    ps.setString(5, fechaActual);
	    
	    rsActivities = ps.executeQuery();

	    while (rsActivities.next()) {
		PlanificacionActividadDto act = new PlanificacionActividadDto();
		act.setId(rsActivities.getLong("id"));
		act.setId_Actividad(rsActivities.getLong("id_Actividad"));
		act.setId_Instalacion(rsActivities.getLong("id_Instalacion"));
		act.setFecha(rsActivities.getString("fecha"));
		act.setHoraInicio(rsActivities.getString("horaInicio"));
		act.setHoraFin(rsActivities.getString("horaFin"));
		act.setNombre(rsActivities.getString("nombre"));

		actividades.add(act);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    //gestorDDBB.close(rsActivities, ps, conn);
	}
	return actividades;
    }
    
    public void eliminarPlanificacionActividad(Long idPlanificacionActividad) {
	PreparedStatement ps = null;
	String consulta = Settings
		.get("SQL_DELETE_PLANIFICACION_ACTIVIDAD");
	
	try {
	    Connection conn = gestorDDBB.getConnection();
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, idPlanificacionActividad);
	    ps.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
//	    gestorDDBB.close(ps);
//	    gestorDDBB.close(conn);
	}
    }

    public List<SocioDto> getSociosApuntados(Long id) {
	List<SocioDto> socios = new ArrayList<SocioDto>();

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String consulta = Settings.get("SQL_LIST_SOCIOS_APUNTADOS_ACTIVIDAD");
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, id);
	    rs = ps.executeQuery();

	    while (rs.next()) {
		SocioDto s = new SocioDto();
		
		s.setId(rs.getLong("id"));
		s.setNombre(rs.getString("nombre"));
		s.setApellidos(rs.getString("apellidos"));
		s.setClave(rs.getString("clave"));
		s.setCorreo(rs.getString("correo"));
		s.setDni(rs.getString("dni"));

		socios.add(s);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	}

	return socios;
    }

    public Long getIdPlanificacionConcreta(Long actividadId, String dia, String horaini, String horafin) {
	Long idPA = null;

	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String consulta = Settings.get("SQL_SELECT_PLANIFICACION_ACTIVIDAD");
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, actividadId);
	    ps.setString(2, dia);
	    ps.setString(3, horaini);
	    ps.setString(4, horafin);
	    rs = ps.executeQuery();

	    while (rs.next()) {
		idPA = rs.getLong("id");
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	}

	return idPA;
    }

}
