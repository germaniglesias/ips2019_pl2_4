package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import database.gestorDDBB;
import exception.ApplicationException;
import utils.Settings;

public class InsertMemberFacilitiesReservation {
    
    private Long member_id;
    private Long id;
    private String date;
    private String startHour;

    public InsertMemberFacilitiesReservation(Long member_id, Long id, String date,
	    String startHour) {
	this.member_id=member_id;
	this.id=id;
	this.date=date;
	this.startHour=startHour;
    }

    public void execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			Settings.get("SQL_INSERT_MEMBER_FACILITIES_RESERVATION"));) {
	    pst.setLong(1, member_id);
	    pst.setLong(2, id);
	    pst.setString(3, date);
	    pst.setString(4, startHour);

	    pst.executeUpdate();

	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD este activada.");
	}

    }
}
