package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import database.gestorDDBB;
import exception.ApplicationException;
import utils.Settings;

public class InsertMemberActivitiesReservation {

    private Long member_id;
    private Long plan_id;

    public InsertMemberActivitiesReservation(Long member_id, Long plan_id) {
	this.member_id = member_id;
	this.plan_id = plan_id;
    }

    public void execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			Settings.get("SQL_INSERT_MEMBER_ACTIVITIES_RESERVATION"));) {
	    pst.setLong(1, member_id);
	    pst.setLong(2, plan_id);

	    pst.executeUpdate();

	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD este activada.");
	}

    }
}

