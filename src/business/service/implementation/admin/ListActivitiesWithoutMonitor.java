package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.PlanificacionActividadDto;
import exception.ApplicationException;
import utils.Fecha;
import utils.Settings;

public class ListActivitiesWithoutMonitor {

    private List<PlanificacionActividadDto> list = new ArrayList<PlanificacionActividadDto>();
    private PlanificacionActividadDto activity_without_monitor;
    private Long id;
    private String endWeek;
    private String today;
    private String startHour;

    public ListActivitiesWithoutMonitor(Long id, String endWeek, String today,
	    String startHour) {
	this.id = id;
	this.endWeek = endWeek;
	this.today = today;
	this.startHour = startHour;
    }

    public List<PlanificacionActividadDto> execute() {

	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn.prepareStatement(
			Settings.get("SQL_LIST_ACTIVITIES_WITHOUT_MONITOR"));) {
	    pst.setLong(1, id);
	    pst.setString(2, today);
	    pst.setString(3, endWeek);
	    pst.setString(4, today);
	    pst.setString(5, startHour);

	    try (ResultSet rs = pst.executeQuery();) {
		while (rs.next()) {
		    activity_without_monitor = new PlanificacionActividadDto();
		    activity_without_monitor.setId_Actividad(rs.getLong("id"));
		    activity_without_monitor.setNombre(rs.getString("nombre"));
		    activity_without_monitor.setFecha(Fecha.fromDDBBtoApp(rs.getString("fecha")));
		    activity_without_monitor
			    .setHoraInicio(rs.getString("horaInicio"));
		    activity_without_monitor
			    .setHoraFin(rs.getString("horaFin"));
		    list.add(activity_without_monitor);
		}
	    }
	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD esté activada.");
	}

	return list;
    }

}
