package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.SocioReservaInstalacionDto;
import utils.Settings;

public class BuscarSocioReservaInstalacion {
    private Connection conn;

    public void setConnection(Connection conn) {
	this.conn = conn;
    }

    /**
     * Busca en la BD alguna reserva de socio en instalación entre la hora de inicio y la hora de fin
     * pasadas como parámetro en esa fecha
     * @param id_Instalacion
     * @param horaInicio
     * @param horaFin
     * @param fecha 
     * @return la reserva de socio, null si no la encuentra
     */
    public List<SocioReservaInstalacionDto> getReservaSocioInstalacion(
	    Long id_Instalacion, String horaInicio, String horaFin, String fecha) {
	List<SocioReservaInstalacionDto> reservas = new ArrayList<SocioReservaInstalacionDto>();
	SocioReservaInstalacionDto reserva = null;
	ResultSet rs = null;
	PreparedStatement ps = null;
	String consulta = Settings.get("SQL_GET_SOCIO_RESERVA_ACTIVIDAD");
	
	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, id_Instalacion);
	    ps.setString(2, horaInicio);
	    ps.setString(3, horaFin);
	    ps.setString(4, horaFin);
	    ps.setString(5, fecha);
	    rs = ps.executeQuery();

	    // Si encuentra las reservas
	    while (rs.next()) {
		reserva = new SocioReservaInstalacionDto();
		reserva.setId(rs.getLong("id"));
		reserva.setId_Socio(rs.getLong("id_Socio"));
		reserva.setId_Instalacion(rs.getLong("id_Instalacion"));
		reserva.setFecha(rs.getString("fecha"));
		reserva.setHoraInicio(rs.getString("horaInicio"));
		
		// extra
		reserva.setNombreCompleto(rs.getString("nombre_socio") + " " + rs.getString("apellidos"));
		reserva.setDni(rs.getString("dni"));
		reserva.setNombreInstalacion(rs.getString("nombre_instalacion"));
		
		reservas.add(reserva);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	}
	return reservas;
    }
    
    /**
     * Busca en la BD alguna reserva de socio en instalación a partir de los argumentos pasados como
     * parámetro
     * @param nombre_Instalacion
     * @param dni_Socio
     * @param fecha
     * @param hora
     * @return reserva de socio en instalación
     */

    public SocioReservaInstalacionDto getReservaSocioInstalacion(
	    String nombre_Instalacion, String dni_Socio, String fecha,
	    String hora) {
	SocioReservaInstalacionDto reserva = null;
	ResultSet rs = null;
	PreparedStatement ps = null;
	String consulta = Settings.get("SQL_GET_RESERVA");
	
	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setString(1, nombre_Instalacion);
	    ps.setString(2, dni_Socio);
	    ps.setString(3, fecha);
	    ps.setString(4, hora);
	    rs = ps.executeQuery();

	    // Si encuentra la reserva
	    if (rs.next()) {
		reserva = new SocioReservaInstalacionDto();
		reserva.setId(rs.getLong("id"));
		reserva.setId_Socio(rs.getLong("id_Socio"));
		reserva.setId_Instalacion(rs.getLong("id_Instalacion"));
		reserva.setFecha(fecha);
		reserva.setHoraInicio(hora);
	
		reserva.setNombreCompleto(rs.getString("nombre") + " " + rs.getString("apellidos"));
		reserva.setDni(dni_Socio);
		reserva.setNombreInstalacion(nombre_Instalacion);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	}
	return reserva;
    }
}
