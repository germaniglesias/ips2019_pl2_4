package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dto.SocioDto;
import utils.Settings;

public class FindSocioById {
    
    private Long id_Socio;
    private Connection conn;

    public FindSocioById(Long id_Socio, Connection conn) {
	this.id_Socio = id_Socio;
	this.conn = conn;
    }

    public SocioDto execute() {
	SocioDto socio = null;
	
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	try {
	    String consulta = Settings.get("SQL_FIND_SOCIO_BY_ID");
	    ps = conn.prepareStatement(consulta);

	    ps.setLong(1, id_Socio);

	    rs = ps.executeQuery();

	    if (rs.next()) {
		socio = new SocioDto();
		socio.setId(rs.getLong("id"));
		socio.setNombre(rs.getString("nombre"));
		socio.setApellidos(rs.getString("apellidos"));
		socio.setDni(rs.getString("dni"));
		socio.setCorreo(rs.getString("correo"));
		socio.setClave(rs.getString("clave"));
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	}
	
	return socio;
    }

}
