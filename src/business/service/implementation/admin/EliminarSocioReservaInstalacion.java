package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import utils.Settings;

public class EliminarSocioReservaInstalacion {
    private Connection conn;
    
    public void setConnection(Connection conn) {
	this.conn = conn;
    }

    /**
     * Elimina una reserva de la BD a partir de su id
     * @param idReserva
     */
    public void eliminarReserva(Long idReserva) {
	PreparedStatement ps = null;
	String consulta = Settings.get("SQL_DELETE_RESERVA_SOCIO_INSTALACION");
	
	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, idReserva);
	    ps.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    try {ps.close();} catch (SQLException e) {e.printStackTrace();}    
	}
    }
}
