package business.service.implementation.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import database.gestorDDBB;
import dto.InstalacionDto;
import exception.ApplicationException;
import utils.Settings;

public class ListFacilities {
    
    private List<InstalacionDto> list = new ArrayList<InstalacionDto>();
    private InstalacionDto instalacion;
    
    public List<InstalacionDto> execute() {
	try (Connection conn = gestorDDBB.getConnection();
		PreparedStatement pst = conn
			.prepareStatement(Settings.get("SQL_LIST_FACILITIES"));
		ResultSet rs = pst.executeQuery();) {

	    while (rs.next()) {
		instalacion = new InstalacionDto();
		instalacion.setId(rs.getLong("id"));
		instalacion.setNombre(rs.getString("nombre"));
		instalacion.setPrecioHora(rs.getInt("precioPorHora"));
		list.add(instalacion);
	    }

	} catch (SQLException e) {
	    throw new ApplicationException(
		    "Es necesario que la BBDD este activada.");
	}

	return list;
    }

}


