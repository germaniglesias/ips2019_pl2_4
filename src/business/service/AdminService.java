package business.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import dto.ActividadDto;
import dto.InstalacionDto;
import dto.MonitorDto;
import dto.PlanificacionActividadDto;
import dto.SocioDto;
import dto.SocioReservaInstalacionDto;

public interface AdminService {

    void createActividad(ActividadDto actividad);

    List<InstalacionDto> listFacilities();

    List<PlanificacionActividadDto> listFacilitiesReservations(Long index,
	    String startWeek, String endWeek);

    void updateActivityAssignMonitor(Long id_monitor, Long id_actividad);

    List<PlanificacionActividadDto> listActivitiesWithLimitSeat(Long id, String date,
	    String startTime);

    String checkMemberSimultaneousActivities(String dni, String date,
	    String startTime, String endTime);
    
    String checkMemberSimultaneousReservations(String dni, String date,
	    String startTime);

    void InsertMemberActivitiesReservations(Long member_id, Long activity_id);

    boolean checkValidMember(String dni);

    void planActivity(PlanificacionActividadDto reserva, Connection conn)
	    throws SQLException;

    Long getIdToAssignMonitor(String s, Long id_Actividad, String startTime,
	    String endTime);

    List<MonitorDto> listAvailableMonitors(List<String> dias, String startTime,
	    String endTime);

    List<PlanificacionActividadDto> listActivitiesWithoutMonitor(Long id,
	    String endWeek, String today, String startHour);

    List<PlanificacionActividadDto> listPlanificationsOnDateAndHour(
	    PlanificacionActividadDto reserva, Connection conn)
	    throws SQLException;

    SocioDto findSocioById(Long id_Socio, Connection conn);

    void insertarReservaImpagada(Long id, Connection conn);

    void insertarReservaEnEfectivo(Long id, Connection conn);

    List<SocioReservaInstalacionDto> listReservasInstalacionSocio(Long id,
	    String startWeek, String endWeek);

    boolean isPagado(Long id, Connection conn);

    void InsertMemberFacilitiesReservations(Long member_id, Long id,
	    String date, String startHour);

    SocioDto findMember(String dni);

    boolean checkSimultaneousActivity(Long id, String date,
	    String startHour);

    boolean checkSimultaneousReservations(Long id, String date,
	    String startHour);
}
