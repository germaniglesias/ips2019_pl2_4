package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class gestorDDBB {
    private static Connection conn;
    private static String DRIVER = "org.sqlite.JDBC";
    private static String URL = "jdbc:sqlite:database.db";

    public static Connection getConnection() {
	try {
	    Class.forName(DRIVER);
	    conn = DriverManager.getConnection(URL);
	} catch (SQLException | ClassNotFoundException ex) {
	    System.out.println("Error en la conexión de la base de datos");
	}
	return conn;
    }

    public static void close(ResultSet rs, PreparedStatement pst, Connection c) {
	close(rs);
	close(pst);
	close(c);
    }

    protected static void close(ResultSet rs) {
	if (rs != null)
	    try {
		rs.close();
	    } catch (SQLException e) {
		/* ignore */}
    }

    public static void close(PreparedStatement pst) {
	if (pst != null)
	    try {
		pst.close();
	    } catch (SQLException e) {
		/* ignore */}
    }

    public static void close(Connection c) {
	if (c != null)
	    try {
		c.close();
	    } catch (SQLException e) {
		/* ignore */}
    }
}
