package ui.monitor;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import business.service.MonitorService;
import business.service.implementation.MonitorServiceImpl;
import dto.SocioDto;
import utils.NonEditableModel;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SociosRegistrarActivityView extends JDialog {

    /**
     * 
     */
    private MonitorService msi;
    private int idPActividad;
    private String horaInicio;

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JScrollPane scSociosApuntados;
    private JLabel lblPlazasRestantes;
    private JLabel lbPlazasRestantesCantidad;
    private JButton btnAadirSocio;
    private JButton btnAtrs;
    private JButton btnAceptar;
    private JTable tbSociosRegistrados;

    private DefaultTableModel modeloTabla;
    private JLabel lblSociosRegistrados;

    private int plazasRestantes;
    private JButton btnEliminarSocio;
    private MonitorMainView monitor;

    public SociosRegistrarActivityView(MonitorMainView monitor,
	    int idPActividad, int plazasRestantes, String horaInicio) {
	setTitle("Registro de socios en actividad");
	this.monitor = monitor;
	msi = new MonitorServiceImpl();
	this.idPActividad = idPActividad;
	this.plazasRestantes = plazasRestantes;
	this.horaInicio = horaInicio;

	setModal(true);
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setBounds(100, 100, 771, 403);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(null);
	contentPane.add(getScSociosApuntados());
	contentPane.add(getLblPlazasRestantes());
	contentPane.add(getLbPlazasRestantesCantidad());
	contentPane.add(getBtnAadirSocio());
	contentPane.add(getBtnAtras());
	contentPane.add(getBtnAceptar());
	contentPane.add(getLblSociosRegistrados());
	contentPane.add(getBtnEliminarSocio());

	setVisible(true);
    }

    private JScrollPane getScSociosApuntados() {
	if (scSociosApuntados == null) {
	    scSociosApuntados = new JScrollPane();
	    scSociosApuntados.setBounds(10, 66, 735, 241);
	    scSociosApuntados.setViewportView(getTbSociosRegistrados());
	}
	return scSociosApuntados;
    }

    private JLabel getLblPlazasRestantes() {
	if (lblPlazasRestantes == null) {
	    lblPlazasRestantes = new JLabel("Plazas restantes:");
	    lblPlazasRestantes.setFont(new Font("Tahoma", Font.PLAIN, 17));
	    lblPlazasRestantes.setBounds(10, 330, 154, 31);
	}
	return lblPlazasRestantes;
    }

    private JLabel getLbPlazasRestantesCantidad() {
	if (lbPlazasRestantesCantidad == null) {
	    lbPlazasRestantesCantidad = new JLabel("");
	    lbPlazasRestantesCantidad
		    .setFont(new Font("Tahoma", Font.PLAIN, 21));
	    lbPlazasRestantesCantidad.setBounds(195, 329, 50, 31);
	    lbPlazasRestantesCantidad
		    .setText(Integer.toString(plazasRestantes));
	}
	return lbPlazasRestantesCantidad;
    }

    private void modificarPlazasRestantes(int plazas) {
	lbPlazasRestantesCantidad.setText(String.valueOf(plazas));
    }

    private JButton getBtnAadirSocio() {
	if (btnAadirSocio == null) {
	    btnAadirSocio = new JButton("A\u00F1adir socio");
	    btnAadirSocio.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    añadirSocio();
		}
	    });
	    btnAadirSocio.setMnemonic('A');
	    btnAadirSocio.setBounds(264, 337, 108, 23);
	}
	return btnAadirSocio;
    }

    private void añadirSocio() {
	String dni = JOptionPane.showInputDialog(null,
		"Introduzca el DNI del usuario a registrar");

	if (dni == null)
	    return;

	if (dni.equals("")) {
	    JOptionPane.showMessageDialog(null,
		    "Error, no has introducido nada", "Error en id",
		    JOptionPane.ERROR_MESSAGE);
	} else {
	    if (!msi.isDniCorrecto(dni)) {
		JOptionPane.showMessageDialog(null, "Error, el DNI no existe",
			"Error en dni", JOptionPane.ERROR_MESSAGE);

	    } else {
		gestionarRegistro(dni);
	    }

	}

    }

    private void gestionarRegistro(String dni) {
	// Si aún hay hueco para algún socio
	if (Integer.parseInt(lbPlazasRestantesCantidad.getText()) > 0) {
	    SocioDto socio = msi.getSocioByDni(dni);
	    /*
	     * Comprobar que el socio no esta ya en la BD, si lo esta no hacer
	     * nada ACTUALIZACIÓN: comprobar que el socio no esté en otra
	     * actividad a la vez
	     */
	    if (!msi.isSocioInActivity(dni, idPActividad)) {

		// Si el socio ya se encuentra en otra actividad al mismo tiempo
		if (msi.isSocioInAnotherActivity(socio.getId(), horaInicio))
		    JOptionPane.showMessageDialog(this,
			    "Error, el socio ya está en otra actividad a la vez",
			    "Error al añadir socio", JOptionPane.ERROR_MESSAGE);
		else {
		    añadirSocioEnTabla(socio);
		    modificarPlazasRestantes(Integer
			    .parseInt(lbPlazasRestantesCantidad.getText()) - 1);
		}
	    }
	} else
	    JOptionPane.showMessageDialog(this,
		    "No quedan plazas libres, "
			    + "ya no pueden añadirse más socios",
		    "Error al añadir socio", JOptionPane.ERROR_MESSAGE);
    }

    private void registrarSocioActividadEnBD(int idSocio) {
	// Si está en la BD pero con reserva CANCELADA,
	// actualizaremos su estado a CORRECTA ( = registrarlo)
	if (msi.isReservaSocioCancelada(idSocio, idPActividad))
	    msi.actualizarEstadoSocioActividad(idSocio, idPActividad);

	// Si no está y no colisiona
	else
	    msi.registrarSocioEnActividad(idSocio, idPActividad);

	// Informamos al usuario de que se ha añadido al socio
	JOptionPane.showMessageDialog(this, "Se ha añadido al socio",
		"Socio añadido", JOptionPane.INFORMATION_MESSAGE);
    }

    private void añadirSocioEnTabla(SocioDto socio) {
	Object[] nuevaFila = new Object[4];

	nuevaFila[0] = socio.getId();
	nuevaFila[1] = socio.getNombre();
	nuevaFila[2] = socio.getApellidos();
	nuevaFila[3] = socio.getDni();
	modeloTabla.addRow(nuevaFila);

    }

    private JButton getBtnAtras() {
	if (btnAtrs == null) {
	    btnAtrs = new JButton("Atr\u00E1s");
	    btnAtrs.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    dispose();
		}
	    });
	    btnAtrs.setMnemonic('t');
	    btnAtrs.setBounds(661, 337, 89, 23);
	}
	return btnAtrs;
    }

    private JButton getBtnAceptar() {
	if (btnAceptar == null) {
	    btnAceptar = new JButton("Aceptar");
	    btnAceptar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    monitor.setVisible(true);
		    dispose();
		    registrarSocios();
		    monitor.actualizarPlazasLibres();
		}
	    });
	    btnAceptar.setMnemonic('c');
	    btnAceptar.setBounds(562, 337, 89, 23);
	}
	return btnAceptar;
    }

    private void registrarSocios() {
	for (int i = 0; i < modeloTabla.getRowCount(); i++) {
	    String dni = (String) modeloTabla.getValueAt(i, 3);

	    SocioDto socio = msi.getSocioByDni(dni);
	    /*
	     * Comprobar que el socio no esta ya en la BD, si lo esta no hacer
	     * nada
	     */
	    if (!msi.isSocioInActivity(dni, idPActividad)) {
		registrarSocioActividadEnBD(
			Integer.parseInt(socio.getId().toString()));
	    }
	}
    }

    private JTable getTbSociosRegistrados() {
	if (tbSociosRegistrados == null) {
	    String[] columnas = { "Id socio", "Nombre", "Apellidos", "DNI" };
	    modeloTabla = new NonEditableModel(columnas, 0);
	    añadirFilas();
	    tbSociosRegistrados = new JTable(modeloTabla);
	}
	return tbSociosRegistrados;
    }

    private void añadirFilas() {
	Object[] nuevaFila = new Object[4];
	List<SocioDto> socios = msi.listSociosByActivity(idPActividad);

	for (SocioDto socio : socios) {
	    nuevaFila[0] = socio.getId();
	    nuevaFila[1] = socio.getNombre();
	    nuevaFila[2] = socio.getApellidos();
	    nuevaFila[3] = socio.getDni();

	    modeloTabla.addRow(nuevaFila);
	}

    }

    private JLabel getLblSociosRegistrados() {
	if (lblSociosRegistrados == null) {
	    lblSociosRegistrados = new JLabel("Socios registrados:");
	    lblSociosRegistrados.setFont(new Font("Tahoma", Font.PLAIN, 28));
	    lblSociosRegistrados.setBounds(10, 11, 449, 40);
	}
	return lblSociosRegistrados;
    }

    private JButton getBtnEliminarSocio() {
	if (btnEliminarSocio == null) {
	    btnEliminarSocio = new JButton("Eliminar socio");
	    btnEliminarSocio.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    eliminarSocio();
		}
	    });
	    btnEliminarSocio.setBounds(382, 337, 116, 23);
	}
	return btnEliminarSocio;
    }

    private void eliminarSocio() {
	int index = tbSociosRegistrados.getSelectedRow();
	if (index != -1) {
	    modeloTabla.removeRow(index);
	    modificarPlazasRestantes(
		    Integer.parseInt(lbPlazasRestantesCantidad.getText()) + 1);

	    if (modeloTabla.getRowCount() == 0) {
		btnEliminarSocio.setEnabled(false);
		btnAceptar.setEnabled(false);
	    }
	}
    }
}
