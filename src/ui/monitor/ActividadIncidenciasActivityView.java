package ui.monitor;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import business.service.MonitorService;
import business.service.implementation.MonitorServiceImpl;
import dto.ActividadDto;
import dto.PlanificacionActividadDto;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JButton;
import javax.swing.SpinnerNumberModel;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ActividadIncidenciasActivityView extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    private JLabel lblActividadElegida;
    private JScrollPane scActividad;
    private JTextArea txActividadElegida;
    private JLabel lblNmeroAproxDe;
    private JSpinner spinner;
    private JLabel lblIncidenciasopcional;
    private JScrollPane scIncidencias;
    private JTextArea txIncidencias;
    private JButton btnNewButton;
    private JButton btnAtrs;

    private MonitorService msi;
    private PlanificacionActividadDto actividad;
    private JLabel lblFecha;
    private JLabel lbFechaCampo;
    private JLabel lbHora;
    private JLabel lbHoraCampo;
    private MonitorMainView monitor;

    public ActividadIncidenciasActivityView(PlanificacionActividadDto actividad,
	    MonitorMainView monitor) {
	setTitle("Parte de incidencias de la actividad");
	this.actividad = actividad;
	this.monitor = monitor;
	msi = new MonitorServiceImpl();

	setModal(true);
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setBounds(100, 100, 578, 440);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(null);
	contentPane.add(getLblActividadElegida());
	contentPane.add(getScActividad());
	contentPane.add(getLblNmeroAproxDe());
	contentPane.add(getSpinner());
	contentPane.add(getLblIncidenciasopcional());
	contentPane.add(getScIncidencias());
	contentPane.add(getBtnNewButton());
	contentPane.add(getBtnAtrs());
	contentPane.add(getLblFecha());
	contentPane.add(getLbFechaCampo());
	contentPane.add(getLbHora());
	contentPane.add(getLbHoraCampo());
	

	gestionarActividad();
	
	setResizable(false);
	setLocationRelativeTo(monitor);
	setVisible(true);

    }

    private JLabel getLblActividadElegida() {
	if (lblActividadElegida == null) {
	    lblActividadElegida = new JLabel("Actividad elegida:");
	    lblActividadElegida.setFont(new Font("Tahoma", Font.PLAIN, 28));
	    lblActividadElegida.setBounds(10, 11, 239, 48);
	}
	return lblActividadElegida;
    }

    private JScrollPane getScActividad() {
	if (scActividad == null) {
	    scActividad = new JScrollPane();
	    scActividad.setBounds(259, 24, 300, 35);
	    scActividad.setViewportView(getTxActividadElegida());
	}
	return scActividad;
    }

    private JTextArea getTxActividadElegida() {
	if (txActividadElegida == null) {
	    txActividadElegida = new JTextArea();
	    txActividadElegida.setLineWrap(true);
	    txActividadElegida.setWrapStyleWord(true);
	    txActividadElegida
		    .setFont(new Font("Malgun Gothic", Font.PLAIN, 22));
	    txActividadElegida.setForeground(new Color(255, 99, 71));
	    txActividadElegida.setEditable(false);
	}
	return txActividadElegida;
    }

    private void gestionarActividad() {
	ActividadDto ac = msi.findActivityByPlanificacionId(
		Integer.parseInt(actividad.getId_Actividad().toString()));
	txActividadElegida.setText(ac.getNombre());

	// Si la actividad tiene limite de plazas el valor del spinner es ese y
	// no es modificable
	if (actividad.isLimitePlazas()) {
	    int plazasOcupadas = msi.getCantidadPersonasApuntadas(
		    Integer.valueOf(actividad.getId().toString()));
	    spinner.setModel(new SpinnerNumberModel(plazasOcupadas,
		    plazasOcupadas, plazasOcupadas, 1));
	    ((DefaultEditor) spinner.getEditor()).getTextField()
		    .setEditable(false);
	}

	lbFechaCampo.setText(actividad.getFecha());
	lbHoraCampo.setText(
		actividad.getHoraInicio() + " - " + actividad.getHoraFin());

    }

    private JLabel getLblNmeroAproxDe() {
	if (lblNmeroAproxDe == null) {
	    lblNmeroAproxDe = new JLabel("Número aprox. de asistentes:");
	    lblNmeroAproxDe.setFont(new Font("Tahoma", Font.PLAIN, 16));
	    lblNmeroAproxDe.setBounds(10, 86, 224, 36);
	}
	return lblNmeroAproxDe;
    }

    private JSpinner getSpinner() {
	if (spinner == null) {
	    spinner = new JSpinner();
	    spinner.setModel(new SpinnerNumberModel(Integer.valueOf(0), 0, null,
		    Integer.valueOf(1)));
	    spinner.setFont(new Font("Tahoma", Font.PLAIN, 15));
	    spinner.setBounds(244, 94, 40, 20);

	}
	return spinner;
    }

    private JLabel getLblIncidenciasopcional() {
	if (lblIncidenciasopcional == null) {
	    lblIncidenciasopcional = new JLabel("Incidencias (opcional) : ");
	    lblIncidenciasopcional.setDisplayedMnemonic('n');
	    lblIncidenciasopcional.setLabelFor(getTxIncidencias());
	    lblIncidenciasopcional.setFont(new Font("Tahoma", Font.PLAIN, 16));
	    lblIncidenciasopcional.setBounds(10, 220, 185, 36);
	}
	return lblIncidenciasopcional;
    }

    private JScrollPane getScIncidencias() {
	if (scIncidencias == null) {
	    scIncidencias = new JScrollPane();
	    scIncidencias.setBounds(215, 227, 344, 129);
	    scIncidencias.setViewportView(getTxIncidencias());
	}
	return scIncidencias;
    }

    private JTextArea getTxIncidencias() {
	if (txIncidencias == null) {
	    txIncidencias = new JTextArea();
	    txIncidencias.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    txIncidencias.setLineWrap(true);
	    txIncidencias.setWrapStyleWord(true);
	}
	return txIncidencias;
    }

    private JButton getBtnNewButton() {
	if (btnNewButton == null) {
	    btnNewButton = new JButton("Generar parte");
	    btnNewButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    generarParte();
		}
	    });
	    btnNewButton.setMnemonic('G');
	    btnNewButton.setBounds(323, 367, 141, 23);
	}
	return btnNewButton;
    }

    private void generarParte() {
	String incidencias = txIncidencias.getText();
	int numAproxAsistentes = (int) spinner.getValue();
	msi.generarParteIncidencias(actividad.getId(), numAproxAsistentes,
		incidencias);

	JOptionPane.showMessageDialog(this,
		"Se ha registrado el parte de incidencias", "Parte generado",
		JOptionPane.INFORMATION_MESSAGE);

	monitor.setVisible(true);
	monitor.actualizarActividadesSinParte();
	dispose();
    }

    private JButton getBtnAtrs() {
	if (btnAtrs == null) {
	    btnAtrs = new JButton("Atrás");
	    btnAtrs.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    dispose();
		}
	    });
	    btnAtrs.setMnemonic('t');
	    btnAtrs.setBounds(470, 367, 89, 23);
	}
	return btnAtrs;
    }

    private JLabel getLblFecha() {
	if (lblFecha == null) {
	    lblFecha = new JLabel("Fecha:");
	    lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 16));
	    lblFecha.setBounds(10, 153, 73, 20);
	}
	return lblFecha;
    }

    private JLabel getLbFechaCampo() {
	if (lbFechaCampo == null) {
	    lbFechaCampo = new JLabel("");
	    lbFechaCampo.setForeground(new Color(255, 99, 71));
	    lbFechaCampo.setFont(new Font("Tahoma", Font.PLAIN, 16));
	    lbFechaCampo.setBounds(80, 150, 155, 23);
	}
	return lbFechaCampo;
    }

    private JLabel getLbHora() {
	if (lbHora == null) {
	    lbHora = new JLabel("Hora:");
	    lbHora.setFont(new Font("Tahoma", Font.PLAIN, 16));
	    lbHora.setBounds(251, 153, 58, 20);
	}
	return lbHora;
    }

    private JLabel getLbHoraCampo() {
	if (lbHoraCampo == null) {
	    lbHoraCampo = new JLabel("");
	    lbHoraCampo.setFont(new Font("Tahoma", Font.PLAIN, 16));
	    lbHoraCampo.setForeground(new Color(255, 99, 71));
	    lbHoraCampo.setBounds(319, 152, 240, 23);
	}
	return lbHoraCampo;
    }
}
