package ui.monitor;

import java.util.List;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import business.service.implementation.MonitorServiceImpl;
import dto.SocioDto;
import utils.NonEditableModel;

import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SociosAsistenciaActivityView extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    private List<SocioDto> socios;

    private JScrollPane spSociosApuntados;

    private JButton btnAñadir;
    private JLabel lblSociosApuntados;
    private JTable tbSociosApuntados;
    private JScrollPane spSociosPresentes;
    private JList<String> listaSociosPresentes;
    private JLabel lblSociosPresentes;
    private JButton btnEliminar;
    private JButton btnAceptar;
    private JButton btnAtras;
    private DefaultTableModel modeloTabla;
    private DefaultListModel<String> modeloLista;

    private MonitorServiceImpl msi;
    private int idPActividad;

    private MonitorMainView monitor;

    public SociosAsistenciaActivityView(List<SocioDto> socios, int idPActividad, MonitorMainView monitor) {
	setTitle("Apuntar socios");
	this.socios = socios;
	this.idPActividad = idPActividad;
	msi = new MonitorServiceImpl();
	this.monitor = monitor;

	setModal(true);
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setBounds(100, 100, 720, 429);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(null);
	contentPane.add(getSpSociosApuntados());

	contentPane.add(getBtnAñadir());
	contentPane.add(getLblSociosApuntados());
	contentPane.add(getSpSociosPresentes());
	contentPane.add(getLblSociosPresentes());
	contentPane.add(getBtnEliminar());
	contentPane.add(getBtnAceptar());
	contentPane.add(getBtnAtras());

	comprobarCantidadSocios();
	
	setLocationRelativeTo(monitor);

	setVisible(true);
    }

    private void comprobarCantidadSocios() {
	if (socios.size() == 0)
	    btnAñadir.setEnabled(false);
    }

    private JScrollPane getSpSociosApuntados() {
	if (spSociosApuntados == null) {
	    spSociosApuntados = new JScrollPane();
	    spSociosApuntados.setBounds(10, 56, 427, 279);
	    spSociosApuntados.setViewportView(getTbSociosApuntados());
	}
	return spSociosApuntados;
    }

    private JButton getBtnAñadir() {
	if (btnAñadir == null) {
	    btnAñadir = new JButton("A\u00F1adir");
	    btnAñadir.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    añadirSocio();
		}
	    });
	    btnAñadir.setMnemonic('A');
	    btnAñadir.setBounds(348, 343, 89, 23);
	}

	return btnAñadir;

    }

    private void añadirSocio() {
	int fila = tbSociosApuntados.getSelectedRow();
	if (fila != -1) {
	    String dni = (String) modeloTabla.getValueAt(fila, 3);
	    if (!modeloLista.contains(dni)) {
		modeloLista.addElement(dni);
		modeloTabla.removeRow(fila);

		btnEliminar.setEnabled(true);
		btnAceptar.setEnabled(true);

		if (modeloTabla.getRowCount() == 0)
		    btnAñadir.setEnabled(false);

	    }
	}

    }

    private JLabel getLblSociosApuntados() {
	if (lblSociosApuntados == null) {
	    lblSociosApuntados = new JLabel("Socios apuntados:");
	    lblSociosApuntados.setFont(new Font("Tahoma", Font.PLAIN, 28));
	    lblSociosApuntados.setBounds(10, 11, 427, 34);
	}
	return lblSociosApuntados;
    }

    private JTable getTbSociosApuntados() {
	if (tbSociosApuntados == null) {
	    String[] columnas = { "Id socio", "Nombre", "Apellidos", "Dni" };
	    modeloTabla = new NonEditableModel(columnas, 0);
	    añadirFilas();
	    tbSociosApuntados = new JTable(modeloTabla);
	    tbSociosApuntados.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    if (e.getClickCount() == 2) {
			añadirSocio();
		    }
		}
	    });
	}
	return tbSociosApuntados;
    }

    private void añadirFilas() {
	Object[] nuevaFila = new Object[4];
	for (SocioDto socio : socios) {
	    nuevaFila[0] = socio.getId();
	    nuevaFila[1] = socio.getNombre();
	    nuevaFila[2] = socio.getApellidos();
	    nuevaFila[3] = socio.getDni();

	    modeloTabla.addRow(nuevaFila);
	}
    }

    private JScrollPane getSpSociosPresentes() {
	if (spSociosPresentes == null) {
	    spSociosPresentes = new JScrollPane();
	    spSociosPresentes.setBounds(458, 56, 236, 167);
	    spSociosPresentes.setViewportView(getListaSociosPresentes());
	}
	return spSociosPresentes;
    }

    private JList<String> getListaSociosPresentes() {
	if (listaSociosPresentes == null) {
	    modeloLista = new DefaultListModel<String>();
	    listaSociosPresentes = new JList<String>(modeloLista);
	    listaSociosPresentes.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    if (e.getClickCount() == 2) {
			eliminarSocio();
		    }
		}
	    });
	}
	return listaSociosPresentes;
    }

    private JLabel getLblSociosPresentes() {
	if (lblSociosPresentes == null) {
	    lblSociosPresentes = new JLabel("Socios presentes:");
	    lblSociosPresentes.setFont(new Font("Tahoma", Font.PLAIN, 28));
	    lblSociosPresentes.setBounds(458, 11, 218, 34);
	}
	return lblSociosPresentes;
    }

    private JButton getBtnEliminar() {
	if (btnEliminar == null) {
	    btnEliminar = new JButton("Eliminar");
	    btnEliminar.setEnabled(false);
	    btnEliminar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    eliminarSocio();
		}
	    });
	    btnEliminar.setMnemonic('E');
	    btnEliminar.setBounds(605, 234, 89, 23);
	}
	return btnEliminar;
    }

    private void eliminarSocio() {
	int index = listaSociosPresentes.getSelectedIndex();
	if (index != -1) {
	    String dni = modeloLista.get(index);
	    SocioDto socio = msi.getSocioByDni(dni);

	    añadirFila(socio);
	    modeloLista.removeElementAt(index);

	    btnAñadir.setEnabled(true);

	    if (modeloLista.getSize() == 0) {
		btnEliminar.setEnabled(false);
		btnAceptar.setEnabled(false);
	    }
	}
    }

    private void añadirFila(SocioDto socio) {
	Object[] nuevaFila = new Object[4];
	nuevaFila[0] = socio.getId();
	nuevaFila[1] = socio.getNombre();
	nuevaFila[2] = socio.getApellidos();
	nuevaFila[3] = socio.getDni();

	modeloTabla.addRow(nuevaFila);

    }

    private JButton getBtnAceptar() {
	if (btnAceptar == null) {
	    btnAceptar = new JButton("Aceptar");
	    btnAceptar.setEnabled(false);
	    btnAceptar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    generarPlazasLibres();
		}
	    });
	    btnAceptar.setBounds(506, 356, 89, 23);
	}
	return btnAceptar;
    }

    private void generarPlazasLibres() {
	// Eliminamos a personas que estaban apuntadas antes
	desapuntarSocios();
	// Avisar al usuario
	JOptionPane.showMessageDialog(null, "Se han apuntado a los socios");
	eliminarVentanas();
    }

    private void eliminarVentanas() {
	monitor.actualizarPlazasLibres();
	dispose();
    }

    private void desapuntarSocios() {
	int idSocio;
	for (int i = 0; i < modeloTabla.getRowCount(); i++) {
	    // Sacamos el id del socio
	    idSocio = Integer.parseInt(modeloTabla.getValueAt(i, 0).toString());
	    // Llamamos a la base de datos y lo eliminamos (eliminar = marcar
	    // como CANCELADA la reserva)
	    msi.eliminarSocioEnActividad(idSocio, idPActividad);
	}
    }

    private JButton getBtnAtras() {
	if (btnAtras == null) {
	    btnAtras = new JButton("Atr\u00E1s");
	    btnAtras.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    dispose();
		}
	    });
	    btnAtras.setMnemonic('t');
	    btnAtras.setBounds(605, 356, 89, 23);
	}
	return btnAtras;
    }
}
