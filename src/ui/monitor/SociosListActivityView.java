package ui.monitor;

import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.SocioDto;
import utils.NonEditableModel;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

public class SociosListActivityView extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private JPanel contentPane;
    private JScrollPane spSocios;
    private JTable tbSocios;
    private DefaultTableModel modeloTabla;

    private List<SocioDto> socios;
    private JButton btnAtrs;
    private JLabel lbActividad;

    private MonitorMainView monitor;
    
    public SociosListActivityView(MonitorMainView monitor,
	    List<SocioDto> socios, String nombreActividad) {
	this.monitor = monitor;
	this.socios = socios;

	setModal(true);
	setTitle("Socios de la actividad");
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setBounds(100, 100, 589, 372);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(null);
	contentPane.add(getSpSocios());
	contentPane.add(getBtnAtrs());
	contentPane.add(getLbActividad());

	asignarValores(nombreActividad);
	setLocationRelativeTo(monitor);

	setVisible(true);
    }

    private JScrollPane getSpSocios() {
	if (spSocios == null) {
	    spSocios = new JScrollPane();
	    spSocios.setBounds(10, 68, 553, 220);
	    spSocios.setViewportView(getTbSocios());
	}
	return spSocios;
    }

    private JTable getTbSocios() {
	if (tbSocios == null) {
	    String[] nombreColumnas = { "Id socio", "Nombre", "Apellidos",
		    "Dni" };
	    modeloTabla = new NonEditableModel(nombreColumnas, 0);
	    añadirFilas();
	    tbSocios = new JTable(modeloTabla);
	}
	return tbSocios;
    }

    private void añadirFilas() {
	Object[] nuevaFila = new Object[4];
	for (SocioDto socio : socios) {
	    nuevaFila[0] = socio.getId();
	    nuevaFila[1] = socio.getNombre();
	    nuevaFila[2] = socio.getApellidos();
	    nuevaFila[3] = socio.getDni();

	    modeloTabla.addRow(nuevaFila);
	}

    }

    private JButton getBtnAtrs() {
	if (btnAtrs == null) {
	    btnAtrs = new JButton("Atr\u00E1s");
	    btnAtrs.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    monitor.setVisible(true);
		    dispose();
		}
	    });
	    btnAtrs.setMnemonic('A');
	    btnAtrs.setBounds(474, 299, 89, 23);
	}
	return btnAtrs;
    }

    private JLabel getLbActividad() {
	if (lbActividad == null) {
	    lbActividad = new JLabel("");
	    lbActividad.setFont(new Font("Tahoma", Font.PLAIN, 21));
	    lbActividad.setBounds(10, 11, 553, 47);
	}
	return lbActividad;
    }

    private void asignarValores(String nombreActividad) {
	lbActividad.setText(nombreActividad);
    }
}
