package ui.monitor;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import business.service.MonitorService;
import business.service.implementation.MonitorServiceImpl;
import dto.MonitorDto;
import dto.PlanificacionActividadDto;
import dto.SocioDto;
import ui.Main;
import utils.NonEditableModel;

import java.awt.GridLayout;

import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Font;
import java.awt.FlowLayout;
import java.awt.CardLayout;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MonitorMainView extends JFrame {
    private static final long serialVersionUID = 1L;

    private JPanel contentPane;
    private JSplitPane splitVista;
    private JPanel pnMenu;
    private JPanel pnInfo;
    private JPanel pnBotones;
    private JLabel lbLogo;
    private JPanel pnDatosMonitor;
    private JLabel lbIdentificador;
    private JButton btnCerrarSesin;
    private JButton btGestionarActividad;
    private JButton btGenerarParte;

    private MonitorService msi = new MonitorServiceImpl();
    private JPanel pnOpciones;
    private JPanel pnGestionarActividad;
    private JPanel pnGenerarParte;
    private JPanel pnTabla;
    private JPanel pnGestiones;
    private JPanel pnActividadElegida;
    private JPanel pnAsistentes;
    private JLabel lbActividadElegidaGestionar;
    private JTextArea txActividadElegidaGestionar;
    private JScrollPane spTablaGestionar;
    private JTable tbActividadesGestionar;
    private JButton btnListaAsistentes;
    private JButton btnPasarLista;
    private JButton btnApuntarSocios;
    private JPanel pnPrincipal;
    private DefaultTableModel modeloTablaGestionar;
    private DefaultTableModel modeloTablaGenerar;

    private ProcesaPaneles pP;
    private int idMonitor = 4; // solo se usa este monitor para ver la app

    private Main main;
    private JPanel pnTablaParte;
    private JPanel pnActividadParte;
    private JPanel pnActividadElegidaParte;
    private JLabel lbActividadElegidaGenerar;
    private JTextArea txActividadElegidaGenerar;
    private JPanel pnCrearParte;
    private JButton btnCrearParte;
    private JScrollPane spTablaGenerar;
    private JTable tbActividadesGenerar;

    /**
     * Create the frame.
     */
    public MonitorMainView(Main main) {
	try {
	    UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
	} catch (Throwable e) {
	    e.printStackTrace();
	}

	this.main = main;

	this.pP = new ProcesaPaneles();

	setTitle("CENTRO DEPORTIVO: MONITOR");
	setIconImage(Toolkit.getDefaultToolkit().getImage(
		MonitorMainView.class.getResource("/img/icon.png")));
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setBounds(41, 31, 790, 494);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(new BorderLayout(0, 0));
	contentPane.add(getSplitVista(), BorderLayout.CENTER);
	setLocationRelativeTo(null);

	añadirEventos();

	iniciar();
    }

    private void iniciar() {
	MonitorDto monitor = msi.buscarMonitor(idMonitor);
	lbIdentificador
		.setText(monitor.getNombre() + " " + monitor.getApellidos());

    }

    private void añadirEventos() {
	btGestionarActividad.addActionListener(pP);
	btGenerarParte.addActionListener(pP);
    }

    private JSplitPane getSplitVista() {
	if (splitVista == null) {
	    splitVista = new JSplitPane();
	    splitVista.setLeftComponent(getPnMenu());
	    splitVista.setRightComponent(getPnOpciones());
	}
	return splitVista;
    }

    private JPanel getPnMenu() {
	if (pnMenu == null) {
	    pnMenu = new JPanel();
	    pnMenu.setPreferredSize(new Dimension(200, 600));
	    pnMenu.setLayout(new GridLayout(2, 1, 0, 10));
	    pnMenu.add(getPnInfo());
	    pnMenu.add(getPnBotones());
	}
	return pnMenu;
    }

    private JPanel getPnInfo() {
	if (pnInfo == null) {
	    pnInfo = new JPanel();
	    pnInfo.setLayout(new GridLayout(2, 1, 2, 2));
	    pnInfo.add(getLbLogo());
	    pnInfo.add(getPnDatosMonitor());
	}
	return pnInfo;
    }

    private JPanel getPnBotones() {
	if (pnBotones == null) {
	    pnBotones = new JPanel();
	    pnBotones.setBorder(new EmptyBorder(10, 5, 10, 5));
	    pnBotones.setLayout(new GridLayout(5, 1, 3, 5));
	    pnBotones.add(getBtGestionarActividad());
	    pnBotones.add(getBtGenerarParte());
	}
	return pnBotones;
    }

    private JLabel getLbLogo() {
	if (lbLogo == null) {
	    lbLogo = new JLabel("");
	    lbLogo.setBorder(new EmptyBorder(15, 0, 10, 0));
	    lbLogo.setHorizontalAlignment(SwingConstants.CENTER);
	    lbLogo.setIcon(new ImageIcon(MonitorMainView.class
		    .getResource("/img/image.png")));
	}
	return lbLogo;
    }

    private JPanel getPnDatosMonitor() {
	if (pnDatosMonitor == null) {
	    pnDatosMonitor = new JPanel();
	    pnDatosMonitor.setBorder(new EmptyBorder(50, 0, 0, 0));
	    pnDatosMonitor.setOpaque(false);
	    pnDatosMonitor.setRequestFocusEnabled(false);
	    pnDatosMonitor.setLayout(null);
	    pnDatosMonitor.add(getLbIdentificador());
	    pnDatosMonitor.add(getBtnCerrarSesin());
	}
	return pnDatosMonitor;
    }

    private JLabel getLbIdentificador() {
	if (lbIdentificador == null) {
	    lbIdentificador = new JLabel("Monitor");
	    lbIdentificador.setFont(new Font("Tahoma", Font.PLAIN, 16));
	    lbIdentificador.setBounds(14, 11, 176, 46);
	    lbIdentificador.setHorizontalAlignment(SwingConstants.CENTER);
	}
	return lbIdentificador;
    }

    private JButton getBtnCerrarSesin() {
	if (btnCerrarSesin == null) {
	    btnCerrarSesin = new JButton("Cerrar sesión");
	    btnCerrarSesin.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    main.setTitle("CENTRO DEPORTIVO");
		    main.card.show(main.getContentPane(), "login");
		    main.setVisible(true);
		    dispose();
		}
	    });
	    btnCerrarSesin.setFont(new Font("Tahoma", Font.PLAIN, 13));
	    btnCerrarSesin.setBounds(43, 68, 124, 23);
	}
	return btnCerrarSesin;
    }

    private JButton getBtGestionarActividad() {
	if (btGestionarActividad == null) {
	    btGestionarActividad = new JButton("Gestionar actividad");
	    btGestionarActividad.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    btGestionarActividad.setActionCommand("Gestionar");
	}
	return btGestionarActividad;
    }

    private JButton getBtGenerarParte() {
	if (btGenerarParte == null) {
	    btGenerarParte = new JButton("Generar parte de incidencias");
	    btGenerarParte.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    btGenerarParte.setActionCommand("Generar");
	}
	return btGenerarParte;
    }

    private JPanel getPnOpciones() {
	if (pnOpciones == null) {
	    pnOpciones = new JPanel();
	    pnOpciones.setLayout(new CardLayout(0, 0));
	    pnOpciones.add(getPnPrincipal(), "Principal");
	    pnOpciones.add(getPnGestionarActividad(), "Gestionar");
	    pnOpciones.add(getPnGenerarParte(), "Generar");
	}
	return pnOpciones;
    }

    private JPanel getPnGestionarActividad() {
	if (pnGestionarActividad == null) {
	    pnGestionarActividad = new JPanel();
	    pnGestionarActividad.setLayout(new BorderLayout(0, 0));
	    pnGestionarActividad.add(getPnTabla(), BorderLayout.CENTER);
	    pnGestionarActividad.add(getPnGestiones(), BorderLayout.SOUTH);
	    pnGestionarActividad.setVisible(false);
	}
	return pnGestionarActividad;
    }

    private JPanel getPnGenerarParte() {
	if (pnGenerarParte == null) {
	    pnGenerarParte = new JPanel();
	    pnGenerarParte.setLayout(new BorderLayout(0, 0));
	    pnGenerarParte.add(getPnTablaParte(), BorderLayout.CENTER);
	    pnGenerarParte.add(getPanel_1_1(), BorderLayout.SOUTH);
	    pnGenerarParte.setVisible(false);
	}
	return pnGenerarParte;
    }

    private JPanel getPnTabla() {
	if (pnTabla == null) {
	    pnTabla = new JPanel();
	    pnTabla.setLayout(new BorderLayout(0, 0));
	    pnTabla.add(getSpTablaGestionar(), BorderLayout.CENTER);
	}
	return pnTabla;
    }

    private JPanel getPnGestiones() {
	if (pnGestiones == null) {
	    pnGestiones = new JPanel();
	    pnGestiones.setLayout(new GridLayout(2, 1, 0, 0));
	    pnGestiones.add(getPnActividadElegida());
	    pnGestiones.add(getPanel_1());
	}
	return pnGestiones;
    }

    private JPanel getPnActividadElegida() {
	if (pnActividadElegida == null) {
	    pnActividadElegida = new JPanel();
	    pnActividadElegida
		    .setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	    pnActividadElegida.add(getLbActividadElegidaGestionar());
	    pnActividadElegida.add(getTxActividadElegidaGestionar());
	}
	return pnActividadElegida;
    }

    private JPanel getPanel_1() {
	if (pnAsistentes == null) {
	    pnAsistentes = new JPanel();
	    pnAsistentes.setLayout(new GridLayout(1, 3, 0, 0));
	    pnAsistentes.add(getBtnListaAsistentes());
	    pnAsistentes.add(getBtnPasarLista());
	    pnAsistentes.add(getBtnApuntarSocios());
	}
	return pnAsistentes;
    }

    private JLabel getLbActividadElegidaGestionar() {
	if (lbActividadElegidaGestionar == null) {
	    lbActividadElegidaGestionar = new JLabel("Actividad elegida:");
	    lbActividadElegidaGestionar
		    .setFont(new Font("Tahoma", Font.PLAIN, 19));
	}
	return lbActividadElegidaGestionar;
    }

    private JTextArea getTxActividadElegidaGestionar() {
	if (txActividadElegidaGestionar == null) {
	    txActividadElegidaGestionar = new JTextArea();
	    txActividadElegidaGestionar
		    .setFont(new Font("Nirmala UI", Font.PLAIN, 20));
	    txActividadElegidaGestionar.setEditable(false);
	}
	return txActividadElegidaGestionar;
    }

    private JScrollPane getSpTablaGestionar() {
	if (spTablaGestionar == null) {
	    spTablaGestionar = new JScrollPane();
	    spTablaGestionar.setViewportView(getTbActividadesGestionar());
	}
	return spTablaGestionar;
    }

    private JTable getTbActividadesGestionar() {
	if (tbActividadesGestionar == null) {
	    String[] columnas = { "Id", "Nombre", "Hora inicio", "Hora fin",
		    "Número de plazas", "Plazas libres" };
	    modeloTablaGestionar = new NonEditableModel(columnas, 0);
	    crearFilasTabla();
	    tbActividadesGestionar = new JTable(modeloTablaGestionar);
	    tbActividadesGestionar.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    if (e.getClickCount() == 1) {
			añadirActividadGestionar();
		    }
		}
	    });
	    tbActividadesGestionar.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    tbActividadesGestionar.getTableHeader()
		    .setFont(new Font("Tahoma", Font.BOLD, 16));
	    centrarValoresTabla(tbActividadesGestionar);
	}
	return tbActividadesGestionar;
    }

    private void añadirActividadGestionar() {
	int fila = tbActividadesGestionar.getSelectedRow();
	if (fila != -1) {
	    // Sacamos el nombre de la actividad
	    String actividad = (String) tbActividadesGestionar.getValueAt(fila,
		    1);
	    txActividadElegidaGestionar.setText(actividad);

	    btnListaAsistentes.setEnabled(true);
	    // Si la actividad tiene limite de plazas se activa el boton de
	    // pasar lista
	    if (!tbActividadesGestionar.getValueAt(fila, 4).toString()
		    .equalsIgnoreCase("-")) {
		btnPasarLista.setEnabled(true);
		// Si en la actividad hay plazas libres o no hay limite de
		// plazas se activa el boton de apuntar a socios
		if (!tbActividadesGestionar.getValueAt(fila, 5).toString()
			.equalsIgnoreCase("0"))
		    btnApuntarSocios.setEnabled(true);
	    } else {
		btnPasarLista.setEnabled(false);
		btnApuntarSocios.setEnabled(false);
	    }
	}
    }

    private void centrarValoresTabla(JTable tbActividades) {
	DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
	tcr.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
	for (int i = 0; i < tbActividades.getColumnModel()
		.getColumnCount(); i++) {
	    tbActividades.getColumnModel().getColumn(i).setCellRenderer(tcr);
	}
    }

    private void crearFilasTabla() {
	Object[] nuevaFila = new Object[6];
	List<PlanificacionActividadDto> actividades = msi
		.listActivitiesByMonitor(idMonitor);
	for (int i = 0; i < actividades.size(); i++) {
	    // Si la actividad es en una hora o menos la aniade
	    if (msi.isListActivityAvailable(actividades.get(i).getFecha(),
		    actividades.get(i).getHoraInicio())) {
		int idPActividad = Integer
			.parseInt(actividades.get(i).getId().toString());
		nuevaFila[0] = idPActividad;
		nuevaFila[1] = actividades.get(i).getNombre();
		nuevaFila[2] = actividades.get(i).getHoraInicio();
		nuevaFila[3] = actividades.get(i).getHoraFin();

		int numeroPersonasApuntadas = msi
			.getCantidadPersonasApuntadas(idPActividad);

		if (actividades.get(i).isLimitePlazas()) {
		    nuevaFila[4] = actividades.get(i).getNumLimitePlazas();
		    nuevaFila[5] = actividades.get(i).getNumLimitePlazas()
			    - numeroPersonasApuntadas;
		} else {
		    nuevaFila[4] = "-";
		    nuevaFila[5] = "-";
		}

		modeloTablaGestionar.addRow(nuevaFila);
	    }
	}
    }

    private JButton getBtnListaAsistentes() {
	if (btnListaAsistentes == null) {
	    btnListaAsistentes = new JButton("Lista de asistentes");
	    btnListaAsistentes.setEnabled(false);
	    btnListaAsistentes.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    listaAsistentes();
		}
	    });
	    btnListaAsistentes.setFont(new Font("Tahoma", Font.PLAIN, 14));
	}
	return btnListaAsistentes;
    }

    private void listaAsistentes() {
	int fila = tbActividadesGestionar.getSelectedRow();
	if (fila != -1) {
	    // Cogemos id de la actividad
	    int idPActividad = Integer.parseInt(
		    tbActividadesGestionar.getValueAt(fila, 0).toString());
	    // Sacamos los socios asociados a esa actividad
	    List<SocioDto> socios = msi.listSociosByActivity(idPActividad);
	    // Los enseniamos en una ventana aparte
	    new SociosListActivityView(this, socios,
		    tbActividadesGestionar.getValueAt(fila, 1).toString());

	}
    }

    private JButton getBtnPasarLista() {
	if (btnPasarLista == null) {
	    btnPasarLista = new JButton("Pasar lista");
	    btnPasarLista.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    pasarLista();
		}
	    });
	    btnPasarLista.setEnabled(false);
	    btnPasarLista.setFont(new Font("Tahoma", Font.PLAIN, 14));
	}
	return btnPasarLista;
    }

    private void pasarLista() {
	int fila = tbActividadesGestionar.getSelectedRow();

	// El monitor solo puede apuntar socios en actividades con limite de
	// plazas
	if (fila != -1) {
	    // Cogemos id de la actividad
	    int idPActividad = Integer.parseInt(
		    tbActividadesGestionar.getValueAt(fila, 0).toString());
	    // Sacamos los socios asociados a esa actividad
	    List<SocioDto> socios = msi.listSociosByActivity(idPActividad);
	    // Los enseniamos en una ventana aparte
	    new SociosAsistenciaActivityView(socios, idPActividad, this);
	}
    }

    private JButton getBtnApuntarSocios() {
	if (btnApuntarSocios == null) {
	    btnApuntarSocios = new JButton("Apuntar socios");
	    btnApuntarSocios.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    apuntarSocios();
		}
	    });
	    btnApuntarSocios.setEnabled(false);
	    btnApuntarSocios.setFont(new Font("Tahoma", Font.PLAIN, 14));
	}
	return btnApuntarSocios;
    }

    private void apuntarSocios() {
	int fila = tbActividadesGestionar.getSelectedRow();
	if (fila != -1) {
	    // Cogemos id de la actividad
	    int idPActividad = Integer.parseInt(
		    tbActividadesGestionar.getValueAt(fila, 0).toString());

	    // ACTUALIZACIÓN: Cogemos hora de inicio de la actividad
	    String horaInicio = tbActividadesGestionar.getValueAt(fila, 2)
		    .toString();

	    // Mostramos los socios asignados a esa actividad y damos la opcion
	    // de añadir mas
	    int plazasRestantes = Integer.parseInt(
		    tbActividadesGestionar.getValueAt(fila, 5).toString());
	    new SociosRegistrarActivityView(this, idPActividad, plazasRestantes,
		    horaInicio);
	}
    }

    private JPanel getPnPrincipal() {
	if (pnPrincipal == null) {
	    pnPrincipal = new JPanel();
	    pnPrincipal.setVisible(true);
	}
	return pnPrincipal;
    }

    protected void actualizarPlazasLibres() {
	int fila = tbActividadesGestionar.getSelectedRow();
	int idPActividad = Integer.parseInt(
		tbActividadesGestionar.getValueAt(fila, 0).toString());
	int plazasActividad = Integer.parseInt(
		tbActividadesGestionar.getValueAt(fila, 4).toString());
	int numeroPersonasApuntadas = msi
		.getCantidadPersonasApuntadas(idPActividad);
	tbActividadesGestionar
		.setValueAt(plazasActividad - numeroPersonasApuntadas, fila, 5);
	if (!tbActividadesGestionar.getValueAt(fila, 5).toString()
		.equalsIgnoreCase("0"))
	    btnApuntarSocios.setEnabled(true);
	else
	    btnApuntarSocios.setEnabled(false);
    }

    class ProcesaPaneles implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
	    JButton boton = (JButton) e.getSource();
	    CardLayout cl = (CardLayout) (pnOpciones.getLayout());
	    cl.show(pnOpciones, boton.getActionCommand());
	}
    }

    private JPanel getPnTablaParte() {
	if (pnTablaParte == null) {
	    pnTablaParte = new JPanel();
	    pnTablaParte.setLayout(new BorderLayout(0, 0));
	    pnTablaParte.add(getSpTablaGenerar(), BorderLayout.CENTER);
	}
	return pnTablaParte;
    }

    private JPanel getPanel_1_1() {
	if (pnActividadParte == null) {
	    pnActividadParte = new JPanel();
	    pnActividadParte.setLayout(new GridLayout(2, 1, 0, 0));
	    pnActividadParte.add(getPnActividadElegidaParte());
	    pnActividadParte.add(getPnCrearParte());
	}
	return pnActividadParte;
    }

    private JPanel getPnActividadElegidaParte() {
	if (pnActividadElegidaParte == null) {
	    pnActividadElegidaParte = new JPanel();
	    pnActividadElegidaParte
		    .setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	    pnActividadElegidaParte.add(getLbActividadElegidaGenerar());
	    pnActividadElegidaParte.add(getTxActividadElegidaGenerar());
	}
	return pnActividadElegidaParte;
    }

    private JLabel getLbActividadElegidaGenerar() {
	if (lbActividadElegidaGenerar == null) {
	    lbActividadElegidaGenerar = new JLabel("Actividad elegida:");
	    lbActividadElegidaGenerar
		    .setFont(new Font("Tahoma", Font.PLAIN, 19));
	}
	return lbActividadElegidaGenerar;
    }

    private JTextArea getTxActividadElegidaGenerar() {
	if (txActividadElegidaGenerar == null) {
	    txActividadElegidaGenerar = new JTextArea();
	    txActividadElegidaGenerar
		    .setFont(new Font("Nirmala UI", Font.PLAIN, 20));
	    txActividadElegidaGenerar.setEditable(false);
	}
	return txActividadElegidaGenerar;
    }

    private JPanel getPnCrearParte() {
	if (pnCrearParte == null) {
	    pnCrearParte = new JPanel();
	    pnCrearParte.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	    pnCrearParte.add(getBtnCrearParte());
	}
	return pnCrearParte;
    }

    private JButton getBtnCrearParte() {
	if (btnCrearParte == null) {
	    btnCrearParte = new JButton("Crear parte");
	    btnCrearParte.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    crearParte();
		}
	    });
	    btnCrearParte.setEnabled(false);
	    btnCrearParte.setFont(new Font("Tahoma", Font.PLAIN, 14));
	}
	return btnCrearParte;
    }

    private void crearParte() {
	int fila = tbActividadesGenerar.getSelectedRow();
	if (fila != -1) {
	    // Cogemos id de la actividad
	    Long idPlanificacionActividad = Long.parseLong(
		    tbActividadesGenerar.getValueAt(fila, 0).toString());
	    PlanificacionActividadDto p = msi
		    .findPlanificacionActivityById(idPlanificacionActividad);
	    // Gestionamos ventana para la creación del parte
	    new ActividadIncidenciasActivityView(p, this);

	}
    }

    private JScrollPane getSpTablaGenerar() {
	if (spTablaGenerar == null) {
	    spTablaGenerar = new JScrollPane();
	    spTablaGenerar.setViewportView(getTbActividadesGenerar());
	}
	return spTablaGenerar;
    }

    private JTable getTbActividadesGenerar() {
	if (tbActividadesGenerar == null) {
	    String[] nombreColumnas = { "Id actividad", "Nombre", "Fecha",
		    "Hora inicio", "Hora fin" };
	    modeloTablaGenerar = new NonEditableModel(nombreColumnas, 0);
	    aniadirFilas();
	    tbActividadesGenerar = new JTable(modeloTablaGenerar);
	    tbActividadesGenerar.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    tbActividadesGenerar.getTableHeader()
		    .setFont(new Font("Tahoma", Font.BOLD, 16));
	    centrarValoresTabla(tbActividadesGenerar);
	    tbActividadesGenerar.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    if (e.getClickCount() == 1) {
			gestionarActividad();
		    }
		}
	    });
	}
	return tbActividadesGenerar;
    }

    /**
     * Añade filas a la tabla de generar
     */
    private void aniadirFilas() {
	Object[] nuevaFila = new Object[5];
	List<PlanificacionActividadDto> actividades = msi
		.listFinishedActivitiesByMonitor(idMonitor);
	/*
	 * Aquí no hace falta comprobar que la actividad ya esté finalizada, ya
	 * se hace en la capa de lógica y solo se eligen aquellas finalizadas
	 * que no tienen generado el parte de incidencias
	 */
	for (int i = 0; i < actividades.size(); i++) {
	    nuevaFila[0] = actividades.get(i).getId();
	    nuevaFila[1] = actividades.get(i).getNombre();
	    nuevaFila[2] = actividades.get(i).getFecha();
	    nuevaFila[3] = actividades.get(i).getHoraInicio();
	    nuevaFila[4] = actividades.get(i).getHoraFin();

	    modeloTablaGenerar.addRow(nuevaFila);
	}
    }

    private void gestionarActividad() {
	int fila = tbActividadesGenerar.getSelectedRow();
	if (fila != -1) {
	    // Sacamos el nombre de la actividad
	    String actividad = (String) tbActividadesGenerar.getValueAt(fila,
		    1);
	    txActividadElegidaGenerar.setText(actividad);

	    btnCrearParte.setEnabled(true);
	}
    }

    public void actualizarActividadesSinParte() {
	modeloTablaGenerar.setRowCount(0);
	aniadirFilas();
	modeloTablaGenerar.fireTableDataChanged();

    }
}
