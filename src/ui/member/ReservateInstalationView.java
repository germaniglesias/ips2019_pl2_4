package ui.member;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import controller.member.ReservateInstalationController;
import controller.member.ReservatePlaceController;
import dto.InstalacionSocioReservaDto;
import dto.PlanificacionActividadDto;
import dto.SocioDto;
import dto.SocioReservaInstalacionDto;
import ui.Main;
import utils.NonEditableModel;

public class ReservateInstalationView extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = -4403173706021662442L;
    private final JPanel contentPanel = new JPanel();
    private JLabel lblDni;
    private JTextField textDNI;
    private JLabel lblNombre;
    private JTextField textNombre;
    private JLabel lblApellidos;
    private JTextField textApellidos;
    private JButton btnConfirmarInscripcion;
    private JButton btnCancelarTramite;
    private JButton button;
    private NonEditableModel modeloTabla;

    @SuppressWarnings("unused")
    private Date hoy = new Date();
    Calendar calendario = Calendar.getInstance();
    private JScrollPane scrollPane;
    private JTable table;
    private JLabel lblId;
    private JTextField textFieldid;
    private JComboBox comboBoxInstalaciones;
    private JLabel lblIdInstalacion;
    private JTextField textFieldInstalacionID;
    private JButton button_1;
    private JLabel lblInstalaciones;
    private JPanel panel;
    private JComboBox Dia;
    private JComboBox Mes;
    private JComboBox Año;
    private JLabel label;
    private JSpinner spinnerInicio;
    private JLabel label_2;

    private Main mainView;

    /**
     * Create the dialog.
     * 
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ReservateInstalationView(Main mainView)
	    throws ClassNotFoundException, SQLException {

	this.setMainView(mainView);

	setModal(true);

	setTitle("Reserva de Instalaciones");
	setBounds(100, 100, 846, 606);
	setLocationRelativeTo(mainView);
	getContentPane().setLayout(new BorderLayout());

	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(null);
	contentPanel.add(getLblDni());
	contentPanel.add(getTextDNI());
	contentPanel.add(getLblNombre());
	contentPanel.add(getTextNombre());
	contentPanel.add(getLabel_1());
	contentPanel.add(getTextApellidos());
	contentPanel.add(getBtnConfirmarInscripcion());
	contentPanel.add(getBtnCancelarTramite());
	contentPanel.add(getButton());
	contentPanel.add(getScrollPane());
	contentPanel.add(getLblId());
	contentPanel.add(getTextFieldid());
	contentPanel.add(getComboBoxInstalaciones());
	contentPanel.add(getLblIdInstalacion());
	contentPanel.add(getTextFieldInstalacionID());
	contentPanel.add(getButton_1());
	contentPanel.add(getLblInstalaciones());
	contentPanel.add(getPanel());
    }

    private JLabel getLblDni() {
	if (lblDni == null) {
	    lblDni = new JLabel("DNI:");
	    lblDni.setBounds(189, 17, 38, 16);
	}
	return lblDni;
    }

    private JTextField getTextDNI() {
	if (textDNI == null) {
	    textDNI = new JTextField();
	    textDNI.setHorizontalAlignment(SwingConstants.CENTER);
	    textDNI.setBounds(237, 14, 226, 22);
	    textDNI.setColumns(10);
	}
	return textDNI;
    }

    private JLabel getLblNombre() {
	if (lblNombre == null) {
	    lblNombre = new JLabel("Nombre: ");
	    lblNombre.setBounds(10, 65, 56, 16);
	}
	return lblNombre;
    }

    private JTextField getTextNombre() {
	if (textNombre == null) {
	    textNombre = new JTextField();
	    textNombre.setEditable(false);
	    textNombre.setBounds(68, 62, 193, 22);
	    textNombre.setColumns(10);
	}
	return textNombre;
    }

    private JLabel getLabel_1() {
	if (lblApellidos == null) {
	    lblApellidos = new JLabel("Apellidos: ");
	    lblApellidos.setBounds(281, 65, 75, 16);
	}
	return lblApellidos;
    }

    private JTextField getTextApellidos() {
	if (textApellidos == null) {
	    textApellidos = new JTextField();
	    textApellidos.setEditable(false);
	    textApellidos.setBounds(356, 62, 183, 22);
	    textApellidos.setColumns(10);
	}
	return textApellidos;
    }

    private JButton getBtnConfirmarInscripcion() {
	if (btnConfirmarInscripcion == null) {
	    btnConfirmarInscripcion = new JButton("Confirmar Inscripcion");
	    btnConfirmarInscripcion.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {

		    String IDsocio = textFieldid.getText();
		    String IDinstalacion = textFieldInstalacionID.getText();
		    String fecha = Año.getSelectedItem().toString() + "-"
			    + Mes.getSelectedItem().toString() + "-" + metodo();
		    String hi = spinnerInicio.getValue().toString() + ":00:00";
		    Date fechaActual = new Date();
		    SimpleDateFormat formateadorFecha = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    String fechaSistema = formateadorFecha.format(fechaActual);

		    try {
			if (!comprobarFecha(fecha, fechaSistema)) {
			    return;
			}

		    } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    }

		    // Comprobamos que la reserva no colisione con nada
		    if (comprobarColisiones(hi, fecha, Long.parseLong(IDsocio)).equalsIgnoreCase("")) {
			new ReservateInstalationController()
				.reservarInstalacion(hi, fecha, IDinstalacion,
					IDsocio);
			JOptionPane.showMessageDialog(null,
				"Se ha registrado la instalacion correctamente");
			limpiaTabla();
		    }
		}

		private String metodo() {
		    String o = "1";
		    String o1 = "2";
		    String o2 = "3";
		    String o3 = "4";
		    String o4 = "5";
		    String o5 = "6";
		    String o6 = "7";
		    String o7 = "8";
		    String o8 = "9";

		    if (Dia.getSelectedItem().toString() == o
			    || Dia.getSelectedItem().toString() == o1
			    || Dia.getSelectedItem().toString() == o2
			    || Dia.getSelectedItem().toString() == o3
			    || Dia.getSelectedItem().toString() == o4
			    || Dia.getSelectedItem().toString() == o5
			    || Dia.getSelectedItem().toString() == o6
			    || Dia.getSelectedItem().toString() == o7
			    || Dia.getSelectedItem().toString() == o8) {
			return "0" + Dia.getSelectedItem().toString();

		    }
		    return Dia.getSelectedItem().toString();

		}
	    });
	    btnConfirmarInscripcion.setBounds(189, 531, 177, 25);
	}
	return btnConfirmarInscripcion;
    }
    
    /**
     * Comprueba las colisiones posibles al reservar una instalación
     * Puede solisionar con una reserva de instalación o con una actividad a la que el socio
     * ya se haya apuntado
     * @param iDsocio 
     * @param iDinstalacion 
     * @param fecha 
     * @param horaInicio 
     * @return null si no hay ninguna colisión
     */
    private String comprobarColisiones(String horaInicio, String fecha, Long idSocio) {
	ReservateInstalationController ric = new ReservateInstalationController();
	ReservatePlaceController rpc = new ReservatePlaceController();
	
	SocioReservaInstalacionDto reservaInstalacionColisiona = ric.comprobarColisionReserva(idSocio, fecha, horaInicio);
	PlanificacionActividadDto reservaActividadColisiona = rpc.comprobarColisionPlaza(idSocio, fecha, horaInicio);
	
	// No hay colisiones
	if (reservaInstalacionColisiona == null && reservaActividadColisiona == null)
	    return "";
	// Hay colisiones, las enseñamos en una ventana aparte
	else {
	    new ColisionReservaInstalacionView(this, reservaInstalacionColisiona, reservaActividadColisiona);
	    return "Colision";
	}
    }

    @SuppressWarnings("deprecation")
    private boolean comprobarFecha(String fecha, String fechaActual)
	    throws ClassNotFoundException, SQLException, ParseException {
	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	int an1 = fechaDate1.getYear();
	int an2 = fechaDate2.getYear();
	int mes1 = fechaDate1.getMonth();
	int mes2 = fechaDate2.getMonth();
	int date1 = fechaDate1.getDate();
	int date2 = fechaDate2.getDate();

	boolean resultado = (an1 - an2 == 0)
		&& (mes1 - mes2 == 0 || mes1 - mes2 == 1)
		&& ((date1 - date2 >= 1 && date1 - date2 <= 15)
			|| (date2 - date1 <= 30 && date2 - date1 >= 15));
	if (!resultado) {
	    JOptionPane.showMessageDialog(null,
		    "No se permite reservar la instalacion ya que las reservas se pueden hacer desde 15 días antes hasta 1 día antes de la fecha de la reserva.",
		    "No se permite hacer la reserva",
		    JOptionPane.ERROR_MESSAGE);
	    return false;
	}
	return true;

    }

    void limpiaTabla() {
	try {
	    NonEditableModel temp = (NonEditableModel) table.getModel();
	    int a = temp.getRowCount();
	    for (int i = 0; i < a; i++)
		temp.removeRow(0);
	} catch (Exception e) {
	    System.out.println(e);
	}
    }

    private JButton getBtnCancelarTramite() {
	if (btnCancelarTramite == null) {
	    btnCancelarTramite = new JButton("Salir");
	    btnCancelarTramite.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    dispose();
		    mainView.setVisible(true);
		}
	    });
	    btnCancelarTramite.setBounds(466, 531, 186, 25);
	}
	return btnCancelarTramite;
    }

    private JButton getButton() {
	if (button == null) {
	    button = new JButton("");

	    button.setBorder(null);
	    button.setIcon(new ImageIcon(
		    CancelReservationView.class.getResource("/img/lupab.jpg")));
	    button.setBounds(473, 11, 46, 42);

	    button.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent arg0) {

		    List<SocioDto> socios = new ReservatePlaceController()
			    .listarSocios();
		    for (SocioDto s : socios) {

			if (s.getDni().equals(textDNI.getText().toString())) {
			    textNombre.setText(s.getNombre());
			    textApellidos.setText(s.getApellidos());
			    textFieldid.setText(String.valueOf(s.getId()));
			}
		    }

		}

	    });
	}
	return button;
    }

    public boolean comparaFecha(String fecha, String fechaActual)
	    throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	boolean resultado = fechaDate1.compareTo(fechaDate2) > 0;

	return resultado;

    }

    public boolean comparaHorario(String hora, String horaaActual, String fecha,
	    String fechaActual) throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat formateador1 = new SimpleDateFormat("yyyy-MM-dd");
	Date horaDate1 = formateador.parse(hora);
	Date horaaDate2 = formateador.parse(horaaActual);
	Date fechaDate1 = formateador1.parse(fecha);
	Date fechaDate2 = formateador1.parse(fechaActual);
	boolean resultado = horaDate1.compareTo(horaaDate2) >= 0
		&& fechaDate1.compareTo(fechaDate2) == 0;
	return resultado;

    }

    public boolean comprobarDNIBase(String dni)
	    throws ClassNotFoundException, SQLException {
	List<SocioDto> socios = new ReservatePlaceController().listarSocios();
	for (SocioDto s : socios) {
	    if (s.getDni().equals(dni)) {
		table.setEnabled(true);
		return true;

	    }
	}
	return false;
    }

//    private boolean comprobarSocio(String idSocio, String idInstalacion,
//	    String fecha, String hora)
//	    throws ClassNotFoundException, SQLException {
//
//	List<InstalacionSocioReservaDto> matriculados = new ReservateInstalationController()
//		.listarMatriculados();
//	for (InstalacionSocioReservaDto matriculadoEn : matriculados) {
//
//	    if (matriculadoEn.dni_Socio.equalsIgnoreCase(dni) && matriculadoEn.id_Socio == idSocio
//		    && matriculadoEn.id_Instalacion == idInstalacion
//		    && matriculadoEn.fecha == fecha
//		    && matriculadoEn.horaInicio == hora) {
//		JOptionPane.showMessageDialog(null,
//			"No se permite inscribir en la actividad debido a que el socio ya esta inscrito en ella",
//			"No se permite hacer la reserva",
//			JOptionPane.ERROR_MESSAGE);
//		return true;
//	    }
//	}
//	return false;
//    }

//    private boolean comprobarSocio(String idSocio, String hora)
//	    throws ClassNotFoundException, SQLException {
//
//	List<InstalacionSocioReservaDto> matriculados = new ReservateInstalationController()
//		.listarMatriculados();
//	for (InstalacionSocioReservaDto matriculadoEn : matriculados) {
//
//	    if (matriculadoEn.id_Socio.equals(idSocio)
//
//		    && matriculadoEn.horaInicio.equals(hora)) {
//		JOptionPane.showMessageDialog(null,
//			"No se permite inscribir en la actividad debido a que el socio ya esta inscrito en ella",
//			"No se permite hacer la reserva",
//			JOptionPane.ERROR_MESSAGE);
//		return true;
//	    }
//	}
//	return false;
//    }

    private JScrollPane getScrollPane() {
	if (scrollPane == null) {
	    scrollPane = new JScrollPane();
	    scrollPane.setBounds(37, 148, 723, 216);
	    scrollPane.setViewportView(getTable());
	    scrollPane.setBorder(new TitledBorder(
		    UIManager.getBorder("TitledBorder.border"),
		    "Lista de reservas segun instalacion:",
		    TitledBorder.LEADING, TitledBorder.TOP, null, null));
	}
	return scrollPane;
    }

    private JTable getTable() {
	if (table == null) {
	    table = new JTable();
	    String[] nombreColumnas = { "ID_reserva", "nombre_Instalacion",
		    "dni_Socio", "Fecha", "HInicio", "Precio" };
	    modeloTabla = new NonEditableModel(nombreColumnas, 0);
	    table = new JTable(modeloTabla);
	    table.getColumnModel().getColumn(2).setPreferredWidth(200);
	    table.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
		    if (table.getRowCount() > 0) {

			btnConfirmarInscripcion.setEnabled(true);
		    }
		}
	    });
	}
	return table;
    }

    private JLabel getLblId() {
	if (lblId == null) {
	    lblId = new JLabel("Id:");
	    lblId.setBounds(559, 66, 38, 15);
	}
	return lblId;
    }

    private JTextField getTextFieldid() {
	if (textFieldid == null) {
	    textFieldid = new JTextField();
	    textFieldid.setEditable(false);
	    textFieldid.setBounds(597, 63, 86, 20);
	    textFieldid.setColumns(10);
	}
	return textFieldid;
    }

    private JComboBox getComboBoxInstalaciones() {
	final List<InstalacionSocioReservaDto> instalaciones;
	if (comboBoxInstalaciones == null) {
	    comboBoxInstalaciones = new JComboBox();
	    comboBoxInstalaciones.setBounds(153, 102, 226, 20);
	}
	instalaciones = new ReservateInstalationController()
		.listarInstalaciones();
	comboBoxInstalaciones.addItem("----------");
	for (InstalacionSocioReservaDto is : instalaciones) {
	    comboBoxInstalaciones.addItem(is.nombre_Instalacion);

	}
	comboBoxInstalaciones.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		textFieldInstalacionID.setText(String.valueOf(getIdInstalacion(
			instalaciones,
			comboBoxInstalaciones.getSelectedItem().toString())));
	    }
	});
	return comboBoxInstalaciones;
    }

    public String getIdInstalacion(List<InstalacionSocioReservaDto> list,
	    String nombre) {
	for (InstalacionSocioReservaDto ins : list) {
	    if (ins.nombre_Instalacion.equals(nombre)) {
		return ins.id_Instalacion;
	    }
	}
	return null;
    }

    private JLabel getLblIdInstalacion() {
	if (lblIdInstalacion == null) {
	    lblIdInstalacion = new JLabel("ID Instalacion:");
	    lblIdInstalacion.setBounds(435, 105, 150, 14);
	}
	return lblIdInstalacion;
    }

    private JTextField getTextFieldInstalacionID() {
	if (textFieldInstalacionID == null) {
	    textFieldInstalacionID = new JTextField();
	    textFieldInstalacionID.setEditable(false);
	    textFieldInstalacionID.setColumns(10);
	    textFieldInstalacionID.setBounds(590, 102, 50, 20);
	}
	return textFieldInstalacionID;
    }

    private JButton getButton_1() {
	if (button_1 == null) {
	    button_1 = new JButton("");
	    button_1.setBorder(null);
	    button_1.setIcon(new ImageIcon(ReservateInstalationView.class
		    .getResource("/img/lupab.jpg")));
	    button_1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    limpiaTabla();

		    Date fechaActual = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    SimpleDateFormat formateadorHora = new SimpleDateFormat(
			    "HH:mm:ss");
		    String fechaSistema = formateador.format(fechaActual);
		    String HoraSistema = formateadorHora.format(fechaActual);
		    String campo = textFieldInstalacionID.getText().toString();
		    List<InstalacionSocioReservaDto> reservas;
		    reservas = new ReservateInstalationController()
			    .listarReservas();
		    for (InstalacionSocioReservaDto r : reservas) {
			if (r.id_Instalacion.equalsIgnoreCase(campo)) {
			    try {
				if (comparaFecha(r.fecha, fechaSistema)
					|| comparaHorario(r.horaInicio,
						HoraSistema, r.fecha,
						fechaSistema)) {
				    Object[] nuevaLinea = new Object[6];
				    nuevaLinea[0] = r.id_Reserva;
				    nuevaLinea[1] = r.nombre_Instalacion;
				    nuevaLinea[2] = r.dni_Socio;
				    nuevaLinea[3] = r.fecha;
				    nuevaLinea[4] = r.horaInicio;
				    nuevaLinea[5] = r.precio;

				    modeloTabla.addRow(nuevaLinea);
				}
			    } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			    }
			}
		    }
		}

	    });
	    button_1.setBorder(null);
	    button_1.setBounds(665, 94, 45, 42);
	}
	return button_1;

    }

    private JLabel getLblInstalaciones() {
	if (lblInstalaciones == null) {
	    lblInstalaciones = new JLabel("Instalaciones:");
	    lblInstalaciones.setBounds(22, 104, 121, 16);
	}
	return lblInstalaciones;
    }

    private JPanel getPanel() {
	if (panel == null) {
	    panel = new JPanel();
	    panel.setLayout(null);
	    panel.setBorder(new TitledBorder(
		    UIManager.getBorder("TitledBorder.border"),

		    "Seleccione fecha de reserva:", TitledBorder.LEADING,

		    TitledBorder.TOP, null, null));
	    panel.setBounds(185, 389, 467, 106);
	    panel.add(getDia());
	    panel.add(getMes());
	    panel.add(getAño());
	    panel.add(getLabel());
	    panel.add(getSpinnerInicio());
	    panel.add(getLabel_2());
	}
	return panel;
    }

    private JComboBox getDia() {
	if (Dia == null) {
	    Dia = new JComboBox();
	    Dia.setEnabled(false);
	    Dia.setBounds(10, 57, 54, 22);
	    String[] Mes31 = new String[] { "1", "2", "3", "4", "5", "6", "7",
		    "8", "9", "10", "11", "12", "13", "14", "15", "16", "17",
		    "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
		    "28", "29", "30", "31" };
	    Dia.setModel(new DefaultComboBoxModel(Mes31));
	}
	return Dia;
    }

    private JComboBox getMes() {
	if (Mes == null) {
	    Mes = new JComboBox();
	    Mes.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3",
		    "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
	    Mes.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    Dia.setEnabled(true);
		    String[] febrero = new String[] { "1", "2", "3", "4", "5",
			    "6", "7", "8", "9", "10", "11", "12", "13", "14",
			    "15", "16", "17", "18", "19", "20", "21", "22",
			    "23", "24", "25", "26", "27", "28" };
		    String[] Mes30 = new String[] { "1", "2", "3", "4", "5",
			    "6", "7", "8", "9", "10", "11", "12", "13", "14",
			    "15", "16", "17", "18", "19", "20", "21", "22",
			    "23", "24", "25", "26", "27", "28", "29", "30" };
		    String[] Mes31 = new String[] { "1", "2", "3", "4", "5",
			    "6", "7", "8", "9", "10", "11", "12", "13", "14",
			    "15", "16", "17", "18", "19", "20", "21", "22",
			    "23", "24", "25", "26", "27", "28", "29", "30",
			    "31" };

		    if (Mes.getSelectedItem().toString().equals("2")) {
			Dia.setModel(new DefaultComboBoxModel(febrero));
		    } else if (Mes.getSelectedItem().toString().equals("1")
			    || Mes.getSelectedItem().toString().equals("3")
			    || Mes.getSelectedItem().toString().equals("5")
			    || Mes.getSelectedItem().toString().equals("7")
			    || Mes.getSelectedItem().toString().equals("8")
			    || Mes.getSelectedItem().toString().equals("10")
			    || Mes.getSelectedItem().toString().equals("12")) {
			Dia.setModel(new DefaultComboBoxModel(Mes31));
		    } else {
			Dia.setModel(new DefaultComboBoxModel(Mes30));
		    }
		}
	    });
	    Mes.setBounds(76, 57, 68, 22);
	}
	return Mes;
    }

    private JComboBox getAño() {
	if (Año == null) {
	    Año = new JComboBox();
	    String[] años = new String[86];

	    for (int i = 19; i < 99; i++) {
		años[i - 19] = "20" + i;
	    }
	    Año.setModel(new DefaultComboBoxModel(años));
	    Año.setBounds(156, 57, 86, 22);
	}
	return Año;
    }

    private JLabel getLabel() {
	if (label == null) {
	    label = new JLabel("Hora Inicio:");
	    label.setBounds(263, 60, 68, 16);
	}
	return label;
    }

    private JSpinner getSpinnerInicio() {
	if (spinnerInicio == null) {
	    spinnerInicio = new JSpinner();
	    spinnerInicio.setModel(new SpinnerNumberModel(8, 1, 23, 1));
	    spinnerInicio.setBounds(329, 57, 68, 22);
	}
	return spinnerInicio;
    }

    private JLabel getLabel_2() {
	if (label_2 == null) {
	    label_2 = new JLabel(":00h");
	    label_2.setBounds(407, 60, 56, 16);
	}
	return label_2;
    }

    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
	mainView.setVisible(true);
    }

    public Main getMainView() {
	return mainView;
    }

    public void setMainView(Main mainView) {
	this.mainView = mainView;
    }
}