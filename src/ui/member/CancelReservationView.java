package ui.member;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import controller.member.CancelReservationController;
import controller.member.ReservatePlaceController;
import dto.ActividadSocioReservaDto;
import dto.SocioDto;
import ui.Main;
import utils.NonEditableModel;

public class CancelReservationView extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JScrollPane scrollPane;
    private JTable table;
    private JButton btnConfirmarCancelacion;
    private JButton btnCancelarProceso;
    private NonEditableModel modeloTabla;
    private JButton button;
    private JTextField textDNI;

    final long MILLSECS_PER_DAY = 60 * 60 * 1000;
    Calendar calendario = Calendar.getInstance();
    private JLabel lblNombre;
    private JTextField textFieldNombre;
    private JLabel lblApellidos;
    private JTextField textFieldApellidos;
    private JLabel lblId;
    private JTextField textFieldId;
    
    private Main mainView;

    /**
     * Create the dialog.
     * @param mainView 
     */
    public CancelReservationView(Main mainView) {
	this.setMainView(mainView);
	
	setModal(true);
	setTitle("Anular inscripcion de socio a actividad");
	calendario = new GregorianCalendar();
	setBounds(100, 100, 838, 543);
	setLocationRelativeTo(mainView);
	
	getContentPane().setLayout(new BorderLayout());
	
	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(null);
	{
	    JLabel lblDniDelCliente = new JLabel("DNI del Cliente:");
	    lblDniDelCliente.setBounds(185, 39, 102, 16);
	    contentPanel.add(lblDniDelCliente);
	}
	contentPanel.add(getScrollPane());
	contentPanel.add(getBtnConfirmarCancelacion());
	contentPanel.add(getBtnCancelarProceso());
	contentPanel.add(getButton());
	contentPanel.add(getTextDNI());
	contentPanel.add(getLblNombre());
	contentPanel.add(getTextFieldNombre());
	contentPanel.add(getLblApellidos());
	contentPanel.add(getTextFieldApellidos());
	contentPanel.add(getLblId());
	contentPanel.add(getTextFieldId());
    }

    private JScrollPane getScrollPane() {
	if (scrollPane == null) {
	    scrollPane = new JScrollPane();
	    scrollPane.setBounds(10, 132, 802, 299);
	    scrollPane.setViewportView(getTable());
	    scrollPane.setBorder(new TitledBorder(
		    UIManager.getBorder("TitledBorder.border"),
		    "Lista de reservas segun socio:",
		    TitledBorder.LEADING, TitledBorder.TOP, null, null));
	
	}
	return scrollPane;
    }

    private JTable getTable() {
	if (table == null) {
	    table = new JTable();
	    String[] nombreColumnas = { "dni_Socio", "nombre_Actividad",
		    "id_Actividad", "plazas", "Fecha", "HInicio", "HFin",
		    "Estado" };
	    modeloTabla = new NonEditableModel(nombreColumnas, 0);
	    table = new JTable(modeloTabla);
	    table.getColumnModel().getColumn(2).setPreferredWidth(200);
	    table.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
		    if (table.getRowCount() > 0) {
			btnConfirmarCancelacion.setEnabled(true);
		    }
		}
	    });
	}
	return table;
    }

    private JButton getBtnConfirmarCancelacion() {
	if (btnConfirmarCancelacion == null) {
	    btnConfirmarCancelacion = new JButton("Cancelar");
	    btnConfirmarCancelacion.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {

		    Date fechaActual = new Date();
		    SimpleDateFormat formateadorFecha = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    SimpleDateFormat formateadorHora = new SimpleDateFormat(
			    "HH:mm:ss");
		    String fechaSistema = formateadorFecha.format(fechaActual);
		    String horaSistema = formateadorHora.format(fechaActual);
		    int fila = table.getSelectedRow();
		    String id = String.valueOf(modeloTabla.getValueAt(fila, 2));
		    String dni = textDNI.getText().toString();
		    String idSocio = textFieldId.getText().toString();
		    String plazas = String
			    .valueOf(modeloTabla.getValueAt(fila, 3));
		    String fecha = String
			    .valueOf(modeloTabla.getValueAt(fila, 4));
		    String horaInicio = String
			    .valueOf(modeloTabla.getValueAt(fila, 5));
		    String estado = String
			    .valueOf(modeloTabla.getValueAt(fila, 7));
		    int plaza = Integer.valueOf(plazas);
		    List<ActividadSocioReservaDto> actividades = new ReservatePlaceController()
			    .listarActividades();
		    for (ActividadSocioReservaDto actividad : actividades) {
			if (actividad.id_Actividad == id)
			    plaza = actividad.numero_Plazas;
		    }

		    try {
			if (comprobarFecha(fecha, fechaSistema, horaInicio,
				horaSistema)) {
			    if (comprobarEstado(dni, Integer.valueOf(id),
				    estado)) {
				return;
			    }

			    new ReservatePlaceController().UpdateActividad(id,
				    plaza + 1);
			    new CancelReservationController()
				    .EliminarMatricula(idSocio, id);
			    JOptionPane.showMessageDialog(null,
				    "La actividad se ha anulado correctamente");
			}
		    } catch (HeadlessException e2) {
			e2.printStackTrace();
		    } catch (ParseException e2) {
			e2.printStackTrace();
		    } catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    } catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    } catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    }

		    limpiaTabla();

		    List<ActividadSocioReservaDto> matriculados;
		    matriculados = new CancelReservationController()
			    .listarMatriculados();
		    for (ActividadSocioReservaDto r : matriculados) {
			if (r.dni_Socio.equalsIgnoreCase(dni))
			    if (r.is_LimitePlazas = true)
			    try {
				if (comparaFecha(r.fecha, fechaSistema)
				    || comparaHorario(r.horario_Inicio,
					    horaSistema, r.fecha,
					    fechaSistema)) {
				Object[] nuevaLinea = new Object[8];
				nuevaLinea[0] = r.dni_Socio;
				nuevaLinea[1] = r.nombre_Actividad;
				nuevaLinea[2] = r.id_Actividad;
				nuevaLinea[3] = r.numero_Plazas;
				nuevaLinea[4] = r.fecha;
				nuevaLinea[5] = r.horario_Inicio;
				nuevaLinea[6] = r.horario_Fin;
				nuevaLinea[7] = r.estado;
				modeloTabla.addRow(nuevaLinea);

				}
			    } catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			    }
		    }

		}

	    });
	    btnConfirmarCancelacion.setEnabled(false);
	    btnConfirmarCancelacion.setBounds(233, 455, 161, 25);
	}
	return btnConfirmarCancelacion;
    }

    void limpiaTabla() {
	try {
	    NonEditableModel temp = (NonEditableModel) table.getModel();
	    int a = temp.getRowCount();
	    for (int i = 0; i < a; i++)
		temp.removeRow(0);
	} catch (Exception e) {
	    System.out.println(e);
	}
    }

    private JButton getBtnCancelarProceso() {
	if (btnCancelarProceso == null) {
	    btnCancelarProceso = new JButton(
		    "Salir");
	    btnCancelarProceso.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    dispose();
		    mainView.setVisible(true);
		}
	    });
	    btnCancelarProceso.setBounds(427, 455, 181, 25);
	}
	return btnCancelarProceso;
    }

    private JButton getButton() {
	if (button == null) {
	    button = new JButton("");
	    button.setBorder(null);
	    button.setIcon(new ImageIcon(
		    CancelReservationView.class.getResource("/img/lupab.jpg")));
	    button.setBounds(485, 30, 46, 42);

	    button.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent arg0) {
		    limpiaTabla();

		    Date fechaActual = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    SimpleDateFormat formateadorHora = new SimpleDateFormat(
			    "HH:mm:ss");
		    String fechaSistema = formateador.format(fechaActual);
		    String HoraSistema = formateadorHora.format(fechaActual);

		    String dni = textDNI.getText().toString();
		    List<SocioDto> socios = new ReservatePlaceController()
			    .listarSocios();
		    for (SocioDto s : socios) {

			if (s.getDni().equals(textDNI.getText().toString())) {
			    textFieldNombre.setText(s.getNombre());
			    textFieldApellidos.setText(s.getApellidos());
			    textFieldId.setText(String.valueOf(s.getId()));
			}
		    }
		    List<ActividadSocioReservaDto> matriculados;
		    matriculados = new CancelReservationController()
			    .listarMatriculados();
		    for (ActividadSocioReservaDto r : matriculados) {

			if (r.dni_Socio.equalsIgnoreCase(dni))
			    if (r.is_LimitePlazas = true)
				try {
				    if (comparaFecha(r.fecha, fechaSistema)
					    || comparaHorario(r.horario_Inicio,
						    HoraSistema, r.fecha,
						    fechaSistema)) {
					Object[] nuevaLinea = new Object[8];
					nuevaLinea[0] = r.dni_Socio;
					nuevaLinea[1] = r.nombre_Actividad;
					nuevaLinea[2] = r.id_Actividad;
					nuevaLinea[3] = r.numero_Plazas;
					nuevaLinea[4] = r.fecha;
					nuevaLinea[5] = r.horario_Inicio;
					nuevaLinea[6] = r.horario_Fin;
					nuevaLinea[7] = r.estado;
					modeloTabla.addRow(nuevaLinea);
				    }
				} catch (ParseException e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				}
		    }

		    if (table.getRowCount() == 0) {
			JOptionPane.showMessageDialog(null,
				"-DNI de socio no registrado.\n-No existen reservas relacionadas con dicho DNI",
				"No se han encontrado resultados",
				JOptionPane.ERROR_MESSAGE);
		    }

		}

	    });
	}
	return button;
    }

    public boolean comparaFecha(String fecha, String fechaActual)
	    throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	boolean resultado = fechaDate1.compareTo(fechaDate2) > 0;

	return resultado;

    }

    public boolean comparaHorario(String hora, String horaaActual, String fecha,
	    String fechaActual) throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat formateador1 = new SimpleDateFormat("yyyy-MM-dd");
	Date horaDate1 = formateador.parse(hora);
	Date horaaDate2 = formateador.parse(horaaActual);
	Date fechaDate1 = formateador1.parse(fecha);
	Date fechaDate2 = formateador1.parse(fechaActual);
	boolean resultado = horaDate1.compareTo(horaaDate2) >= 0
		&& fechaDate1.compareTo(fechaDate2) == 0;
	return resultado;

    }

    private boolean comprobarEstado(String dni, int idActividad, String estado)
	    throws ClassNotFoundException, SQLException {

	List<ActividadSocioReservaDto> matriculados = new CancelReservationController()
		.comprobarEstado();
	for (ActividadSocioReservaDto matriculadoEn : matriculados) {

	    if (matriculadoEn.dni_Socio.equalsIgnoreCase(dni)
		    && Integer
			    .parseInt(matriculadoEn.id_Actividad) == idActividad
		    && matriculadoEn.estado.equalsIgnoreCase("CANCELADA")) {
		JOptionPane.showMessageDialog(null,
			"No se permite cancelar la actividad debido a que el socio ya tienen cancelada dicha Actividad",
			"No se permite cancelar la reserva",
			JOptionPane.ERROR_MESSAGE);
		return true;
	    }
	}
	return false;
    }

    private JTextField getTextDNI() {
	if (textDNI == null) {
	    textDNI = new JTextField();
	    textDNI.setHorizontalAlignment(SwingConstants.CENTER);
	    textDNI.setBounds(296, 36, 179, 22);
	    textDNI.setColumns(10);
	}
	return textDNI;
    }

    public boolean comprobarFecha(String fecha, String fechaActual, String hora,
	    String horaActual) throws ParseException {

	SimpleDateFormat formateadorFecha = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat formateadorHora = new SimpleDateFormat("HH:mm:ss");
	Date fechaDate1 = formateadorFecha.parse(fecha);
	Date fechaDate2 = formateadorFecha.parse(fechaActual);
	Date horaDate1 = formateadorHora.parse(hora);
	Date horaDate2 = formateadorHora.parse(horaActual);
	int hora1 = horaDate1.getHours();
	int hora2 = horaDate2.getHours();
	int date1 = fechaDate1.getDate();
	int date2 = fechaDate2.getDate();
	boolean resultado2 =  hora1 <= hora2  ;
	boolean resultado3 = date1 - date2 == 0  ;
	if ( (resultado2 && resultado3)) {
	    JOptionPane.showMessageDialog(null,
		    "No se permite cancelar la actividad debido a que dicha actividad ya ha dado comienzo",
		    "No se permite cancelar la reserva",
		    JOptionPane.ERROR_MESSAGE);
	    return false;
	}
	return true;

    }

    private JLabel getLblNombre() {
	if (lblNombre == null) {
	    lblNombre = new JLabel("Nombre:");
	    lblNombre.setBounds(10, 82, 63, 25);
	}
	return lblNombre;
    }

    private JTextField getTextFieldNombre() {
	if (textFieldNombre == null) {
	    textFieldNombre = new JTextField();
	    textFieldNombre.setBounds(71, 84, 140, 25);
	    textFieldNombre.setColumns(10);
	}
	return textFieldNombre;
    }

    private JLabel getLblApellidos() {
	if (lblApellidos == null) {
	    lblApellidos = new JLabel("Apellidos:");
	    lblApellidos.setBounds(233, 82, 78, 25);
	}
	return lblApellidos;
    }

    private JTextField getTextFieldApellidos() {
	if (textFieldApellidos == null) {
	    textFieldApellidos = new JTextField();
	    textFieldApellidos.setBounds(296, 83, 312, 23);
	    textFieldApellidos.setColumns(10);
	}
	return textFieldApellidos;
    }

    private JLabel getLblId() {
	if (lblId == null) {
	    lblId = new JLabel("Id:");
	    lblId.setBounds(621, 86, 29, 16);
	}
	return lblId;
    }

    private JTextField getTextFieldId() {
	if (textFieldId == null) {
	    textFieldId = new JTextField();
	    textFieldId.setBounds(672, 83, 78, 23);
	    textFieldId.setColumns(10);
	}
	return textFieldId;
    }
    
    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
	mainView.setVisible(true);
    }

    public Main getMainView() {
	return mainView;
    }

    public void setMainView(Main mainView) {
	this.mainView = mainView;
    }

}
