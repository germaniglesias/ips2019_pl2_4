package ui.member;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dto.PlanificacionActividadDto;
import dto.SocioReservaInstalacionDto;
import utils.Fecha;

import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextArea;
import java.awt.CardLayout;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ColisionReservaInstalacionView extends JDialog {

    private ReservateInstalationView ventanaReserva;
    private SocioReservaInstalacionDto reservaInstalacionColisiona;
    private PlanificacionActividadDto reservaActividadColisiona;

    private static final long serialVersionUID = 1L;
    private JPanel pnPrincipal;
    private JLabel lbIcono;
    private JTextArea txtrLaReservaColisiona;
    private JPanel pnOpciones;
    private JPanel pnReservaInstalacion;
    private JPanel pnReservaActividad;
    private JButton btnAceptar;
    private JLabel lblReservaDeInstalacin;
    private JLabel lblReservaDeActividad;
    private JScrollPane spInstalacion;
    private JTextArea txInstalacion;
    private JScrollPane spActividad;
    private JTextArea txActividad;

    /**
     * Constructor si colisiona una reserva de plaza en actividad
     * 
     * @param reservateInstalationView
     * @param reservaInstalacionColisiona
     * @param reservaActividadColisiona
     */
    public ColisionReservaInstalacionView(
	    ReservateInstalationView reservateInstalationView,
	    SocioReservaInstalacionDto reservaInstalacionColisiona,
	    PlanificacionActividadDto reservaActividadColisiona) {
    	setModal(true);
	this.ventanaReserva = reservateInstalationView;
	this.reservaInstalacionColisiona = reservaInstalacionColisiona;
	this.reservaActividadColisiona = reservaActividadColisiona;

	setIconImage(Toolkit.getDefaultToolkit()
		.getImage(ColisionReservaInstalacionView.class
			.getResource("/img/error.png")));
	setTitle("Error al reservar instalación");
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setBounds(100, 100, 511, 258);
	pnPrincipal = new JPanel();
	pnPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(pnPrincipal);
	pnPrincipal.setLayout(null);
	pnPrincipal.add(getLbIcono());
	pnPrincipal.add(getTxtrLaReservaColisiona());
	pnPrincipal.add(getPnOpciones());
	pnPrincipal.add(getBtnAceptar());

	modificarContenidoPanel();
	setResizable(false);
	setLocationRelativeTo(ventanaReserva);
	setVisible(true);
    }

    /**
     * Modifica el panel dependiendo de si lo que colisiona es una reserva de
     * instalacion o una actividad
     */
    private void modificarContenidoPanel() {
	CardLayout cl = (CardLayout) (pnOpciones.getLayout());
	// Colisiona una reserva de instalación
	if (reservaInstalacionColisiona != null) {
	    cl.show(pnOpciones, "Instalacion");
	    mostrarInstalacion();
	}
	// Colisiona una reserva de plaza en una actividad
	else if (reservaActividadColisiona != null) {
	    cl.show(pnOpciones, "Actividad");
	    mostrarActividad();
	}
    }

    /**
     * Formato: Gimnasio, el 23-12-2019 a las 14:00:00
     */
    private void mostrarInstalacion() {
	txInstalacion.setText(reservaInstalacionColisiona.getNombreInstalacion()
		+ ", el "
		+ Fecha.fromDDBBtoApp(reservaInstalacionColisiona.getFecha())
		+ " a las " + reservaInstalacionColisiona.getHoraInicio());
    }

    /**
     * Formato: Yoga, instalación: Gimnasio, el 23-12-2019 de 14:00:00 a 16:00:00
     */
    private void mostrarActividad() {
	txActividad.setText(reservaActividadColisiona.getNombre()
		+ ", instalación: "
		+ reservaActividadColisiona.getNombreInstalacion() + ", el "
		+ Fecha.fromDDBBtoApp(reservaActividadColisiona.getFecha())
		+ " de " + reservaActividadColisiona.getHoraInicio() + " a "
		+ reservaActividadColisiona.getHoraFin());
    }

    private JLabel getLbIcono() {
	if (lbIcono == null) {
	    lbIcono = new JLabel("");
	    lbIcono.setIcon(new ImageIcon(ColisionReservaInstalacionView.class
		    .getResource("/img/error.png")));
	    lbIcono.setBounds(10, 11, 93, 93);
	}
	return lbIcono;
    }

    private JTextArea getTxtrLaReservaColisiona() {
	if (txtrLaReservaColisiona == null) {
	    txtrLaReservaColisiona = new JTextArea();
	    txtrLaReservaColisiona.setEditable(false);
	    txtrLaReservaColisiona.setOpaque(false);
	    txtrLaReservaColisiona.setBorder(null);
	    txtrLaReservaColisiona.setFont(new Font("Tahoma", Font.PLAIN, 24));
	    txtrLaReservaColisiona.setWrapStyleWord(true);
	    txtrLaReservaColisiona.setLineWrap(true);
	    txtrLaReservaColisiona.setText(
		    "La reserva de instalación colisiona con otra reserva");
	    txtrLaReservaColisiona.setBounds(113, 18, 378, 72);
	}
	return txtrLaReservaColisiona;
    }

    private JPanel getPnOpciones() {
	if (pnOpciones == null) {
	    pnOpciones = new JPanel();
	    pnOpciones.setBounds(10, 108, 481, 85);
	    pnOpciones.setLayout(new CardLayout(0, 0));
	    pnOpciones.add(getPnReservaInstalacion(), "Instalacion");
	    pnOpciones.add(getPnReservaActividad(), "Actividad");
	}
	return pnOpciones;
    }

    private JPanel getPnReservaInstalacion() {
	if (pnReservaInstalacion == null) {
	    pnReservaInstalacion = new JPanel();
	    pnReservaInstalacion.setBorder(null);
	    pnReservaInstalacion.setLayout(null);
	    pnReservaInstalacion.add(getLblReservaDeInstalacin());
	    pnReservaInstalacion.add(getSpInstalacion());
	}
	return pnReservaInstalacion;
    }

    private JPanel getPnReservaActividad() {
	if (pnReservaActividad == null) {
	    pnReservaActividad = new JPanel();
	    pnReservaActividad.setLayout(null);
	    pnReservaActividad.add(getLblReservaDeActividad());
	    pnReservaActividad.add(getSpActividad());
	}
	return pnReservaActividad;
    }

    private JButton getBtnAceptar() {
	if (btnAceptar == null) {
	    btnAceptar = new JButton("Aceptar");
	    btnAceptar.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    	    dispose();	    	    
	    	}
	    });
	    btnAceptar.setMnemonic('c');
	    btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    btnAceptar.setBounds(402, 196, 89, 23);
	}
	return btnAceptar;
    }

    private JLabel getLblReservaDeInstalacin() {
	if (lblReservaDeInstalacin == null) {
	    lblReservaDeInstalacin = new JLabel("Reserva de instalación:");
	    lblReservaDeInstalacin
		    .setHorizontalAlignment(SwingConstants.CENTER);
	    lblReservaDeInstalacin.setFont(new Font("Tahoma", Font.PLAIN, 20));
	    lblReservaDeInstalacin.setBounds(10, 0, 461, 23);
	}
	return lblReservaDeInstalacin;
    }

    private JLabel getLblReservaDeActividad() {
	if (lblReservaDeActividad == null) {
	    lblReservaDeActividad = new JLabel("Reserva de actividad:");
	    lblReservaDeActividad.setBounds(10, 0, 461, 22);
	    lblReservaDeActividad.setHorizontalAlignment(SwingConstants.CENTER);
	    lblReservaDeActividad.setFont(new Font("Tahoma", Font.PLAIN, 20));
	}
	return lblReservaDeActividad;
    }

    private JScrollPane getSpInstalacion() {
	if (spInstalacion == null) {
	    spInstalacion = new JScrollPane();
	    spInstalacion.setBounds(10, 37, 461, 45);
	    spInstalacion.setViewportView(getTxInstalacion());
	}
	return spInstalacion;
    }

    private JTextArea getTxInstalacion() {
	if (txInstalacion == null) {
	    txInstalacion = new JTextArea();
	    txInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    txInstalacion.setLineWrap(true);
	    txInstalacion.setWrapStyleWord(true);
	    txInstalacion.setEditable(false);
	}
	return txInstalacion;
    }

    private JScrollPane getSpActividad() {
	if (spActividad == null) {
	    spActividad = new JScrollPane();
	    spActividad.setBounds(10, 37, 461, 45);
	    spActividad.setViewportView(getTxActividad());
	}
	return spActividad;
    }

    private JTextArea getTxActividad() {
	if (txActividad == null) {
	    txActividad = new JTextArea();
	    txActividad.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    txActividad.setWrapStyleWord(true);
	    txActividad.setLineWrap(true);
	    txActividad.setEditable(false);
	}
	return txActividad;
    }
}
