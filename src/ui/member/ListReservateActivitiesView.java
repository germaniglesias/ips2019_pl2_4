package ui.member;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import controller.member.ListReservationController;
import controller.member.ReservatePlaceController;
import dto.ActividadSocioReservaDto;
import dto.SocioDto;
import ui.Main;
import utils.NonEditableModel;

public class ListReservateActivitiesView extends JDialog {

    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JScrollPane scrollPane;
    private JTable table;
    private JButton btnAceptar;
    private NonEditableModel modeloTabla;
    private JButton button;
    private JTextField textDNI;

    final long MILLSECS_PER_DAY = 60 * 60 * 1000;
    @SuppressWarnings("unused")
    private Date hoy = new Date();
    Calendar calendario = Calendar.getInstance();
    private JLabel lblNombre;
    private JTextField textFieldNombre;
    private JLabel lblApellidos;
    private JTextField textFieldApellidos;
    private JLabel lblId;
    private JTextField textFieldId;

    private Main mainView;

    /**
     * Create the dialog.
     * 
     * @param mainView
     */
    public ListReservateActivitiesView(Main mainView) {
	this.setMainView(mainView);

	setModal(true);
	setTitle("Listado de las reservas");
	calendario = new GregorianCalendar();
	setBounds(100, 100, 858, 565);
	setLocationRelativeTo(mainView);

	getContentPane().setLayout(new BorderLayout());

	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(null);
	{
	    JLabel lblDniDelCliente = new JLabel("DNI del Socio:");
	    lblDniDelCliente.setBounds(140, 39, 102, 16);
	    contentPanel.add(lblDniDelCliente);
	}
	contentPanel.add(getScrollPane());
	contentPanel.add(getBtnAceptar());
	contentPanel.add(getButton());
	contentPanel.add(getTextDNI());
	contentPanel.add(getLblNombre());
	contentPanel.add(getTextFieldNombre());
	contentPanel.add(getLblApellidos());
	contentPanel.add(getTextFieldApellidos());
	contentPanel.add(getLblId());
	contentPanel.add(getTextFieldId());
    }

    private JScrollPane getScrollPane() {
	if (scrollPane == null) {
	    scrollPane = new JScrollPane();
	    scrollPane.setBounds(10, 132, 822, 321);
	    scrollPane.setViewportView(getTable());
	    scrollPane.setBorder(new TitledBorder(
		    UIManager.getBorder("TitledBorder.border"),
		    "Lista de reservas segun socio:", TitledBorder.LEADING,
		    TitledBorder.TOP, null, null));

	}
	return scrollPane;
    }

    private JTable getTable() {
	if (table == null) {
	    table = new JTable();
	    String[] nombreColumnas = { "id_Actividad", "nombre_Actividad",
		    "Fecha", "HInicio", "HFin", "plazas", "Estado" };
	    modeloTabla = new NonEditableModel(nombreColumnas, 0);
	    table = new JTable(modeloTabla);
	    table.getColumnModel().getColumn(2).setPreferredWidth(200);

	}
	return table;
    }

    void limpiaTabla() {
	try {
	    NonEditableModel temp = (NonEditableModel) table.getModel();
	    int a = temp.getRowCount();
	    for (int i = 0; i < a; i++)
		temp.removeRow(0);
	} catch (Exception e) {
	    System.out.println(e);
	}
    }

    private JButton getBtnAceptar() {
	if (btnAceptar == null) {
	    btnAceptar = new JButton("Aceptar");
	    btnAceptar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    dispose();
		    mainView.setVisible(true);
		}
	    });
	    btnAceptar.setBounds(273, 490, 249, 25);
	}
	return btnAceptar;
    }

    private JButton getButton() {
	if (button == null) {
	    button = new JButton("");

	    button.setBorder(null);
	    button.setIcon(new ImageIcon(
		    CancelReservationView.class.getResource("/img/lupab.jpg")));
	    button.setBounds(529, 23, 46, 42);

	    button.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent arg0) {

		    Date fechaActual = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    SimpleDateFormat formateadorHora = new SimpleDateFormat(
			    "HH:mm:ss");
		    String fechaSistema = formateador.format(fechaActual);
		    String HoraSistema = formateadorHora.format(fechaActual);

		    String dni = textDNI.getText().toString();
		    List<SocioDto> socios = new ReservatePlaceController()
			    .listarSocios();
		    for (SocioDto s : socios) {

			if (s.getDni().equals(textDNI.getText().toString())) {
			    textFieldNombre.setText(s.getNombre());
			    textFieldApellidos.setText(s.getApellidos());
			    textFieldId.setText(String.valueOf(s.getId()));
			}
		    }
		    List<ActividadSocioReservaDto> matriculados;
		    matriculados = new ListReservationController()
			    .listarReservas(dni);
		    for (ActividadSocioReservaDto r : matriculados) {
			try {
			    if (comparaFecha(r.fecha, fechaSistema)
				    || comparaHorario(r.horario_Inicio,
					    HoraSistema, r.fecha, fechaSistema))
				if (r.is_LimitePlazas = true) {
				    Object[] nuevaLinea = new Object[7];

				    nuevaLinea[0] = r.id_Actividad;
				    nuevaLinea[1] = r.nombre_Actividad;
				    nuevaLinea[2] = r.fecha;
				    nuevaLinea[3] = r.horario_Inicio;
				    nuevaLinea[4] = r.horario_Fin;
				    nuevaLinea[5] = r.numero_Plazas;
				    nuevaLinea[6] = r.estado;
				    modeloTabla.addRow(nuevaLinea);
				}
			} catch (ParseException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		    }

		    if (table.getRowCount() == 0) {
			JOptionPane.showMessageDialog(null,
				"-DNI de socio no registrado.\n-No existen reservas relacionadas con dicho DNI",
				"No se han encontrado resultados",
				JOptionPane.ERROR_MESSAGE);
		    }

		}

	    });
	}
	return button;
    }

    private JTextField getTextDNI() {
	if (textDNI == null) {
	    textDNI = new JTextField();
	    textDNI.setHorizontalAlignment(SwingConstants.CENTER);
	    textDNI.setBounds(252, 36, 249, 22);
	    textDNI.setColumns(10);
	}
	return textDNI;
    }

    public boolean comparaFecha(String fecha, String fechaActual)
	    throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	boolean resultado = fechaDate1.compareTo(fechaDate2) > 0;

	return resultado;

    }

    public boolean comparaHorario(String hora, String horaaActual, String fecha,
	    String fechaActual) throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat formateador1 = new SimpleDateFormat("yyyy-MM-dd");
	Date horaDate1 = formateador.parse(hora);
	Date horaaDate2 = formateador.parse(horaaActual);
	Date fechaDate1 = formateador1.parse(fecha);
	Date fechaDate2 = formateador1.parse(fechaActual);
	boolean resultado = horaDate1.compareTo(horaaDate2) >= 0
		&& fechaDate1.compareTo(fechaDate2) == 0;
	return resultado;

    }

    private JLabel getLblNombre() {
	if (lblNombre == null) {
	    lblNombre = new JLabel("Nombre:");
	    lblNombre.setBounds(10, 82, 63, 25);
	}
	return lblNombre;
    }

    private JTextField getTextFieldNombre() {
	if (textFieldNombre == null) {
	    textFieldNombre = new JTextField();
	    textFieldNombre.setBounds(71, 84, 153, 25);
	    textFieldNombre.setColumns(10);
	}
	return textFieldNombre;
    }

    private JLabel getLblApellidos() {
	if (lblApellidos == null) {
	    lblApellidos = new JLabel("Apellidos:");
	    lblApellidos.setBounds(252, 82, 78, 25);
	}
	return lblApellidos;
    }

    private JTextField getTextFieldApellidos() {
	if (textFieldApellidos == null) {
	    textFieldApellidos = new JTextField();
	    textFieldApellidos.setBounds(321, 83, 335, 23);
	    textFieldApellidos.setColumns(10);
	}
	return textFieldApellidos;
    }

    private JLabel getLblId() {
	if (lblId == null) {
	    lblId = new JLabel("Id:");
	    lblId.setBounds(683, 86, 29, 16);
	}
	return lblId;
    }

    private JTextField getTextFieldId() {
	if (textFieldId == null) {
	    textFieldId = new JTextField();
	    textFieldId.setBounds(722, 83, 78, 23);
	    textFieldId.setColumns(10);
	}
	return textFieldId;
    }

    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
	mainView.setVisible(true);
    }

    public Main getMainView() {
	return mainView;
    }

    public void setMainView(Main mainView) {
	this.mainView = mainView;
    }
}
