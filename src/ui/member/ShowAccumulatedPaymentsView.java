package ui.member;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.member.MemberMainController;
import controller.member.ShowAccumulatedPaymentsController;
import dto.SocioDto;
import dto.SocioReservaInstalacionDto;
import utils.ComparadorSocioReservaInstalacion;
import utils.Fecha;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JCheckBox;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ShowAccumulatedPaymentsView extends JDialog {

    private MemberMainController memberMainController;
    private ShowAccumulatedPaymentsController sap = new ShowAccumulatedPaymentsController();
    private SocioDto socio;
    private List<SocioReservaInstalacionDto> reservas;

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JPanel panelSeleccionMensualidad;
    private JCheckBox chckbxPagosPasados;
    private JCheckBox chckbxPagosSiguienteRecibo;
    private JCheckBox chckbxPagosMesSiguiente;
    private JScrollPane scrollPane;
    private JTextArea textArea;

    public ShowAccumulatedPaymentsView(MemberMainController mem, SocioDto s) {
	this();
	memberMainController = mem;
	socio = s;

	cargarReservas();
    }

    public ShowAccumulatedPaymentsView() {
    	setModal(true);
	setTitle("SOCIO: VER MIS PAGOS ACUMULADOS");
	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		cerrarVentana(arg0);
	    }
	});
	setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	setBounds(100, 100, 800, 500);
	setLocationRelativeTo(null);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(new BorderLayout(0, 0));
	setContentPane(contentPane);
	contentPane.add(getPanelSeleccionMensualidad(), BorderLayout.NORTH);
	contentPane.add(getScrollPane(), BorderLayout.CENTER);

//	//Testeo
//	memberMainController = new MemberMainController(new MemberMainView(), new Main());
//	socio = new PayFacilityReservationController().findSocioById((long) 1);
//	System.out.println(socio.datos());
//	cargarReservas();
//	System.out.println(reservas);
    }

    private JPanel getPanelSeleccionMensualidad() {
	if (panelSeleccionMensualidad == null) {
	    panelSeleccionMensualidad = new JPanel();
	    panelSeleccionMensualidad.add(getChckbxPagosPasados());
	    panelSeleccionMensualidad.add(getChckbxPagosPresentes());
	    panelSeleccionMensualidad.add(getChckbxPagosFuturos());
	}
	return panelSeleccionMensualidad;
    }

    private JCheckBox getChckbxPagosPasados() {
	if (chckbxPagosPasados == null) {
	    chckbxPagosPasados = new JCheckBox("Pagos pasados");
	    chckbxPagosPasados.addActionListener(new RecargarReservas());
	    chckbxPagosPasados.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxPagosPasados;
    }

    private JCheckBox getChckbxPagosPresentes() {
	if (chckbxPagosSiguienteRecibo == null) {
	    chckbxPagosSiguienteRecibo = new JCheckBox(
		    "Pagos siguiente recibo");
	    chckbxPagosSiguienteRecibo
		    .addActionListener(new RecargarReservas());
	    chckbxPagosSiguienteRecibo
		    .setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxPagosSiguienteRecibo;
    }

    private JCheckBox getChckbxPagosFuturos() {
	if (chckbxPagosMesSiguiente == null) {
	    chckbxPagosMesSiguiente = new JCheckBox("Pagos mes siguiente");
	    chckbxPagosMesSiguiente.addActionListener(new RecargarReservas());
	    chckbxPagosMesSiguiente.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxPagosMesSiguiente;
    }

    private JScrollPane getScrollPane() {
	if (scrollPane == null) {
	    scrollPane = new JScrollPane();
	    scrollPane.setViewportView(getTextArea());
	}
	return scrollPane;
    }

    private JTextArea getTextArea() {
	if (textArea == null) {
	    textArea = new JTextArea();
	    textArea.setFont(new Font("Monospaced", Font.PLAIN, 16));
	}
	return textArea;
    }

    // MIS METODOS

    // Inicio

    protected void cerrarVentana(WindowEvent arg0) {
	memberMainController.getView().setVisible(true);
	arg0.getWindow().dispose();
    }

    private void cargarReservas() {
	List<SocioReservaInstalacionDto> list = sap
		.listReservasSocio(socio.getId());
	list.sort(new ComparadorSocioReservaInstalacion());
	reservas = new ArrayList<SocioReservaInstalacionDto>(list);
    }

    // Recargar

    private class RecargarReservas implements ActionListener {
	public void actionPerformed(ActionEvent arg0) {
	    recargarTextoRecibos();
	}
    }

    public void recargarTextoRecibos() {
	String str = "";

	if (getChckbxPagosPasados().isSelected()) {
	    str += textoPagosPasados();
	}
	if (getChckbxPagosPresentes().isSelected()) {
	    str += textoPagosPresentes();
	}
	if (getChckbxPagosFuturos().isSelected()) {
	    str += textoPagosFuturos();
	}

	getTextArea().setText(str);
    }

    // Texto pagos

    private String textoPagosPasados() {
	String str = "";
	List<SocioReservaInstalacionDto> res = reservasPasadas();

	if (!res.isEmpty()) {
	    String mesActual = "";
	    for (SocioReservaInstalacionDto sri : res) {
		// Se ha cambiado de mes
		if (!nombreMes(sri).equals(mesActual)) {
		    str += nombreMes(sri) + "\n";
		    mesActual = nombreMes(sri);
		}
		str += "\t" + sri.datos() + "\n";
	    }
	}

	return str;
    }

    private String textoPagosPresentes() {
	String str = "";
	List<SocioReservaInstalacionDto> res = reservasPresente();

	if (!res.isEmpty()) {
	    // Añadir el mes a str
	    if (Fecha.getDay(res.get(0).getFecha()) > 20)
		str += nombreSiguienteMes(res.get(0)) + "\n";
	    else
		str += nombreMes(res.get(0)) + "\n";

	    // listar todos los datos de las reservas de este mes
	    for (SocioReservaInstalacionDto reserva : res) {
		str += "\t" + reserva.datos() + "\n";
	    }
	}

	return str;
    }

    private String textoPagosFuturos() {
	String str = "";
	List<SocioReservaInstalacionDto> res = reservasFuturas();

	if (!res.isEmpty()) {
	    // Añadir el mes a str
	    if (Fecha.getDay(res.get(0).getFecha()) >= 20)
		str += nombreSiguienteMes(res.get(0)) + "\n";
	    else
		str += nombreMes(res.get(0)) + "\n";

	    // listar todos los datos de las reservas de este mes
	    for (SocioReservaInstalacionDto reserva : res) {
		str += "\t" + reserva.datos() + "\n";
	    }
	}

	return str;
    }

    // Listar Reservas

    private List<SocioReservaInstalacionDto> reservasPasadas() {
	List<SocioReservaInstalacionDto> res = new ArrayList<SocioReservaInstalacionDto>();

	// Conseguir las fechas limite
	Date limite = Fecha.getLastMonth20th(new Date());

	for (SocioReservaInstalacionDto reserva : reservas) {
	    try {
		Date date = Fecha.getDateFrom(reserva.getFecha());

		if (date.compareTo(limite) <= 0) {
		    res.add(reserva);
		}
	    } catch (ParseException e) {
		e.printStackTrace();
	    }
	}

	return res;
    }

    private List<SocioReservaInstalacionDto> reservasPresente() {
	List<SocioReservaInstalacionDto> res = new ArrayList<SocioReservaInstalacionDto>();

	// Conseguir las fechas limite
	Date limiteInferior, limiteSuperior; // El mes siempre es el actual
	limiteInferior = Fecha.getLastMonth20th(new Date());
	limiteSuperior = Fecha.getThisMonth20th(new Date());

	for (SocioReservaInstalacionDto reserva : reservas) {
	    try {
		Date date = Fecha.getDateFrom(reserva.getFecha());

		if (date.compareTo(limiteInferior) > 0
			&& date.compareTo(limiteSuperior) < 0) {
		    res.add(reserva);
		}
	    } catch (ParseException e) {
		e.printStackTrace();
	    }
	}

	return res;
    }

    private List<SocioReservaInstalacionDto> reservasFuturas() {
	List<SocioReservaInstalacionDto> res = new ArrayList<SocioReservaInstalacionDto>();

	// Conseguir las fechas limite
	Date limite = Fecha.getThisMonth20th(new Date());

	for (SocioReservaInstalacionDto reserva : reservas) {
	    try {
		Date date = Fecha.getDateFrom(reserva.getFecha());

		if (date.compareTo(limite) >= 0) {
		    res.add(reserva);
		}
	    } catch (ParseException e) {
		e.printStackTrace();
	    }
	}

	return res;
    }

    // Funcionalidad extra
    private String nombreMes(SocioReservaInstalacionDto res) {
	// Añadir el mes a str
	Date date = new Date();
	try {
	    date = Fecha.getDateFrom(res.getFecha());
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	return Fecha.getMonthName(date);
    }

    private String nombreSiguienteMes(SocioReservaInstalacionDto res) {
	// Añadir el mes a str
	Date date = new Date();
	try {
	    date = Fecha.getDateFrom(res.getFecha());
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	date = Fecha.getNextMonth20th(date);
	return Fecha.getMonthName(date);
    }

}
