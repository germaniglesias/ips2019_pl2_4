package ui.member;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import business.service.MemberService;
import business.service.implementation.MemberServiceImpl;
import controller.member.ActivitiesScheduleController;
import dto.ActividadesSocioDto;
import ui.Main;
import utils.NonEditableModel;

public class ActivitiesScheduleView extends JFrame {

    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    private final JPanel panelPrincipal = new JPanel();
    private JTable tablaHorariosActividad;
    private NonEditableModel modeloTabla;
    private MemberService ms = new MemberServiceImpl();

    private Main mainView;

    /**
     * Create the dialog.
     * 
     * @param mainView
     * 
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws ParseException
     */
    public ActivitiesScheduleView(Main mainView)
	    throws ClassNotFoundException, SQLException, ParseException {
	setTitle("Horario de las actividades");

	this.setMainView(mainView);
	setBounds(100, 100, 852, 572);
	setLocationRelativeTo(mainView);

	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

	getContentPane().setLayout(new BorderLayout());
	panelPrincipal.setBackground(Color.LIGHT_GRAY);
	panelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(panelPrincipal, BorderLayout.CENTER);
	panelPrincipal.setLayout(new BorderLayout(0, 0));
	{
	    JScrollPane spTabla = new JScrollPane();
	    panelPrincipal.add(spTabla, BorderLayout.CENTER);
	    {
		modeloTabla = new NonEditableModel(
			new String[] { "NOMBRE", "FECHA", "HORA INICIO",
				"HORA FIN", "LIMITE PLAZAS", "NUMERO PLAZAS" },
			0);
		tablaHorariosActividad = new JTable();
		tablaHorariosActividad.setModel(modeloTabla);
		tablaHorariosActividad.setBackground(Color.WHITE);
		spTabla.setViewportView(tablaHorariosActividad);
		{
		    JPanel panel = new JPanel();
		    getContentPane().add(panel, BorderLayout.SOUTH);
		    {
			JButton btAceptar = new JButton("Aceptar");
			btAceptar.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent arg0) {
				dispose();
				mainView.setVisible(true);
			    }
			});
			panel.add(btAceptar);
		    }
		}
		ms.listaHorariosActividades();
		meterTablas();
	    }
	}
    }

    public void meterTablas()
	    throws ClassNotFoundException, SQLException, ParseException {
	Date fechaActual = new Date();
	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat formateadorHora = new SimpleDateFormat("HH:mm:ss");
	String fechaSistema = formateador.format(fechaActual);
	String HoraSistema = formateadorHora.format(fechaActual);
	List<ActividadesSocioDto> actividades = new ActivitiesScheduleController()
		.execute();
	for (ActividadesSocioDto actividades1 : actividades) {
	    if (comparaFecha(actividades1.getFecha(), fechaSistema)
		    || comparaHorario(actividades1.getHoraInicio(), HoraSistema,
			    actividades1.getFecha(), fechaSistema))
		modeloTabla.addRow(new String[] { actividades1.getNombre(),
			actividades1.getFecha(), actividades1.getHoraInicio(),
			actividades1.getHoraFin(),
			actividades1.isTieneLimitePlazas(),
			actividades1.getNumLimitePlazas() });

	}

    }

    public boolean comparaFecha(String fecha, String fechaActual)
	    throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	boolean resultado = fechaDate1.compareTo(fechaDate2) > 0;
	return resultado;

    }

    public boolean comparaHorario(String hora, String horaaActual, String fecha,
	    String fechaActual) throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat formateador1 = new SimpleDateFormat("yyyy-MM-dd");
	Date horaDate1 = formateador.parse(hora);
	Date horaaDate2 = formateador.parse(horaaActual);
	Date fechaDate1 = formateador1.parse(fecha);
	Date fechaDate2 = formateador1.parse(fechaActual);
	boolean resultado = horaDate1.compareTo(horaaDate2) >= 0
		&& fechaDate1.compareTo(fechaDate2) == 0;
	return resultado;

    }

    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
	mainView.setVisible(true);
    }

    public Main getMainView() {
	return mainView;
    }

    public void setMainView(Main mainView) {
	this.mainView = mainView;
    }
}
