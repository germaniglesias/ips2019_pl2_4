package ui.member;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import controller.member.ReservateInstalationController;
import controller.member.ReservatePlaceController;
import dto.InstalacionSocioReservaDto;
import dto.SocioDto;
import ui.Main;
import utils.NonEditableModel;

public class CancellInstalationView extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JScrollPane scrollPane;
    private JTable table;
    private JButton btnConfirmarCancelacion;
    private JButton btnCancelarProceso;
    private NonEditableModel modeloTabla;
    private JButton button;
    private JTextField textDNI;

    final long MILLSECS_PER_DAY = 60 * 60 * 1000;
    Calendar calendario = Calendar.getInstance();
    private JLabel lblNombre;
    private JTextField textFieldNombre;
    private JLabel lblApellidos;
    private JTextField textFieldApellidos;
    private JLabel lblId;
    private JTextField textFieldId;

    private Main mainView;

    /**
     * Create the dialog.
     */
    public CancellInstalationView(Main mainView) {
	this.setMainView(mainView);

	setModal(true);
	setTitle("Cancelar reserva instalacion de un socio");

	calendario = new GregorianCalendar();
	setBounds(100, 100, 766, 481);
	setLocationRelativeTo(mainView);
	getContentPane().setLayout(new BorderLayout());
	
	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(null);
	{
	    JLabel lblDniDelCliente = new JLabel("DNI Socio:");
	    lblDniDelCliente.setBounds(109, 39, 102, 16);
	    contentPanel.add(lblDniDelCliente);
	}
	contentPanel.add(getScrollPane());
	contentPanel.add(getBtnConfirmarCancelacion());
	contentPanel.add(getBtnCancelarProceso());
	contentPanel.add(getButton());
	contentPanel.add(getTextDNI());
	contentPanel.add(getLblNombre());
	contentPanel.add(getTextFieldNombre());
	contentPanel.add(getLblApellidos());
	contentPanel.add(getTextFieldApellidos());
	contentPanel.add(getLblId());
	contentPanel.add(getTextFieldId());
    }

    private JScrollPane getScrollPane() {
	if (scrollPane == null) {
	    scrollPane = new JScrollPane();
	    scrollPane.setBorder(
		    new TitledBorder(UIManager.getBorder("TitledBorder.border"),

			    "Lista de las reservas de instalacion del socio:",
			    TitledBorder.LEADING,

			    TitledBorder.TOP, null, null));
	    scrollPane.setBounds(10, 132, 728, 248);
	    scrollPane.setViewportView(getTable());
	}
	return scrollPane;
    }

    private JTable getTable() {
	if (table == null) {
	    table = new JTable();
	    String[] nombreColumnas = { "id_Reserva", "nombre_Instalacion",
		    "id_Instalacion", "Fecha", "HInicio", "PrecioPorHora" };

	    modeloTabla = new NonEditableModel(nombreColumnas, 0);
	    table = new JTable(modeloTabla);
	    table.getColumnModel().getColumn(2).setPreferredWidth(200);
	    table.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
		    if (table.getRowCount() > 0) {
			btnConfirmarCancelacion.setEnabled(true);
		    }
		}
	    });
	}
	return table;
    }

    private JButton getBtnConfirmarCancelacion() {
	if (btnConfirmarCancelacion == null) {
	    btnConfirmarCancelacion = new JButton("Cancelar");
	    btnConfirmarCancelacion.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {

		    Date fechaActual = new Date();
		    SimpleDateFormat formateadorFecha = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    SimpleDateFormat formateadorHora = new SimpleDateFormat(
			    "HH:mm:ss");
		    String fechaSistema = formateadorFecha.format(fechaActual);
		    String horaSistema = formateadorHora.format(fechaActual);
		    int fila = table.getSelectedRow();
		    String id = String.valueOf(modeloTabla.getValueAt(fila, 0));
		    String idInstalacion = String
			    .valueOf(modeloTabla.getValueAt(fila, 2));
		    String dni = textDNI.getText().toString();
		    String idSocio = textFieldId.getText().toString();

		    String fecha = String
			    .valueOf(modeloTabla.getValueAt(fila, 3));
		    String horaInicio = String
			    .valueOf(modeloTabla.getValueAt(fila, 3));

		    List<InstalacionSocioReservaDto> matriculados;
		    matriculados = new ReservateInstalationController()
			    .listarMatriculados();

		    try {
			if (!comprobarFecha(fecha, fechaSistema)) {

			    return;
			}
			System.out.println("\nBotonConfirmar");
			System.out.println(
				"\tfechaSistema date2: " + fechaSistema);
			System.out.println("\tfechaBBDD date1: " + fecha);
			System.out.println("\n");
			System.out.println("--------------------");

			new ReservateInstalationController()
				.eliminarInstalacion(id, idSocio,
					idInstalacion);

			JOptionPane.showMessageDialog(null,
				"La reserva se ha eliminado correctamente");
		    } catch (HeadlessException e2) {
			e2.printStackTrace();
		    } catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    } catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    } catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    } catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    }

		    limpiaTabla();

		    List<InstalacionSocioReservaDto> instalaciones;
		    instalaciones = new ReservateInstalationController()
			    .listarMatriculados();
		    for (InstalacionSocioReservaDto r : instalaciones) {
			if (r.dni_Socio.equalsIgnoreCase(dni)) {
			    try {
				if (comparaFecha(r.fecha, fechaSistema)
					|| comparaHorario(r.horaInicio,
						horaSistema, r.fecha,
						fechaSistema)) {
				    Object[] nuevaLinea = new Object[6];
				    nuevaLinea[0] = r.id_Reserva;
				    nuevaLinea[1] = r.nombre_Instalacion;
				    nuevaLinea[2] = r.id_Instalacion;
				    nuevaLinea[3] = r.fecha;
				    nuevaLinea[4] = r.horaInicio;
				    nuevaLinea[5] = r.precio;
				    modeloTabla.addRow(nuevaLinea);

				}
			    } catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			    }
			}

		    }

		}
	    });
	    btnConfirmarCancelacion.setEnabled(false);
	    btnConfirmarCancelacion.setBounds(170, 406, 161, 25);
	}
	return btnConfirmarCancelacion;

    }

    void limpiaTabla() {
	try {
	    NonEditableModel temp = (NonEditableModel) table.getModel();
	    int a = temp.getRowCount();
	    for (int i = 0; i < a; i++)
		temp.removeRow(0);
	} catch (Exception e) {
	    System.out.println(e);
	}
    }

    private JButton getBtnCancelarProceso() {
	if (btnCancelarProceso == null) {
	    btnCancelarProceso = new JButton("Salir");
	    btnCancelarProceso.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    dispose();
		    mainView.setVisible(true);
		}
	    });
	    btnCancelarProceso.setBounds(359, 406, 186, 25);
	}
	return btnCancelarProceso;
    }

    private JButton getButton() {
	if (button == null) {
	    button = new JButton("");
	    button.setIcon(new ImageIcon(
		    CancelReservationView.class.getResource("/img/lupab.jpg")));
	    button.setBounds(549, 25, 46, 42);

	    button.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent arg0) {

		    Date fechaActual = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    SimpleDateFormat formateadorHora = new SimpleDateFormat(
			    "HH:mm:ss");
		    String fechaSistema = formateador.format(fechaActual);
		    String HoraSistema = formateadorHora.format(fechaActual);

		    String dni = textDNI.getText().toString();
		    List<SocioDto> socios = new ReservatePlaceController()
			    .listarSocios();
		    for (SocioDto s : socios) {

			if (s.getDni().equals(textDNI.getText().toString())) {
			    textFieldNombre.setText(s.getNombre());
			    textFieldApellidos.setText(s.getApellidos());
			    textFieldId.setText(String.valueOf(s.getId()));
			}
		    }
		    List<InstalacionSocioReservaDto> matriculados;
		    matriculados = new ReservateInstalationController()
			    .listarMatriculados();
		    for (InstalacionSocioReservaDto r : matriculados) {

			if (r.dni_Socio.equalsIgnoreCase(dni))
			    try {
				if (comparaFecha(r.fecha, fechaSistema)
					|| comparaHorario(r.horaInicio,
						HoraSistema, r.fecha,
						fechaSistema)) {
				    Object[] nuevaLinea = new Object[6];
				    nuevaLinea[0] = r.id_Reserva;
				    nuevaLinea[1] = r.nombre_Instalacion;
				    nuevaLinea[2] = r.id_Instalacion;
				    nuevaLinea[3] = r.fecha;
				    nuevaLinea[4] = r.horaInicio;
				    nuevaLinea[5] = r.precio;
				    modeloTabla.addRow(nuevaLinea);
				}
			    } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			    }
		    }

		    if (table.getRowCount() == 0) {
			JOptionPane.showMessageDialog(null,
				"-DNI de socio no registrado.\n-No existen reservas relacionadas con dicho DNI",
				"No se han encontrado resultados",
				JOptionPane.ERROR_MESSAGE);
		    }

		}

	    });
	}
	return button;
    }

    public boolean comparaFecha(String fecha, String fechaActual)
	    throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	boolean resultado = fechaDate1.compareTo(fechaDate2) > 0;

	return resultado;

    }

    public boolean comparaHorario(String hora, String horaaActual, String fecha,
	    String fechaActual) throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat formateador1 = new SimpleDateFormat("yyyy-MM-dd");
	Date horaDate1 = formateador.parse(hora);
	Date horaaDate2 = formateador.parse(horaaActual);
	Date fechaDate1 = formateador1.parse(fecha);
	Date fechaDate2 = formateador1.parse(fechaActual);
	boolean resultado = horaDate1.compareTo(horaaDate2) >= 0
		&& fechaDate1.compareTo(fechaDate2) == 0;
	return resultado;

    }

//    private boolean comprobarEstado(String dni, int idActividad, String estado)
//	    throws ClassNotFoundException, SQLException {
//
//	List<ActividadSocioReservaDto> matriculados = new CancelReservationController()
//		.comprobarEstado();
//	for (ActividadSocioReservaDto matriculadoEn : matriculados) {
//
//	    if (matriculadoEn.dni_Socio.equalsIgnoreCase(dni)
//		    && Integer
//			    .parseInt(matriculadoEn.id_Actividad) == idActividad
//		    && matriculadoEn.estado.equalsIgnoreCase("CANCELADA")) {
//		JOptionPane.showMessageDialog(null,
//			"No se permite cancelar la actividad debido a que el socio ya tienen cancelada dicha Actividad",
//			"No se permite cancelar la reserva",
//			JOptionPane.ERROR_MESSAGE);
//		return true;
//	    }
//	}
//	return false;
//    }

    private JTextField getTextDNI() {
	if (textDNI == null) {
	    textDNI = new JTextField();
	    textDNI.setHorizontalAlignment(SwingConstants.CENTER);
	    textDNI.setBounds(233, 36, 243, 22);
	    textDNI.setColumns(10);
	}
	return textDNI;
    }

    @SuppressWarnings("deprecation")
    private boolean comprobarFecha(String fecha, String fechaActual)
	    throws ClassNotFoundException, SQLException, ParseException {
	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	int date1 = fechaDate1.getDate();
	int date2 = fechaDate2.getDate();
	System.out.println("\nMetodo");
	System.out.println("\tfechaSistema date2: " + date2);
	System.out.println("\tfechaBBDD date1: " + date1);
	System.out.println("\n");
	System.out.println("--------------------");
	boolean resultado =  date1 - date2 == 0;
	if (resultado) {
	    JOptionPane.showMessageDialog(null,
		    "No se permite cancelar la reserva de la instalacion, ya que solamente se puede cancelar hasta un día antes de la reserva",
		    "No se permite cancelar la reserva",
		    JOptionPane.ERROR_MESSAGE);
	    return false;
	}
	return true;

    }

    private JLabel getLblNombre() {
	if (lblNombre == null) {
	    lblNombre = new JLabel("Nombre:");
	    lblNombre.setBounds(10, 82, 63, 25);
	}
	return lblNombre;
    }

    private JTextField getTextFieldNombre() {
	if (textFieldNombre == null) {
	    textFieldNombre = new JTextField();
	    textFieldNombre.setBounds(71, 84, 140, 25);
	    textFieldNombre.setColumns(10);
	}
	return textFieldNombre;
    }

    private JLabel getLblApellidos() {
	if (lblApellidos == null) {
	    lblApellidos = new JLabel("Apellidos:");
	    lblApellidos.setBounds(233, 82, 78, 25);
	}
	return lblApellidos;
    }

    private JTextField getTextFieldApellidos() {
	if (textFieldApellidos == null) {
	    textFieldApellidos = new JTextField();
	    textFieldApellidos.setBounds(306, 84, 249, 23);
	    textFieldApellidos.setColumns(10);
	}
	return textFieldApellidos;
    }

    private JLabel getLblId() {
	if (lblId == null) {
	    lblId = new JLabel("Id:");
	    lblId.setBounds(579, 87, 29, 16);
	}
	return lblId;
    }

    private JTextField getTextFieldId() {
	if (textFieldId == null) {
	    textFieldId = new JTextField();
	    textFieldId.setBounds(618, 84, 78, 23);
	    textFieldId.setColumns(10);
	}
	return textFieldId;

    }

    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
	mainView.setVisible(true);
    }

    public Main getMainView() {
	return mainView;
    }

    public void setMainView(Main mainView) {
	this.mainView = mainView;
    }

}
