package ui.member;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import controller.member.ReservatePlaceController;
import dto.ActividadSocioReservaDto;
import dto.SocioDto;
import ui.Main;
import utils.NonEditableModel;

public class ReservatePlaceView extends JDialog {

    private static final long serialVersionUID = -4403173706021662442L;
    private final JPanel contentPanel = new JPanel();
    private JLabel lblDni;
    private JTextField textDNI;
    private JLabel lblNombre;
    private JTextField textNombre;
    private JLabel lblApellidos;
    private JTextField textApellidos;
    private JButton btnConfirmarInscripcion;
    private JButton btnCancelarTramite;
    private JButton button;
    private NonEditableModel modeloTabla;

    @SuppressWarnings("unused")
    private Date hoy = new Date();
    Calendar calendario = Calendar.getInstance();
    private JScrollPane scrollPane;
    private JTable table;
    private JLabel lblId;
    private JTextField textFieldid;
    
    private Main mainView;

    /**
     * Create the dialog.
     * @param mainView 
     * 
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ReservatePlaceView(Main mainView) throws ClassNotFoundException, SQLException {
	this.setMainView(mainView);

	setModal(true);
	setTitle("Reserva de Plazas");
	setBounds(100, 100, 828, 567);
	setLocationRelativeTo(mainView);
	getContentPane().setLayout(new BorderLayout());
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	
	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(null);
	contentPanel.add(getLblDni());
	contentPanel.add(getTextDNI());
	contentPanel.add(getLblNombre());
	contentPanel.add(getTextNombre());
	contentPanel.add(getLabel_1());
	contentPanel.add(getTextApellidos());
	contentPanel.add(getBtnConfirmarInscripcion());
	contentPanel.add(getBtnCancelarTramite());
	contentPanel.add(getButton());
	contentPanel.add(getScrollPane());
	contentPanel.add(getLblId());
	contentPanel.add(getTextFieldid());
    }

    private JLabel getLblDni() {
	if (lblDni == null) {
	    lblDni = new JLabel("DNI:");
	    lblDni.setBounds(177, 40, 38, 16);
	}
	return lblDni;
    }

    private JTextField getTextDNI() {
	if (textDNI == null) {
	    textDNI = new JTextField();
	    textDNI.setHorizontalAlignment(SwingConstants.CENTER);
	    textDNI.setBounds(226, 29, 282, 30);
	    textDNI.setColumns(10);
	}
	return textDNI;
    }

    private JLabel getLblNombre() {
	if (lblNombre == null) {
	    lblNombre = new JLabel("Nombre: ");
	    lblNombre.setBounds(10, 92, 56, 16);
	}
	return lblNombre;
    }

    private JTextField getTextNombre() {
	if (textNombre == null) {
	    textNombre = new JTextField();
	    textNombre.setEditable(false);
	    textNombre.setBounds(65, 89, 193, 22);
	    textNombre.setColumns(10);
	}
	return textNombre;
    }

    private JLabel getLabel_1() {
	if (lblApellidos == null) {
	    lblApellidos = new JLabel("Apellidos: ");
	    lblApellidos.setBounds(291, 92, 75, 16);
	}
	return lblApellidos;
    }

    private JTextField getTextApellidos() {
	if (textApellidos == null) {
	    textApellidos = new JTextField();
	    textApellidos.setEditable(false);
	    textApellidos.setBounds(353, 89, 252, 22);
	    textApellidos.setColumns(10);
	}
	return textApellidos;
    }

    private JButton getBtnConfirmarInscripcion() {
	if (btnConfirmarInscripcion == null) {
	    btnConfirmarInscripcion = new JButton("Reservar");
	    btnConfirmarInscripcion.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {

		    Date fechaActual = new Date();
		    SimpleDateFormat formateadorFecha = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    SimpleDateFormat formateadorHora = new SimpleDateFormat(
			    "HH:mm:ss");
		    String fechaSistema = formateadorFecha.format(fechaActual);
		    String horaSistema = formateadorHora.format(fechaActual);
		    int fila = table.getSelectedRow();
		    String id = String.valueOf(modeloTabla.getValueAt(fila, 1));
		    String idSocio = textFieldid.getText().toString();
		    String plazas = String
			    .valueOf(modeloTabla.getValueAt(fila, 5));
		    String fecha = String
			    .valueOf(modeloTabla.getValueAt(fila, 2));
		    String horaInicio = String
			    .valueOf(modeloTabla.getValueAt(fila, 3));
		    int plaza = Integer.valueOf(plazas);

		    List<ActividadSocioReservaDto> actividades = new ReservatePlaceController()
			    .listarActividades();
		    for (ActividadSocioReservaDto actividad : actividades) {
			if (actividad.id_Actividad == id)
			    plaza = actividad.numero_Plazas;
		    }

		    try {
			{
			    if ((!comprobarFecha(fecha, fechaSistema))) {

				return;
			    }


			    if (!comprobarFecha1(fecha, fechaSistema,horaInicio, horaSistema)) {

				return;
			    }
			    if (comprobarSocio(textDNI.getText(),
				    Integer.valueOf(id))) {

				return;
			    }
			    if (comprobarPlazas(Integer.valueOf(id)))
				return;

			    new ReservatePlaceController().UpdateActividad(id,
				    plaza - 1);
			    String estado = "CORRECTA";
			    new ReservatePlaceController()
				    .InsertarMatricula(idSocio, id, estado);
			    JOptionPane.showMessageDialog(null,
				    "La actividad se ha registrado correctamente");
			}
		    } catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    } catch (HeadlessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    }

		    limpiaTabla();

		    List<ActividadSocioReservaDto> matriculados;
		    matriculados = new ReservatePlaceController()
			    .listarActividades();
		    for (ActividadSocioReservaDto r : matriculados) {
			try {
			    if (comparaFecha(r.fecha, fechaSistema)
				    || comparaHorario(r.horario_Inicio,
					    horaSistema, r.fecha,
					    fechaSistema)) {
				Object[] nuevaLinea = new Object[6];
				nuevaLinea[0] = r.nombre_Actividad;
				nuevaLinea[1] = r.id_Actividad;
				nuevaLinea[2] = r.fecha;
				nuevaLinea[3] = r.horario_Inicio;
				nuevaLinea[4] = r.horario_Fin;
				nuevaLinea[5] = r.numero_Plazas;
				modeloTabla.addRow(nuevaLinea);

			    }
			} catch (ParseException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		    }

		}

	    });
	    btnConfirmarInscripcion.setBounds(226, 463, 177, 25);
	}
	return btnConfirmarInscripcion;
    }

    @SuppressWarnings("deprecation")
    private boolean comprobarFecha(String fecha, String fechaActual)
	    throws ClassNotFoundException, SQLException, ParseException {
	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	int date1 = fechaDate1.getDate();
	int date2 = fechaDate2.getDate();
	boolean resultado = date1 - date2 == 1 || date1 - date2 == 0
		|| date2 - date1 == 29;
	if (!resultado) {
	    JOptionPane.showMessageDialog(null,
		    "No se permite inscribir en la actividad debido a que el comienzo de la actividad sobrepasa el dia",
		    "No se permite hacer la reserva",
		    JOptionPane.ERROR_MESSAGE);
	    return false;
	}
	return true;

    }
    
    @SuppressWarnings("deprecation")
    private boolean comprobarFecha1(String fecha, String fechaActual,String hora, String horaActual)
	    throws ClassNotFoundException, SQLException, ParseException {
	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat formateador1 = new SimpleDateFormat("HH:mm:ss");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	Date horaDate1 = formateador1.parse(hora);
	Date horaDate2 = formateador1.parse(horaActual);
	int hora1 = horaDate1.getHours();
	int hora2 = horaDate2.getHours();
	int date1 = fechaDate1.getDate();
	int date2 = fechaDate2.getDate();
	boolean resultado = date1 - date2 == 1 || date1 - date2 == 0
		|| date2 - date1 == 29;
	boolean resultado2 =  hora1 <= hora2 +1 ;
	boolean resultado3 = date1 - date2 == 0  ;
	if ( (resultado2 && resultado3)) {
	    JOptionPane.showMessageDialog(null,
		    "No se permite inscribir en la actividad debido a que falta menos de una hora para que empiece",
		    "No se permite hacer la reserva",
		    JOptionPane.ERROR_MESSAGE);
	    return false;
	}
	return true;

    }

    @SuppressWarnings("deprecation")
    private boolean comprobarHora(String hora, String horaActual)
	    throws ClassNotFoundException, SQLException, ParseException {
	SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
	Date horaDate1 = formateador.parse(hora);
	Date horaDate2 = formateador.parse(horaActual);
	int hora1 = horaDate1.getHours();
	int hora2 = horaDate2.getHours();
	boolean resultado = hora2 - hora1 == 1 || hora2 - hora1 == 0;
	if (!resultado) {
	    JOptionPane.showMessageDialog(null,
		    "No se permite inscribir en la actividad debido a que el comienzo de la clase le falta menos de una hora para empezar",
		    "No se permite hacer la reserva",
		    JOptionPane.ERROR_MESSAGE);

	    return true;
	}
	return false;

    }

    void limpiaTabla() {
	try {
	    NonEditableModel temp = (NonEditableModel) table.getModel();
	    int a = temp.getRowCount();
	    for (int i = 0; i < a; i++)
		temp.removeRow(0);
	} catch (Exception e) {
	    System.out.println(e);
	}
    }

    private JButton getBtnCancelarTramite() {
	if (btnCancelarTramite == null) {
	    btnCancelarTramite = new JButton("Salir");
	    btnCancelarTramite.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    dispose();
		    mainView.setVisible(true);
		}
	    });
	    btnCancelarTramite.setBounds(419, 463, 186, 25);
	}
	return btnCancelarTramite;
    }

    private JButton getButton() {
	if (button == null) {
	    button = new JButton("");
	    button.setBorder(null);
	    button.setIcon(new ImageIcon(
		    CancelReservationView.class.getResource("/img/lupab.jpg")));
	    button.setBounds(521, 29, 46, 42);

	    button.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent arg0) {
		    Date fechaActual = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    SimpleDateFormat formateadorHora = new SimpleDateFormat(
			    "HH:mm:ss");
		    String fechaSistema = formateador.format(fechaActual);
		    String HoraSistema = formateadorHora.format(fechaActual);

		    List<SocioDto> socios = new ReservatePlaceController()
			    .listarSocios();
		    for (SocioDto s : socios) {

			if (s.getDni().equals(textDNI.getText().toString())) {
			    textNombre.setText(s.getNombre());
			    textApellidos.setText(s.getApellidos());
			    textFieldid.setText(String.valueOf(s.getId()));
			}
		    }
		    try {
			comprobacion();

			table.setEnabled(true);
			List<ActividadSocioReservaDto> actividades;
			actividades = new ReservatePlaceController()
				.listarActividades();

			for (ActividadSocioReservaDto r : actividades) {
			    if (r.is_LimitePlazas = true)
				if (comparaFecha(r.fecha, fechaSistema)
					|| comparaHorario(r.horario_Inicio,
						HoraSistema, r.fecha,
						fechaSistema)) {
				    Object[] nuevaLinea = new Object[6];
				    nuevaLinea[0] = r.nombre_Actividad;
				    nuevaLinea[1] = r.id_Actividad;
				    nuevaLinea[2] = r.fecha;
				    nuevaLinea[3] = r.horario_Inicio;
				    nuevaLinea[4] = r.horario_Fin;
				    nuevaLinea[5] = r.numero_Plazas;

				    modeloTabla.addRow(nuevaLinea);
				}
			}

			if (table.getRowCount() == 0) {
			    JOptionPane.showMessageDialog(null,
				    "-DNI de socio no registrado.\n-No existen reservas relacionadas con dicho DNI",
				    "No se han encontrado resultados",
				    JOptionPane.ERROR_MESSAGE);
			}

		    } catch (HeadlessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    }

		}

	    });
	}
	return button;
    }

    public boolean comparaFecha(String fecha, String fechaActual)
	    throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	boolean resultado = fechaDate1.compareTo(fechaDate2) > 0;

	return resultado;

    }

    public boolean comparaHorario(String hora, String horaaActual, String fecha,
	    String fechaActual) throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat formateador1 = new SimpleDateFormat("yyyy-MM-dd");
	Date horaDate1 = formateador.parse(hora);
	Date horaaDate2 = formateador.parse(horaaActual);
	Date fechaDate1 = formateador1.parse(fecha);
	Date fechaDate2 = formateador1.parse(fechaActual);
	boolean resultado = horaDate1.compareTo(horaaDate2) >= 0
		&& fechaDate1.compareTo(fechaDate2) == 0;
	return resultado;

    }

    public boolean comprobacion()
	    throws HeadlessException, ClassNotFoundException, SQLException {
	if (textDNI.getText().toString().equals("")) {
	    JOptionPane.showMessageDialog(null,
		    "Se debe proporcionar el dni del socio en el campo pertinente",
		    "Campos obligatorios", JOptionPane.ERROR_MESSAGE);
	    textNombre.setText(" ");
	    textApellidos.setText(" ");
	    return false;
	}
	if (!comprobarDNIBase(textDNI.getText().toString())) {
	    JOptionPane.showMessageDialog(null,
		    "El socio no se encuentra registrado como tal en la base de datos",
		    "Socio no registrado", JOptionPane.ERROR_MESSAGE);
	    textNombre.setText(" ");
	    textApellidos.setText(" ");
	    return false;
	}

	return false;
    }

    public boolean comprobarDNIBase(String dni)
	    throws ClassNotFoundException, SQLException {
	List<SocioDto> socios = new ReservatePlaceController().listarSocios();
	for (SocioDto s : socios) {
	    if (s.getDni().equals(dni)) {
		table.setEnabled(true);
		return true;

	    }
	}
	return false;
    }

    private boolean comprobarSocio(String dni, int idActividad)
	    throws ClassNotFoundException, SQLException {

	List<ActividadSocioReservaDto> matriculados = new ReservatePlaceController()
		.listarMatriculados();
	for (ActividadSocioReservaDto matriculadoEn : matriculados) {

	    if (matriculadoEn.dni_Socio.equalsIgnoreCase(dni) && Integer
		    .parseInt(matriculadoEn.id_Actividad) == idActividad) {
		JOptionPane.showMessageDialog(null,
			"No se permite inscribir en la actividad debido a que el socio ya esta inscrito en ella",
			"No se permite hacer la reserva",
			JOptionPane.ERROR_MESSAGE);
		return true;
	    }
	}
	return false;
    }

    private boolean comprobarPlazas(int idActividad)
	    throws ClassNotFoundException, SQLException {

	List<ActividadSocioReservaDto> actividades = new ReservatePlaceController()
		.listarActividades();
	for (ActividadSocioReservaDto actividad : actividades) {
	    if (Integer.parseInt(actividad.id_Actividad) == idActividad
		    && actividad.numero_Plazas == 0) {
		JOptionPane.showMessageDialog(null,
			"No se permite inscribir en la actividad debido a que no quedan plazas libres",
			"No se permite hacer la reserva",
			JOptionPane.ERROR_MESSAGE);
		return true;
	    }
	}
	return false;
    }

    private JScrollPane getScrollPane() {
	if (scrollPane == null) {
	    scrollPane = new JScrollPane();
	    scrollPane.setBounds(35, 133, 738, 289);
	    scrollPane.setViewportView(getTable());
	    scrollPane.setBorder(new TitledBorder(
		    UIManager.getBorder("TitledBorder.border"),
		    "Lista de actividades con plazas limitadas:",
		    TitledBorder.LEADING, TitledBorder.TOP, null, null));
	}
	return scrollPane;
    }

    private JTable getTable() {
	if (table == null) {
	    table = new JTable();
	    String[] nombreColumnas = { "nombre_Actividad", "id_Actividad",
		    "Fecha", "HInicio", "HFin", "Plazas" };
	    modeloTabla = new NonEditableModel(nombreColumnas, 0);
	    table = new JTable(modeloTabla);
	    table.getColumnModel().getColumn(2).setPreferredWidth(200);
	    table.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
		    if (table.getRowCount() > 0) {

			btnConfirmarInscripcion.setEnabled(true);
		    }
		}
	    });
	}
	return table;
    }

    private JLabel getLblId() {
	if (lblId == null) {
	    lblId = new JLabel("Id:");
	    lblId.setBounds(629, 93, 38, 15);
	}
	return lblId;
    }

    private JTextField getTextFieldid() {
	if (textFieldid == null) {
	    textFieldid = new JTextField();
	    textFieldid.setEditable(false);
	    textFieldid.setBounds(665, 90, 86, 20);
	    textFieldid.setColumns(10);
	}
	return textFieldid;
    }
    
    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
	mainView.setVisible(true);
    }

    public Main getMainView() {
	return mainView;
    }

    public void setMainView(Main mainView) {
	this.mainView = mainView;
    }
}