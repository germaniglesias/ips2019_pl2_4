package ui.admin;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.admin.AdminMainController;
import controller.admin.PayFacilityReservationController;
import dto.SocioDto;
import dto.SocioReservaInstalacionDto;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;

public class PayFacilityReservationView extends JDialog {

    private PayFacilityReservationController pf = new PayFacilityReservationController();
    private SocioReservaInstalacionDto reserva;
    private SocioDto socio;

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JPanel panelSur;
    private JPanel panelCentro;
    private JButton btnPagar;
    private JPanel panelDatosSocio;
    private JPanel panelDatosReserva;
    private JScrollPane scrollPane;
    private JTextArea txtrDatosReserva;
    private JLabel lblNombre;
    private JTextField txtNombre;
    private JLabel lblApellidos;
    private JTextField txtApellidos;
    private JLabel lblDni;
    private JTextField txtDni;
    private JPanel panelMetodoDePago;
    private JRadioButton rdbtnEnEfectivo;
    private JRadioButton rdbtnInsertarALa;

    private ButtonGroup bgroup;

    public PayFacilityReservationView(AdminMainController adminMainController,
	    SocioReservaInstalacionDto reserva) {
	this();
	this.reserva = reserva;
	buscarSocio();
    }

    private PayFacilityReservationView() {
	setTitle("Socio: Pagar Reserva Instalacion");
	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		cerrarVentana(arg0);
	    }
	});
	setModal(true);
	setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	setBounds(100, 100, 800, 500);
	setLocationRelativeTo(null);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(new BorderLayout(0, 0));
	setContentPane(contentPane);
	contentPane.add(getPanelCentro(), BorderLayout.CENTER);
	contentPane.add(getPanelSur(), BorderLayout.SOUTH);

	bgroup = new ButtonGroup();
	bgroup.add(getRdbtnEnEfectivo());
	bgroup.add(getRdbtnInsertarALa());
    }

    protected void cerrarVentana(WindowEvent arg0) {
	arg0.getWindow().dispose();
    }

    private JPanel getPanelSur() {
	if (panelSur == null) {
	    panelSur = new JPanel();
	    FlowLayout flowLayout = (FlowLayout) panelSur.getLayout();
	    flowLayout.setAlignment(FlowLayout.RIGHT);
	    panelSur.add(getBtnPagar());
	}
	return panelSur;
    }

    private JPanel getPanelCentro() {
	if (panelCentro == null) {
	    panelCentro = new JPanel();
	    panelCentro.setLayout(new BorderLayout(0, 0));
	    panelCentro.add(getPanelDatosSocio(), BorderLayout.NORTH);
	    panelCentro.add(getPanelDatosReserva(), BorderLayout.CENTER);
	    panelCentro.add(getPanelMetodoDePago(), BorderLayout.SOUTH);
	}
	return panelCentro;
    }

    private JButton getBtnPagar() {
	if (btnPagar == null) {
	    btnPagar = new JButton("Pagar");
	    btnPagar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    pagar();
		}
	    });
	    btnPagar.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return btnPagar;
    }

    private JPanel getPanelDatosSocio() {
	if (panelDatosSocio == null) {
	    panelDatosSocio = new JPanel();
	    panelDatosSocio.setBorder(new TitledBorder(
		    new EtchedBorder(EtchedBorder.LOWERED,
			    new Color(255, 255, 255), new Color(160, 160, 160)),
		    "Datos Socio", TitledBorder.LEADING, TitledBorder.TOP, null,
		    new Color(0, 0, 0)));
	    panelDatosSocio.add(getLblNombre());
	    panelDatosSocio.add(getTxtNombre());
	    panelDatosSocio.add(getLblApellidos());
	    panelDatosSocio.add(getTxtApellidos());
	    panelDatosSocio.add(getLblDni());
	    panelDatosSocio.add(getTxtDni());
	}
	return panelDatosSocio;
    }

    private JPanel getPanelDatosReserva() {
	if (panelDatosReserva == null) {
	    panelDatosReserva = new JPanel();
	    panelDatosReserva.setBorder(new TitledBorder(null, "Datos Reserva",
		    TitledBorder.LEADING, TitledBorder.TOP, null, null));
	    panelDatosReserva.setLayout(new BorderLayout(0, 0));
	    panelDatosReserva.add(getScrollPane(), BorderLayout.CENTER);
	}
	return panelDatosReserva;
    }

    private JScrollPane getScrollPane() {
	if (scrollPane == null) {
	    scrollPane = new JScrollPane();
	    scrollPane.setViewportView(getTxtrDatosReserva());
	}
	return scrollPane;
    }

    private JTextArea getTxtrDatosReserva() {
	if (txtrDatosReserva == null) {
	    txtrDatosReserva = new JTextArea();
	    txtrDatosReserva.setWrapStyleWord(true);
	    txtrDatosReserva.setEditable(false);
	    txtrDatosReserva.setEnabled(true);
	    txtrDatosReserva.setFont(new Font("Monospaced", Font.PLAIN, 15));
	    txtrDatosReserva.setText(
		    "Por favor introduzca su DNI en el campo de búsqueda.");
	}
	return txtrDatosReserva;
    }

    private JLabel getLblNombre() {
	if (lblNombre == null) {
	    lblNombre = new JLabel("Nombre:");
	    lblNombre.setEnabled(true);
	    lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblNombre;
    }

    private JTextField getTxtNombre() {
	if (txtNombre == null) {
	    txtNombre = new JTextField();
	    txtNombre.setEditable(false);
	    txtNombre.setEnabled(true);
	    txtNombre.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    txtNombre.setColumns(10);
	}
	return txtNombre;
    }

    private JLabel getLblApellidos() {
	if (lblApellidos == null) {
	    lblApellidos = new JLabel("Apellidos:");
	    lblApellidos.setEnabled(true);
	    lblApellidos.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblApellidos;
    }

    private JTextField getTxtApellidos() {
	if (txtApellidos == null) {
	    txtApellidos = new JTextField();
	    txtApellidos.setEditable(false);
	    txtApellidos.setEnabled(true);
	    txtApellidos.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    txtApellidos.setColumns(10);
	}
	return txtApellidos;
    }

    private JLabel getLblDni() {
	if (lblDni == null) {
	    lblDni = new JLabel("DNI:");
	    lblDni.setEnabled(true);
	    lblDni.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblDni;
    }

    private JTextField getTxtDni() {
	if (txtDni == null) {
	    txtDni = new JTextField();
	    txtDni.setEditable(false);
	    txtDni.setEnabled(true);
	    txtDni.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    txtDni.setColumns(10);
	}
	return txtDni;
    }

    private JPanel getPanelMetodoDePago() {
	if (panelMetodoDePago == null) {
	    panelMetodoDePago = new JPanel();
	    panelMetodoDePago.setBorder(new TitledBorder(null,
		    "Metodos de pago", TitledBorder.LEADING, TitledBorder.TOP,
		    null, null));
	    panelMetodoDePago.add(getRdbtnEnEfectivo());
	    panelMetodoDePago.add(getRdbtnInsertarALa());
	}
	return panelMetodoDePago;
    }

    private JRadioButton getRdbtnEnEfectivo() {
	if (rdbtnEnEfectivo == null) {
	    rdbtnEnEfectivo = new JRadioButton("En Efectivo");
	    rdbtnEnEfectivo.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return rdbtnEnEfectivo;
    }

    private JRadioButton getRdbtnInsertarALa() {
	if (rdbtnInsertarALa == null) {
	    rdbtnInsertarALa = new JRadioButton("Insertar a la cuota");
	    rdbtnInsertarALa.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return rdbtnInsertarALa;
    }

    // MIS METODOS
    private void habilitarElementos() {
	getLblApellidos().setEnabled(true);
	getLblDni().setEnabled(true);
	getLblNombre().setEnabled(true);
	getTxtApellidos().setEnabled(true);
	getTxtDni().setEnabled(true);
	getTxtNombre().setEnabled(true);
	getTxtrDatosReserva().setEnabled(true);
    }

    private void buscarSocio() {
	socio = pf.findSocioById(reserva.getId_Socio());

	if (socio != null) {
	    cargarDatosSocio();
	    cargarDatosReservaActual();
	    habilitarElementos();
	} else {
	    JOptionPane.showMessageDialog(null,
		    "No existe un Socio con ese DNI. Por favor intentelo de nuevo.");
	}
    }

    private void cargarDatosSocio() {
	getTxtNombre().setText(socio.getNombre());
	getTxtApellidos().setText(socio.getApellidos());
	getTxtDni().setText(socio.getDni());
    }

    private void cargarDatosReservaActual() {
	// reserva = pf.findReservaSocio(socio);
	if (reserva != null) {
	    getTxtrDatosReserva().setText(reserva.datos());
	} else {
	    getTxtrDatosReserva().setText(
		    "No tiene ninguna Reserva de Instalación a esta hora.");
	}
    }

    protected void pagar() {
	if (reserva != null) {
	    if (!pf.isPagado(reserva.getId())) {
		if (getRdbtnEnEfectivo().isSelected()) {
		    pf.insertReservaImpagada(reserva.getId());
		    JOptionPane.showMessageDialog(null,
			    "La reserva se añadió a pagados en efectivo correctamente.");
		} else if (getRdbtnInsertarALa().isSelected()) {
		    pf.insertarReservaEnEfectivo(reserva.getId());
		    JOptionPane.showMessageDialog(null,
			    "La reserva se añadió a la cuota correctamente.");
		} else
		    JOptionPane.showMessageDialog(null,
			    "No ha seleccionado ningún método de pago.");
	    } else {
		JOptionPane.showMessageDialog(null,
			"Esta reserva ya está pagada.");
	    }
	} else {
	    JOptionPane.showMessageDialog(null,
		    "No tiene ninguna reserva a pagar.");
	}
    }
}
