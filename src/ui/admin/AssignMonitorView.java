package ui.admin;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import dto.MonitorDto;
import ui.Main;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.SpinnerDateModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import java.text.SimpleDateFormat;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

public class AssignMonitorView extends JDialog {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JDialog frame;
    private JButton btnAssign;
    private JLabel lblAvailableMonitors;
    private JComboBox<MonitorDto> comboBoxMonitors;
    private JLabel lblName;
    private JLabel labelHour;
    private JPanel panelWeek;
    private JCheckBox checkBoxLunes;
    private JCheckBox checkBoxMartes;
    private JCheckBox checkBoxMiercoles;
    private JCheckBox checkBoxJueves;
    private JCheckBox checkBoxViernes;
    private JCheckBox checkBoxSabado;
    private JLabel labelStartDate;
    private JLabel labelEndDate;
    private JSpinner spinnerStartDate;
    private JButton btnAddList;
    private JLabel lblListaDeCambios;
    private JScrollPane scrollPane;
    private JTable table;
    private JSpinner spinnerEndDate;
    private JButton btnDeleteList;

    public AssignMonitorView(Main main) {
	setModal(true);
	frame = new JDialog();
	frame.setModal(true);
	frame.setResizable(false);
	frame.setTitle("ADMINISTRADOR: Asignar monitor");
	frame.setBackground(Color.WHITE);
	frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	frame.setBounds(100, 100, 800, 500);
	frame.setLocationRelativeTo(main);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(null);
	contentPane.add(getBtnAssign());
	contentPane.add(getLblAvailableMonitors());
	contentPane.add(getComboBoxMonitors());
	frame.setContentPane(contentPane);
	contentPane.add(getLblName());
	contentPane.add(getLabelHour());
	contentPane.add(getPanelWeek());
	contentPane.add(getLabelStartDate());
	contentPane.add(getLabelEndDate());
	contentPane.add(getSpinnerStartDate());
	contentPane.add(getBtnAddList());
	contentPane.add(getLblListaDeCambios());
	contentPane.add(getScrollPane());
	contentPane.add(getSpinnerEndDate());
	contentPane.add(getBtnDeleteList());
    }

    public JDialog getFrame() {
	return frame;
    }

    public JButton getBtnAssign() {
	if (btnAssign == null) {
	    btnAssign = new JButton("Asignar");
	    btnAssign.setFocusPainted(false);
	    btnAssign.setEnabled(false);
	    btnAssign.setForeground(Color.WHITE);
	    btnAssign.setMnemonic('A');
	    btnAssign.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnAssign.setBackground(new Color(0, 128, 0));
	    btnAssign.setBounds(618, 401, 154, 42);
	}
	return btnAssign;
    }

    private JLabel getLblAvailableMonitors() {
	if (lblAvailableMonitors == null) {
	    lblAvailableMonitors = new JLabel("Monitores disponibles: ");
	    lblAvailableMonitors.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblAvailableMonitors.setBounds(356, 139, 147, 42);
	}
	return lblAvailableMonitors;
    }

    public JComboBox<MonitorDto> getComboBoxMonitors() {
	if (comboBoxMonitors == null) {
	    comboBoxMonitors = new JComboBox<MonitorDto>();
	    comboBoxMonitors.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    comboBoxMonitors.setBounds(513, 139, 259, 42);
	}
	return comboBoxMonitors;
    }

    public JLabel getLblName() {
	if (lblName == null) {
	    lblName = new JLabel("PLANIFICACION");
	    lblName.setBackground(Color.WHITE);
	    lblName.setHorizontalAlignment(SwingConstants.CENTER);
	    lblName.setFont(new Font("Tahoma", Font.BOLD, 18));
	    lblName.setBounds(20, 11, 752, 42);
	}
	return lblName;
    }

    public JLabel getLabelHour() {
	if (labelHour == null) {
	    labelHour = new JLabel("8:00:00 - 9:00:00");
	    labelHour.setBackground(Color.WHITE);
	    labelHour.setHorizontalAlignment(SwingConstants.CENTER);
	    labelHour.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    labelHour.setBounds(20, 53, 752, 26);
	}
	return labelHour;
    }

    private JPanel getPanelWeek() {
	if (panelWeek == null) {
	    panelWeek = new JPanel();
	    panelWeek.setBorder(new TitledBorder(null, "D\u00EDas de la semana",
		    TitledBorder.LEADING, TitledBorder.TOP, null, null));
	    panelWeek.setBounds(10, 80, 762, 55);
	    panelWeek.add(getCheckBoxLunes());
	    panelWeek.add(getCheckBoxMartes());
	    panelWeek.add(getCheckBoxMiercoles());
	    panelWeek.add(getCheckBoxJueves());
	    panelWeek.add(getCheckBoxViernes());
	    panelWeek.add(getCheckBoxSabado());
	}
	return panelWeek;
    }

    public JCheckBox getCheckBoxLunes() {
	if (checkBoxLunes == null) {
	    checkBoxLunes = new JCheckBox("Lunes");
	    checkBoxLunes.setFocusPainted(false);
	    checkBoxLunes.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return checkBoxLunes;
    }

    public JCheckBox getCheckBoxMartes() {
	if (checkBoxMartes == null) {
	    checkBoxMartes = new JCheckBox("Martes");
	    checkBoxMartes.setFocusPainted(false);
	    checkBoxMartes.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return checkBoxMartes;
    }

    public JCheckBox getCheckBoxMiercoles() {
	if (checkBoxMiercoles == null) {
	    checkBoxMiercoles = new JCheckBox("Miércoles");
	    checkBoxMiercoles.setFocusPainted(false);
	    checkBoxMiercoles.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return checkBoxMiercoles;
    }

    public JCheckBox getCheckBoxJueves() {
	if (checkBoxJueves == null) {
	    checkBoxJueves = new JCheckBox("Jueves");
	    checkBoxJueves.setFocusPainted(false);
	    checkBoxJueves.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return checkBoxJueves;
    }

    public JCheckBox getCheckBoxViernes() {
	if (checkBoxViernes == null) {
	    checkBoxViernes = new JCheckBox("Viernes");
	    checkBoxViernes.setFocusPainted(false);
	    checkBoxViernes.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return checkBoxViernes;
    }

    public JCheckBox getCheckBoxSabado() {
	if (checkBoxSabado == null) {
	    checkBoxSabado = new JCheckBox("Sábado");
	    checkBoxSabado.setFocusPainted(false);
	    checkBoxSabado.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return checkBoxSabado;
    }

    private JLabel getLabelStartDate() {
	if (labelStartDate == null) {
	    labelStartDate = new JLabel("Fecha Inicio:");
	    labelStartDate.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    labelStartDate.setBounds(10, 149, 101, 22);
	}
	return labelStartDate;
    }

    public JSpinner getSpinnerStartDate() {
	if (spinnerStartDate == null) {
	    SimpleDateFormat model = new SimpleDateFormat("dd-MM-yyyy");
	    spinnerStartDate = new JSpinner(new SpinnerDateModel());
	    spinnerStartDate.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    spinnerStartDate.setEditor(new JSpinner.DateEditor(spinnerStartDate,
		    model.toPattern()));
	    spinnerStartDate.setBounds(91, 146, 115, 28);
	}
	return spinnerStartDate;
    }

    private JLabel getLabelEndDate() {
	if (labelEndDate == null) {
	    labelEndDate = new JLabel("Fecha fin:");
	    labelEndDate.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    labelEndDate.setBounds(10, 194, 78, 22);
	}
	return labelEndDate;
    }

    public JSpinner getSpinnerEndDate() {
	if (spinnerEndDate == null) {
	    SimpleDateFormat model = new SimpleDateFormat("dd-MM-yyyy");
	    spinnerEndDate = new JSpinner(new SpinnerDateModel());
	    spinnerEndDate.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    spinnerEndDate.setEditor(
		    new JSpinner.DateEditor(spinnerEndDate, model.toPattern()));
	    spinnerEndDate.setBounds(91, 196, 115, 28);

	}
	return spinnerEndDate;
    }

    public JButton getBtnAddList() {
	if (btnAddList == null) {
	    btnAddList = new JButton("Añadir a la lista");
	    btnAddList.setFocusPainted(false);
	    btnAddList.setBackground(new Color(240, 97, 79));
	    btnAddList.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnAddList.setBounds(618, 295, 154, 42);
	}
	return btnAddList;
    }

    private JLabel getLblListaDeCambios() {
	if (lblListaDeCambios == null) {
	    lblListaDeCambios = new JLabel("LISTA DE CAMBIOS");
	    lblListaDeCambios.setHorizontalAlignment(SwingConstants.CENTER);
	    lblListaDeCambios.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblListaDeCambios.setBounds(10, 241, 762, 26);
	}
	return lblListaDeCambios;
    }

    private JScrollPane getScrollPane() {
	if (scrollPane == null) {
	    scrollPane = new JScrollPane();
	    scrollPane.setBounds(20, 295, 571, 148);
	    scrollPane.setViewportView(getTable());
	}
	return scrollPane;
    }

    public JTable getTable() {
	if (table == null) {
	    table = new JTable() {
		private static final long serialVersionUID = 1L;

		public boolean isCellEditable(int row, int column) {
		    return false;
		};
	    };
	    table.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    table.setFillsViewportHeight(true);
	    table.setRowSelectionAllowed(true);
	    table.setColumnSelectionAllowed(false);
	    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

	}
	return table;
    }

    public JButton getBtnDeleteList() {
	if (btnDeleteList == null) {
	    btnDeleteList = new JButton("Borrar de la lista");
	    btnDeleteList.setEnabled(false);
	    btnDeleteList.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnDeleteList.setFocusPainted(false);
	    btnDeleteList.setBackground(new Color(240, 97, 79));
	    btnDeleteList.setBounds(618, 348, 154, 42);
	}
	return btnDeleteList;
    }
}
