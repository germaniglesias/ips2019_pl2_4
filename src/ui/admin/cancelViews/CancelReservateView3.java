package ui.admin.cancelViews;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.admin.CancelReservaController;
import dto.ActividadSocioReservaDto;
import dto.InstalacionDto;
import ui.Main;
import ui.admin.CancelReservateMainView;
import utils.NonEditableModel;

public class CancelReservateView3 extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    java.util.Date hoy = new Date();
    final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;
    private final JPanel contentPanel = new JPanel();
    private JButton btBuscar;
    private JScrollPane scTable;
    private JTable tableReserva;
    private NonEditableModel modeloTabla;
    private JButton btModificarReserva;
    private JButton btCancelar;
    private Object ID;
    private JLabel lblIdDeLa;
    private JTextField textIDReserva;
    private JPanel panel;
    private JLabel lblHoraInicio;
    private JLabel lblHoraFin;
    private JSpinner spinnerHoraInicio;
    private JSpinner spinnerHoraFin;
    private JLabel lblh;
    private JLabel lblh_1;
    @SuppressWarnings("rawtypes")
    private JComboBox comboBoxActividades;
    private JTextField textFieldActividadID;
    private JLabel lblIdactividad;

    @SuppressWarnings("rawtypes")
    private JComboBox comboBoxDIA;
    @SuppressWarnings("rawtypes")
    private JComboBox comboBoxMES;
    @SuppressWarnings("rawtypes")
    private JComboBox comboBoxAÑO;
    @SuppressWarnings("rawtypes")
    private JComboBox comboBoxMes;
    @SuppressWarnings("rawtypes")
    private JComboBox comboBoxFecha;
    @SuppressWarnings("rawtypes")
    private JComboBox comboBoxDia;

    private Main mainView;

    /**
     * Launch the application.
     */

    /**
     * Create the dialog.
     * 
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public CancelReservateView3(Main mainView) throws ClassNotFoundException, SQLException {
	setModal(true);
	// gbd= new GestorBaseDatos();
	// gestion = new GestionApliIMP();
	setTitle("Cancelar Reserva [by RangoFechas]");
	this.setMainView(mainView);
	setBounds(100, 100, 800, 500);
	setLocationRelativeTo(mainView);

	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	getContentPane().setLayout(new BorderLayout());
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(null);
	contentPanel.add(getBtBuscar());
	contentPanel.add(getScTable());
	contentPanel.add(getBtModificarReserva());
	contentPanel.add(getBtCancelar());
	contentPanel.add(getLblIdDeLa());
	contentPanel.add(getTextIDReserva());
	contentPanel.add(getPanel());
	contentPanel.add(getComboBoxActividades());
	contentPanel.add(getTextFieldActividadID());
	contentPanel.add(getLblIdactividad());

    }

    private JButton getBtBuscar() {
	if (btBuscar == null) {
	    btBuscar = new JButton("");
	    btBuscar.setBorder(null);
	    btBuscar.setIcon(new ImageIcon(
		    CancelReservateView.class.getResource("/img/lupab.jpg")));
	    btBuscar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    limpiaTabla();
		    ;
		    String campo = textFieldActividadID.getText().toString();
		    List<ActividadSocioReservaDto> reservas;
		    reservas = new CancelReservaController().listarReservas();
		    for (ActividadSocioReservaDto r : reservas) {
			if (r.id_Actividad.equalsIgnoreCase(campo)) {
			    Object[] nuevaLinea = new Object[6];
			    nuevaLinea[0] = r.dni_Socio;
			    nuevaLinea[1] = r.fecha;
			    nuevaLinea[2] = r.horario_Inicio;
			    nuevaLinea[3] = r.horario_Fin;
			    nuevaLinea[4] = r.id_Actividad;
			    nuevaLinea[5] = r.id_SocioReservaActividad;

			    modeloTabla.addRow(nuevaLinea);
			}
		    }

		}
	    });
	    btBuscar.setBounds(421, 11, 45, 42);
	}
	return btBuscar;
    }

    private JScrollPane getScTable()
	    throws ClassNotFoundException, SQLException {
	if (scTable == null) {
	    scTable = new JScrollPane();

	    scTable.setBounds(36, 78, 838, 113);
	    scTable.setViewportView(getTableReserva());
	}
	return scTable;
    }

    private JTable getTableReserva()
	    throws ClassNotFoundException, SQLException {
	if (tableReserva == null) {
	    String[] nombreColumnas = { "DNI_Socio", "Fecha Reserva",
		    "Hora De Inicio", "Hora de Fin", "ID Actividad",
	    "ID Reserva" };
	    modeloTabla = new NonEditableModel(nombreColumnas, 0);
	    tableReserva = new JTable(modeloTabla);
	    tableReserva.addMouseListener(new MouseAdapter() {

		@Override
		public void mouseClicked(MouseEvent arg0) {
		    if (tableReserva.getRowCount() > 0) {
			btModificarReserva.setEnabled(true);
			for (int i = 0; i < modeloTabla.getRowCount(); i++) {

			    if (tableReserva.getSelectedRow() == i) {

				ID = modeloTabla.getValueAt(i, 4);
				textIDReserva.setText(" " + ID);

			    }
			}
		    }
		}
	    });

	}
	return tableReserva;
    }

    private JButton getBtModificarReserva() {
	if (btModificarReserva == null) {
	    btModificarReserva = new JButton("Cancelar reserva");
	    btModificarReserva.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {

		    // reservarInstalacion();
		    String hora = metodo() + "-"
			    + comboBoxMES.getSelectedItem().toString() + "-"
			    + comboBoxAÑO.getSelectedItem().toString();
		    String hora2 = metodo2() + "-"
			    + comboBoxMes.getSelectedItem().toString() + "-"
			    + comboBoxFecha.getSelectedItem().toString();
		    String hi = spinnerHoraInicio.getValue().toString()
			    + ":00:00";
		    String hf = spinnerHoraFin.getValue().toString() + ":00:00";

		    // if (reservaDuplicada(ID,IDInstalacionEscogida,hi, hf,
		    // hora)==true) {
		    //  						
		    //  						return;
		    //  					}
		    // if (reservarInstalacion() == true) {
		    new CancelReservaController().deleteReserva(hi, hf, hora, hora2,
			    ID);
		    //  		    new CancelReservaController().deleteReserva2(hora, hora2,
		    //  			    ID);
		    //  		    new CancelReservaController().deleteReserva(hi, hf, ID);
		    JOptionPane.showMessageDialog(null,
			    "Ha realizado su cancelación con éxito");
		    limpiaTabla();
		    // dispose();
		    // }

		}

		private String metodo() {
		    String o = "1";
		    String o1 = "2";
		    String o2 = "3";
		    String o3 = "4";
		    String o4 = "5";
		    String o5 = "6";
		    String o6 = "7";
		    String o7 = "8";
		    String o8 = "9";

		    if(comboBoxDIA.getSelectedItem().toString()== o || comboBoxDIA.getSelectedItem().toString() == o1||
			    comboBoxDIA.getSelectedItem().toString() == o2 || comboBoxDIA.getSelectedItem().toString() == o3||
			    comboBoxDIA.getSelectedItem().toString() == o4 || comboBoxDIA.getSelectedItem().toString() == o5||
			    comboBoxDIA.getSelectedItem().toString() == o6 || comboBoxDIA.getSelectedItem().toString() == o7||
			    comboBoxDIA.getSelectedItem().toString() == o8) {
			return "0" +comboBoxDIA.getSelectedItem().toString() ;

		    }
		    return comboBoxDIA.getSelectedItem().toString();

		}

		private String metodo2() {
		    String o = "1";
		    String o1 = "2";
		    String o2 = "3";
		    String o3 = "4";
		    String o4 = "5";
		    String o5 = "6";
		    String o6 = "7";
		    String o7 = "8";
		    String o8 = "9";

		    if(comboBoxDia.getSelectedItem().toString()== o || comboBoxDia.getSelectedItem().toString() == o1||
			    comboBoxDia.getSelectedItem().toString() == o2 || comboBoxDia.getSelectedItem().toString() == o3||
			    comboBoxDia.getSelectedItem().toString() == o4 || comboBoxDia.getSelectedItem().toString() == o5||
			    comboBoxDia.getSelectedItem().toString() == o6 || comboBoxDia.getSelectedItem().toString() == o7||
			    comboBoxDia.getSelectedItem().toString() == o8) {
			return "0" +comboBoxDia.getSelectedItem().toString() ;

		    }
		    return comboBoxDia.getSelectedItem().toString();

		}
	    });
	    btModificarReserva.setEnabled(false);
	    btModificarReserva.setBounds(320, 339, 148, 41);
	}
	return btModificarReserva;
    }


    private JButton getBtCancelar() {
	if (btCancelar == null) {
	    btCancelar = new JButton("Cancelar trámite");
	    btCancelar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    dispose();
		    CancelReservateMainView c = new CancelReservateMainView(mainView); 
		    c.setVisible(true);
		}
	    });
	    btCancelar.setBounds(480, 339, 135, 41);
	}
	return btCancelar;
    }

    public String getNombreActividad(int id)
	    throws ClassNotFoundException, SQLException {
	List<InstalacionDto> act = new CancelReservaController()
		.listarInstalaciones();
	for (InstalacionDto ins : act) {
	    if (ins.getId() == id) {

		return ins.getNombre();
	    }
	}
	return null;
    }

    private JLabel getLblIdDeLa() {
	if (lblIdDeLa == null) {
	    lblIdDeLa = new JLabel("ID Actividad a cancelar:");
	    lblIdDeLa.setBounds(27, 262, 149, 16);
	}
	return lblIdDeLa;
    }

    private JTextField getTextIDReserva() {
	if (textIDReserva == null) {
	    textIDReserva = new JTextField();
	    textIDReserva.setEditable(false);
	    textIDReserva.setBounds(186, 259, 116, 22);
	    textIDReserva.setColumns(10);
	}
	return textIDReserva;
    }

    private JPanel getPanel() {
	if (panel == null) {
	    panel = new JPanel();
	    panel.setBorder(new TitledBorder(
		    UIManager.getBorder("TitledBorder.border"),
		    "Seleccione horario:",
		    TitledBorder.LEADING, TitledBorder.TOP, null, null));
	    panel.setBounds(366, 203, 467, 125);
	    panel.setLayout(null);
	    panel.add(getComboBoxDIA());
	    panel.add(getComboBoxMES());
	    panel.add(getComboBox_1());
	    panel.add(getLblHoraInicio());
	    panel.add(getLblHoraFin());
	    panel.add(getSpinnerHoraInicio());
	    panel.add(getSpinnerHoraFin());
	    panel.add(getLblh());
	    panel.add(getLblh_1());
	    panel.add(getComboBoxMes());
	    panel.add(getComboBoxFecha());
	    panel.add(getComboBoxDia());
	}
	return panel;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private JComboBox getComboBoxDIA() {
	if (comboBoxDIA == null) {
	    comboBoxDIA = new JComboBox();
	    comboBoxDIA.setEnabled(false);

	    comboBoxDIA.setBounds(10, 29, 54, 22);
	    String[] Mes31 = new String[] { "1", "2", "3", "4", "5", "6", "7",
		    "8", "9", "10", "11", "12", "13", "14", "15", "16", "17",
		    "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
		    "28", "29", "30", "31" };
	    comboBoxDIA.setModel(new DefaultComboBoxModel(Mes31));
	}
	return comboBoxDIA;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private JComboBox getComboBoxMES() {
	if (comboBoxMES == null) {
	    comboBoxMES = new JComboBox();
	    comboBoxMES.setModel(
		    new DefaultComboBoxModel(new String[] { "1", "2", "3", "4",
			    "5", "6", "7", "8", "9", "10", "11", "12" }));
	    comboBoxMES.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    comboBoxDIA.setEnabled(true);
		    String[] febrero = new String[] { "1", "2", "3", "4", "5",
			    "6", "7", "8", "9", "10", "11", "12", "13", "14",
			    "15", "16", "17", "18", "19", "20", "21", "22",
			    "23", "24", "25", "26", "27", "28" };
		    String[] Mes30 = new String[] { "1", "2", "3", "4", "5",
			    "6", "7", "8", "9", "10", "11", "12", "13", "14",
			    "15", "16", "17", "18", "19", "20", "21", "22",
			    "23", "24", "25", "26", "27", "28", "29", "30" };
		    String[] Mes31 = new String[] { "1", "2", "3", "4", "5",
			    "6", "7", "8", "9", "10", "11", "12", "13", "14",
			    "15", "16", "17", "18", "19", "20", "21", "22",
			    "23", "24", "25", "26", "27", "28", "29", "30",
		    "31" };

		    if (comboBoxMES.getSelectedItem().toString().equals("2")) {
			comboBoxDIA.setModel(new DefaultComboBoxModel(febrero));
		    } else if (comboBoxMES.getSelectedItem().toString()
			    .equals("1")
			    || comboBoxMES.getSelectedItem().toString()
			    .equals("3")
			    || comboBoxMES.getSelectedItem().toString()
			    .equals("5")
			    || comboBoxMES.getSelectedItem().toString()
			    .equals("7")
			    || comboBoxMES.getSelectedItem().toString()
			    .equals("8")
			    || comboBoxMES.getSelectedItem().toString()
			    .equals("10")
			    || comboBoxMES.getSelectedItem().toString()
			    .equals("12")) {
			comboBoxDIA.setModel(new DefaultComboBoxModel(Mes31));
		    } else {
			comboBoxDIA.setModel(new DefaultComboBoxModel(Mes30));
		    }
		}
	    });
	    comboBoxMES.setBounds(74, 29, 68, 22);
	}
	return comboBoxMES;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private JComboBox getComboBox_1() {
	if (comboBoxAÑO == null) {
	    comboBoxAÑO = new JComboBox();
	    String[] años = new String[86];

	    for (int i = 19; i < 99; i++) {
		años[i - 19] = "20" + i;
	    }
	    comboBoxAÑO.setModel(new DefaultComboBoxModel(años));
	    comboBoxAÑO.setBounds(152, 29, 86, 22);

	}
	return comboBoxAÑO;
    }

    private JLabel getLblHoraInicio() {
	if (lblHoraInicio == null) {
	    lblHoraInicio = new JLabel("Hora Inicio:");
	    lblHoraInicio.setBounds(256, 46, 68, 16);
	}
	return lblHoraInicio;
    }

    private JLabel getLblHoraFin() {
	if (lblHoraFin == null) {
	    lblHoraFin = new JLabel("Hora Fin:");
	    lblHoraFin.setBounds(256, 75, 56, 16);
	}
	return lblHoraFin;
    }

    private JSpinner getSpinnerHoraInicio() {
	if (spinnerHoraInicio == null) {
	    spinnerHoraInicio = new JSpinner();
	    spinnerHoraInicio.setModel(new SpinnerNumberModel(8, 8, 22, 1));
	    spinnerHoraInicio.setBounds(341, 43, 68, 22);
	    spinnerHoraInicio.addChangeListener(new ChangeListener() {
		public void stateChanged(ChangeEvent arg0) {
		    spinnerHoraFin.setModel(new SpinnerNumberModel(
			    ((Integer) getSpinnerHoraInicio().getValue()) + 1,
			    ((Integer) getSpinnerHoraInicio().getValue()) + 1,
			    ((Integer) getSpinnerHoraInicio().getValue()) + 2,
			    1));
		}
	    });
	}
	return spinnerHoraInicio;
    }

    private JSpinner getSpinnerHoraFin() {
	if (spinnerHoraFin == null) {
	    spinnerHoraFin = new JSpinner();
	    spinnerHoraFin.setModel(new SpinnerNumberModel(9, 9, 23, 1));
	    spinnerHoraFin.setBounds(340, 72, 69, 22);
	}
	return spinnerHoraFin;
    }

    private JLabel getLblh() {
	if (lblh == null) {
	    lblh = new JLabel(":00h");
	    lblh.setBounds(411, 46, 56, 16);
	}
	return lblh;
    }

    private JLabel getLblh_1() {
	if (lblh_1 == null) {
	    lblh_1 = new JLabel(":00h");
	    lblh_1.setBounds(411, 75, 56, 16);
	}
	return lblh_1;
    }


    public boolean reservaDuplicada(Object iD2, int id_instalacion, int fi,
	    int ff, String horario)
		    throws ClassNotFoundException, SQLException {
	int finter = 0;
	boolean rel = false;
	if (ff - fi == 2)
	    finter = ff - 1;
	List<ActividadSocioReservaDto> reservas = new CancelReservaController()
		.listarReservas();
	for (ActividadSocioReservaDto r : reservas) {

	    if (r.id_Actividad == String.valueOf(iD2) && r.fecha.equals(horario)
		    && r.id_Instalacion == id_instalacion)
		return false;
	    if (r.fecha.equals(horario) && (r.horario_Fin == String.valueOf(fi)
		    || r.horario_Inicio == String.valueOf(finter)
		    || r.horario_Fin == String.valueOf(ff)
		    || r.horario_Fin == String.valueOf(finter))) {
		rel = true;
		JOptionPane.showMessageDialog(null,
			"Ya existe una reserva para esa instalacion con la misma fecha y el siguiente horario  -->"
				+ r.horario_Inicio + ":00-" + r.horario_Fin
				+ ":00",
				"Reserva duplicada", JOptionPane.ERROR_MESSAGE);

	    }

	}

	return rel;
    }

    void limpiaTabla() {
	try {
	    NonEditableModel temp = (NonEditableModel) tableReserva.getModel();
	    int a = temp.getRowCount();
	    for (int i = 0; i < a; i++)
		temp.removeRow(0);
	} catch (Exception e) {
	    System.out.println(e);
	}
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private JComboBox getComboBoxActividades() {
	final List<ActividadSocioReservaDto> actividades;
	if (comboBoxActividades == null) {
	    comboBoxActividades = new JComboBox();
	    comboBoxActividades.setBounds(36, 24, 179, 17);
	}
	actividades = new CancelReservaController().ListarActividades();
	comboBoxActividades.addItem("----------");
	for (ActividadSocioReservaDto ac : actividades) {
	    comboBoxActividades.addItem(ac.nombre_Actividad);

	}
	comboBoxActividades.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		textFieldActividadID.setText(String.valueOf(getIdActividad(
			actividades,
			comboBoxActividades.getSelectedItem().toString())));
		//		nombreActividad = comboBoxActividades.getSelectedItem()
		//			.toString();
	    }
	});
	return comboBoxActividades;

    }

    public String getIdActividad(List<ActividadSocioReservaDto> list,
	    String nombre) {
	for (ActividadSocioReservaDto ins : list) {
	    if (ins.nombre_Actividad.equals(nombre)) {
		return ins.id_Actividad;
	    }
	}
	return null;
    }

    private JTextField getTextFieldActividadID() {
	if (textFieldActividadID == null) {
	    textFieldActividadID = new JTextField();
	    textFieldActividadID.setEditable(false);
	    textFieldActividadID.setBounds(345, 24, 50, 20);
	    textFieldActividadID.setColumns(10);
	}
	return textFieldActividadID;
    }

    private JLabel getLblIdactividad() {
	if (lblIdactividad == null) {
	    lblIdactividad = new JLabel("ID Actividad:");
	    lblIdactividad.setBounds(244, 27, 74, 14);
	}
	return lblIdactividad;
    }





    @SuppressWarnings({ "rawtypes", "unchecked" })
    private JComboBox getComboBoxMes() {
	if (comboBoxMes == null) {
	    comboBoxMes = new JComboBox();
	    comboBoxMes.setModel(
		    new DefaultComboBoxModel(new String[] { "1", "2", "3", "4",
			    "5", "6", "7", "8", "9", "10", "11", "12" }));
	    comboBoxMes.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    comboBoxDia.setEnabled(true);
		    String[] febrero = new String[] { "1", "2", "3", "4", "5",
			    "6", "7", "8", "9", "10", "11", "12", "13", "14",
			    "15", "16", "17", "18", "19", "20", "21", "22",
			    "23", "24", "25", "26", "27", "28" };
		    String[] Mes30 = new String[] { "1", "2", "3", "4", "5",
			    "6", "7", "8", "9", "10", "11", "12", "13", "14",
			    "15", "16", "17", "18", "19", "20", "21", "22",
			    "23", "24", "25", "26", "27", "28", "29", "30" };
		    String[] Mes31 = new String[] { "1", "2", "3", "4", "5",
			    "6", "7", "8", "9", "10", "11", "12", "13", "14",
			    "15", "16", "17", "18", "19", "20", "21", "22",
			    "23", "24", "25", "26", "27", "28", "29", "30",
		    "31" };

		    if (comboBoxMes.getSelectedItem().toString().equals("2")) {
			comboBoxDia.setModel(new DefaultComboBoxModel(febrero));
		    } else if (comboBoxMes.getSelectedItem().toString()
			    .equals("1")
			    || comboBoxMes.getSelectedItem().toString()
			    .equals("3")
			    || comboBoxMes.getSelectedItem().toString()
			    .equals("5")
			    || comboBoxMes.getSelectedItem().toString()
			    .equals("7")
			    || comboBoxMes.getSelectedItem().toString()
			    .equals("8")
			    || comboBoxMes.getSelectedItem().toString()
			    .equals("10")
			    || comboBoxMes.getSelectedItem().toString()
			    .equals("12")) {
			comboBoxDia.setModel(new DefaultComboBoxModel(Mes31));
		    } else {
			comboBoxDia.setModel(new DefaultComboBoxModel(Mes30));
		    }
		}
	    });
	    comboBoxMes.setBounds(74, 72, 68, 22);
	}
	return comboBoxMes;
    }


    @SuppressWarnings({ "rawtypes", "unchecked" })
    private JComboBox getComboBoxFecha() {
	if (comboBoxFecha == null) {
	    comboBoxFecha = new JComboBox();
	    String[] años = new String[86];

	    for (int i = 19; i < 99; i++) {
		años[i - 19] = "20" + i;
	    }
	    comboBoxFecha.setModel(new DefaultComboBoxModel(años));
	    comboBoxFecha.setBounds(152, 73, 86, 22);

	}
	return comboBoxFecha;
    }



    @SuppressWarnings({ "rawtypes", "unchecked" })
    private JComboBox getComboBoxDia() {
	if (comboBoxDia == null) {
	    comboBoxDia = new JComboBox();
	    comboBoxDia.setEnabled(false);

	    comboBoxDia.setBounds(10, 73, 54, 22);
	    String[] Mes31 = new String[] { "1", "2", "3", "4", "5", "6", "7",
		    "8", "9", "10", "11", "12", "13", "14", "15", "16", "17",
		    "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
		    "28", "29", "30", "31" };
	    comboBoxDia.setModel(new DefaultComboBoxModel(Mes31));
	}
	return comboBoxDia;
    }

    public Main getMainView() {
	return mainView;
    }

    public void setMainView(Main mainView) {
	this.mainView = mainView;
    }

    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
	mainView.setVisible(true);
    }

}
