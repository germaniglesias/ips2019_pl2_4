package ui.admin.cancelViews;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.admin.CancelReservaController;
import dto.ActividadSocioReservaDto;
import dto.InstalacionDto;
import ui.Main;
import ui.admin.CancelReservateMainView;
import utils.NonEditableModel;

public class CancelReservateView extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    java.util.Date hoy = new Date();
    final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;
    private final JPanel contentPanel = new JPanel();
    private JButton btBuscar;
    private JScrollPane scTable;
    private JTable tableReserva;
    private NonEditableModel modeloTabla;
    private JButton btModificarReserva;
    private JButton btCancelar;
    private Object ID;
    private JLabel lblIdDeLa;
    private JTextField textIDReserva;
    private JPanel panel;
    private JLabel lblHoraInicio;
    private JLabel lblHoraFin;
    private JSpinner spinnerHoraInicio;
    private JSpinner spinnerHoraFin;
    private JLabel lblh;
    private JLabel lblh_1;
    @SuppressWarnings("rawtypes")
    private JComboBox comboBoxActividades;
    private JTextField textFieldActividadID;
    private JLabel lblIdactividad;

    private Main mainView;

    /**
     * Launch the application.
     */

    /**
     * Create the dialog.
     * 
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public CancelReservateView(Main mainView) throws ClassNotFoundException, SQLException {
	setModal(true);
	// gbd= new GestorBaseDatos();
	// gestion = new GestionApliIMP();
	setTitle("Cancelar Reserva [by Hora]");
	this.setMainView(mainView);
	setBounds(100, 100, 905, 560);
	setLocationRelativeTo(mainView);

	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	getContentPane().setLayout(new BorderLayout());
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(null);
	contentPanel.add(getBtBuscar());
	contentPanel.add(getScTable());
	contentPanel.add(getBtModificarReserva());
	contentPanel.add(getBtCancelar());
	contentPanel.add(getLblIdDeLa());
	contentPanel.add(getTextIDReserva());
	contentPanel.add(getPanel());
	contentPanel.add(getComboBoxActividades());
	contentPanel.add(getTextFieldActividadID());
	contentPanel.add(getLblIdactividad());

    }

    private JButton getBtBuscar() {
	if (btBuscar == null) {
	    btBuscar = new JButton("");
	    btBuscar.setBorder(null);
	    btBuscar.setIcon(new ImageIcon(
		    CancelReservateView.class.getResource("/img/lupab.jpg")));
	    btBuscar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    limpiaTabla();

		    Date fechaActual = new Date();
		    SimpleDateFormat formateador = new SimpleDateFormat(
			    "yyyy-MM-dd");
		    SimpleDateFormat formateadorHora = new SimpleDateFormat(
			    "HH:mm:ss");
		    String fechaSistema = formateador.format(fechaActual);
		    String HoraSistema = formateadorHora.format(fechaActual);
		    String campo = textFieldActividadID.getText().toString();
		    List<ActividadSocioReservaDto> reservas;
		    reservas = new CancelReservaController().listarReservas();
		    for (ActividadSocioReservaDto r : reservas) {
			if (r.id_Actividad.equalsIgnoreCase(campo))
			    try {
				if (comparaFecha(r.fecha, fechaSistema)
				    || comparaHorario(r.horario_Inicio,
					    HoraSistema, r.fecha,
					    fechaSistema)) {
				Object[] nuevaLinea = new Object[7];
				nuevaLinea[0] = r.dni_Socio;
				nuevaLinea[1] = r.fecha;
				nuevaLinea[2] = r.horario_Inicio;
				nuevaLinea[3] = r.horario_Fin;
				nuevaLinea[4] = r.id_Actividad;
				nuevaLinea[5] = r.id_SocioReservaActividad;
				nuevaLinea[6] = r.nombre_Instalacion;

				modeloTabla.addRow(nuevaLinea);
				}
			    } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			    }
		    }

		}
	    });
	    btBuscar.setBounds(539, 11, 45, 42);
	}
	return btBuscar;
    }

    public boolean comparaFecha(String fecha, String fechaActual)
	    throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
	Date fechaDate1 = formateador.parse(fecha);
	Date fechaDate2 = formateador.parse(fechaActual);
	boolean resultado = fechaDate1.compareTo(fechaDate2) > 0;

	return resultado;

    }

    public boolean comparaHorario(String hora, String horaaActual, String fecha,
	    String fechaActual) throws ParseException {

	SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat formateador1 = new SimpleDateFormat("yyyy-MM-dd");
	Date horaDate1 = formateador.parse(hora);
	Date horaaDate2 = formateador.parse(horaaActual);
	Date fechaDate1 = formateador1.parse(fecha);
	Date fechaDate2 = formateador1.parse(fechaActual);
	boolean resultado = horaDate1.compareTo(horaaDate2) >= 0
		&& fechaDate1.compareTo(fechaDate2) == 0;
	return resultado;

    }

    private JScrollPane getScTable()
	    throws ClassNotFoundException, SQLException {
	if (scTable == null) {
	    scTable = new JScrollPane();
	    scTable.setBorder(
		    new TitledBorder(UIManager.getBorder("TitledBorder.border"),

			    "Lista de reservas de segun activivdad:",
			    TitledBorder.LEADING,

			    TitledBorder.TOP, null, null));

	    scTable.setBounds(27, 79, 838, 180);
	    scTable.setViewportView(getTableReserva());
	}
	return scTable;
    }

    private JTable getTableReserva()
	    throws ClassNotFoundException, SQLException {
	if (tableReserva == null) {
	    String[] nombreColumnas = { "DNI_Socio", "Fecha Reserva",
		    "Hora De Inicio", "Hora de Fin", "ID Actividad",
		    "ID Reserva", "Instalacion" };
	    modeloTabla = new NonEditableModel(nombreColumnas, 0);
	    tableReserva = new JTable(modeloTabla);
	    tableReserva.addMouseListener(new MouseAdapter() {

		@Override
		public void mouseClicked(MouseEvent arg0) {
		    if (tableReserva.getRowCount() > 0) {
			btModificarReserva.setEnabled(true);
			for (int i = 0; i < modeloTabla.getRowCount(); i++) {

			    if (tableReserva.getSelectedRow() == i) {

				ID = modeloTabla.getValueAt(i, 4);
				textIDReserva.setText(" " + ID);

			    }
			}
		    }
		}
	    });

	}
	return tableReserva;
    }

    private JButton getBtModificarReserva() {
	if (btModificarReserva == null) {
	    btModificarReserva = new JButton("Cancelar ");
	    btModificarReserva.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {

		    // reservarInstalacion();
		    // String
		    // hora=comboBoxDIA.getSelectedItem().toString()+"-"+comboBoxMES.getSelectedItem().toString()+"-"+comboBoxAÑO.getSelectedItem().toString();
		    String hi = spinnerHoraInicio.getValue().toString()
			    + ":00:00";
		    String hf = spinnerHoraFin.getValue().toString() + ":00:00";

		    // if (reservaDuplicada(ID,IDInstalacionEscogida,hi, hf,
		    // hora)==true) {
//						
//						return;
//					}
		    // if (reservarInstalacion() == true) {
		    // new CancelReservaController().deleteReserva(
		    // hi,hf,hora, ID);
		    new CancelReservaController().deleteReserva(hi, hf, ID);
		    JOptionPane.showMessageDialog(null,
			    "Ha realizado su cancelación con éxito");
		    limpiaTabla();
		    // dispose();
		    // }

		}
	    });
	    btModificarReserva.setEnabled(false);
	    btModificarReserva.setBounds(302, 446, 148, 41);
	}
	return btModificarReserva;
    }

    private JButton getBtCancelar() {
	if (btCancelar == null) {
	    btCancelar = new JButton("Salir");
	    btCancelar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    dispose();
		    mainView.setVisible(true);
		}
	    });
	    btCancelar.setBounds(489, 446, 135, 41);
	}
	return btCancelar;
    }

    public String getNombreActividad(int id)
	    throws ClassNotFoundException, SQLException {
	List<InstalacionDto> act = new CancelReservaController()
		.listarInstalaciones();
	for (InstalacionDto ins : act) {
	    if (ins.getId() == id) {

		return ins.getNombre();
	    }
	}
	return null;
    }

    private JLabel getLblIdDeLa() {
	if (lblIdDeLa == null) {
	    lblIdDeLa = new JLabel("ID Actividad a cancelar:");
	    lblIdDeLa.setBounds(27, 308, 149, 16);
	}
	return lblIdDeLa;
    }

    private JTextField getTextIDReserva() {
	if (textIDReserva == null) {
	    textIDReserva = new JTextField();
	    textIDReserva.setEditable(false);
	    textIDReserva.setBounds(155, 305, 116, 22);
	    textIDReserva.setColumns(10);
	}
	return textIDReserva;
    }

    private JPanel getPanel() {
	if (panel == null) {
	    panel = new JPanel();
	    panel.setBorder(
		    new TitledBorder(UIManager.getBorder("TitledBorder.border"),
			    "Rango de horas:", TitledBorder.LEADING,
			    TitledBorder.TOP, null, null));
	    panel.setBounds(345, 287, 482, 122);
	    panel.setLayout(null);
	    panel.add(getLblHoraInicio());
	    panel.add(getLblHoraFin());
	    panel.add(getSpinnerHoraInicio());
	    panel.add(getSpinnerHoraFin());
	    panel.add(getLblh());
	    panel.add(getLblh_1());
	}
	return panel;
    }

    private JLabel getLblHoraInicio() {
	if (lblHoraInicio == null) {
	    lblHoraInicio = new JLabel("Hora Inicio:");
	    lblHoraInicio.setBounds(26, 75, 68, 16);
	}
	return lblHoraInicio;
    }

    private JLabel getLblHoraFin() {
	if (lblHoraFin == null) {
	    lblHoraFin = new JLabel("Hora Fin:");
	    lblHoraFin.setBounds(256, 75, 56, 16);
	}
	return lblHoraFin;
    }

    private JSpinner getSpinnerHoraInicio() {
	if (spinnerHoraInicio == null) {
	    spinnerHoraInicio = new JSpinner();
	    spinnerHoraInicio.setModel(new SpinnerNumberModel(8, 1, 22, 1));
	    spinnerHoraInicio.setBounds(88, 72, 68, 22);
//	    spinnerHoraInicio.addChangeListener(new ChangeListener() {
//		public void stateChanged(ChangeEvent arg0) {
//		    spinnerHoraFin.setModel(new SpinnerNumberModel(
//			    ((Integer) getSpinnerHoraInicio().getValue()) + 1,
//			    ((Integer) getSpinnerHoraInicio().getValue()) + 1,
//			    ((Integer) getSpinnerHoraInicio().getValue()) + 2,
//			    1));
//		}
//	    });
	}
	return spinnerHoraInicio;
    }

    private JSpinner getSpinnerHoraFin() {
	if (spinnerHoraFin == null) {
	    spinnerHoraFin = new JSpinner();
	    spinnerHoraFin.setModel(new SpinnerNumberModel(9, 1, 23, 1));
	    spinnerHoraFin.setBounds(305, 72, 69, 22);
	}
	return spinnerHoraFin;
    }

    private JLabel getLblh() {
	if (lblh == null) {
	    lblh = new JLabel(":00h");
	    lblh.setBounds(166, 75, 56, 16);
	}
	return lblh;
    }

    private JLabel getLblh_1() {
	if (lblh_1 == null) {
	    lblh_1 = new JLabel(":00h");
	    lblh_1.setBounds(384, 75, 56, 16);
	}
	return lblh_1;
    }

    public boolean reservaDuplicada(Object iD2, int id_instalacion, int fi,
	    int ff, String horario)
	    throws ClassNotFoundException, SQLException {
	int finter = 0;
	boolean rel = false;
	if (ff - fi == 2)
	    finter = ff - 1;
	List<ActividadSocioReservaDto> reservas = new CancelReservaController()
		.listarReservas();
	for (ActividadSocioReservaDto r : reservas) {

	    if (r.id_Actividad == String.valueOf(iD2) && r.fecha.equals(horario)
		    && r.id_Instalacion == id_instalacion)
		return false;
	    if (r.fecha.equals(horario) && (r.horario_Fin == String.valueOf(fi)
		    || r.horario_Inicio == String.valueOf(finter)
		    || r.horario_Fin == String.valueOf(ff)
		    || r.horario_Fin == String.valueOf(finter))) {
		rel = true;
		JOptionPane.showMessageDialog(null,
			"Ya existe una reserva para esa instalacion con la misma fecha y el siguiente horario  -->"
				+ r.horario_Inicio + ":00-" + r.horario_Fin
				+ ":00",
			"Reserva duplicada", JOptionPane.ERROR_MESSAGE);

	    }

	}

	return rel;
    }

    void limpiaTabla() {
	try {
	    NonEditableModel temp = (NonEditableModel) tableReserva.getModel();
	    int a = temp.getRowCount();
	    for (int i = 0; i < a; i++)
		temp.removeRow(0);
	} catch (Exception e) {
	    System.out.println(e);
	}
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private JComboBox getComboBoxActividades() {
	final List<ActividadSocioReservaDto> actividades;
	if (comboBoxActividades == null) {
	    comboBoxActividades = new JComboBox();
	    comboBoxActividades.setBounds(36, 24, 179, 17);
	}
	actividades = new CancelReservaController().ListarActividades();
	comboBoxActividades.addItem("----------");
	for (ActividadSocioReservaDto ac : actividades) {
	    comboBoxActividades.addItem(ac.nombre_Actividad);

	}
	comboBoxActividades.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		textFieldActividadID.setText(String.valueOf(getIdActividad(
			actividades,
			comboBoxActividades.getSelectedItem().toString())));
	    }
	});
	return comboBoxActividades;

    }

    public String getIdActividad(List<ActividadSocioReservaDto> list,
	    String nombre) {
	for (ActividadSocioReservaDto ins : list) {
	    if (ins.nombre_Actividad.equals(nombre)) {
		return ins.id_Actividad;
	    }
	}
	return null;
    }

    private JTextField getTextFieldActividadID() {
	if (textFieldActividadID == null) {
	    textFieldActividadID = new JTextField();
	    textFieldActividadID.setEditable(false);
	    textFieldActividadID.setBounds(444, 24, 85, 20);
	    textFieldActividadID.setColumns(10);
	}
	return textFieldActividadID;
    }

    private JLabel getLblIdactividad() {
	if (lblIdactividad == null) {
	    lblIdactividad = new JLabel("ID Actividad:");
	    lblIdactividad.setBounds(340, 27, 74, 14);
	}
	return lblIdactividad;
    }

    public Main getMainView() {
	return mainView;
    }

    public void setMainView(Main mainView) {
	this.mainView = mainView;
    }

    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
	mainView.setVisible(true);
    }
}
