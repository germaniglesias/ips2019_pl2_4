package ui.admin;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.admin.AdminMainController;
import controller.admin.CancelActivitySelectedDayController;
import dto.ActividadDto;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.Component;

import java.awt.GridLayout;
import java.util.Date;
import java.util.List;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import lu.tudor.santec.jtimechooser.JTimeChooser;
import utils.GestorCalendario;

import com.toedter.calendar.JDateChooser;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;

import javax.swing.border.LineBorder;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.beans.PropertyChangeEvent;
import javax.swing.JList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CancelActivityView extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JLabel lblActividad;
    private JComboBox<ActividadDto> cbActividad;
    private JLabel lbInstalacionTexto;
    private JPanel pnCancelar;
    private JPanel pnDiaSemana;
    private JPanel pnDiaConcreto;
    private JRadioButton rdbtnDaConcreto;
    private JRadioButton rdbtnUnoOVarios;
    private final ButtonGroup buttonGroup = new ButtonGroup();
    private JLabel lbDiaSemana;
    private JComboBox<String> cbDiaSemana;
    private JLabel lbDiaConcreto;
    private JLabel lblEligeUnRango;
    private JPanel pnRango;
    private JLabel lblHoraInicio;
    private JLabel lblHoraFin;
    private JTimeChooser timeChooser;
    private JTimeChooser timeChooser_1;
    private JButton btnCancelarActividad;
    private JLabel lblHora;
    private JComboBox<String> cbHora;

    private CancelActivitySelectedDayController casdc;
    private ProcesaDias pd;
    private JDateChooser dateChooser;
    private JList<String> listaDias;
    private DefaultListModel<String> modeloLista;
    private JButton btnAceptar;
    private JButton btnCancelarPlanificacin;
    private JButton btnAadirALa;
    private AdminMainController adminMainController;

    /**
     * Create the frame.
     * 
     * @param adminMainController
     */
    public CancelActivityView(AdminMainController adminMainController) {
	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		adminMainController.getView().setVisible(true);
		arg0.getWindow().dispose();
	    }
	});
	this.adminMainController = adminMainController;
	pd = new ProcesaDias();
	casdc = new CancelActivitySelectedDayController();

	setModal(true);
	setTitle("Cancelar actividades");
	setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	setBounds(100, 100, 692, 530);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(null);
	contentPane.add(getLblActividad());
	contentPane.add(getCbActividad());
	contentPane.add(getLbInstalacionTexto());
	contentPane.add(getPnCancelar());
	contentPane.add(getRdbtnDaConcreto());
	contentPane.add(getRdbtnUnoOVarios());
	contentPane.add(getBtnAceptar());
	setResizable(false);
	setLocationRelativeTo(adminMainController.getView());

	iniciarVentana();
    }

    private void iniciarVentana() {
	mostrarActividades();

	habilitarPaneles();

	cbActividad.setSelectedIndex(0);
	dateChooser.getJCalendar().getDayChooser()
	.addDateEvaluator(new GestorCalendario(
		(ActividadDto) cbActividad.getSelectedItem()));
	btnCancelarActividad.setEnabled(false);
	btnCancelarPlanificacin.setEnabled(false);
    }

    private void mostrarActividades() {
	List<ActividadDto> actividades = casdc.getListActivities();
	getCbActividad().setModel(new DefaultComboBoxModel<ActividadDto>(
		actividades.toArray(new ActividadDto[actividades.size()])));

    }

    private JLabel getLblActividad() {
	if (lblActividad == null) {
	    lblActividad = new JLabel("Actividad:");
	    lblActividad.setFont(new Font("Tahoma", Font.PLAIN, 26));
	    lblActividad.setBounds(118, 11, 142, 37);
	}
	return lblActividad;
    }

    private JComboBox<ActividadDto> getCbActividad() {
	if (cbActividad == null) {
	    cbActividad = new JComboBox<ActividadDto>();
	    cbActividad.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    cbActividad.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    gestionarActividadElegida();
		}
	    });
	    cbActividad.setBounds(331, 20, 285, 25);
	}
	return cbActividad;
    }

    private void gestionarActividadElegida() {
	ActividadDto a = (ActividadDto) cbActividad.getSelectedItem();
	dateChooser.getJCalendar().getDayChooser()
	.addDateEvaluator(new GestorCalendario((ActividadDto) a));

    }

    private JLabel getLbInstalacionTexto() {
	if (lbInstalacionTexto == null) {
	    lbInstalacionTexto = new JLabel("");
	    lbInstalacionTexto.setForeground(Color.DARK_GRAY);
	    lbInstalacionTexto.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    lbInstalacionTexto.setBounds(463, 10, 215, 37);
	}
	return lbInstalacionTexto;
    }

    private JPanel getPnCancelar() {
	if (pnCancelar == null) {
	    pnCancelar = new JPanel();
	    pnCancelar.setBounds(10, 97, 668, 368);
	    pnCancelar.setLayout(new GridLayout(1, 2, 0, 0));
	    pnCancelar.add(getPnDiaSemana());
	    pnCancelar.add(getPnDiaConcreto());
	}
	return pnCancelar;
    }

    private JPanel getPnDiaSemana() {
	if (pnDiaSemana == null) {
	    pnDiaSemana = new JPanel();
	    pnDiaSemana.setBorder(new LineBorder(new Color(0, 0, 0), 2));
	    pnDiaSemana.setLayout(null);
	    pnDiaSemana.add(getLbDiaSemana());
	    pnDiaSemana.add(getCbDiaSemana());
	    pnDiaSemana.add(getLblEligeUnRango());
	    pnDiaSemana.add(getPnRango());

	    JScrollPane spListaDias = new JScrollPane();
	    spListaDias.setBounds(10, 246, 314, 76);
	    pnDiaSemana.add(spListaDias);
	    spListaDias.setViewportView(getListaDias());
	    pnDiaSemana.add(getBtnCancelarPlanificacin());
	    pnDiaSemana.add(getBtnAadirALa());
	}
	return pnDiaSemana;
    }

    private JPanel getPnDiaConcreto() {
	if (pnDiaConcreto == null) {
	    pnDiaConcreto = new JPanel();
	    pnDiaConcreto.setBorder(new LineBorder(new Color(0, 0, 0), 2));
	    pnDiaConcreto.setLayout(null);
	    pnDiaConcreto.add(getLbDiaConcreto());
	    pnDiaConcreto.add(getBtnCancelarActividad());
	    pnDiaConcreto.add(getLblHora());
	    pnDiaConcreto.add(getCbHora());
	    pnDiaConcreto.add(getDateChooser());
	}
	return pnDiaConcreto;
    }

    private JRadioButton getRdbtnDaConcreto() {
	if (rdbtnDaConcreto == null) {
	    rdbtnDaConcreto = new JRadioButton("Día concreto");
	    rdbtnDaConcreto.setSelected(true);
	    rdbtnDaConcreto.setActionCommand("Concreto");
	    buttonGroup.add(rdbtnDaConcreto);
	    rdbtnDaConcreto.addItemListener(pd);
	    rdbtnDaConcreto.setBounds(463, 67, 122, 23);
	}
	return rdbtnDaConcreto;
    }

    private JRadioButton getRdbtnUnoOVarios() {
	if (rdbtnUnoOVarios == null) {
	    rdbtnUnoOVarios = new JRadioButton(
		    "Uno o varios días de la semana");
	    rdbtnUnoOVarios.setActionCommand("Semana");
	    rdbtnUnoOVarios.setSelected(false);
	    buttonGroup.add(rdbtnUnoOVarios);
	    rdbtnUnoOVarios.addItemListener(pd);
	    rdbtnUnoOVarios.setBounds(82, 67, 230, 23);
	}
	return rdbtnUnoOVarios;
    }

    private JLabel getLbDiaSemana() {
	if (lbDiaSemana == null) {
	    lbDiaSemana = new JLabel("Elige un día de la semana:");
	    lbDiaSemana.setDisplayedMnemonic('l');
	    lbDiaSemana.setLabelFor(getCbDiaSemana());
	    lbDiaSemana.setToolTipText("");
	    lbDiaSemana.setHorizontalAlignment(SwingConstants.CENTER);
	    lbDiaSemana.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    lbDiaSemana.setBounds(10, 11, 314, 30);
	}
	return lbDiaSemana;
    }

    private JComboBox<String> getCbDiaSemana() {
	if (cbDiaSemana == null) {
	    cbDiaSemana = new JComboBox<String>();
	    cbDiaSemana.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    mostrarRangoHorasPorDia();
		}
	    });
	    String[] diasSemana = { "Lunes", "Martes", "Miércoles", "Jueves",
		    "Viernes", "Sábado" };
	    cbDiaSemana.setModel(new DefaultComboBoxModel<>(diasSemana));
	    cbDiaSemana.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    cbDiaSemana.setBounds(10, 52, 314, 20);
	}
	return cbDiaSemana;
    }

    private void mostrarRangoHorasPorDia() {

    }

    private JLabel getLbDiaConcreto() {
	if (lbDiaConcreto == null) {
	    lbDiaConcreto = new JLabel("Elige un día concreto:");
	    lbDiaConcreto.setDisplayedMnemonic('g');
	    lbDiaConcreto.setLabelFor(getDateChooser());
	    lbDiaConcreto.setHorizontalAlignment(SwingConstants.CENTER);
	    lbDiaConcreto.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    lbDiaConcreto.setBounds(10, 11, 314, 30);
	}
	return lbDiaConcreto;
    }

    private JLabel getLblEligeUnRango() {
	if (lblEligeUnRango == null) {
	    lblEligeUnRango = new JLabel("Elige un rango de horas:");
	    lblEligeUnRango.setHorizontalAlignment(SwingConstants.CENTER);
	    lblEligeUnRango.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    lblEligeUnRango.setBounds(10, 83, 314, 30);
	}
	return lblEligeUnRango;
    }

    private JPanel getPnRango() {
	if (pnRango == null) {
	    pnRango = new JPanel();
	    pnRango.setBorder(new TitledBorder(null, "Rango de horas",
		    TitledBorder.LEADING, TitledBorder.TOP, null, null));
	    pnRango.setBounds(10, 124, 314, 91);
	    pnRango.setLayout(null);
	    pnRango.add(getLblHoraInicio());
	    pnRango.add(getLblHoraFin());
	    pnRango.add(getTimeChooserInicio());
	    pnRango.add(getTimeChooserFinal());
	}
	return pnRango;
    }

    private JLabel getLblHoraInicio() {
	if (lblHoraInicio == null) {
	    lblHoraInicio = new JLabel("Hora inicio:");
	    lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    lblHoraInicio.setBounds(10, 23, 109, 22);
	}
	return lblHoraInicio;
    }

    private JLabel getLblHoraFin() {
	if (lblHoraFin == null) {
	    lblHoraFin = new JLabel("Hora fin:");
	    lblHoraFin.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    lblHoraFin.setBounds(10, 56, 109, 29);
	}
	return lblHoraFin;
    }

    private JTimeChooser getTimeChooserInicio() {
	if (timeChooser == null) {
	    timeChooser = new JTimeChooser();
	    timeChooser.getTimeField()
	    .setFont(new Font("Tahoma", Font.PLAIN, 16));
	    timeChooser.setBounds(115, 23, 189, 22);
	}
	return timeChooser;
    }

    private JTimeChooser getTimeChooserFinal() {
	if (timeChooser_1 == null) {
	    timeChooser_1 = new JTimeChooser();
	    timeChooser_1.getTimeField()
	    .setFont(new Font("Tahoma", Font.PLAIN, 16));
	    timeChooser_1.setBounds(115, 56, 189, 24);
	}
	return timeChooser_1;
    }

    private void gestionarDiaElegido() {
	modificarComboHoras();
	btnCancelarActividad.setEnabled(true);
    }

    /**
     * Cambia el valor del combo de horas para el día concreto, dependiendo de
     * las horas a las que haya la actividad ese día
     */
    private void modificarComboHoras() {
	if (!dateChooser.getDateFormatString().equals("")) {
	    // Sacamos las horas de la actividad ese día
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	    List<String> horas = casdc.getHorasParaActividadDia(
		    (ActividadDto) cbActividad.getSelectedItem(),
		    sdf.format(dateChooser.getDate()));
	    // Las añadimos al combo
	    cbHora.setModel(new DefaultComboBoxModel<String>(
		    horas.toArray(new String[horas.size()])));
	}

    }

    private JButton getBtnCancelarActividad() {
	if (btnCancelarActividad == null) {
	    btnCancelarActividad = new JButton("Cancelar actividad");
	    btnCancelarActividad.setEnabled(false);
	    btnCancelarActividad.setMnemonic('n');
	    btnCancelarActividad.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    cancelarActividadDiaConcreto();
		}
	    });
	    btnCancelarActividad.setBounds(164, 334, 160, 23);
	}
	return btnCancelarActividad;
    }

    private void cancelarActividadDiaConcreto() {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	String error = casdc.cancelarActividad((ActividadDto) cbActividad.getSelectedItem(),
		sdf.format(dateChooser.getDate()),
		(String) cbHora.getSelectedItem());
	if (error == "") {
	    resetear();
	    JOptionPane.showMessageDialog(this, "Se ha anulado la actividad",
		    "Actividad anulada", JOptionPane.INFORMATION_MESSAGE);
	} else {
	    JOptionPane.showMessageDialog(this,
		    "La actividad que ha intentado eliminar tiene socios apuntados:\n"
			    + error);
	}
    }

    private void resetear() {
	dateChooser.setDate(null);
	dateChooser.validate();
	dateChooser.repaint();
	cbHora.setModel(new DefaultComboBoxModel<String>());
	cbDiaSemana.setSelectedIndex(0);

	getTimeChooserInicio().setTime(JTimeChooser.MIDNIGHT);
	getTimeChooserFinal().setTime(JTimeChooser.MIDNIGHT);

	modeloLista.removeAllElements();
	listaDias.validate();
	listaDias.repaint();

	btnCancelarPlanificacin.setEnabled(false);
	btnCancelarActividad.setEnabled(false);
    }

    /**
     * Habilita o deshabilita los elementos de los paneles dependiendo del radio
     * botón seleccionado
     * 
     * @param pnDiaSemana
     * @param pnDiaConcreto
     */
    private void habilitarPaneles() {
	this.pnDiaSemana.setEnabled(rdbtnUnoOVarios.isSelected());
	this.pnDiaConcreto.setEnabled(rdbtnDaConcreto.isSelected());

	for (Component c : this.pnDiaConcreto.getComponents()) {
	    c.setEnabled(rdbtnDaConcreto.isSelected());
	}
	for (Component c : this.pnDiaSemana.getComponents()) {
	    c.setEnabled(rdbtnUnoOVarios.isSelected());
	}
	btnCancelarActividad.setEnabled(false);
	btnCancelarPlanificacin.setEnabled(false);
    }

    /**
     * Clase para la gestión de eventos de los radiobotones
     * 
     * @author UO264703
     *
     */
    class ProcesaDias implements ItemListener {

	@Override
	public void itemStateChanged(ItemEvent e) {
	    if (e.getStateChange() == ItemEvent.SELECTED) {
		habilitarPaneles();
	    }
	}
    }

    private JLabel getLblHora() {
	if (lblHora == null) {
	    lblHora = new JLabel("Hora:");
	    lblHora.setDisplayedMnemonic('H');
	    lblHora.setLabelFor(getCbHora());
	    lblHora.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    lblHora.setBounds(27, 131, 102, 23);
	}
	return lblHora;
    }

    private JComboBox<String> getCbHora() {
	if (cbHora == null) {
	    cbHora = new JComboBox<String>();

	    cbHora.setBounds(156, 131, 147, 23);
	}
	return cbHora;
    }

    private JDateChooser getDateChooser() {
	if (dateChooser == null) {
	    dateChooser = new JDateChooser();
	    dateChooser.addPropertyChangeListener(new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent e) {
		    if (dateChooser.getDate() != null)
			gestionarDiaElegido();
		}
	    });
	    dateChooser.setDateFormatString("d-MM-yyyy");
	    dateChooser.setBounds(57, 52, 229, 20);
	    dateChooser.setMinSelectableDate(new Date());
	}
	return dateChooser;
    }

    private JList<String> getListaDias() {
	if (listaDias == null) {
	    modeloLista = new DefaultListModel<String>();
	    listaDias = new JList<String>(modeloLista);
	    listaDias.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    if (e.getClickCount() == 2)
			eliminarDiaRango();
		}
	    });
	}
	return listaDias;
    }

    private void eliminarDiaRango() {
	int index = listaDias.getSelectedIndex();
	if (index != -1) {
	    modeloLista.removeElementAt(index);
	    if (modeloLista.isEmpty())
		btnCancelarPlanificacin.setEnabled(false);
	}
    }

    private JButton getBtnAceptar() {
	if (btnAceptar == null) {
	    btnAceptar = new JButton("Aceptar");
	    btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    btnAceptar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    adminMainController.getView().setVisible(true);
		    dispose();
		}
	    });
	    btnAceptar.setMnemonic('p');
	    btnAceptar.setBounds(569, 476, 109, 25);
	}
	return btnAceptar;
    }

    private JButton getBtnCancelarPlanificacin() {
	if (btnCancelarPlanificacin == null) {
	    btnCancelarPlanificacin = new JButton("Cancelar planificación");
	    btnCancelarPlanificacin.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    cancelarPlanificacionSemanal();
		}
	    });
	    btnCancelarPlanificacin.setEnabled(false);
	    btnCancelarPlanificacin.setBounds(154, 334, 170, 23);
	}
	return btnCancelarPlanificacin;
    }

    private void cancelarPlanificacionSemanal() {

	for (int i = 0; i < modeloLista.getSize(); i++) {
	    String[] actividad = modeloLista.get(i).split("-");
	    String error = casdc.cancelarPlanificacionActividad(actividad);
	    if (error == "") {
		JOptionPane.showMessageDialog(this,
			"Se han eliminado las actividades en el rango",
			"Actividades eliminadas", JOptionPane.INFORMATION_MESSAGE);
		resetear();
	    } else {
		JOptionPane.showMessageDialog(this,
			"Las siguientes actividades que ha intentado eliminar tienen socios apuntados:\n"
				+ error);
	    }
	}
    }

    private JButton getBtnAadirALa() {
	if (btnAadirALa == null) {
	    btnAadirALa = new JButton("Añadir a la lista");
	    btnAadirALa.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    añadirDiaALista();
		}
	    });
	    btnAadirALa.setMnemonic('d');
	    btnAadirALa.setBounds(203, 218, 121, 23);
	}
	return btnAadirALa;
    }

    private void añadirDiaALista() {
	String diaSemana = (String) cbDiaSemana.getSelectedItem();
	String horaInicio = getTimeChooserInicio().getFormatedTime();
	String horaFinal = getTimeChooserFinal().getFormatedTime();

	String nuevoElementoLista = diaSemana + "-" + horaInicio + "-"
		+ horaFinal;
	if (!modeloLista.contains(nuevoElementoLista))
	    modeloLista.addElement(nuevoElementoLista);
	btnCancelarPlanificacin.setEnabled(true);
    }
}
