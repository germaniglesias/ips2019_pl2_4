package ui.admin;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dto.ActividadDto;
import dto.ActividadesNecesitanRecursosDto;
import controller.admin.AdminMainController;
import controller.admin.NewActivityController;

import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JDialog;

import dto.RecursoDto;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class NewActivityView extends JDialog {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JPanel panelDatos;
    private JPanel panelBoton;
    private JButton btnAadirActividad;
    private JLabel lblNombreActividad;
    private JTextField txtNombreactividad;
    private JPanel panelNombre;
    private JPanel panelIntensidad;
    private JLabel lblIntensidad;
    private JCheckBox chckbxBaja;
    private JCheckBox chckbxModerada;
    private JCheckBox chckbxAlta;
    private AdminMainController adminMainView;
    private JPanel panelRecursos;
    private JLabel lblRecursos;
    private JComboBox<RecursoDto> cmbRecursos;
    private JButton btnAnadir;
    private JPanel panelRecursosAnadidos;
    private JPanel panelUsaRecursos;
    private JCheckBox chckbxNecesitaRecursos;
    private JPanel panelRecursosBuscados;
    private JPanel panelCmbRecursos;
    private JScrollPane scrollPaneRecursosSeleccionados;
    private JTextArea textAreaRecursosSeleccionados;
    private JLabel lblRecursosSeleccionados;

    private List<RecursoDto> listaRecursos = new ArrayList<RecursoDto>();
    private List<RecursoDto> listaRecursosAniadidos = new ArrayList<RecursoDto>();

    /**
     * Create the frame.
     */
    public NewActivityView(AdminMainController listFacilitiesController) {
	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	this.adminMainView = listFacilitiesController;
	setTitle("New Activity");
	setResizable(false);
	setModal(true);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	setBounds(100, 100, 800, 500);
	setLocationRelativeTo(adminMainView.getView());
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(new BorderLayout(0, 0));
	contentPane.add(getPanelDatos(), BorderLayout.CENTER);
	contentPane.add(getPanelBoton(), BorderLayout.SOUTH);
	contentPane.add(getPanelRecursos(), BorderLayout.EAST);

	cargarRecursos();
	cambiarEstadoRecursos();
    }

    private JPanel getPanelDatos() {
	if (panelDatos == null) {
	    panelDatos = new JPanel();
	    panelDatos.setLayout(new GridLayout(3, 1, 0, 0));
	    panelDatos.add(getPanelNombre());
	    panelDatos.add(getPanelIntensidad());
	    panelDatos.add(getPanelUsaRecursos());
	}
	return panelDatos;
    }
    private JPanel getPanelBoton() {
	if (panelBoton == null) {
	    panelBoton = new JPanel();
	    FlowLayout flowLayout = (FlowLayout) panelBoton.getLayout();
	    flowLayout.setAlignment(FlowLayout.RIGHT);
	    panelBoton.add(getBtnAadirActividad());
	}
	return panelBoton;
    }
    private JButton getBtnAadirActividad() {
	if (btnAadirActividad == null) {
	    btnAadirActividad = new JButton("A\u00F1adir Actividad");
	    btnAadirActividad.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    btnAadirActividad.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    if (comprobarValidez()) {
			createActividad();
			mostrarMensajeActividadInsertadaCorrectamente();
		    } else {
			mostrarMensajeDeError();
		    }
		}
	    });
	}
	return btnAadirActividad;
    }

    private JLabel getLblNombreActividad() {
	if (lblNombreActividad == null) {
	    lblNombreActividad = new JLabel("Nombre Actividad:");
	    lblNombreActividad.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblNombreActividad;
    }
    private JTextField getTxtNombreactividad() {
	if (txtNombreactividad == null) {
	    txtNombreactividad = new JTextField();
	    txtNombreactividad.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    txtNombreactividad.setColumns(10);
	}
	return txtNombreactividad;
    }
    private JPanel getPanelNombre() {
	if (panelNombre == null) {
	    panelNombre = new JPanel();
	    FlowLayout fl_panelNombre = (FlowLayout) panelNombre.getLayout();
	    fl_panelNombre.setAlignment(FlowLayout.LEFT);
	    panelNombre.add(getLblNombreActividad());
	    panelNombre.add(getTxtNombreactividad());
	}
	return panelNombre;
    }
    private JPanel getPanelIntensidad() {
	if (panelIntensidad == null) {
	    panelIntensidad = new JPanel();
	    FlowLayout flowLayout = (FlowLayout) panelIntensidad.getLayout();
	    flowLayout.setAlignment(FlowLayout.LEFT);
	    panelIntensidad.add(getLblIntensidad());
	    panelIntensidad.add(getChckbxBaja());
	    panelIntensidad.add(getChckbxModerada());
	    panelIntensidad.add(getChckbxAlta());
	}
	return panelIntensidad;
    }
    private JLabel getLblIntensidad() {
	if (lblIntensidad == null) {
	    lblIntensidad = new JLabel("Intensidad: ");
	    lblIntensidad.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblIntensidad;
    }
    private JCheckBox getChckbxBaja() {
	if (chckbxBaja == null) {
	    chckbxBaja = new JCheckBox("Baja");
	    chckbxBaja.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    chckbxBaja.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    if (chckbxBaja.isSelected()) {
			getChckbxAlta().setSelected(false);
			getChckbxModerada().setSelected(false);
		    }
		}
	    });
	}
	return chckbxBaja;
    }
    private JCheckBox getChckbxModerada() {
	if (chckbxModerada == null) {
	    chckbxModerada = new JCheckBox("Moderada");
	    chckbxModerada.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    chckbxModerada.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if (chckbxModerada.isSelected()) {
			getChckbxAlta().setSelected(false);
			getChckbxBaja().setSelected(false);
		    }
		}
	    });
	}
	return chckbxModerada;
    }
    private JCheckBox getChckbxAlta() {
	if (chckbxAlta == null) {
	    chckbxAlta = new JCheckBox("Alta");
	    chckbxAlta.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    chckbxAlta.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if (chckbxAlta.isSelected()) {
			getChckbxBaja().setSelected(false);
			getChckbxModerada().setSelected(false);
		    }
		}
	    });
	}
	return chckbxAlta;
    }
    private JPanel getPanelRecursos() {
	if (panelRecursos == null) {
	    panelRecursos = new JPanel();
	    panelRecursos.setLayout(new BorderLayout(0, 0));
	    panelRecursos.add(getPanelRecursosBuscados(), BorderLayout.NORTH);
	    panelRecursos.add(getPanelRecursosAnadidos());
	}
	return panelRecursos;
    }
    private JLabel getLblRecursos() {
	if (lblRecursos == null) {
	    lblRecursos = new JLabel("Recursos:");
	    lblRecursos.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblRecursos;
    }
    private JComboBox<RecursoDto> getCmbRecursos() {
	if (cmbRecursos == null) {
	    cmbRecursos = new JComboBox<RecursoDto>();
	    cmbRecursos.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return cmbRecursos;
    }
    private JButton getBtnAnadir() {
	if (btnAnadir == null) {
	    btnAnadir = new JButton("A\u00F1adir");
	    btnAnadir.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    aniadirRecursoSeleccionado();
		    desactivarBotonAniadirSiNecesario();
		}
	    });
	    btnAnadir.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return btnAnadir;
    }
    private JPanel getPanelRecursosAnadidos() {
	if (panelRecursosAnadidos == null) {
	    panelRecursosAnadidos = new JPanel();
	    panelRecursosAnadidos.setLayout(new BorderLayout(3, 5));
	    panelRecursosAnadidos.add(getLblRecursosSeleccionados(), BorderLayout.NORTH);
	    panelRecursosAnadidos.add(getScrollPaneRecursosSeleccionados(), BorderLayout.CENTER);
	}
	return panelRecursosAnadidos;
    }
    private JPanel getPanelUsaRecursos() {
	if (panelUsaRecursos == null) {
	    panelUsaRecursos = new JPanel();
	    FlowLayout flowLayout = (FlowLayout) panelUsaRecursos.getLayout();
	    flowLayout.setAlignment(FlowLayout.LEFT);
	    panelUsaRecursos.add(getChckbxNecesitaRecursos());
	}
	return panelUsaRecursos;
    }
    private JCheckBox getChckbxNecesitaRecursos() {
	if (chckbxNecesitaRecursos == null) {
	    chckbxNecesitaRecursos = new JCheckBox("Necesita Recursos");
	    chckbxNecesitaRecursos.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    	    cambiarEstadoRecursos();
	    	}
	    });
	    chckbxNecesitaRecursos.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxNecesitaRecursos;
    }
    private JPanel getPanelRecursosBuscados() {
	if (panelRecursosBuscados == null) {
	    panelRecursosBuscados = new JPanel();
	    panelRecursosBuscados.setLayout(new GridLayout(2, 1, 5, 5));
	    panelRecursosBuscados.add(getPanel_1());
	    panelRecursosBuscados.add(getBtnAnadir());
	}
	return panelRecursosBuscados;
    }
    private JPanel getPanel_1() {
	if (panelCmbRecursos == null) {
	    panelCmbRecursos = new JPanel();
	    panelCmbRecursos.add(getLblRecursos());
	    panelCmbRecursos.add(getCmbRecursos());
	}
	return panelCmbRecursos;
    }
    private JScrollPane getScrollPaneRecursosSeleccionados() {
	if (scrollPaneRecursosSeleccionados == null) {
	    scrollPaneRecursosSeleccionados = new JScrollPane();
	    scrollPaneRecursosSeleccionados.setViewportView(getTextAreaRecursosSeleccionados());
	}
	return scrollPaneRecursosSeleccionados;
    }
    private JTextArea getTextAreaRecursosSeleccionados() {
	if (textAreaRecursosSeleccionados == null) {
	    textAreaRecursosSeleccionados = new JTextArea();
	}
	return textAreaRecursosSeleccionados;
    }
    private JLabel getLblRecursosSeleccionados() {
	if (lblRecursosSeleccionados == null) {
	    lblRecursosSeleccionados = new JLabel("Recursos Seleccionados:");
	    lblRecursosSeleccionados.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblRecursosSeleccionados;
    }

    //Mis metodos
    protected boolean comprobarValidez() {
	return !getTxtNombreactividad().getText().isEmpty() &&	//Si el nombre no est� vac�o
		(getChckbxBaja().isSelected() || getChckbxModerada().isSelected() || getChckbxAlta().isSelected()); //Si alguna intensidad est� seleccionada
    }

    protected void createActividad() {
	ActividadDto actividad = new ActividadDto();
	actividad.setNombre(getTxtNombreactividad().getText());

	if (getChckbxBaja().isSelected()) {
	    actividad.setIntensidad("BAJA");
	} else if (getChckbxModerada().isSelected()) {
	    actividad.setIntensidad("MODERADA");
	} else if (getChckbxAlta().isSelected()) {
	    actividad.setIntensidad("ALTA");
	}

	actividad.setUsaRecursos(getChckbxNecesitaRecursos().isSelected());

	//Pasarle el objeto actividad a quienquiera que se encargue de manejarlo (capa bussiness �service?)
	NewActivityController nac = new NewActivityController();
	nac.createActividad(actividad);

	linkActividadRecurso(actividad);
    }
    protected void linkActividadRecurso(ActividadDto actividad) {
	List<ActividadesNecesitanRecursosDto> listaActNecRec = new ArrayList<ActividadesNecesitanRecursosDto>();
	
	long idActividad = conseguirIdActividad(actividad);
	
	for (RecursoDto recurso : listaRecursosAniadidos) {
	    ActividadesNecesitanRecursosDto anr = new ActividadesNecesitanRecursosDto();

	    anr.setId_Actividad(idActividad);
	    anr.setId_Recurso(recurso.getId());

	    listaActNecRec.add(anr);
	}
	NewActivityController nac = new NewActivityController();
	nac.createANRs(listaActNecRec);
    }

    private long conseguirIdActividad(ActividadDto actividad) {
	NewActivityController nac = new NewActivityController();
	return nac.getIdActividad(actividad);
    }

    protected void mostrarMensajeDeError() {
	//Crea un dialogo que informe que hay campos que no son validos
	JOptionPane.showMessageDialog(null, "Se ha producido un error por campos no válidos. Por favor rellene correctamente todos los campos.", "Error campos no validos", JOptionPane.WARNING_MESSAGE);
    }

    protected void mostrarMensajeActividadInsertadaCorrectamente() {
	JOptionPane.showMessageDialog(null, "La actividad se ha añadido correctamente.");
    }

    
    // Mis metodos

    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
    }

    private void cargarRecursos() {
	listaRecursos = getListaRecursos();
	recargarRecursos();
    }

    private void recargarRecursos() {
	RecursoDto[] array = transformarListaRecToArray();
	getCmbRecursos().setModel(new DefaultComboBoxModel<RecursoDto>(array));
    }

    private RecursoDto[] transformarListaRecToArray() {
	RecursoDto[] array = new RecursoDto[listaRecursos.size()];
	for (int i=0; i<listaRecursos.size(); i++) {
	    array[i] = listaRecursos.get(i);
	}
	return array;
    }

    protected List<RecursoDto> getListaRecursos(){
	NewActivityController pa = new NewActivityController();
	List<RecursoDto> recursos = pa.getRecursos();
	return recursos;
    }

    protected void aniadirRecursoSeleccionado() {
	//Consigue el recurso a a�adir
	RecursoDto recursoSeleccionado = (RecursoDto) getCmbRecursos().getSelectedItem();
	//Añade ese recurso a la lista de recursos que necesita la actividad
	listaRecursosAniadidos.add(recursoSeleccionado);
	//Borra dicho recurso de la lista de recursos generales para que no se pueda a�adir m�s de una vez
	listaRecursos.remove(recursoSeleccionado);

	//Escribir recursos en el txtarea de recursos
	escribirRecursosSeleccionados();

	//Recargar cmbRecursos
	recargarRecursos();
    }

    private void escribirRecursosSeleccionados() {
	String str = "";
	for (RecursoDto r : listaRecursosAniadidos) {
	    str+=r.getNombre();
	    str+="\n";
	}
	getTextAreaRecursosSeleccionados().setText(str);
    }
    protected void desactivarBotonAniadirSiNecesario() {
	if (listaRecursos.isEmpty()) {
	    getBtnAnadir().setEnabled(false);
	}
    }
    
    protected void cambiarEstadoRecursos() {
	if (getChckbxNecesitaRecursos().isSelected()) {
	    getLblRecursos().setEnabled(true);
	    getCmbRecursos().setEnabled(true);
	    getLblRecursosSeleccionados().setEnabled(true);
	    getScrollPaneRecursosSeleccionados().setEnabled(true);
	    getTextAreaRecursosSeleccionados().setEnabled(true);
	    getBtnAnadir().setEnabled(true);
	    
	} else {
	    getLblRecursos().setEnabled(false);
	    getCmbRecursos().setEnabled(false);
	    getLblRecursosSeleccionados().setEnabled(false);
	    getScrollPaneRecursosSeleccionados().setEnabled(false);
	    getTextAreaRecursosSeleccionados().setEnabled(false);
	    getBtnAnadir().setEnabled(false);
	}
    }

}
