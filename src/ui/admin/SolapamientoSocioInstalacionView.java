package ui.admin;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import controller.admin.PlanPeriodicActivityController;
import dto.SocioReservaInstalacionDto;
import utils.NonEditableModel;

import java.awt.Color;
import javax.swing.JButton;

/**
 * Diálogo que aparece cuando la administración intenta planificar una actividad
 * reservando una instalación y ésta ya está reservada a la misma fecha y hora
 * que una reserva por parte de socio
 * 
 * @author UO264703
 *
 */
public class SolapamientoSocioInstalacionView extends JDialog {
    public static final String RESERVAS = "R";
    public static final String RESERVAS_LISTA = "RL";

    private static final long serialVersionUID = 1L;

    private PlanPeriodicActivityController controller;
    private List<SocioReservaInstalacionDto> reservas; // reservas de
						       // instalaciones que
						       // colisionan
    private List<SocioReservaInstalacionDto> reservasCanceladas;

    private JPanel contentPane;
    private JLabel lbIcono;
    private JTextArea txtrSolapamientoDeActividades;
    private JScrollPane spSolapamientos;
    private JLabel lblSolapamientosCon;
    private JTable tbSolapamientos;
    private JList<String> listaCancelarReservas;
    private JLabel lbReservasCancelar;
    private JButton btnAadir;
    private JButton btnEliminarDeLista;
    private JButton btnFinalizar;
    private JScrollPane spReservasACancelar;
    private JButton btnCancelarReservas;
    private JButton btnCancelarTodas;

    private DefaultTableModel modeloTabla;
    private DefaultListModel<String> modeloLista;

    private ProcesaAñadir pA;
    private ProcesaEliminar pE;
    private ProcesaCancelar pC;

    /**
     * Create the frame.
     * 
     * @param colisionesSocios
     * @param planPeriodicActivityController
     */
    public SolapamientoSocioInstalacionView(
	    PlanPeriodicActivityController planPeriodicActivityController,
	    List<SocioReservaInstalacionDto> colisionesSocios) {
	setFont(new Font("Dialog", Font.PLAIN, 12));

	this.pA = new ProcesaAñadir();
	this.pE = new ProcesaEliminar();
	this.pC = new ProcesaCancelar();
	this.controller = planPeriodicActivityController;
	this.reservas = colisionesSocios;
	this.reservasCanceladas = new ArrayList<SocioReservaInstalacionDto>();

	setIconImage(Toolkit.getDefaultToolkit()
		.getImage(SolapamientoSocioInstalacionView.class
			.getResource("/img/error.png")));
	setTitle("Error con planificación");
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	setBounds(100, 100, 790, 470);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(null);
	contentPane.add(getLbIcono());
	contentPane.add(getTxtrSolapamientoDeActividades());
	contentPane.add(getSpSolapamientos());
	contentPane.add(getLblSolapamientosCon());
	contentPane.add(getLbReservasCancelar());
	contentPane.add(getBtnAadir());
	contentPane.add(getBtnEliminarDeLista());
	contentPane.add(getBtnFinalizar());
	contentPane.add(getSpReservasACancelar());
	contentPane.add(getBtnCancelarReservas());
	contentPane.add(getBtnCancelarTodas());

	setLocationRelativeTo(null);
	setResizable(false);

	añadirManejadoresEventos();
	inicializar();
    }

    /**
     * Añade todos los manejadores de eventos que usa la clase
     */
    private void añadirManejadoresEventos() {
	btnAadir.addActionListener(pA);
	btnEliminarDeLista.addActionListener(pE);
	btnCancelarReservas.addActionListener(pC);
	btnCancelarTodas.addActionListener(pC);
    }

    /**
     * Inicializa todo
     */
    private void inicializar() {
	/*
	 * Las reservas que colisionan nunca van a estar vacías porque el
	 * diálogo solo se crea si existen algunas, pero por si acaso pudiese
	 * ocurrir se realiza la comprobación
	 */
	if (!reservas.isEmpty()) {
	    btnAadir.setEnabled(true);
	    btnCancelarTodas.setEnabled(true);
	}
    }

    private JLabel getLbIcono() {
	if (lbIcono == null) {
	    lbIcono = new JLabel("");
	    lbIcono.setToolTipText(
		    "Hay actividades que coinciden con reservas hechas por socios en instalaciones");
	    lbIcono.setIcon(new ImageIcon(SolapamientoSocioInstalacionView.class
		    .getResource("/img/error.png")));
	    lbIcono.setBounds(10, 11, 93, 93);
	}
	return lbIcono;
    }

    private JTextArea getTxtrSolapamientoDeActividades() {
	if (txtrSolapamientoDeActividades == null) {
	    txtrSolapamientoDeActividades = new JTextArea();
	    txtrSolapamientoDeActividades.setOpaque(false);
	    txtrSolapamientoDeActividades.setBorder(null);
	    txtrSolapamientoDeActividades.setEditable(false);
	    txtrSolapamientoDeActividades.setFont(
		    new Font("MS Reference Sans Serif", Font.PLAIN, 26));
	    txtrSolapamientoDeActividades.setWrapStyleWord(true);
	    txtrSolapamientoDeActividades.setLineWrap(true);
	    txtrSolapamientoDeActividades.setText(
		    "Solapamiento de actividades con reservas de instalaciones por socios");
	    txtrSolapamientoDeActividades.setBounds(127, 17, 560, 80);
	}
	return txtrSolapamientoDeActividades;
    }

    private JScrollPane getSpSolapamientos() {
	if (spSolapamientos == null) {
	    spSolapamientos = new JScrollPane();
	    spSolapamientos.setBorder(new LineBorder(new Color(0, 0, 0)));
	    spSolapamientos.setBounds(10, 152, 429, 205);
	    spSolapamientos.setViewportView(getTbSolapamientos());
	}
	return spSolapamientos;
    }

    private JLabel getLblSolapamientosCon() {
	if (lblSolapamientosCon == null) {
	    lblSolapamientosCon = new JLabel("Solapamientos:");
	    lblSolapamientosCon.setHorizontalAlignment(SwingConstants.CENTER);
	    lblSolapamientosCon
		    .setHorizontalTextPosition(SwingConstants.LEADING);
	    lblSolapamientosCon
		    .setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 18));
	    lblSolapamientosCon.setBounds(102, 115, 218, 36);
	}
	return lblSolapamientosCon;
    }

    private JTable getTbSolapamientos() {
	if (tbSolapamientos == null) {
	    String[] columnas = { "Socio", "DNI", "Instalación", "Fecha",
		    "Hora reserva" };
	    modeloTabla = new NonEditableModel(columnas, 0);
	    aniadirFilas();

	    tbSolapamientos = new JTable(modeloTabla);
	    tbSolapamientos.setFont(new Font("Tahoma", Font.PLAIN, 14));
	}
	return tbSolapamientos;
    }

    private void aniadirFilas() {
	Object[] nuevaFila = new Object[5];
	for (int i = 0; i < reservas.size(); i++) {
	    nuevaFila[0] = reservas.get(i).getNombreCompleto();
	    nuevaFila[1] = reservas.get(i).getDni();
	    nuevaFila[2] = reservas.get(i).getNombreInstalacion();
	    nuevaFila[3] = reservas.get(i).getFecha();
	    nuevaFila[4] = reservas.get(i).getHoraInicio();

	    modeloTabla.addRow(nuevaFila);
	}
    }

    private JList<String> getListaCancelarReservas() {
	if (listaCancelarReservas == null) {
	    modeloLista = new DefaultListModel<String>();
	    listaCancelarReservas = new JList<String>(modeloLista);
	    listaCancelarReservas.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    listaCancelarReservas.setBorder(new LineBorder(new Color(0, 0, 0)));
	    listaCancelarReservas.setOpaque(false);
	    listaCancelarReservas.setSelectionMode(
		    ListSelectionModel.SINGLE_INTERVAL_SELECTION);
	}
	return listaCancelarReservas;
    }

    private JLabel getLbReservasCancelar() {
	if (lbReservasCancelar == null) {
	    lbReservasCancelar = new JLabel("Reservas a cancelar:");
	    lbReservasCancelar
		    .setHorizontalTextPosition(SwingConstants.LEADING);
	    lbReservasCancelar.setHorizontalAlignment(SwingConstants.CENTER);
	    lbReservasCancelar
		    .setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 18));
	    lbReservasCancelar.setBounds(499, 115, 218, 36);
	}
	return lbReservasCancelar;
    }

    private JButton getBtnAadir() {
	if (btnAadir == null) {
	    btnAadir = new JButton("Añadir a la lista");
	    btnAadir.setToolTipText(
		    "Añadir la reserva de instalación a la lista de las que se queiren cancelar");
	    btnAadir.setMnemonic('d');
	    btnAadir.setEnabled(false);
	    btnAadir.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    btnAadir.setBounds(275, 361, 164, 23);
	}
	return btnAadir;
    }

    private JButton getBtnEliminarDeLista() {
	if (btnEliminarDeLista == null) {
	    btnEliminarDeLista = new JButton("Eliminar de la lista");
	    btnEliminarDeLista.setToolTipText(
		    "Eliminar reserva seleccionada para que no se cancele ");
	    btnEliminarDeLista.setMnemonic('l');
	    btnEliminarDeLista.setEnabled(false);
	    btnEliminarDeLista.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    btnEliminarDeLista.setBounds(620, 361, 154, 23);
	}
	return btnEliminarDeLista;
    }

    private JButton getBtnFinalizar() {
	if (btnFinalizar == null) {
	    btnFinalizar = new JButton("Finalizar");
	    btnFinalizar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    dispose();
		}
	    });
	    btnFinalizar.setToolTipText(
		    "Volver a la ventana anterior sin cancelar nada");
	    btnFinalizar.setMnemonic('f');
	    btnFinalizar.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    btnFinalizar.setBounds(681, 409, 93, 23);
	}
	return btnFinalizar;
    }

    private JScrollPane getSpReservasACancelar() {
	if (spReservasACancelar == null) {
	    spReservasACancelar = new JScrollPane();
	    spReservasACancelar.setBounds(458, 152, 316, 205);
	    spReservasACancelar.setViewportView(getListaCancelarReservas());
	}
	return spReservasACancelar;
    }

    private JButton getBtnCancelarReservas() {
	if (btnCancelarReservas == null) {
	    btnCancelarReservas = new JButton("Cancelar reservas de lista");
	    btnCancelarReservas.setActionCommand("lista");
	    btnCancelarReservas.setEnabled(false);
	    btnCancelarReservas.setToolTipText(
		    "Cancelar las reservas que estén en la lista de Reservas a cancelar");
	    btnCancelarReservas.setMnemonic('n');
	    btnCancelarReservas.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    btnCancelarReservas.setBounds(458, 409, 214, 23);
	}
	return btnCancelarReservas;
    }

    private JButton getBtnCancelarTodas() {
	if (btnCancelarTodas == null) {
	    btnCancelarTodas = new JButton("Cancelar todas");
	    btnCancelarTodas.setActionCommand("todas");
	    btnCancelarTodas.setEnabled(false);
	    btnCancelarTodas.setToolTipText(
		    "Cancelar todas las reservas de socios en instalaciones que colisionan");
	    btnCancelarTodas.setMnemonic('C');
	    btnCancelarTodas.setFont(new Font("Tahoma", Font.PLAIN, 14));
	    btnCancelarTodas.setBounds(304, 409, 135, 23);
	}
	return btnCancelarTodas;
    }

    /**
     * Obtiene la reserva que aparece en la fila pasada como parámetro en la
     * tabla
     * 
     * @param fila de la tabla
     * @return socio que aparece en la fila
     */
    private SocioReservaInstalacionDto obtenerReserva(int fila) {
	String nombre_Instalacion = (String) modeloTabla.getValueAt(fila, 2);
	String dni_Socio = (String) modeloTabla.getValueAt(fila, 1);
	String fecha = (String) modeloTabla.getValueAt(fila, 3);
	String hora = (String) modeloTabla.getValueAt(fila, 4);

	SocioReservaInstalacionDto reserva = controller
		.obtenerReserva(nombre_Instalacion, dni_Socio, fecha, hora);
	return reserva;
    }

    /**
     * Añade una reserva a la lista de reservas canceladas
     * 
     * @param reserva a añadir
     */
    private void addReservaListaCanceladas(SocioReservaInstalacionDto reserva) {
	String cadenaReserva = reserva.otrosDatos();
	if (!modeloLista.contains(cadenaReserva)) {
	    modeloLista.addElement(cadenaReserva);
	    // Para quitar más facilmente las reservas de canceladas:
	    reservasCanceladas.add(reserva);

	    if (!btnEliminarDeLista.isEnabled()) {
		btnEliminarDeLista.setEnabled(true);
		btnCancelarReservas.setEnabled(true);
	    }
	}
    }

    /**
     * Elimina una reserva de la tabla según su fila
     * 
     * @param fila de la tabla
     */
    private void eliminarReservaTabla(int fila) {
	if (fila != -1) {
	    modeloTabla.removeRow(fila);
	    if (modeloTabla.getRowCount() == 0)
		btnAadir.setEnabled(false);
	}

    }

    /**
     * ELimina una reserva de la lista de reservas a cancelar
     * 
     * @param reserva
     * @param fila,   índice de la reserva en la lista
     */
    private void eliminarReservaLista(SocioReservaInstalacionDto reserva,
	    int fila) {
	modeloLista.remove(fila);
	reservasCanceladas.remove(fila);

	añadirReservaTabla(reserva);

	if (modeloLista.size() == 0) {
	    btnEliminarDeLista.setEnabled(false);
	    btnCancelarReservas.setEnabled(false);
	}
    }

    /**
     * Añade una reserva a la tabla de reservas de instalaciones por socios que
     * colisionan
     * 
     * @param reserva
     */
    private void añadirReservaTabla(SocioReservaInstalacionDto reserva) {
	Object[] nuevaFila = new Object[5];
	nuevaFila[0] = reserva.getNombreCompleto();
	nuevaFila[1] = reserva.getDni();
	nuevaFila[2] = reserva.getNombreInstalacion();
	nuevaFila[3] = reserva.getFecha();
	nuevaFila[4] = reserva.getHoraInicio();
	modeloTabla.addRow(nuevaFila);

	if (!btnAadir.isEnabled())
	    btnAadir.setEnabled(true);
    }

    /**
     * Para añadir reservas de instalación a la lista de reservas que el
     * administrador quiere cancelar
     * 
     * @author UO264703
     *
     */
    class ProcesaAñadir implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
	    int fila = tbSolapamientos.getSelectedRow();
	    // Si se ha seleccionado alguna fila
	    if (fila != -1) {
		// Sacamos la reserva asociada a esa fila
		SocioReservaInstalacionDto reserva = obtenerReserva(fila);
		// La añadimos a la lista de reservas canceladas
		addReservaListaCanceladas(reserva);
		// La eliminamos de la tabla
		eliminarReservaTabla(fila);
	    }
	}
    }

    /**
     * Para quitar elementos de la lista de reservas a cancelar Añade el
     * elemento seleccionado de nuevo a la tabla
     * 
     * @author UO264703
     *
     */
    class ProcesaEliminar implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
	    int fila = listaCancelarReservas.getSelectedIndex();
	    // Si se ha seleccionado alguna fila
	    if (fila != -1) {
		SocioReservaInstalacionDto reserva = reservasCanceladas
			.get(fila);
		// Eliminamos la reserva de la lista de canceladas
		eliminarReservaLista(reserva, fila);
	    }

	}
    }

    /**
     * Cancela las reservas Dependiendo del botón pulsado se cancelarán todas
     * las reservas que colisionan o solamente las que aparezcan en la lista de
     * reservas a cancelar
     * 
     * @author UO264703
     *
     */
    class ProcesaCancelar implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
	    JButton boton = (JButton) e.getSource();
	    switch (boton.getActionCommand()) {
	    case "todas":
		cancelarReservas(reservas, RESERVAS);
	    case "lista":
		cancelarReservas(reservasCanceladas, RESERVAS_LISTA);
	    }
	}

    }

    private void cancelarReservas(List<SocioReservaInstalacionDto> reservas, String tipoReservas) {
	if (tipoReservas.equals(RESERVAS)) {
	    for (int i = 0; i < reservas.size(); i++) {
		reservas.set(i, obtenerReserva(i));
	    }
	}
	
	for (SocioReservaInstalacionDto reserva: reservas){
	    controller.borrar(reserva.getId());
	}
	
	JOptionPane.showMessageDialog(this,
		"Se han cancelado las reservas", "Reservas canceladas", JOptionPane.INFORMATION_MESSAGE);
	dispose();
    }
}
