package ui.admin;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ui.Main;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.SwingConstants;

public class SeatReservationView extends JDialog {

    private static final long serialVersionUID = 1L;
    private JDialog frame;
    private JPanel contentPane;
    private JButton btnReservate;
    private JLabel lblName;
    private JLabel lblHour;
    private JLabel lblNmeroDePlazas;
    private JTextField txtTotalSeats;
    private JLabel lblPlazasDisponibles;
    private JTextField txtAvailableSeats;
    private JLabel lblApuntarASocio;
    private JLabel lblDni;
    private JTextField txtDNI;

    /**
     * Create the frame.
     */
    public SeatReservationView(Main main) {
	setModal(true);
	frame = new JDialog();
	frame.setTitle("ADMINISTRADOR: Reserva de plazas para socios ");
	frame.setResizable(false);
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	frame.setBounds(100, 100, 800, 500);
	frame.setLocationRelativeTo(main);
	frame.setModal(true);
	frame.getRootPane().setDefaultButton(getBtnReservate());
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(null);
	frame.setContentPane(contentPane);
	contentPane.add(getBtnReservate());
	contentPane.add(getLblName());
	contentPane.add(getLblHour());
	contentPane.add(getLblNmeroDePlazas());
	contentPane.add(getTxtTotalSeats());
	contentPane.add(getLblPlazasDisponibles());
	contentPane.add(getTxtAvailableSeats());
	contentPane.add(getLblApuntarASocio());
	contentPane.add(getLblDni());
	contentPane.add(getTxtDNI());
    }

    public JDialog getFrame() {
	return frame;
    }

    public JButton getBtnReservate() {
	if (btnReservate == null) {
	    btnReservate = new JButton("Reservar");
	    btnReservate.setBounds(670, 409, 95, 42);
	    btnReservate.setMnemonic('A');
	    btnReservate.setForeground(Color.WHITE);
	    btnReservate.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnReservate.setEnabled(false);
	    btnReservate.setBackground(new Color(0, 128, 0));
	}
	return btnReservate;
    }

    public JLabel getLblName() {
	if (lblName == null) {
	    lblName = new JLabel("PLANIFICACION");
	    lblName.setBounds(13, 11, 752, 42);
	    lblName.setHorizontalAlignment(SwingConstants.CENTER);
	    lblName.setFont(new Font("Tahoma", Font.BOLD, 18));
	}
	return lblName;
    }

    public JLabel getLblHour() {
	if (lblHour == null) {
	    lblHour = new JLabel("8:00:00 - 9:00:00");
	    lblHour.setBounds(13, 48, 752, 26);
	    lblHour.setHorizontalAlignment(SwingConstants.CENTER);
	    lblHour.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblHour.setBackground(Color.WHITE);
	}
	return lblHour;
    }

    private JLabel getLblNmeroDePlazas() {
	if (lblNmeroDePlazas == null) {
	    lblNmeroDePlazas = new JLabel("NÚMERO DE PLAZAS: ");
	    lblNmeroDePlazas.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblNmeroDePlazas.setBounds(57, 114, 142, 50);
	}
	return lblNmeroDePlazas;
    }

    public JTextField getTxtTotalSeats() {
	if (txtTotalSeats == null) {
	    txtTotalSeats = new JTextField();
	    txtTotalSeats.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    txtTotalSeats.setEditable(false);
	    txtTotalSeats.setBounds(209, 122, 116, 35);
	    txtTotalSeats.setColumns(10);
	}
	return txtTotalSeats;
    }

    private JLabel getLblPlazasDisponibles() {
	if (lblPlazasDisponibles == null) {
	    lblPlazasDisponibles = new JLabel("PLAZAS DISPONIBLES");
	    lblPlazasDisponibles.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblPlazasDisponibles.setBounds(429, 114, 133, 50);
	}
	return lblPlazasDisponibles;
    }

    public JTextField getTxtAvailableSeats() {
	if (txtAvailableSeats == null) {
	    txtAvailableSeats = new JTextField();
	    txtAvailableSeats.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    txtAvailableSeats.setEditable(false);
	    txtAvailableSeats.setColumns(10);
	    txtAvailableSeats.setBounds(587, 122, 116, 35);
	}
	return txtAvailableSeats;
    }

    private JLabel getLblApuntarASocio() {
	if (lblApuntarASocio == null) {
	    lblApuntarASocio = new JLabel("RESERVAR PLAZA PARA UN SOCIO:");
	    lblApuntarASocio.setHorizontalAlignment(SwingConstants.CENTER);
	    lblApuntarASocio.setFont(new Font("Tahoma", Font.BOLD, 18));
	    lblApuntarASocio.setBounds(13, 224, 773, 35);
	}
	return lblApuntarASocio;
    }

    private JLabel getLblDni() {
	if (lblDni == null) {
	    lblDni = new JLabel("DNI: ");
	    lblDni.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblDni.setBounds(57, 301, 90, 26);
	}
	return lblDni;
    }

    public JTextField getTxtDNI() {
	if (txtDNI == null) {
	    txtDNI = new JTextField();
	    txtDNI.setBounds(157, 297, 200, 35);
	    txtDNI.grabFocus();
	    txtDNI.setColumns(10);
	}
	return txtDNI;
    }

}
