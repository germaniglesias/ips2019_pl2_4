package ui.admin;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import ui.Main;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
public class FacilityReservationView extends JDialog {

    private static final long serialVersionUID = 1L;
    private JDialog frame;
    private JPanel contentPane;
    private JButton btnRservate;
    private JLabel lblName;
    private JLabel lblHour;
    private JLabel lblPricePerHour;
    private JTextField txtPricePerHour;
    private JLabel lblNumberHours;
    private JLabel lblApuntarASocio;
    private JLabel lblDni;
    private JTextField txtDNI;
    private JSpinner spinnerHours;

    /**
     * Create the frame.
     */
    public FacilityReservationView(Main main) {
	setModal(true);
	frame = new JDialog();
	frame.setModal(true);
	frame.setTitle("ADMINISTRADOR: Reserva de plazas para socios ");
	frame.setResizable(false);
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	frame.getRootPane().setDefaultButton(getBtnRservate());
	frame.setBounds(100, 100, 800, 500);
	frame.setLocationRelativeTo(main);
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(null);
	frame.setContentPane(contentPane);
	contentPane.add(getBtnRservate());
	contentPane.add(getLblName());
	contentPane.add(getLblHour());
	contentPane.add(getLblPricePerHour());
	contentPane.add(getTxtPricePerHour());
	contentPane.add(getLblNumberHours());
	contentPane.add(getLblApuntarASocio());
	contentPane.add(getLblDni());
	contentPane.add(getTxtDNI());
	contentPane.add(getSpinnerHours());
    }

    public JDialog getFrame() {
	return frame;
    }

    public JButton getBtnRservate() {
	if (btnRservate == null) {
	    btnRservate = new JButton("Reservar");
	    btnRservate.setBounds(598, 409, 95, 42);
	    btnRservate.setMnemonic('r');
	    btnRservate.setForeground(Color.WHITE);
	    btnRservate.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnRservate.setEnabled(false);
	    btnRservate.setBackground(new Color(0, 128, 0));
	}
	return btnRservate;
    }

    public JLabel getLblName() {
	if (lblName == null) {
	    lblName = new JLabel("INSTALACION");
	    lblName.setBounds(13, 11, 752, 42);
	    lblName.setHorizontalAlignment(SwingConstants.CENTER);
	    lblName.setFont(new Font("Tahoma", Font.BOLD, 18));
	}
	return lblName;
    }

    public JLabel getLblHour() {
	if (lblHour == null) {
	    lblHour = new JLabel("8:00:00 - 9:00:00");
	    lblHour.setBounds(13, 48, 752, 26);
	    lblHour.setHorizontalAlignment(SwingConstants.CENTER);
	    lblHour.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblHour.setBackground(Color.WHITE);
	}
	return lblHour;
    }

    private JLabel getLblPricePerHour() {
	if (lblPricePerHour == null) {
	    lblPricePerHour = new JLabel("PRECIO POR HORA");
	    lblPricePerHour.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblPricePerHour.setBounds(57, 114, 142, 50);
	}
	return lblPricePerHour;
    }

    public JTextField getTxtPricePerHour() {
	if (txtPricePerHour == null) {
	    txtPricePerHour = new JTextField();
	    txtPricePerHour.setEditable(false);
	    txtPricePerHour.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    txtPricePerHour.setBounds(209, 122, 106, 35);
	    txtPricePerHour.setColumns(10);
	}
	return txtPricePerHour;
    }

    private JLabel getLblNumberHours() {
	if (lblNumberHours == null) {
	    lblNumberHours = new JLabel("NÚMERO DE HORAS");
	    lblNumberHours.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblNumberHours.setBounds(429, 114, 133, 50);
	}
	return lblNumberHours;
    }

    private JLabel getLblApuntarASocio() {
	if (lblApuntarASocio == null) {
	    lblApuntarASocio = new JLabel("RESERVAR PLAZA PARA UN SOCIO:");
	    lblApuntarASocio.setHorizontalAlignment(SwingConstants.CENTER);
	    lblApuntarASocio.setFont(new Font("Tahoma", Font.BOLD, 18));
	    lblApuntarASocio.setBounds(13, 224, 773, 35);
	}
	return lblApuntarASocio;
    }

    private JLabel getLblDni() {
	if (lblDni == null) {
	    lblDni = new JLabel("DNI: ");
	    lblDni.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    lblDni.setBounds(57, 301, 90, 26);
	}
	return lblDni;
    }

    public JTextField getTxtDNI() {
	if (txtDNI == null) {
	    txtDNI = new JTextField();
	    txtDNI.grabFocus();
	    txtDNI.setBounds(157, 297, 200, 35);
	    txtDNI.setColumns(10);
	}
	return txtDNI;
    }

    @SuppressWarnings("deprecation")
    public JSpinner getSpinnerHours() {
	if (spinnerHours == null) {
	    spinnerHours = new JSpinner();
	    spinnerHours.setModel(new SpinnerNumberModel(new Integer(1),
		    new Integer(1), null, new Integer(1)));
	    spinnerHours.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    spinnerHours.setBounds(587, 122, 106, 35);
	}
	return spinnerHours;
    }
}