package ui.admin;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

import ui.Main;
import ui.admin.cancelViews.CancelReservateView;
import ui.admin.cancelViews.CancelReservateView2;
import ui.admin.cancelViews.CancelReservateView3;

public class CancelReservateMainView extends JDialog {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JButton btnActivitiesSchedule;
    private Main main;
    private JButton btnCancelarReservasPor;

    /**
     * Create the frame.
     */
    public CancelReservateMainView(Main main) {
    	setTitle("Cancelar Actividades Socio");
	this.setMain(main);
	setResizable(false);
	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	setBounds(100, 100, 800, 500);
	setLocationRelativeTo(main);
	contentPane = new JPanel();
	contentPane.setBackground(Color.WHITE);
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	setContentPane(contentPane);
	contentPane.setLayout(null);
	contentPane.add(getBtnActivitiesSchedule());
	contentPane.add(getBtnCancelarReservasPor());
    }

    private JButton getBtnActivitiesSchedule() {
	if (btnActivitiesSchedule == null) {
	    btnActivitiesSchedule = new JButton("CANCELAR [Por rango de horas]");
	    btnActivitiesSchedule.setBorder(
		    new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
	    btnActivitiesSchedule.setFont(new Font("Tahoma", Font.BOLD, 11));
	    btnActivitiesSchedule.setBounds(112, 195, 261, 123);
	    btnActivitiesSchedule.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    setVisible(false);
		    CancelReservateView frame = null;
		    try {
			frame = new CancelReservateView(main);
		    } catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    }
		    frame.setVisible(true);
		}
	    });
	}
	return btnActivitiesSchedule;
    }

    public Main getMain() {
	return main;
    }

    public void setMain(Main main) {
	this.main = main;
    }

    private JButton getBtnCancelarReservasPor() {
	if (btnCancelarReservasPor == null) {
	    btnCancelarReservasPor = new JButton("CANCELAR [Por rangor de horas y fecha]");
	    btnCancelarReservasPor.setBorder(
		    new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
	    btnCancelarReservasPor.setFont(new Font("Tahoma", Font.BOLD, 11));
	    btnCancelarReservasPor.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    setVisible(false);
		    CancelReservateView2 frame = null;
		    try {
			frame = new CancelReservateView2(main);
		    } catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    }
		    frame.setVisible(true);

		}

	    });
	    btnCancelarReservasPor.setBounds(431, 195, 261, 123);
	}
	return btnCancelarReservasPor;
    }
}
