package ui.admin;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.admin.AdminMainController;
import controller.admin.PlanActivityWithResourcesController;
import controller.admin.PlanPeriodicActivityController;
import dto.ActividadDto;
import dto.InstalacionDto;
import dto.InstalacionesTienenRecursosDto;
import dto.PlanificacionActividadDto;
import utils.Fecha;

public class PlanPeriodicActivityView extends JDialog {
    private static final long serialVersionUID = 1L;

    private JPanel contentPane;
    private AdminMainController adminMainController;
    private JPanel panelRecursos;
    private JPanel panelSiguiente;
    private JButton btnAceptar;
    private JLabel lblActividad;
    private JComboBox<ActividadDto> cmbActividades;

    private List<ActividadDto> listaActividades = new ArrayList<ActividadDto>();
    private List<InstalacionesTienenRecursosDto> listaInstalaciones = new ArrayList<InstalacionesTienenRecursosDto>();
    private List<PlanificacionActividadDto> listaReservasAHacer = new ArrayList<PlanificacionActividadDto>();

    private JPanel panelDatosReserva;
    private JPanel panelInstalaciones;
    private JPanel panelFecha;
    private JLabel lblInstalacion;
    private JComboBox<InstalacionesTienenRecursosDto> cmbInstalaciones;
    private JLabel lblFechaInicio;
    private JSpinner spnFechaInicio;
    private JPanel panelActividad;
    private JPanel panelOcupanMenos;
    private JLabel lblFechaFin;
    private JSpinner spnFechaFin;
    private JPanel panelDuracion;
    private JPanel panelDiasDeLaSemana;
    private JCheckBox chckbxLunes;
    private JCheckBox chckbxMartes;
    private JCheckBox chckbxMiercoles;
    private JCheckBox chckbxJueves;
    private JCheckBox chckbxViernes;
    private JCheckBox chckbxSabado;
    private JLabel lblHoraInicio;
    private JSpinner spnHoraInicio;
    private JLabel lblHoraFin;
    private JSpinner spnHoraFin;
    private JCheckBox chckbxDuraTodoEl;
    private JPanel panelLimitePlazas;
    private JCheckBox chckbxTieneLmiteDe;
    private JSpinner spnNumeroLimitePlazas;
    private JLabel lblNmeroLmiteDe;
    private JPanel panel;
    private JPanel panel_1;
    private JButton btnAadirALista;

    public PlanPeriodicActivityView(AdminMainController adminMainView) {
	this.setAdminMainView(adminMainView);
	addWindowListener(new WindowAdapter() {
	    @Override
	    public void windowClosing(WindowEvent arg0) {
		mostrarAdminMainView(arg0);
	    }
	});
	setResizable(false);
	setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	setBounds(100, 100, 800, 500);
	setLocationRelativeTo(adminMainView.getView());
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(new BorderLayout(0, 0));
	setContentPane(contentPane);
	contentPane.add(getPanelRecursos(), BorderLayout.CENTER);
	contentPane.add(getPanelSiguiente(), BorderLayout.SOUTH);

	setTitle("Administración: Planificación Actividad");

	cargarActividades();
	cargarInstalacionesConLosRecursosDeLaActividadSeleccionada();
	activarDesactivarLimitePlazasSiRecursos();
	deshabilitarNumPlazas();
    }

    public AdminMainController getAdminMainView() {
	return adminMainController;
    }

    public void setAdminMainView(AdminMainController adminMainView) {
	this.adminMainController = adminMainView;
    }
    private JPanel getPanelRecursos() {
	if (panelRecursos == null) {
	    panelRecursos = new JPanel();
	    panelRecursos.setLayout(new BorderLayout(0, 0));
	    panelRecursos.add(getPanelDatosReserva(), BorderLayout.CENTER);
	}
	return panelRecursos;
    }
    private JPanel getPanelSiguiente() {
	if (panelSiguiente == null) {
	    panelSiguiente = new JPanel();
	    FlowLayout fl_panelSiguiente = (FlowLayout) panelSiguiente.getLayout();
	    fl_panelSiguiente.setAlignment(FlowLayout.RIGHT);
	    panelSiguiente.add(getBtnAadirALista());
	    panelSiguiente.add(getBtnAceptar());
	}
	return panelSiguiente;
    }
    private JButton getBtnAceptar() {
	if (btnAceptar == null) {
	    btnAceptar = new JButton("Planificar");
	    btnAceptar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    if (comprobarCamposValidos()) {
			reservarTodo();
		    }
		}
	    });
	    btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return btnAceptar;
    }
    private JLabel getLblActividad() {
	if (lblActividad == null) {
	    lblActividad = new JLabel("Actividad:");
	    lblActividad.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblActividad;
    }
    private JComboBox<ActividadDto> getCmbActividades() {
	if (cmbActividades == null) {
	    cmbActividades = new JComboBox<ActividadDto>();
	    cmbActividades.addItemListener(new ItemListener() {
		public void itemStateChanged(ItemEvent arg0) {
		    cargarInstalacionesConLosRecursosDeLaActividadSeleccionada();
		    activarDesactivarLimitePlazasSiRecursos();
		}
	    });
	    cmbActividades.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return cmbActividades;
    }

    private JPanel getPanelDatosReserva() {
	if (panelDatosReserva == null) {
	    panelDatosReserva = new JPanel();
	    panelDatosReserva.setLayout(new BorderLayout(0, 0));
	    panelDatosReserva.add(getPanelOcupanMenos(), BorderLayout.CENTER);
	}
	return panelDatosReserva;
    }
    private JPanel getPanelInstalaciones() {
	if (panelInstalaciones == null) {
	    panelInstalaciones = new JPanel();
	    panelInstalaciones.add(getLblInstalacion());
	    panelInstalaciones.add(getCmbInstalaciones());
	}
	return panelInstalaciones;
    }
    private JPanel getPanelFecha() {
	if (panelFecha == null) {
	    panelFecha = new JPanel();
	    panelFecha.add(getLblFechaInicio());
	    panelFecha.add(getSpnFechaInicio());
	    panelFecha.add(getLblFechaFin());
	    panelFecha.add(getSpnFechaFin());
	}
	return panelFecha;
    }
    private JLabel getLblInstalacion() {
	if (lblInstalacion == null) {
	    lblInstalacion = new JLabel("Instalaci\u00F3nes:");
	    lblInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblInstalacion;
    }
    private JComboBox<InstalacionesTienenRecursosDto> getCmbInstalaciones() {
	if (cmbInstalaciones == null) {
	    cmbInstalaciones = new JComboBox<InstalacionesTienenRecursosDto>();
	    cmbInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return cmbInstalaciones;
    }
    private JLabel getLblFechaInicio() {
	if (lblFechaInicio == null) {
	    lblFechaInicio = new JLabel("Fecha Inicio:");
	    lblFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblFechaInicio;
    }
    private JSpinner getSpnFechaInicio() {
	if (spnFechaInicio == null) {
	    spnFechaInicio = new JSpinner();
	    spnFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    SimpleDateFormat model = new SimpleDateFormat("dd/MM/yy");
	    spnFechaInicio = new JSpinner(new SpinnerDateModel());
	    spnFechaInicio.setEditor(new JSpinner.DateEditor(spnFechaInicio, model.toPattern()));
	    spnFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    
	    spnFechaInicio.addChangeListener(new ChangeListener() {
		    public void stateChanged(ChangeEvent e) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DATE, -1);
			Date yesterday = calendar.getTime();
			if (yesterday.compareTo((Date) spnFechaInicio.getValue()) <= 0) { //Si el spinner no esta en el pasado
			    
			} else {
			    JOptionPane.showMessageDialog(null,
				    "No se pueden planificar actividades en el pasado.");
			    spnFechaInicio.setValue(new Date());

			}
		    }
		});
	}
	return spnFechaInicio;
    }
    private JPanel getPanelActividad() {
	if (panelActividad == null) {
	    panelActividad = new JPanel();
	    panelActividad.add(getLblActividad());
	    panelActividad.add(getCmbActividades());
	}
	return panelActividad;
    }
    private JPanel getPanelOcupanMenos() {
	if (panelOcupanMenos == null) {
	    panelOcupanMenos = new JPanel();
	    panelOcupanMenos.setLayout(new GridLayout(0, 1, 0, 0));
	    panelOcupanMenos.add(getPanelActividad());
	    panelOcupanMenos.add(getPanelInstalaciones());
	    panelOcupanMenos.add(getPanelFecha());
	    panelOcupanMenos.add(getPanelDiasDeLaSemana());
	    panelOcupanMenos.add(getPanelDuracion());
	    panelOcupanMenos.add(getPanelLimitePlazas());
	}
	return panelOcupanMenos;
    }
    private JLabel getLblFechaFin() {
	if (lblFechaFin == null) {
	    lblFechaFin = new JLabel("Fecha fin:");
	    lblFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblFechaFin;
    }
    private JSpinner getSpnFechaFin() {
	if (spnFechaFin == null) {
	    spnFechaFin = new JSpinner();
	    spnFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    SimpleDateFormat model = new SimpleDateFormat("dd/MM/yy");
	    spnFechaFin = new JSpinner(new SpinnerDateModel());
	    spnFechaFin.setEditor(new JSpinner.DateEditor(spnFechaFin, model.toPattern()));
	    spnFechaFin.setFont(new Font("Tahoma", Font.PLAIN, 18));
	    
	    spnFechaFin.addChangeListener(new ChangeListener() {
		    public void stateChanged(ChangeEvent e) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DATE, -1);
			Date yesterday = calendar.getTime();
			
			calendar.setTime((Date) spnFechaInicio.getValue());
			calendar.add(Calendar.DATE, -1);
			Date ini = calendar.getTime();
			
			if (yesterday.compareTo((Date) spnFechaFin.getValue()) <= 0
				&& ((Date) spnFechaFin.getValue()).compareTo(ini) > 0) { 
			    //Si el spinner no esta en el pasado ni fin es antes que inicio
			    
			} else {
			    JOptionPane.showMessageDialog(null,
				    "No se pueden planificar actividades en el pasado ni que la fecha fin sea anterior a la fecha inicio.");
			    spnFechaFin.setValue((Date) spnFechaInicio.getValue());
			}
		    }
		});
	}
	return spnFechaFin;
    }
    private JPanel getPanelDuracion() {
	if (panelDuracion == null) {
	    panelDuracion = new JPanel();
	    panelDuracion.add(getLblHoraInicio());
	    panelDuracion.add(getSpnHoraInicio());
	    panelDuracion.add(getLblHoraFin());
	    panelDuracion.add(getSpnHoraFin());
	    panelDuracion.add(getChkbxDuraTodoElDia());
	}
	return panelDuracion;
    }
    private JPanel getPanelDiasDeLaSemana() {
	if (panelDiasDeLaSemana == null) {
	    panelDiasDeLaSemana = new JPanel();
	    panelDiasDeLaSemana.setBorder(new TitledBorder(null, "D\u00EDas de la semana", TitledBorder.LEADING, TitledBorder.TOP, null, null));
	    panelDiasDeLaSemana.add(getChkbxLunes());
	    panelDiasDeLaSemana.add(getChkbxMartes());
	    panelDiasDeLaSemana.add(getChkbxMiercoles());
	    panelDiasDeLaSemana.add(getChkbxJueves());
	    panelDiasDeLaSemana.add(getChkbxViernes());
	    panelDiasDeLaSemana.add(getChkbxSabado());
	}
	return panelDiasDeLaSemana;
    }
    private JCheckBox getChkbxLunes() {
	if (chckbxLunes == null) {
	    chckbxLunes = new JCheckBox("Lunes");
	    chckbxLunes.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxLunes;
    }
    private JCheckBox getChkbxMartes() {
	if (chckbxMartes == null) {
	    chckbxMartes = new JCheckBox("Martes");
	    chckbxMartes.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxMartes;
    }
    private JCheckBox getChkbxMiercoles() {
	if (chckbxMiercoles == null) {
	    chckbxMiercoles = new JCheckBox("Mi\u00E9rcoles");
	    chckbxMiercoles.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxMiercoles;
    }
    private JCheckBox getChkbxJueves() {
	if (chckbxJueves == null) {
	    chckbxJueves = new JCheckBox("Jueves");
	    chckbxJueves.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxJueves;
    }
    private JCheckBox getChkbxViernes() {
	if (chckbxViernes == null) {
	    chckbxViernes = new JCheckBox("Viernes");
	    chckbxViernes.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxViernes;
    }
    private JCheckBox getChkbxSabado() {
	if (chckbxSabado == null) {
	    chckbxSabado = new JCheckBox("Sabado");
	    chckbxSabado.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxSabado;
    }
    private JLabel getLblHoraInicio() {
	if (lblHoraInicio == null) {
	    lblHoraInicio = new JLabel("Hora Inicio: ");
	    lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblHoraInicio;
    }
    private JSpinner getSpnHoraInicio() {
	if (spnHoraInicio == null) {
	    spnHoraInicio = new JSpinner();
	    spnHoraInicio.setToolTipText("Horas en punto");
	    spnHoraInicio.setModel(new SpinnerNumberModel(8, 8, 22, 1));
	    spnHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return spnHoraInicio;
    }
    private JLabel getLblHoraFin() {
	if (lblHoraFin == null) {
	    lblHoraFin = new JLabel("Hora fin: ");
	    lblHoraFin.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblHoraFin;
    }
    private JSpinner getSpnHoraFin() {
	if (spnHoraFin == null) {
	    spnHoraFin = new JSpinner();
	    spnHoraFin.setToolTipText("Horas en punto");
	    spnHoraFin.setModel(new SpinnerNumberModel(9, 8, 23, 1));
	    spnHoraFin.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return spnHoraFin;
    }
    private JCheckBox getChkbxDuraTodoElDia() {
	if (chckbxDuraTodoEl == null) {
	    chckbxDuraTodoEl = new JCheckBox("Dura todo el d\u00EDa");
	    chckbxDuraTodoEl.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    cambiarHabilitacionHoras();
		}
	    });
	    chckbxDuraTodoEl.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxDuraTodoEl;
    }

    private JPanel getPanelLimitePlazas() {
	if (panelLimitePlazas == null) {
	    panelLimitePlazas = new JPanel();
	    panelLimitePlazas.setLayout(new GridLayout(0, 2, 0, 0));
	    panelLimitePlazas.add(getPanel());
	    panelLimitePlazas.add(getPanel_1());
	}
	return panelLimitePlazas;
    }
    private JCheckBox getChckbxTieneLmiteDe() {
	if (chckbxTieneLmiteDe == null) {
	    chckbxTieneLmiteDe = new JCheckBox("Tiene l\u00EDmite de plazas");
	    chckbxTieneLmiteDe.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    habilitarDeshabilitarNumLim();
		}
	    });
	    chckbxTieneLmiteDe.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return chckbxTieneLmiteDe;
    }

    private JSpinner getSpnNumeroLimitePlazas() {
	if (spnNumeroLimitePlazas == null) {
	    spnNumeroLimitePlazas = new JSpinner();
	    spnNumeroLimitePlazas.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
	    spnNumeroLimitePlazas.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return spnNumeroLimitePlazas;
    }

    private JLabel getLblNmeroLmiteDe() {
	if (lblNmeroLmiteDe == null) {
	    lblNmeroLmiteDe = new JLabel("N\u00FAmero l\u00EDmite de plazas:");
	    lblNmeroLmiteDe.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return lblNmeroLmiteDe;
    }
    private JPanel getPanel() {
	if (panel == null) {
	    panel = new JPanel();
	    panel.add(getChckbxTieneLmiteDe());
	}
	return panel;
    }
    private JPanel getPanel_1() {
	if (panel_1 == null) {
	    panel_1 = new JPanel();
	    panel_1.add(getLblNmeroLmiteDe());
	    panel_1.add(getSpnNumeroLimitePlazas());
	}
	return panel_1;
    }

    private JButton getBtnAadirALista() {
	if (btnAadirALista == null) {
	    btnAadirALista = new JButton("Añadir a lista");
	    btnAadirALista.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		    añadirReservas();
		}
	    });
	    btnAadirALista.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	return btnAadirALista;
    }

    //	Mis métodos

    protected boolean comprobarCamposValidos() {
	return !(listaInstalaciones.isEmpty() || listaActividades.isEmpty());
    }
    protected void mostrarMensajeCorrecto() {
	JOptionPane.showMessageDialog(null, "La actividad ha reservado la instalaci�n con sus recursos necesarios correctamente.");
    }

    protected void mostrarAdminMainView(WindowEvent arg0) {
	arg0.getWindow().dispose();
    }

    protected void cambiarHabilitacionHoras() {
	if (chckbxDuraTodoEl.isSelected()) {
	    getLblHoraInicio().setEnabled(false);
	    getSpnHoraInicio().setEnabled(false);
	    getLblHoraFin().setEnabled(false);
	    getSpnHoraFin().setEnabled(false);
	} else {
	    getLblHoraInicio().setEnabled(true);
	    getSpnHoraInicio().setEnabled(true);
	    getLblHoraFin().setEnabled(true);
	    getSpnHoraFin().setEnabled(true);
	}
    }

    protected void deshabilitarNumPlazas() {
	getLblNmeroLmiteDe().setEnabled(false);
	getSpnNumeroLimitePlazas().setEnabled(false);
    }

    protected void habilitarNumPlazas() {
	getLblNmeroLmiteDe().setEnabled(true);
	getSpnNumeroLimitePlazas().setEnabled(true);
    }

    protected void habilitarDeshabilitarNumLim() {
	if (chckbxTieneLmiteDe.isSelected()) {
	    habilitarNumPlazas();
	} else {
	    deshabilitarNumPlazas();
	}
    }

    protected void activarDesactivarLimitePlazasSiRecursos() {
	if (listaActividades.get(getCmbActividades().getSelectedIndex()).isUsaRecursos()) {
	    getChckbxTieneLmiteDe().setEnabled(false);
	    getLblNmeroLmiteDe().setEnabled(false);
	    getSpnNumeroLimitePlazas().setEnabled(false);
	} else {
	    getChckbxTieneLmiteDe().setEnabled(true);
	    getLblNmeroLmiteDe().setEnabled(true);
	    getSpnNumeroLimitePlazas().setEnabled(true);

	    habilitarDeshabilitarNumLim();
	}
    }

    // Actividades

    private void cargarActividades() {
	listaActividades = getListaActividades();
	ActividadDto[] arrayActividades = transformarListaToArray();
	getCmbActividades().setModel(new DefaultComboBoxModel<ActividadDto>(arrayActividades));
    }

    private ActividadDto[] transformarListaToArray() {
	ActividadDto[] arrayActividades = new ActividadDto[listaActividades.size()];

	for (int i=0; i<listaActividades.size(); i++) {
	    arrayActividades[i] = listaActividades.get(i);
	}

	return arrayActividades;
    }

    protected List<ActividadDto> getListaActividades() {
	PlanActivityWithResourcesController pa = new PlanActivityWithResourcesController();
	List<ActividadDto> actividades = pa.getAllActividades();
	return actividades;
    }

    // Instalaciones

    private void cargarInstalacionesConLosRecursosDeLaActividadSeleccionada() {
	//Buscar instalaciones 
	listaInstalaciones = getListaInstalaciones();

	//Las a�ado al combobox
	recargarInstalaciones();
    }

    private void recargarInstalaciones() {
	InstalacionesTienenRecursosDto[] array = transformarListaInstToArray();
	getCmbInstalaciones().setModel(new DefaultComboBoxModel<InstalacionesTienenRecursosDto>(array));
    }

    private InstalacionesTienenRecursosDto[] transformarListaInstToArray() {
	InstalacionesTienenRecursosDto[] array = new InstalacionesTienenRecursosDto[listaInstalaciones.size()];
	for (int i=0; i<listaInstalaciones.size(); i++) {
	    array[i] = listaInstalaciones.get(i);
	}
	return array;
    }

    private List<InstalacionesTienenRecursosDto> getListaInstalaciones() {
	PlanActivityWithResourcesController pa = new PlanActivityWithResourcesController();
	List<InstalacionesTienenRecursosDto> instalaciones;
	if (listaActividades.get(getCmbActividades().getSelectedIndex()).isUsaRecursos()) {
	    instalaciones = pa.getInstalacionesQueContenganRecursos(listaActividades.get(getCmbActividades().getSelectedIndex()));
	} else {
	    instalaciones = pa.getAllInstalaciones();
	}

	//	System.out.println(instalaciones);

	return instalaciones;
    }

    // Reserva

    void añadirReservas() {
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

	//Reunir datos Actividad
	ActividadDto actividad = listaActividades.get(getCmbActividades().getSelectedIndex());

	//Reunir los datos InstalacionTieneRecursos
	InstalacionesTienenRecursosDto instalacionTR = listaInstalaciones.get(getCmbInstalaciones().getSelectedIndex());

	//Pasar a InstalacionDto
	InstalacionDto instalacion = new InstalacionDto();
	instalacion.setId(instalacionTR.getId_Instalacion());
	instalacion.setNombre(instalacionTR.getNombre_Instalacion());
	
	if (!comprobacionFechas()) {
	    JOptionPane.showMessageDialog(null, "Las fechas no son validas.");
	    return;
	}
	    

	//Se consigue la lista de fechas en las que la actividad reserva la instalacion
	List<Date> fechas = Fecha.getDatesOfWeekdaysInRange((Date) getSpnFechaInicio().getValue(), (Date) getSpnFechaFin().getValue(), 
		getChkbxLunes().isSelected(), getChkbxMartes().isSelected(), getChkbxMiercoles().isSelected(), 
		getChkbxJueves().isSelected(), getChkbxViernes().isSelected(), getChkbxSabado().isSelected());

	//Para cada fecha en la lista de fechas se hace una planificacion
	List<PlanificacionActividadDto> listaReservas = new ArrayList<PlanificacionActividadDto>();
	for (Date date : fechas) {
	    //id_actividad, id_instalacion, fecha, horainicio, horafin, limiteplazas, numlimiteplazas
	    PlanificacionActividadDto reserva = new PlanificacionActividadDto();
	    reserva.setId_Actividad(actividad.getId());
	    reserva.setNombre(actividad.getNombre());
	    reserva.setId_Instalacion(instalacion.getId());
	    reserva.setNombreInstalacion(instalacion.getNombre());
	    //Fecha
	    String format = formatter.format(date);
	    reserva.setFecha(format);
	    //Horas
	    if (chckbxDuraTodoEl.isSelected()) {
		reserva.setHoraInicio("08");
		reserva.setHoraFin("22");
	    } else {
		reserva.setHoraInicio(formatearHora((int) getSpnHoraInicio().getValue()));
		reserva.setHoraFin(formatearHora((int) getSpnHoraFin().getValue()));
	    }
	    //Limite de plazas
	    if (actividad.isUsaRecursos()) { //Actividad usa recursos
		reserva.setLimitePlazas(actividad.isUsaRecursos());
		reserva.setNumLimitePlazas(instalacionTR.getCantidadRecurso());
	    } else if (!actividad.isUsaRecursos() && getChckbxTieneLmiteDe().isSelected()) { //Actividad no usa recursos pero s� tiene limite de plazas
		reserva.setLimitePlazas(getChckbxTieneLmiteDe().isSelected());
		reserva.setNumLimitePlazas((int) getSpnNumeroLimitePlazas().getValue());
	    } else { //Actividad no usa recursos ni tiene limite de plazas
		reserva.setLimitePlazas(false);
		reserva.setNumLimitePlazas(0);
	    }

	    listaReservas.add(reserva);
	}

	//Añadir las nuevas reservas a la lista de reservas a hacer
	añadirReservasALista(listaReservas);
    }

    private boolean comprobacionFechas() {
	Date ini = (Date) getSpnFechaInicio().getValue();
	Date fin = (Date) getSpnFechaFin().getValue();
	return ini.compareTo(fin) <= 0
		&& ini.compareTo(new Date()) >= 0;
    }

    private String formatearHora(int value) {
	if (value < 10)
	    return "0" + value;
	return value + "";
    }

    private void añadirReservasALista(List<PlanificacionActividadDto> listaReservas) {
	PlanPeriodicActivityController pc = new PlanPeriodicActivityController();
	//Clon de listaReservasAHacer
	List<PlanificacionActividadDto> copia = new ArrayList<PlanificacionActividadDto>(listaReservasAHacer);

	if (copia.isEmpty())
	    for (PlanificacionActividadDto p : listaReservas)
		listaReservasAHacer.add(p);
	else {
	    for (PlanificacionActividadDto p : listaReservas) {
		boolean añadir = true;
		for (PlanificacionActividadDto p2 : copia) {
		    //Si es la misma fecha e instalacion
		    if (p.getFecha().equals(p2.getFecha()) && p.getId_Instalacion() == p2.getId_Instalacion()) {
			if (pc.seSolapan(p, p2)) {
			    añadir = false;
			    break;
			}
		    }
		}
		if (añadir) {
		    listaReservasAHacer.add(p);
		}
	    }
	}

//	System.out.println(listaReservasAHacer);
    }

    protected void reservarTodo() {
	if (!listaReservasAHacer.isEmpty()) {
	    //Realizar reservas
	    PlanPeriodicActivityController ppac = new PlanPeriodicActivityController();
	    mostrarMensajeErrorSiNecesario(ppac.planActivities(listaReservasAHacer));
	    listaReservasAHacer = new ArrayList<PlanificacionActividadDto>();
	} else {
	    JOptionPane.showMessageDialog(null, "No ha guardado ninguna planficación de actividad a realizar.");
	}
    }

    private void mostrarMensajeErrorSiNecesario(String planActivities) {
	if (planActivities == "")	
	    mostrarMensajeCorrecto();
	else
	    JOptionPane.showMessageDialog(null, "Ha ocurrido un error debido a colisiones con:\n" + planActivities);
    }
}