package ui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.ListModel;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.AbstractListModel;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import controller.MainViewController;
import dto.InstalacionDto;
import utils.RowHeaderRenderer;
import utils.TableRender;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.border.EmptyBorder;

public class Main extends JFrame {
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JLabel lblBackground;
    private JPanel center;
    private JPanel panel;
    private JPanel panelTitle;
    private JLabel lblGym;
    private JPanel panelUnregistered;
    private JPanel panelLeft;
    private JPanel panelRight;
    private JPanel panelCenter;
    private JLabel lblEmail;
    private JTextField txtEmail;
    private JLabel lblPass;
    private JTextField txtPass;
    private Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    private ImageIcon imageIcon = new ImageIcon(
	    Main.class.getResource("/img/Gym-Background-Final.jpg"));
    private Image i = (imageIcon.getImage().getScaledInstance(d.width, d.height,
	    Image.SCALE_FAST));
    private JLabel lblContinuarSinIniciar;
    private JPanel panelLogin;
    public CardLayout card;
    private JPanel panelMain;
    private JSplitPane splitPane;
    private JPanel panelMenu;
    private JLabel lblNewLabel;
    private JToggleButton btnReservarPlaza;
    private JToggleButton btnAsignarMonitor;
    private JButton btnPlanificarActividad;
    private JButton btnCrearActividad;
    private JButton btnCancelarPlanificacion;
    private JButton btnCancelarReserva;
    private JPanel panelView;
    private JToolBar toolBar;
    private JPanel panelTag;
    private JScrollPane scrollPane;
    private JPanel panelDays;
    private JLabel lblLunes;
    private JLabel lblMartes;
    private JLabel lblMiercoles;
    private JLabel lblJueves;
    private JLabel lblViernes;
    private JLabel lblSabado;
    private JLabel lblDomingo;
    private JLabel lblPickFacility;
    private JComboBox<InstalacionDto> comboBoxFacilities;
    private JLabel lblDate;
    private JSpinner spinner;
    private JPanel panelTop;
    private JTable table;
    private JLabel empty;
    private JList<String> rowHeader;
    private ListModel<String> lm;
    private JPanel panelTagReservations;
    private JLabel lblColor1;
    private JPanel panelTagPlanning;
    private JLabel lblReservasDeSocios;
    private JLabel lblColor2;
    private JLabel lblActividadesPlanificadas;
    private JPanel panelImage;
    private JPanel panelBtn;
    private JPanel panelTagAvailable;
    private JLabel labelColor3;
    private JLabel lblDisponible;
    // para poder deseleccionar el actual
    private final ButtonGroup buttonGroup = new ButtonGroup() {
	private static final long serialVersionUID = 7495914453451496362L;

	@Override
	public void setSelected(ButtonModel model, boolean selected) {
	    if (selected) {
		super.setSelected(model, selected);
	    } else {
		clearSelection();
	    }
	}
    };

    private JLabel lblAdministrador;
    private JPanel panelData;
    private JButton btnCerrarSesion;
    private JPanel panelMemberBtn;
    private JButton btnListaReservas;
    private JButton btnReservatePlace;
    private JButton btnActivitiesSchedule;
    private JButton btnCancelReservation;
    private JButton btnUndoAnularReserva;
    private JButton btnCancelReservaInstalacion;
    private JButton btnReservaInstalacion;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
	try {
	    UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
	} catch (Throwable e) {
	    e.printStackTrace();
	}

	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    Main frame = new Main();
		    frame.setVisible(true);
		    MainViewController mc = new MainViewController(frame);
		    mc.initController();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the frame.
     */
    public Main() {
	card = new CardLayout();
	addComponentListener(new ComponentAdapter() {
	    @Override
	    public void componentResized(ComponentEvent e) {
		d = new Dimension(e.getComponent().getWidth(),
			e.getComponent().getHeight());
		center.setBounds(0, 0, d.width, d.height);
		splitPane.setBounds(0, 0, d.width, d.height);
		rowHeader.setFixedCellHeight(
			(int) (scrollPane.getHeight() / 15.5));
		table.setRowHeight((int) (scrollPane.getHeight() / 15.5));

	    }
	});
	setIconImage(Toolkit.getDefaultToolkit()
		.getImage(Main.class.getResource("/img/icon.png")));
	setTitle("CENTRO DEPORTIVO");
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	contentPane = new JPanel();
	contentPane.setLayout(card);
	contentPane.add(getPanelLogin(), "login");
	contentPane.add(getPanelMain(), "main");
	setMinimumSize(new Dimension(1000, 600));
	setContentPane(contentPane);
	d = this.getBounds().getSize();
	setLocationRelativeTo(null);
	// TODO VISTA MOVIL?
	// setMinimumSize(new Dimension(300, 500));
    }

    private JLabel getLblBackground() {
	if (lblBackground == null) {
	    lblBackground = new JLabel(new ImageIcon(i));
	    lblBackground.setBounds(0, 0, d.width, d.height);
	}
	return lblBackground;
    }

    private JPanel getCenter() {
	if (center == null) {
	    center = new JPanel();
	    center.setBounds(0, 0, 1280, 720);
	    center.setOpaque(false);
	    center.setBackground(Color.LIGHT_GRAY);
	    center.setLayout(new GridBagLayout());
	    GridBagConstraints gbc_panel = new GridBagConstraints();
	    gbc_panel.gridx = 1;
	    gbc_panel.gridy = 1;
	    center.add(getPanel(), gbc_panel);
	}
	return center;
    }

    private JPanel getPanel() {
	if (panel == null) {
	    panel = new JPanel();
	    panel.setBounds(41, 31, 398, 248);
	    panel.setBackground(new Color(240, 97, 79));
	    panel.setLayout(new BorderLayout(0, 0));
	    panel.add(getPanelTitle(), BorderLayout.NORTH);
	    panel.add(getPanelUnregistered(), BorderLayout.SOUTH);
	    panel.add(getPanelLeft(), BorderLayout.WEST);
	    panel.add(getPanelRight(), BorderLayout.EAST);
	    panel.add(getPanelCenter(), BorderLayout.CENTER);
	}
	return panel;
    }

    private JPanel getPanelTitle() {
	if (panelTitle == null) {
	    panelTitle = new JPanel();
	    panelTitle.setBorder(new LineBorder(new Color(240, 97, 79), 20));
	    panelTitle.setOpaque(false);
	    panelTitle.add(getLabel_1());
	}
	return panelTitle;
    }

    private JLabel getLabel_1() {
	if (lblGym == null) {
	    lblGym = new JLabel("GYM");
	    lblGym.setFont(new Font("MS Gothic", Font.BOLD, 25));
	    lblGym.setHorizontalAlignment(SwingConstants.CENTER);
	    lblGym.setForeground(new Color(255, 255, 255));
	}
	return lblGym;
    }

    private JPanel getPanelUnregistered() {
	if (panelUnregistered == null) {
	    panelUnregistered = new JPanel();
	    panelUnregistered
		    .setBorder(new LineBorder(new Color(240, 97, 79), 20));
	    panelUnregistered.setOpaque(false);
	    panelUnregistered.add(getLblContinuarSinIniciar());
	}
	return panelUnregistered;
    }

    private JPanel getPanelLeft() {
	if (panelLeft == null) {
	    panelLeft = new JPanel();
	    panelLeft.setBorder(new LineBorder(new Color(240, 97, 79), 20));
	    panelLeft.setOpaque(false);
	}
	return panelLeft;
    }

    private JPanel getPanelRight() {
	if (panelRight == null) {
	    panelRight = new JPanel();
	    panelRight.setBorder(new LineBorder(new Color(240, 97, 79), 20));
	    panelRight.setOpaque(false);
	}
	return panelRight;
    }

    private JPanel getPanelCenter() {
	if (panelCenter == null) {
	    panelCenter = new JPanel();
	    panelCenter.setOpaque(false);
	    GridBagLayout gbl_panelCenter = new GridBagLayout();
	    gbl_panelCenter.columnWidths = new int[] { 100, 180, 0 };
	    gbl_panelCenter.rowHeights = new int[] { 60, 60, 0 };
	    gbl_panelCenter.columnWeights = new double[] { 0.0, 0.0,
		    Double.MIN_VALUE };
	    gbl_panelCenter.rowWeights = new double[] { 0.0, 0.0,
		    Double.MIN_VALUE };
	    panelCenter.setLayout(gbl_panelCenter);
	    GridBagConstraints gbc_lblEmail = new GridBagConstraints();
	    gbc_lblEmail.anchor = GridBagConstraints.WEST;
	    gbc_lblEmail.fill = GridBagConstraints.VERTICAL;
	    gbc_lblEmail.insets = new Insets(0, 0, 5, 5);
	    gbc_lblEmail.gridx = 0;
	    gbc_lblEmail.gridy = 0;
	    panelCenter.add(getLblEmail(), gbc_lblEmail);
	    GridBagConstraints gbc_txtEmail = new GridBagConstraints();
	    gbc_txtEmail.fill = GridBagConstraints.BOTH;
	    gbc_txtEmail.insets = new Insets(0, 0, 5, 0);
	    gbc_txtEmail.gridx = 1;
	    gbc_txtEmail.gridy = 0;
	    panelCenter.add(getTxtEmail(), gbc_txtEmail);
	    GridBagConstraints gbc_lblPass = new GridBagConstraints();
	    gbc_lblPass.fill = GridBagConstraints.BOTH;
	    gbc_lblPass.insets = new Insets(0, 0, 0, 5);
	    gbc_lblPass.gridx = 0;
	    gbc_lblPass.gridy = 1;
	    panelCenter.add(getLblPass(), gbc_lblPass);
	    GridBagConstraints gbc_txtPass = new GridBagConstraints();
	    gbc_txtPass.fill = GridBagConstraints.BOTH;
	    gbc_txtPass.gridx = 1;
	    gbc_txtPass.gridy = 1;
	    panelCenter.add(getTxtPass(), gbc_txtPass);
	}
	return panelCenter;
    }

    private JLabel getLblEmail() {
	if (lblEmail == null) {
	    lblEmail = new JLabel("EMAIL: ");
	    lblEmail.setForeground(new Color(255, 255, 255));
	    lblEmail.setLabelFor(getTxtEmail());
	    lblEmail.setDisplayedMnemonic('e');
	    lblEmail.setFont(new Font("Tahoma", Font.BOLD, 14));
	}
	return lblEmail;
    }

    public JTextField getTxtEmail() {
	if (txtEmail == null) {
	    txtEmail = new JTextField();
	    txtEmail.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    getTxtPass().grabFocus();
		}
	    });
	    txtEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
	}
	return txtEmail;
    }

    private JLabel getLblPass() {
	if (lblPass == null) {
	    lblPass = new JLabel("PASS: ");
	    lblPass.setForeground(new Color(255, 255, 255));
	    lblPass.setLabelFor(getTxtPass());
	    lblPass.setDisplayedMnemonic('p');
	    lblPass.setFont(new Font("Tahoma", Font.BOLD, 14));
	}
	return lblPass;
    }

    public JTextField getTxtPass() {
	if (txtPass == null) {
	    txtPass = new JTextField();
	    txtPass.setFont(new Font("Tahoma", Font.PLAIN, 14));
	}
	return txtPass;
    }

    private JLabel getLblContinuarSinIniciar() {
	if (lblContinuarSinIniciar == null) {
	    lblContinuarSinIniciar = new JLabel(
		    "Continuar sin iniciar sesión ");
	    lblContinuarSinIniciar.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
		    JOptionPane.showMessageDialog(null, "Pendiente...");
		}
	    });
	    lblContinuarSinIniciar.setDisplayedMnemonic('c');
	}
	return lblContinuarSinIniciar;
    }

    private JPanel getPanelLogin() {
	if (panelLogin == null) {
	    panelLogin = new JPanel();
	    panelLogin.setLayout(null);
	    panelLogin.add(getCenter());
	    panelLogin.add(getLblBackground());
	}
	return panelLogin;
    }

    public JPanel getPanelMain() {
	if (panelMain == null) {
	    panelMain = new JPanel();
	    panelMain.setLayout(new BorderLayout(0, 0));
	    panelMain.add(getSplitPane());
	}
	return panelMain;
    }

    private JSplitPane getSplitPane() {
	if (splitPane == null) {
	    splitPane = new JSplitPane();
	    splitPane.setEnabled(false);
	    splitPane.setBackground(Color.DARK_GRAY);
	    splitPane.setLeftComponent(getPanelMenu());
	    splitPane.setRightComponent(getPanelView());
	    // TODO CAMBIAR POR DEFECTO
	    splitPane.setDividerLocation(200);
	}
	return splitPane;
    }

    public JPanel getPanelMenu() {
	if (panelMenu == null) {
	    panelMenu = new JPanel();
	    panelMenu.setBackground(Color.GRAY);
	    panelMenu.setPreferredSize(new Dimension(200, 600));
	    panelMenu.setLayout(new GridLayout(2, 1, 0, 10));
	    panelMenu.add(getPanel_2_1());
	    panelMenu.add(getPanelBtn());
	}
	return panelMenu;
    }

    private JLabel getLblNewLabel() {
	if (lblNewLabel == null) {
	    lblNewLabel = new JLabel("");
	    lblNewLabel.setBorder(new EmptyBorder(15, 0, 10, 0));
	    lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    lblNewLabel.setIcon(
		    new ImageIcon(Main.class.getResource("/img/image.png")));
	}
	return lblNewLabel;
    }

    private JPanel getPanelView() {
	if (panelView == null) {
	    panelView = new JPanel();
	    panelView.setOpaque(false);
	    panelView.setLayout(new BorderLayout(0, 0));
	    panelView.add(getPanelTag(), BorderLayout.SOUTH);
	    panelView.add(getScrollPane(), BorderLayout.CENTER);
	    panelView.add(getPanelTop(), BorderLayout.NORTH);
	}
	return panelView;
    }

    private JToolBar getToolBar() {
	if (toolBar == null) {
	    toolBar = new JToolBar();
	    toolBar.setFloatable(false);
	    toolBar.setBackground(Color.WHITE);
	    toolBar.add(getLblPickFacility());
	    toolBar.add(getComboBoxFacilities());
	    toolBar.add(getLblDate());
	    toolBar.add(getSpinnerDate());
	}
	return toolBar;
    }

    private JPanel getPanelTag() {
	if (panelTag == null) {
	    panelTag = new JPanel();
	    panelTag.setOpaque(false);
	    panelTag.setLayout(new GridLayout(1, 2, 0, 0));
	    panelTag.add(getPanelTagReservations());
	    panelTag.add(getPanelTagPlanning());
	    panelTag.add(getPanelTagAvailable());
	}
	return panelTag;
    }

    private JScrollPane getScrollPane() {
	if (scrollPane == null) {
	    scrollPane = new JScrollPane();
	    scrollPane.setViewportView(getTable());
	    scrollPane.setViewportBorder(null);
	    scrollPane.setRowHeaderView(rowHeader);
	}
	return scrollPane;
    }

    private JPanel getPanelDays() {
	if (panelDays == null) {
	    panelDays = new JPanel();
	    panelDays.setBackground(Color.GRAY);
	    panelDays.setOpaque(false);
	    panelDays.setLayout(new GridLayout(0, 8, 0, 0));
	    panelDays.add(getLabelEmpty());
	    panelDays.add(getLblLunes());
	    panelDays.add(getLblMartes());
	    panelDays.add(getLblMiercoles());
	    panelDays.add(getLblJueves());
	    panelDays.add(getLblViernes());
	    panelDays.add(getLblSabado());
	    panelDays.add(getLblDomingo());
	}
	return panelDays;
    }

    private JLabel getLabelEmpty() {
	if (empty == null) {
	    empty = new JLabel("");
	}
	return empty;
    }

    private JLabel getLblLunes() {
	if (lblLunes == null) {
	    lblLunes = new JLabel("LUNES");
	    lblLunes.setForeground(Color.WHITE);
	    lblLunes.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return lblLunes;
    }

    private JLabel getLblMartes() {
	if (lblMartes == null) {
	    lblMartes = new JLabel("MARTES");
	    lblMartes.setForeground(Color.WHITE);
	    lblMartes.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return lblMartes;
    }

    private JLabel getLblMiercoles() {
	if (lblMiercoles == null) {
	    lblMiercoles = new JLabel("MIÉRCOLES");
	    lblMiercoles.setForeground(Color.WHITE);
	    lblMiercoles.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return lblMiercoles;
    }

    private JLabel getLblJueves() {
	if (lblJueves == null) {
	    lblJueves = new JLabel("JUEVES");
	    lblJueves.setForeground(Color.WHITE);
	    lblJueves.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return lblJueves;
    }

    private JLabel getLblViernes() {
	if (lblViernes == null) {
	    lblViernes = new JLabel("VIERNES");
	    lblViernes.setForeground(Color.WHITE);
	    lblViernes.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return lblViernes;
    }

    private JLabel getLblSabado() {
	if (lblSabado == null) {
	    lblSabado = new JLabel("SÁBADO");
	    lblSabado.setForeground(Color.WHITE);
	    lblSabado.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return lblSabado;
    }

    private JLabel getLblDomingo() {
	if (lblDomingo == null) {
	    lblDomingo = new JLabel("DOMINGO");
	    lblDomingo.setForeground(Color.WHITE);
	    lblDomingo.setFont(new Font("Tahoma", Font.PLAIN, 12));
	}
	return lblDomingo;
    }

    private JLabel getLblPickFacility() {
	if (lblPickFacility == null) {
	    lblPickFacility = new JLabel("Instalación: ");
	    lblPickFacility.setFont(new Font("Dialog", Font.BOLD, 16));
	    lblPickFacility.setForeground(Color.WHITE);
	    lblPickFacility.setDisplayedMnemonic('I');
	}
	return lblPickFacility;
    }

    public JComboBox<InstalacionDto> getComboBoxFacilities() {
	if (comboBoxFacilities == null) {
	    comboBoxFacilities = new JComboBox<InstalacionDto>();
	    comboBoxFacilities.setOpaque(false);
	    comboBoxFacilities.setFont(new Font("Tahoma", Font.PLAIN, 16));
	}
	return comboBoxFacilities;
    }

    private JLabel getLblDate() {
	if (lblDate == null) {
	    lblDate = new JLabel("Fecha: ");
	    lblDate.setFont(new Font("Dialog", Font.BOLD, 16));
	}
	return lblDate;
    }

    public JSpinner getSpinnerDate() {
	if (spinner == null) {
	    SimpleDateFormat model = new SimpleDateFormat("dd-MM-yyyy");
	    spinner = new JSpinner(new SpinnerDateModel());
	    spinner.setOpaque(false);
	    spinner.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    spinner.setEditor(
		    new JSpinner.DateEditor(spinner, model.toPattern()));
	    spinner.setBounds(868, 44, 105, 20);
	}
	return spinner;
    }

    private JPanel getPanelTop() {
	if (panelTop == null) {
	    panelTop = new JPanel();
	    panelTop.setForeground(Color.WHITE);
	    panelTop.setLayout(new GridLayout(0, 1, 0, 0));
	    panelTop.add(getToolBar());
	    panelTop.add(getPanelDays());
	}
	return panelTop;
    }

    public JTable getTable() {
	if (table == null) {
	    table = new JTable() {
		private static final long serialVersionUID = 1L;

		public boolean isCellEditable(int row, int column) {
		    return false;
		};
	    };

	    table.getTableHeader().setReorderingAllowed(false);
	    lm = getFixedRowNameModel();
	    rowHeader = new JList<String>(lm);
	    rowHeader.setOpaque(false);
	    rowHeader.setBackground(Color.DARK_GRAY);
	    rowHeader.setFixedCellWidth(80);
	    rowHeader.setFixedCellHeight(25);
	    table.setRowHeight(25);
	    rowHeader.setCellRenderer(new RowHeaderRenderer(table));
	    TableRender tr = new TableRender();
	    table.setDefaultRenderer(Object.class, tr);
	}
	return table;
    }

    private AbstractListModel<String> getFixedRowNameModel() {
	return new AbstractListModel<String>() {
	    private static final long serialVersionUID = 1L;
	    String[] rowTitle = { "08:00:00", "09:00:00", "10:00:00",
		    "11:00:00", "12:00:00", "13:00:00", "14:00:00", "15:00:00",
		    "16:00:00", "17:00:00", "18:00:00", "19:00:00", "20:00:00",
		    "21:00:00", "22:00:00" };

	    public int getSize() {
		return rowTitle.length;
	    }

	    public String getElementAt(int index) {
		return rowTitle[index];
	    }
	};
    }

    public ListModel<String> getRowTitle() {
	return lm;
    }

    private JPanel getPanelTagReservations() {
	if (panelTagReservations == null) {
	    panelTagReservations = new JPanel();
	    panelTagReservations.add(getLabel_1_1());
	    panelTagReservations.add(getLabel_1_2());
	}
	return panelTagReservations;
    }

    private JLabel getLabel_1_1() {
	if (lblColor1 == null) {
	    lblColor1 = new JLabel("     ");
	    lblColor1.setOpaque(true);
	    lblColor1.setBackground(Color.DARK_GRAY);
	}
	return lblColor1;
    }

    private JPanel getPanelTagPlanning() {
	if (panelTagPlanning == null) {
	    panelTagPlanning = new JPanel();
	    panelTagPlanning.add(getLabel_1_3());
	    panelTagPlanning.add(getLblActividadesPlanificadas());
	}
	return panelTagPlanning;
    }

    private JLabel getLabel_1_2() {
	if (lblReservasDeSocios == null) {
	    lblReservasDeSocios = new JLabel("Reservas de socios");
	}
	return lblReservasDeSocios;
    }

    private JLabel getLabel_1_3() {
	if (lblColor2 == null) {
	    lblColor2 = new JLabel("     ");
	    lblColor2.setOpaque(true);
	    lblColor2.setBackground(new Color(240, 97, 79));
	}
	return lblColor2;
    }

    private JLabel getLblActividadesPlanificadas() {
	if (lblActividadesPlanificadas == null) {
	    lblActividadesPlanificadas = new JLabel("Actividades planificadas");
	}
	return lblActividadesPlanificadas;
    }

    private JPanel getPanel_2_1() {
	if (panelImage == null) {
	    panelImage = new JPanel();
	    panelImage.setBackground(Color.GRAY);
	    panelImage.setLayout(new GridLayout(2, 1, 2, 2));
	    panelImage.add(getLblNewLabel());
	    panelImage.add(getPanelData());
	}
	return panelImage;
    }

    public JPanel getPanelBtn() {
	if (panelBtn == null) {
	    panelBtn = new JPanel();
	    panelBtn.setBackground(Color.GRAY);
	    panelBtn.setBorder(new EmptyBorder(10, 5, 10, 5));
	    panelBtn.setLayout(new GridLayout(0, 1, 3, 5));
	    panelBtn.add(getBtnAsignarMonitor());
	    panelBtn.add(getBtnCancelarPlanificacion());
	    panelBtn.add(getBtnCancelarReserva());
	    panelBtn.add(getBtnCrearActividad());
	    panelBtn.add(getBtnPlanificarActividad());
	    panelBtn.add(getBtnReservarPlaza());
	}
	return panelBtn;
    }

    public JToggleButton getBtnReservarPlaza() {
	if (btnReservarPlaza == null) {
	    btnReservarPlaza = new JToggleButton("Reservar Plaza");
	    btnReservarPlaza.setFocusPainted(false);
	    buttonGroup.add(btnReservarPlaza);
	    btnReservarPlaza.setForeground(Color.WHITE);
	    btnReservarPlaza.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnReservarPlaza.setBackground(Color.BLACK);
	}
	return btnReservarPlaza;
    }

    public JToggleButton getBtnAsignarMonitor() {
	if (btnAsignarMonitor == null) {
	    btnAsignarMonitor = new JToggleButton("Asignar Monitor");
	    btnAsignarMonitor.setFocusPainted(false);
	    btnAsignarMonitor.setForeground(Color.WHITE);
	    buttonGroup.add(btnAsignarMonitor);
	    btnAsignarMonitor.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnAsignarMonitor.setBackground(Color.BLACK);
	}
	return btnAsignarMonitor;
    }

    public JButton getBtnPlanificarActividad() {
	if (btnPlanificarActividad == null) {
	    btnPlanificarActividad = new JButton("Planificar Actividad");
	    btnPlanificarActividad.setFocusPainted(false);
	    btnPlanificarActividad.setForeground(Color.WHITE);
	    btnPlanificarActividad.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnPlanificarActividad.setBackground(Color.BLACK);
	}
	return btnPlanificarActividad;
    }

    public JButton getBtnCrearActividad() {
	if (btnCrearActividad == null) {
	    btnCrearActividad = new JButton("Crear Actividad");
	    btnCrearActividad.setFocusPainted(false);
	    btnCrearActividad.setForeground(Color.WHITE);
	    btnCrearActividad.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnCrearActividad.setBackground(Color.BLACK);
	}
	return btnCrearActividad;
    }

    public JButton getBtnCancelarPlanificacion() {
	if (btnCancelarPlanificacion == null) {
	    btnCancelarPlanificacion = new JButton("Cancelar Planificación");
	    btnCancelarPlanificacion.setFocusPainted(false);
	    btnCancelarPlanificacion.setForeground(Color.WHITE);
	    btnCancelarPlanificacion
		    .setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnCancelarPlanificacion.setBackground(Color.BLACK);
	}
	return btnCancelarPlanificacion;
    }

    public JButton getBtnCancelarReserva() {
	if (btnCancelarReserva == null) {
	    btnCancelarReserva = new JButton("Cancelar Reserva Plaza");
	    btnCancelarReserva.setFocusPainted(false);
	    btnCancelarReserva.setForeground(Color.WHITE);
	    btnCancelarReserva.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnCancelarReserva.setBackground(Color.BLACK);
	}
	return btnCancelarReserva;
    }

    private JPanel getPanelTagAvailable() {
	if (panelTagAvailable == null) {
	    panelTagAvailable = new JPanel();
	    panelTagAvailable.add(getLabelColor3());
	    panelTagAvailable.add(getLblDisponible());
	}
	return panelTagAvailable;
    }

    private JLabel getLabelColor3() {
	if (labelColor3 == null) {
	    labelColor3 = new JLabel("     ");
	    labelColor3.setOpaque(true);
	    labelColor3.setBackground(Color.WHITE);
	}
	return labelColor3;
    }

    private JLabel getLblDisponible() {
	if (lblDisponible == null) {
	    lblDisponible = new JLabel("Disponible");
	}
	return lblDisponible;
    }

    public JLabel getLblName() {
	if (lblAdministrador == null) {
	    lblAdministrador = new JLabel("Administrador");
	    lblAdministrador.setBounds(12, 23, 176, 59);
	    lblAdministrador.setHorizontalAlignment(SwingConstants.CENTER);
	}
	return lblAdministrador;
    }

    private JPanel getPanelData() {
	if (panelData == null) {
	    panelData = new JPanel();
	    panelData.setBorder(new EmptyBorder(50, 0, 0, 0));
	    panelData.setOpaque(false);
	    panelData.setRequestFocusEnabled(false);
	    panelData.setLayout(null);
	    panelData.add(getLblName());
	    panelData.add(getBtnCerrarSesion());
	}
	return panelData;
    }

    public JButton getBtnCerrarSesion() {
	if (btnCerrarSesion == null) {
	    btnCerrarSesion = new JButton("Cerrar sesion");
	    btnCerrarSesion.setFocusPainted(false);
	    btnCerrarSesion.setBounds(53, 83, 93, 24);
	}
	return btnCerrarSesion;
    }

    // cambios socio
    public JPanel getPanelMemberBtn() {
	if (panelMemberBtn == null) {
	    panelMemberBtn = new JPanel();
	    panelMemberBtn.setBackground(Color.GRAY);
	    panelMemberBtn.setBorder(new EmptyBorder(10, 5, 10, 5));
	    panelMemberBtn.setLayout(new GridLayout(0, 1, 3, 5));
	    panelMemberBtn.add(getBtnListaReservas());
	    panelMemberBtn.add(getBtnReservatePlace());
	    panelMemberBtn.add(getBtnActivitiesSchedule());
	    panelMemberBtn.add(getBtnCancelReservation());
	    panelMemberBtn.add(getBtnUndoAnularReserva());
	    panelMemberBtn.add(getBtnReservaInstalacion());
	    panelMemberBtn.add(getBtnCancelReservaInstalacion());
	}
	return panelMemberBtn;
    }

    public JButton getBtnListaReservas() {
	if (btnListaReservas == null) {
	    btnListaReservas = new JButton("Listar reservas");
	    btnListaReservas.setFocusPainted(false);
	    btnListaReservas.setForeground(Color.WHITE);
	    btnListaReservas.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnListaReservas.setBackground(Color.BLACK);
	}
	return btnListaReservas;
    }

    public JButton getBtnReservatePlace() {
	if (btnReservatePlace == null) {
	    btnReservatePlace = new JButton("Reservar plaza");
	    btnReservatePlace.setFocusPainted(false);
	    btnReservatePlace.setForeground(Color.WHITE);
	    btnReservatePlace.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnReservatePlace.setBackground(Color.BLACK);
	}
	return btnReservatePlace;
    }

    public JButton getBtnActivitiesSchedule() {
	if (btnActivitiesSchedule == null) {
	    btnActivitiesSchedule = new JButton("Ver horario actividades");
	    btnActivitiesSchedule.setFocusPainted(false);
	    btnActivitiesSchedule.setForeground(Color.WHITE);
	    btnActivitiesSchedule.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnActivitiesSchedule.setBackground(Color.BLACK);
	}
	return btnActivitiesSchedule;
    }

    public JButton getBtnCancelReservation() {
	if (btnCancelReservation == null) {
	    btnCancelReservation = new JButton("Anular reserva");
	    btnCancelReservation.setFocusPainted(false);
	    btnCancelReservation.setForeground(Color.WHITE);
	    btnCancelReservation.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnCancelReservation.setBackground(Color.BLACK);
	}
	return btnCancelReservation;
    }

    public JButton getBtnUndoAnularReserva() {
	if (btnUndoAnularReserva == null) {
	    btnUndoAnularReserva = new JButton("Undo anular reserva");
	    btnUndoAnularReserva.setFocusPainted(false);
	    btnUndoAnularReserva.setForeground(Color.WHITE);
	    btnUndoAnularReserva.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    btnUndoAnularReserva.setBackground(Color.BLACK);
	}
	return btnUndoAnularReserva;
    }
    
    public JButton getBtnReservaInstalacion() {
  	if (btnReservaInstalacion == null) {
  	    btnReservaInstalacion = new JButton("Reserva instalacion");
  	    btnReservaInstalacion.setFocusPainted(false);
  	    btnReservaInstalacion.setForeground(Color.WHITE);
  	    btnReservaInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 12));
  	    btnReservaInstalacion.setBackground(Color.BLACK);
  	}
  	return btnReservaInstalacion;
      }

      public JButton getBtnCancelReservaInstalacion() {
  	if (btnCancelReservaInstalacion == null) {
  	    btnCancelReservaInstalacion = new JButton("Cancelar instalacion");
  	    btnCancelReservaInstalacion.setFocusPainted(false);
  	    btnCancelReservaInstalacion.setForeground(Color.WHITE);
  	    btnCancelReservaInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 12));
  	    btnCancelReservaInstalacion.setBackground(Color.BLACK);
  	}
  	return btnCancelReservaInstalacion;
      }
      
      
}
