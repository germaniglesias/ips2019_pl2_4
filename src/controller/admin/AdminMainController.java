package controller.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import controller.MainViewController;
import dto.InstalacionDto;
import dto.PlanificacionActividadDto;
import dto.SocioReservaInstalacionDto;
import ui.Main;
import ui.admin.NewActivityView;
import ui.admin.PayFacilityReservationView;
import ui.admin.PlanPeriodicActivityView;
import utils.Fecha;
import utils.StateBackground;
import ui.admin.CancelActivityView;
import ui.admin.CancelReservateMainView;

public class AdminMainController {

    private Main mainView;
    private StateBackground state;
    private MainViewController mainController;

    public AdminMainController(MainViewController instance) {
	this.mainController = instance;
	this.mainView = instance.getView();
	initView();
    }

    public Main getView() {
	return mainView;
    }

    private void initView() {
	mainView.setTitle("CENTRO DEPORTIVO: ADMINISTRADOR");
	mainView.getLblName().setText("Administrador");
	mainView.getPanelMenu().remove(mainView.getPanelMemberBtn());
	mainView.getPanelMenu().add(mainView.getPanelBtn());
	mainView.card.show(mainView.getContentPane(), "main");
	state = new StateBackground(mainController);
    }

    public void initController() {
	mainView.getBtnCrearActividad().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		crearNewActivityView();
	    }
	});

	mainView.getBtnPlanificarActividad()
		.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
			createPlanPeriodicActivity();
		    }
		});

	// Cancelar actividades
	mainView.getBtnCancelarPlanificacion()
		.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			cancelPlanActivityView();
		    }
		});

	mainView.getBtnCancelarReserva()
		.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			cancelReservateView();

		    }
		});


	mainView.getTable().addMouseListener(new MouseAdapter() {
	    public void mouseClicked(MouseEvent e) {
		int row = mainView.getTable().rowAtPoint(e.getPoint());
		int col = mainView.getTable().columnAtPoint(e.getPoint());
		if (mainView.getTable().getValueAt(row, col) != null) {
		    if (mainView.getTable().getValueAt(row,
			    col) instanceof SocioReservaInstalacionDto) {
			pagarReservaView((SocioReservaInstalacionDto) mainView
				.getTable().getValueAt(row, col));
		    } else if (mainView.getBtnAsignarMonitor().isSelected()) {
			PlanificacionActividadDto dto = (PlanificacionActividadDto) mainView
				.getTable().getValueAt(row, col);
			new AssignMonitorController(mainController, dto);
		    } else if (mainView.getBtnReservarPlaza().isSelected()) {
			PlanificacionActividadDto dto = (PlanificacionActividadDto) mainView
				.getTable().getValueAt(row, col);
			new SeatReservationController(mainController, dto);
		    }

		} else {
		    if (isValidReservationDate(row, col)) {
			String day = mainView.getTable().getModel()
				.getColumnName(col);
			String hour = mainView.getRowTitle().getElementAt(row);
			new FacilityReservationController(mainController,
				(InstalacionDto) mainView
					.getComboBoxFacilities()
					.getSelectedItem(),
				day, hour);
		    }
		}
	    }
	});

	mainView.getBtnAsignarMonitor().addItemListener(state);
	mainView.getBtnReservarPlaza().addItemListener(state);

    }

    protected boolean isValidReservationDate(int row, int col) {
	LocalDate maxDayRange = LocalDate.now().plusDays(15);
	Date date;
	try {
	    date = Fecha.getDateFrom(
		    mainView.getTable().getModel().getColumnName(col));
	    LocalDate selectedDay = Fecha.dateToLocalDate(date);
	    if (maxDayRange.isAfter(selectedDay)
		    && LocalDate.now().isBefore(selectedDay)) {
		return true;
	    }
	} catch (ParseException e1) {

	    e1.printStackTrace();
	}

	return false;
    }

    protected void crearNewActivityView() {
	NewActivityView frame = new NewActivityView(this);
	frame.setVisible(true);
    }

    protected void createPlanPeriodicActivity() {
	PlanPeriodicActivityView frame = new PlanPeriodicActivityView(this);
	frame.setVisible(true);
    }

    protected void cancelPlanActivityView() {
	CancelActivityView frame = new CancelActivityView(this);
	frame.setVisible(true);
    }

    protected void cancelReservateView() {
	CancelReservateMainView frame = null;
	frame = new CancelReservateMainView(this.getView());
	frame.setVisible(true);
    }

    // Ventana emergente desde tabla
    protected void pagarReservaView(SocioReservaInstalacionDto reserva) {
	PayFacilityReservationView frame = new PayFacilityReservationView(this,
		reserva);
	frame.setVisible(true);
    }

}
