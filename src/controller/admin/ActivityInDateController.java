package controller.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import business.service.implementation.admin.ActivityInDate;
import database.gestorDDBB;
import dto.ActividadDto;

public class ActivityInDateController {
    private ActivityInDate aid = new ActivityInDate();

    public boolean isFechaValida(ActividadDto actividad, Date date) {
	boolean valida = false;

	try (Connection con = gestorDDBB.getConnection();) {
	    aid.setConnection(con);
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    valida = aid.isFechaValida(actividad, sdf.format(date));
	} catch (SQLException e) {
	    throw new RuntimeException("Error en la conexión");
	}

	return valida;
    }

}
