package controller.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.ActividadSocioReservaDto;
import dto.InstalacionDto;
import utils.Settings;

public class CancelReservaController {

    List<ActividadSocioReservaDto> asrs = new ArrayList<ActividadSocioReservaDto>();
    private ActividadSocioReservaDto asr;

    List<InstalacionDto> is = new ArrayList<InstalacionDto>();
    private InstalacionDto i;

    public List<ActividadSocioReservaDto> listarReservas() {

	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;
	try {
	    String consulta = Settings.get("SQL_CANCEL_LISTAR_RESERVAS_SOCIO");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    asrs = new ArrayList<ActividadSocioReservaDto>();

	    while (rs.next()) {
		asr = new ActividadSocioReservaDto();
		asr.dni_Socio = rs.getString("dni");

		asr.fecha = rs.getString("fecha");
		asr.horario_Inicio = rs.getString("horaInicio");
		asr.horario_Fin = rs.getString("horaFin");
		asr.id_Actividad = rs.getString("id_Actividad");
		asr.id_SocioReservaActividad = rs.getString("id");

		asr.nombre_Instalacion = rs.getString("nombre");
		asrs.add(asr);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return asrs;
    }

    public void modificarReserva(int iDInstalacionEscogida, int hi, int hf,
	    String hora, Object iD) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_CANCEL_UPDATE_ACTIVIDAD_SOCIO");
	    ps = conn.prepareStatement(consulta);

	    ps.setInt(1, iDInstalacionEscogida);
	    ps.setString(2, hora);
	    ps.setInt(3, hi);
	    ps.setInt(4, hf);
	    ps.setObject(5, iD);

	    ps.executeUpdate();
	    conn.commit();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}

    }

    public List<InstalacionDto> listarInstalaciones() {
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;
	try {
	    String consulta = Settings.get("SQL_CANCEL_LISTAR_INSTALACIONES");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    is = new ArrayList<InstalacionDto>();

	    while (rs.next()) {
		i = new InstalacionDto();
		i.setId(rs.getLong("id"));
		i.setNombre(rs.getString("nombre"));

		is.add(i);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return is;
    }

    public void deleteReserva(String hInicio, String hFin, String hora,
	    Object iD) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_DELETE_RESERVA");
	    ps = conn.prepareStatement(consulta);

	    ps.setObject(1, iD);
	    ps.setString(2, hora);
	    ps.setString(3, hInicio);
	    ps.setString(4, hFin);

	    ps.executeUpdate();
	    conn.commit();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}

    }

    public void deleteReserva(String hora, Object iD) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_DELETE_RESERVA_Prueba");
	    ps = conn.prepareStatement(consulta);

	    ps.setObject(1, iD);
	    ps.setString(2, hora);
	    ps.executeUpdate();
	    conn.commit();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}

    }

    public void deleteReserva(String hi, String hf, Object iD) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_DELETE_RESERVA_Prueba1");
	    ps = conn.prepareStatement(consulta);

	    ps.setObject(1, iD);
	    ps.setString(2, hi);
	    ps.setString(3, hf);
	    ps.executeUpdate();
	    conn.commit();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}

    }

    public List<ActividadSocioReservaDto> ListarActividades() {

	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;
	try {
	    String consulta = Settings.get("SQL_CANCEL_LISTAR_ACTIVIDADES");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    asrs = new ArrayList<ActividadSocioReservaDto>();

	    while (rs.next()) {
		asr = new ActividadSocioReservaDto();
		asr.id_Actividad = rs.getString("id");
		asr.nombre_Actividad = rs.getString("nombre");

		asrs.add(asr);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return asrs;

    }

    public void deleteReserva(String hi, String hf, String hora, String hora2, 
	    Object iD) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_DELETE_RESERVA_VARIAS_CONDICIONES");
	    ps = conn.prepareStatement(consulta);

	    ps.setObject(1, iD);
	    ps.setString(2, hora);
	    ps.setString(3, hora2);
	    ps.setString(4, hi);
	    ps.setString(5, hf);

	    ps.executeUpdate();
	    conn.commit();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	
    }

    public void deleteReserva2(String hora, String hora2, Object iD) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_DELETE_RESERVA_VARIAS_CONDICIONES_2");
	    ps = conn.prepareStatement(consulta);

	    ps.setObject(1, iD);
	    ps.setString(2, hora);
	    ps.setString(3, hora2);
	    

	    ps.executeUpdate();
	    conn.commit();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	
    }
}
