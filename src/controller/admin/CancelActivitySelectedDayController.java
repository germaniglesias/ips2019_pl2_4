package controller.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import business.service.implementation.admin.CancelActivitySelectedDay;
import database.gestorDDBB;
import dto.ActividadDto;
import dto.PlanificacionActividadDto;
import dto.SocioDto;

public class CancelActivitySelectedDayController {
    private CancelActivitySelectedDay ca = new CancelActivitySelectedDay();

    public List<ActividadDto> getListActivities() {
	List<ActividadDto> actividades = null;

	try (Connection con = gestorDDBB.getConnection();) {
	    ca.setConnection(con);
	    actividades = ca.getListActivities();
	} catch (SQLException e) {
	    throw new RuntimeException("Error en la conexión");
	}

	return actividades;
    }

    public List<String> getHorasParaActividadDia(ActividadDto act, String dia) {
	List<String> horas = new ArrayList<String>();
	try (Connection con = gestorDDBB.getConnection();) {
	    ca.setConnection(con);
	    horas = ca.getHorasParaActividadDia(act, dia);
	} catch (SQLException e) {
	    throw new RuntimeException("Error en la conexión");
	}

	return horas;
    }

    public String cancelarActividad(ActividadDto act, String dia, String hora) {
	String colisiones = "";
	try (Connection con = gestorDDBB.getConnection();) {
	    ca.setConnection(con);
	    //Comprobamos que no tenga socios apuntados
	    Long p = ca.getIdPlanificacionConcreta(act.getId(), dia, hora.split("-")[0],
			hora.split("-")[1]);
	    List<SocioDto> sociosApuntados = ca.getSociosApuntados(p);
	    if (sociosApuntados.isEmpty()) {
		ca.eliminarActividad(act.getId(), dia, hora.split("-")[0],
			hora.split("-")[1]);
	    } else {
		for (SocioDto s : sociosApuntados) {
		    colisiones += s.datos();
		}
	    }
	} catch (SQLException e) {
	    throw new RuntimeException("Error en la conexión");
	}

	return colisiones;
    }

    public String cancelarPlanificacionActividad(String[] actividad) {
	String colisiones = "";
	try (Connection con = gestorDDBB.getConnection();) {
	    ca.setConnection(con);
	    // NOTA: actividad[0] = Dia semana, actividad[1] = hora inicio
	    // rango, actividad[2] = hora fin rango
	    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	    Calendar cal = Calendar.getInstance();
	    String diaDeLaSemana;

	    List<PlanificacionActividadDto> planificaciones = ca
		    .getListPlanificacionActividadPorRango(actividad[1],
			    actividad[2], sdf.format(new Date()));

	    // Miramos todas las planificaciones de la base de datos
	    for (PlanificacionActividadDto p : planificaciones) {
		cal.setTime(sdf.parse(p.getFecha()));
		diaDeLaSemana = sacarDiaDeLaSemana(
			cal.get(Calendar.DAY_OF_WEEK));

		//Comprobamos que no tenga socios apuntados
		List<SocioDto> sociosApuntados = ca.getSociosApuntados(p.getId());
		if (sociosApuntados.isEmpty()) {
		    // Comparamos el dia de la semana de la actividad con el pasado
		    // como parámetro
		    // Si es el mismo, anulamos esa actividad
		    if (diaDeLaSemana.equalsIgnoreCase(actividad[0])) {
			ca.eliminarPlanificacionActividad(p.getId());
			System.out.println("Eliminada: " + p.getId());
		    }
		} else {
		    colisiones += p.getNombre() + "\n";
		    for (SocioDto s : sociosApuntados) {
			colisiones += s.datos();
		    }
		}
	    }
	} catch (SQLException e) {
	    throw new RuntimeException("Error en la conexión");
	} catch (ParseException p) {
	    p.printStackTrace();
	}

	return colisiones;
    }

    private String sacarDiaDeLaSemana(int diaSemana) {
	switch (diaSemana) {
	case 1:
	    return "Domingo";
	case 2:
	    return "Lunes";
	case 3:
	    return "Martes";
	case 4:
	    return "Miércoles";
	case 5:
	    return "Jueves";
	case 6:
	    return "Viernes";
	case 7:
	    return "Sábado";
	default:
	    return null;
	}
    }

}
