package controller.admin;

import java.util.List;

import business.service.implementation.AdminServiceImpl;
import dto.ActividadDto;
import dto.ActividadesNecesitanRecursosDto;
import dto.RecursoDto;

public class NewActivityController {
    
    public void createActividad(ActividadDto actividad) {
	//A�adir la actividad a la base de datos
	AdminServiceImpl nac = new AdminServiceImpl();
	nac.createActividad(actividad);
    }

    public void createANRs(List<ActividadesNecesitanRecursosDto> listaActNecRec) {
	AdminServiceImpl nac = new AdminServiceImpl();
	nac.insertarActividadNecesitaRecursoS(listaActNecRec);
    }

    public long getIdActividad(ActividadDto actividad) {
	AdminServiceImpl nac = new AdminServiceImpl();
	return nac.getIdActividad(actividad);
    }
    
    public List<RecursoDto> getRecursos() {
	AdminServiceImpl as = new AdminServiceImpl();
	return as.getListaRecursos();
    }

}
