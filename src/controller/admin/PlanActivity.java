package controller.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dto.PlanificacionActividadDto;
import utils.Fecha;
import utils.Settings;

public class PlanActivity {

    private Connection conn;
    private PlanificacionActividadDto reserva;

    public PlanActivity(PlanificacionActividadDto reserva, Connection conn) {
	this.reserva = reserva;
	this.conn = conn;
    }

    public void execute() throws SQLException {
	PreparedStatement ps = null;
	try {
	    String consulta = Settings.get("SQL_INSERT_ARI");
	    ps = conn.prepareStatement(consulta);

	    ps.setLong(1, reserva.getId_Actividad());
	    ps.setLong(2, reserva.getId_Instalacion());
	    ps.setString(3, Fecha.fromApptoDDBB(reserva.getFecha()));
	    ps.setString(4, reserva.getHoraInicio() + ":00:00");
	    ps.setString(5, reserva.getHoraFin() + ":00:00");
	    ps.setBoolean(6, reserva.isLimitePlazas());
	    ps.setInt(7, reserva.getNumLimitePlazas());

	    ps.executeUpdate();

	} catch (SQLException e) {
	    throw e;
	}
    }
}
