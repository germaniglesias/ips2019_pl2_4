package controller.admin;

import java.sql.Connection;

import business.service.AdminService;
import business.service.implementation.AdminServiceImpl;
import database.gestorDDBB;
import dto.SocioDto;

public class PayFacilityReservationController {
    
    private Connection conn = gestorDDBB.getConnection();
    private AdminService as = new AdminServiceImpl();
    
    public SocioDto findSocioById(Long id_Socio) {
	return as.findSocioById(id_Socio, conn);
    }

    public void insertReservaImpagada(Long id) {
	as.insertarReservaImpagada(id, conn);
	actualizarConn();
    }

    public void insertarReservaEnEfectivo(Long id) {
	as.insertarReservaEnEfectivo(id, conn);
	actualizarConn();
    }
    
    public boolean isPagado(Long id) {
	boolean pagado = as.isPagado(id, conn);
	actualizarConn();
	return pagado;
    }
    
    private void actualizarConn() {
	gestorDDBB.close(conn);
	conn = gestorDDBB.getConnection();
    }
}
