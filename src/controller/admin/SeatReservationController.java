package controller.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import business.service.AdminService;
import controller.MainViewController;
import dto.InstalacionDto;
import dto.PlanificacionActividadDto;
import ui.admin.SeatReservationView;
import utils.Fecha;

public class SeatReservationController {
    private SeatReservationView view;
    private AdminService as;
    private MainViewController mvc;
    private String date;
    private String startTime;
    private String endTime;
    private PlanificacionActividadDto activity;
	
    public SeatReservationController(MainViewController mainController,
	    PlanificacionActividadDto dto) {
	this.mvc = mainController;
	this.as = mainController.getAS();
	this.view = new SeatReservationView(mainController.getView());
	this.activity = dto;
	initView();
    }

    private void initView() {
	view.getLblName().setText(activity.getNombre());
	date = activity.getFecha();
	startTime = activity.getHoraInicio();
	endTime = activity.getHoraFin();
	view.getLblHour().setText(startTime + "-" + endTime);
	view.getTxtTotalSeats()
		.setText(String.valueOf(activity.getNumLimitePlazas()));
	view.getTxtAvailableSeats()
		.setText(String.valueOf(activity.getNumPlazasDisponibles()));
	initController();
	view.getFrame().setVisible(true);
    }

    public void initController() {
	view.getBtnReservate().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		String dni = view.getTxtDNI().getText().trim();
		if (as.checkValidMember(dni)) {
		    Long member_id = as.findMember(dni).getId();
		    String activityName = as.checkMemberSimultaneousActivities(
			    dni, Fecha.fromApptoDDBB(date), startTime, endTime);
		    if (activityName.equals("")) {
			String facilityName = as
				.checkMemberSimultaneousReservations(dni, Fecha.fromApptoDDBB(date),
					startTime);
			if (facilityName.equals("")) {
			    as.InsertMemberActivitiesReservations(member_id,
				    activity.getId());
			    JOptionPane.showMessageDialog(view,
				    "Se ha reservado una plaza correctamente.");
			    view.getFrame().dispose();
			    
			   mvc.initTable((InstalacionDto) mvc.getView().getComboBoxFacilities()
				    .getSelectedItem());
			} else {
			    JOptionPane.showMessageDialog(view,
				    "El socio ya tiene una reserva simultánea de "
					    + facilityName);
			}

		    } else {
			JOptionPane.showMessageDialog(view,
				"El socio ya tiene una planificación simultánea de "
					+ activityName);
		    }

		} else {
		    JOptionPane.showMessageDialog(view, "El socio no existe.");
		}

	    }
	});

	view.getTxtDNI().getDocument()
		.addDocumentListener(new DocumentListener() {

		    @Override
		    public void insertUpdate(DocumentEvent e) {
			changed();
		    }

		    @Override
		    public void removeUpdate(DocumentEvent e) {
			changed();
		    }

		    @Override
		    public void changedUpdate(DocumentEvent e) {
			changed();
		    }
		});

    }

    public void changed() {
	if (view.getTxtDNI().getText().equals("")) {
	    view.getBtnReservate().setEnabled(false);
	} else {
	    view.getBtnReservate().setEnabled(true);
	}

    }
}
