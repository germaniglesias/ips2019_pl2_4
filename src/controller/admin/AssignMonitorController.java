package controller.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import business.service.AdminService;
import controller.MainViewController;
import dto.InstalacionDto;
import dto.ListaAsignacionMonitoresDto;
import dto.MonitorDto;
import dto.PlanificacionActividadDto;
import ui.admin.AssignMonitorView;
import utils.Fecha;

public class AssignMonitorController {

    private AssignMonitorView view;
    private AdminService as;
    private JComboBox<MonitorDto> monitors;
    private String startTime;
    private String endTime;
    private PlanificacionActividadDto activity;
    private Date dateStart;
    private Date dateEnd;
    private Date date;
    Calendar calendar = Calendar.getInstance();
    List<ListaAsignacionMonitoresDto> list = new ArrayList<ListaAsignacionMonitoresDto>();
    private MainViewController mvc;

    public AssignMonitorController(MainViewController mainController,
	    PlanificacionActividadDto dto) {
	this.mvc = mainController;
	this.view = new AssignMonitorView(mvc.getView());
	this.activity = dto;
	this.as = mainController.getAS();
	initView();
    }

    private void initView() {
	view.getLblName().setText(activity.getNombre());
	startTime = activity.getHoraInicio();
	endTime = activity.getHoraFin();
	view.getLabelHour().setText(startTime + "-" + endTime);

	try {
	    dateStart = dateEnd = date = new SimpleDateFormat("dd-MM-yyyy")
		    .parse(activity.getFecha());
	    view.getSpinnerStartDate().setValue(date);
	    view.getSpinnerEndDate().setValue(date);
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	Calendar c = Calendar.getInstance();
	c.setTime(date);
	int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
	if (dayOfWeek == 2) {
	    view.getCheckBoxLunes().setSelected(true);
	} else if (dayOfWeek == 3) {
	    view.getCheckBoxMartes().setSelected(true);
	} else if (dayOfWeek == 4) {
	    view.getCheckBoxMiercoles().setSelected(true);
	} else if (dayOfWeek == 5) {
	    view.getCheckBoxJueves().setSelected(true);
	} else if (dayOfWeek == 6) {
	    view.getCheckBoxViernes().setSelected(true);
	} else if (dayOfWeek == 7) {
	    view.getCheckBoxSabado().setSelected(true);
	}

	monitors = view.getComboBoxMonitors();
	monitors.setModel(
		new DefaultComboBoxModel<MonitorDto>(getAvailableMonitors()));
	initController();
	view.getFrame().setVisible(true);
    }

    public void initController() {
	view.getSpinnerStartDate().addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, -1);
		Date yesterday = calendar.getTime();
		if (yesterday.compareTo(
			(Date) view.getSpinnerStartDate().getValue()) <= 0) {
		    dateStart = (Date) view.getSpinnerStartDate().getValue();
		    if (dateEnd.compareTo(dateStart) <= 0) {
			view.getSpinnerEndDate().setValue(dateStart);
		    }
		    monitors.setModel(new DefaultComboBoxModel<MonitorDto>(
			    getAvailableMonitors()));
		} else {
		    JOptionPane.showMessageDialog(null,
			    "No se puede cambiar una asignación de una planificación ya empezada");
		    view.getSpinnerStartDate().setValue(new Date());

		}
	    }
	});
	view.getSpinnerEndDate().addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		calendar.setTime(dateStart);
		calendar.add(Calendar.DATE, -1);
		Date before = calendar.getTime();
		if (before.compareTo(
			(Date) view.getSpinnerEndDate().getValue()) < 0) {
		    dateEnd = (Date) view.getSpinnerEndDate().getValue();
		    monitors.setModel(new DefaultComboBoxModel<MonitorDto>(
			    getAvailableMonitors()));
		} else {
		    JOptionPane.showMessageDialog(null,
			    "La fecha final no puede ser inferior a la inicial");
		    view.getSpinnerEndDate().setValue(dateStart);
		}
	    }
	});
	view.getBtnAddList().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		MonitorDto m = (MonitorDto) monitors.getSelectedItem();
		String problem = "";
		List<ListaAsignacionMonitoresDto> selected = new ArrayList<ListaAsignacionMonitoresDto>();
		for (int i = 0; i < getSelectedDates().size(); i++) {
		    ListaAsignacionMonitoresDto p = new ListaAsignacionMonitoresDto();
		    p.date = new SimpleDateFormat("dd-MM-yyyy")
			    .format(getSelectedDates().get(i));
		    p.monitor_id = m.getId();
		    p.monitor = m.getNombre() + " " + m.getApellidos();
		    p.plan_id = as.getIdToAssignMonitor(
			    Fecha.fromApptoDDBB(p.date),
			    activity.getId_Actividad(), startTime, endTime);

		    if (find(p.plan_id) != null) {
			problem += "Ya se ha asignado un monitor a esta actividad: "
				+ find(p.plan_id) + "\n";
		    } else if (p.plan_id == null) {
			
		    } else {
			selected.add(p);
		    }

		}
		if (problem.equals("")) {
		    list.addAll(selected);
		    initTable();
		    view.getBtnAssign().setEnabled(true);
		    view.getBtnDeleteList().setEnabled(false);
		} else {
		    selected.clear();
		    JOptionPane.showMessageDialog(null, problem);
		}

	    }
	});
	view.getBtnDeleteList().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		int i = view.getTable().getSelectedRow();
		list.remove(i);
		initTable();
		if (list.isEmpty()) {
		    view.getBtnAssign().setEnabled(false);
		    view.getBtnDeleteList().setEnabled(false);
		}
	    }
	});

	view.getTable().addMouseListener(new MouseAdapter() {
	    public void mouseClicked(MouseEvent e) {
		view.getBtnDeleteList().setEnabled(true);
	    }
	});
	view.getBtnAssign().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		for (ListaAsignacionMonitoresDto p : list) {
		    as.updateActivityAssignMonitor(p.monitor_id, p.plan_id);
		}
		JOptionPane.showMessageDialog(view,
			"El monitor se ha asignado correctamente");
		mvc.initTable((InstalacionDto) mvc.getView()
			.getComboBoxFacilities().getSelectedItem());
		view.getFrame().dispose();
	    }
	});

    }

    protected String find(Long plan_id) {
	for (ListaAsignacionMonitoresDto p : list) {
	    if (p.plan_id == plan_id) {
		return p.date + " - " + p.monitor;
	    }
	}
	return null;
    }

    protected void initTable() {
	String[] columnTitle = { "FECHA", "MONITOR" };
	DefaultTableModel tableModel = new DefaultTableModel(columnTitle, 0);
	for (int i = 0; i < list.size(); i++) {
	    tableModel.addRow(
		    new String[] { list.get(i).date, list.get(i).monitor });
	}

	view.getTable().setModel(tableModel);

    }

    private List<Date> getSelectedDates() {
	return Fecha.getDatesOfWeekdaysInRange(dateStart, dateEnd,
		view.getCheckBoxLunes().isSelected(),
		view.getCheckBoxMartes().isSelected(),
		view.getCheckBoxMiercoles().isSelected(),
		view.getCheckBoxJueves().isSelected(),
		view.getCheckBoxViernes().isSelected(),
		view.getCheckBoxSabado().isSelected());
    }

    private MonitorDto[] getAvailableMonitors() {
	List<String> dias = new ArrayList<String>();
	for (Date date : getSelectedDates()) {
	    String s = new SimpleDateFormat("yyyy-MM-dd").format(date);
	    dias.add(s);
	}
	List<MonitorDto> list = as.listAvailableMonitors(dias, startTime,
		endTime);
	MonitorDto[] availableMonitors = new MonitorDto[list.size()];
	return availableMonitors = list.toArray(availableMonitors);
    }

}
