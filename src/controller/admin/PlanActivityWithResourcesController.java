package controller.admin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import business.service.implementation.AdminServiceImpl;
import dto.ActividadDto;
import dto.InstalacionDto;
import dto.InstalacionesTienenRecursosDto;
import dto.PlanificacionActividadDto;
import dto.RecursoDto;
import utils.ComparadorInstalacionesTienenRecursosPorInstalacionId;

public class PlanActivityWithResourcesController {

    public void planActivityWithResourcesController(PlanificacionActividadDto ari) {
	AdminServiceImpl as = new AdminServiceImpl();
	as.insertarPlanificacionActividad(ari);
    }

    public List<InstalacionesTienenRecursosDto> getInstalacionesQueContenganRecursos(ActividadDto actividadDto) {
	AdminServiceImpl as = new AdminServiceImpl();
	List<RecursoDto> listaRecursosQueTieneActividad = as.getRecursosQueTieneActividad(actividadDto.getId());
	List<InstalacionesTienenRecursosDto> listITR = as.getListaInstalacionesQueContenganRecursos(listaRecursosQueTieneActividad);

	//creo una lista con los ids de los recursos que necesita la actividad
	List<Long> idRecursosActividad = new ArrayList<Long>();
	for (RecursoDto r : listaRecursosQueTieneActividad) {
	    idRecursosActividad.add(r.getId());
	}

	if (!listITR.isEmpty()) {
	    //Ordena la lista de instalaciones por id de instalacion
	    listITR.sort(new ComparadorInstalacionesTienenRecursosPorInstalacionId());

	    //Lista de Ids de instalaciones
	    List<Long> listaIdInstalaciones = new ArrayList<Long>();
	    for (InstalacionesTienenRecursosDto i : listITR) {
		listaIdInstalaciones.add(i.getId_Instalacion());
	    }

	    //Lista de ids de instalacion que deben de ser eliminados
	    List<Long> listaIdInstalacionesQueDebenSerEliminados = new ArrayList<Long>();

	    //Cuento el numero de veces que aparece un id en la listaIdInstalaciones
	    //Si son < que el nº de elementos en la lista de recursos necesarios
	    //se ha de eliminar esa actividad dado que no tiene todos los recursos que se piden
	    Long currentId = new Long(-1);
	    for (Long id : listaIdInstalaciones) {
		if (id != currentId) { //Si el id no es el que estoy mirando
		    //Cuento las apariciones que tiene este id en la lista y lo comparo con el numero de recursos que hay
		    if (Collections.frequency(listaIdInstalaciones, id) != idRecursosActividad.size()) {
			//Si entra aqui es que todas las apariciones de esta instalacion deben ser eliminadas
			//Porque no tiene los suficientes recursos necesarios
			listaIdInstalacionesQueDebenSerEliminados.add(id);
		    }
		}
		currentId = id;
	    }

	    //Elimina los que no tienen suficientes recursos
	    for (Long id : listaIdInstalacionesQueDebenSerEliminados) {
		listITR.removeIf( i -> {return i.getId_Instalacion() == id;});
	    }

	    //Eliminar duplicados
	    if (!listITR.isEmpty()) {
		InstalacionesTienenRecursosDto in = listITR.get(0);
		for (int i=1; i<listITR.size(); i++) {
		    if (in.getId_Instalacion() == listITR.get(i).getId_Instalacion()) { //Son la misma instalacion
			if (in.getCantidadRecurso() < listITR.get(i).getCantidadRecurso()) { //anterior < presente
			    listITR.remove(listITR.get(i));
			}
		    } else {
			in = listITR.get(i);
		    }
		}
	    }

	    //deprecated(listITR, idRecursosActividad);
	}

	return listITR;
    }

//    private void deprecated(List<InstalacionesTienenRecursosDto> listITR,
//	    List<Long> idRecursosActividad) {
//	//Quiero eliminar las instalaciones que no tienen ambos recursos
//	int contador = 0;
//	Long instalacionActual = new Long(-1);
//	List<Long> instalacionesAEliminarId = new ArrayList<Long>();
//
//	for (InstalacionesTienenRecursosDto instalacion : listITR) {
//	    //Si la instalacion no es la misma que la que estaba mirando
//	    if (instalacion.getId_Instalacion() != instalacionActual) {
//		//Si la instalacion no contiene todos los recursos (y el contador no es 0 [no es la primera iteracion])
//		if (contador != idRecursosActividad.size() && contador != 0) {
//		    //Añadir el id de la instalacion a la lista de instalaciones a eliminar
//		    instalacionesAEliminarId.add(instalacionActual);
//		    contador = 0;
//		    instalacionActual = instalacion.getId_Instalacion();
//		}
//		instalacionActual = instalacion.getId_Instalacion();
//		//Si el contador es igual al numero de recursos que tiene que tener todo va bien y esa instalacion se queda
//	    } else {
//		//Seguimos con la misma instalacion o es la primera iteracion
//		//Entonces no hacemos nada
//	    }
//	    contador++;
//	}
//	//Eliminar las instalaciones que no contenian todos los recursos
//	for (int i=0; i<listITR.size(); i++) {
//	    Long id = listITR.get(i).getId_Instalacion();
//	    if (instalacionesAEliminarId.contains(id)) {
//		listITR.remove(listITR.get(i));
//	    }
//	}
//
//	if (!listITR.isEmpty()) {
//	    //Eliminar duplicados
//	    InstalacionesTienenRecursosDto in = listITR.get(0);
//	    for (int i=1; i<listITR.size(); i++) {
//		if (in.getId_Instalacion() == listITR.get(i).getId_Instalacion()) { //Son la misma instalacion
//		    if (in.getCantidadRecurso() < listITR.get(i).getCantidadRecurso()) { //anterior < presente
//			listITR.remove(listITR.get(i));
//		    }
//		} else {
//		    instalacionActual = listITR.get(i).getId_Instalacion();
//		}
//	    }
//	}
//    }

    public List<ActividadDto> getActividades(boolean tieneRecursos) {
	AdminServiceImpl as = new AdminServiceImpl();
	return as.getListaActividades(tieneRecursos);
    }

    public List<ActividadDto> getAllActividades() {
	AdminServiceImpl as = new AdminServiceImpl();
	return as.getListaActividades();
    }

    public List<InstalacionesTienenRecursosDto> getAllInstalaciones() {
	AdminServiceImpl as = new AdminServiceImpl();
	List<InstalacionDto> inst = as.getListaInstalaciones();
	List<InstalacionesTienenRecursosDto> lista = new ArrayList<InstalacionesTienenRecursosDto>();

	for (InstalacionDto i : inst) {
	    InstalacionesTienenRecursosDto a = new InstalacionesTienenRecursosDto();
	    a.setCantidadRecurso(0);
	    a.setId_Instalacion(i.getId());
	    a.setNombre_Instalacion(i.getNombre());
	    lista.add(a);
	}

	return lista;
    }

}
