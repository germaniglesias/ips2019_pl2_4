package controller.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import business.service.AdminService;
import business.service.implementation.AdminServiceImpl;
import business.service.implementation.admin.BuscarSocioReservaInstalacion;
import business.service.implementation.admin.EliminarSocioReservaInstalacion;
import database.gestorDDBB;
import dto.PlanificacionActividadDto;
import dto.SocioReservaInstalacionDto;
import ui.admin.SolapamientoSocioInstalacionView;
import utils.Fecha;
import utils.Hora;

public class PlanPeriodicActivityController {

    private Connection conn = gestorDDBB.getConnection();

    public String planActivities(
	    List<PlanificacionActividadDto> listaReservas) {

	AdminService as = new AdminServiceImpl();

	// Ampliación, historia 8290, colisiones de reservas de instalaciones
	// por socios
	List<SocioReservaInstalacionDto> colisionesSocios = new ArrayList<SocioReservaInstalacionDto>();
	List<PlanificacionActividadDto> colisiones = new ArrayList<PlanificacionActividadDto>();
	List<PlanificacionActividadDto> aReservar = new ArrayList<PlanificacionActividadDto>();
	try {
	    for (PlanificacionActividadDto reserva : listaReservas) {
		List<PlanificacionActividadDto> planificaciones = as
			.listPlanificationsOnDateAndHour(reserva, conn);
		// Ampliación, historia 8290, ahora se comprueba si hay
		// solapamiento entre la planificación y la reserva por parte de socio
		List<SocioReservaInstalacionDto> reservasSocioInstalacion = getReservaSocioInstalacion(reserva, conn);
		
		// No hay planificadas actividades ni reservas por parte de socios en esa fecha y horas
		// O
		// No hay solapamientos de actividades con la nueva planificación ni reservas de socios
		if ((planificaciones.isEmpty() && reservasSocioInstalacion.isEmpty()) ||
			(!haySolapamientos(reserva, planificaciones) && reservasSocioInstalacion.isEmpty())) {
			aReservar.add(reserva);
		} else {
		    // Hay planificadas actividades y algunas se solapan con la nueva que se quiere planificar
		    if (!planificaciones.isEmpty() && haySolapamientos(reserva, planificaciones))
			colisiones.add(reserva);
		    // Hay solapamiento con alguna reserva de socio
		    if (!reservasSocioInstalacion.isEmpty())
			reservasSocioInstalacion.forEach(r -> colisionesSocios.add(r));
		}
	    }

	    // Ampliación, historia 8290, ahora se comprueba que tampoco haya
	    // colisiones por reservas de socios en instalaciones
	    if (colisiones.isEmpty() && colisionesSocios.isEmpty()) {
		for (PlanificacionActividadDto reserva : aReservar) {
		    as.planActivity(reserva, conn);
		}
	    }
	} catch (SQLException e) {
	    try {
		conn.rollback();
	    } catch (SQLException e2) {
	    }
	    e.printStackTrace();
	}

	// Ampliación, historia 8290
	// Ahora se muestra un mensaje por colisiones de socios
	String col = toStringg(colisiones);
	String cols = toStringColisionesSocios(colisionesSocios);
	if (col=="" && cols =="")
	    return "";
	return col + cols;
    }

    /**
     * Ampliación, historia 8290
     * Comprueba si un socio ha reservado la instalación a la misma fecha y hora a la que quiere
     * hacerlo la administración planificando la nueva actividad
     * 
     * @param nuevaPlanificacion, actividad que quiere crear la administración
     * @param conn conexión a la base de datos
     * @return las reservas por parte del socio en la instalación si existe, null en caso contrario
     */
    private List<SocioReservaInstalacionDto> getReservaSocioInstalacion(
	    PlanificacionActividadDto nuevaPlanificacion, Connection conn) {
	BuscarSocioReservaInstalacion bsri = new BuscarSocioReservaInstalacion();
	bsri.setConnection(conn);
	
	List<SocioReservaInstalacionDto> reservasSocioColisiona = null;
	
	
	// Buscamos alguna reserva por parte de socio en la instalación que se solape
	reservasSocioColisiona = bsri.getReservaSocioInstalacion(nuevaPlanificacion.getId_Instalacion(), Hora.crearHora(nuevaPlanificacion.getHoraInicio()),
		Hora.crearHora(nuevaPlanificacion.getHoraFin()), Fecha.fromApptoDDBB(nuevaPlanificacion.getFecha()));
	
	return reservasSocioColisiona;
    }

    /**
     * Ampliación, historia 8290
     * Muestra en un diálogo aparte los datos del socio
     * y la instalación cuya reserva colisiona en hora y lugar con la que quiere
     * hacer la administración
     * 
     * @param colisionesSocios, colisiones por parte de socio
     */
    private void mostrarColisionesSocios(
	    List<SocioReservaInstalacionDto> colisionesSocios) {
	// Mostramos los datos en otra ventana
	SolapamientoSocioInstalacionView sol = new SolapamientoSocioInstalacionView(this, colisionesSocios);
	sol.setVisible(true);
    }

    private String toStringg(List<PlanificacionActividadDto> colisiones) {
	if (!colisiones.isEmpty()) {
	    String str = "";
	    System.out.println(colisiones);
	    for (PlanificacionActividadDto p : colisiones) {
		str += p.datos();
		str += "\n";
	    }
	    try {
		conn.rollback();
	    } catch (SQLException e2) {
	    }
	    return str;
	} else {
	    return "";
	}
    }
    
    /**
     * Ampliación, historia 8290
     * @param colisiones por parte de socios
     * @return mensaje de que hay colisiones con reservas de socios si las hay, una cadena vacía en
     * caso contrario
     */
    private String toStringColisionesSocios(List<SocioReservaInstalacionDto> colisiones) {
	if (!colisiones.isEmpty()) {
	    mostrarColisionesSocios(colisiones);
	   return "Varios socios han reservado las instalaciones";
	} else {
	    return "";
	}
    }

    private boolean haySolapamientos(PlanificacionActividadDto reserva,
	    List<PlanificacionActividadDto> planificaciones) {
	for (PlanificacionActividadDto p : planificaciones) {
	    if (seSolapan(reserva, p))
		return true;
	}
	return false;
    }

    public boolean seSolapan(PlanificacionActividadDto p, PlanificacionActividadDto p2) {
	int pini = Integer.parseInt(p.getHoraInicio());
	int pfin = Integer.parseInt(p.getHoraFin());
	int p2ini = Integer.parseInt(p2.getHoraInicio());
	int p2fin = Integer.parseInt(p2.getHoraFin());
	return p.getFecha().equals(p2.getFecha()) //Es el mismo día
		&& p.getId_Instalacion() == p2.getId_Instalacion() //Es la misma instalacion
		// QUITADO, no se solapan al ser la misma actividad sino misma instalacion en fecha y hora
		// && p.getId_Actividad() == p2.getId_Actividad()
		//Las horas se solapen
		&& ( (pini == p2ini && pfin == p2fin) //Son iguales
			|| (pini > p2ini && pfin >= p2fin && p2fin > pini) //se solapan por abajo
			|| (pini <= p2ini && pfin < p2fin && p2ini < pfin) //se solapan por arriba
			|| (pini > p2ini && pfin < p2fin) //p esta dentro de p2
			|| (pini < p2ini && pfin > p2fin) //p2 esta dentro de p
			);
    }

    /**
     * Obtiene una reserva en función de los elementos pasados como parámetro
     * @param nombre_Instalacion, nombre de la instalación reservada
     * @param dni_Socio, dni del socio que hizo la reserva
     * @param fecha en la que se reservó
     * @param hora en la que se reservó
     * @return socio si lo encuentra, null en caso contrario
     */
    public SocioReservaInstalacionDto obtenerReserva(String nombre_Instalacion,
	    String dni_Socio, String fecha, String hora) {
	BuscarSocioReservaInstalacion bsri = new BuscarSocioReservaInstalacion();
	bsri.setConnection(conn);
	SocioReservaInstalacionDto reserva = bsri.getReservaSocioInstalacion(nombre_Instalacion, dni_Socio, fecha, hora);
	
	if (reserva == null)
	    try {conn.rollback();} catch (SQLException e) {e.printStackTrace();}
	
	return reserva;
    }

    /**
     * Elimina la reserva de instalación por parte de socio a partir del id pasado como parámetro
     * @param idReserva, id de la reserva que se quiere borrar
     */
    public void borrar(Long idReserva) {
	EliminarSocioReservaInstalacion esri = new EliminarSocioReservaInstalacion();
	esri.setConnection(conn);
	esri.eliminarReserva(idReserva);
    }

}
