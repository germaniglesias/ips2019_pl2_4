package controller.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import business.service.AdminService;
import controller.MainViewController;
import dto.InstalacionDto;
import dto.ListaAsignacionMonitoresDto;
import ui.admin.FacilityReservationView;
import utils.Fecha;

public class FacilityReservationController {

    private FacilityReservationView view;
    private AdminService as;
    private InstalacionDto facility;
    private String date;
    private String startHour;
    private String endHour;
    private int numberHours;
    private int start;
    private int end;

    List<ListaAsignacionMonitoresDto> list = new ArrayList<ListaAsignacionMonitoresDto>();
    private MainViewController mvc;

    public FacilityReservationController(MainViewController mvc,
	    InstalacionDto dto, String day, String hour) {
	this.view = new FacilityReservationView(mvc.getView());
	this.as = mvc.getAS();
	this.mvc = mvc;
	this.facility = dto;
	this.startHour = hour;
	this.date = day;
	initView();
    }

    private void initView() {
	view.getLblName().setText(facility.getNombre());
	start = Integer.parseInt(startHour.substring(0, 2));
	end = start + 1;
	endHour = end + startHour.substring(2);
	view.getLblHour().setText(startHour);
	view.getTxtPricePerHour()
		.setText(String.valueOf(facility.getPrecioPorHora()));
	numberHours = (int) view.getSpinnerHours().getValue();
	initController();
	view.getFrame().setVisible(true);
    }

    public void initController() {
	view.getBtnRservate().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		String dni = view.getTxtDNI().getText().trim();
		List<String> hours = new ArrayList<String>();
		if (as.checkValidMember(dni)) {
		    Long member_id = as.findMember(dni).getId();
		    boolean finish = false;
		    for (int i = 1; i <= (int) view.getSpinnerHours()
			    .getValue(); i++) {

			String activityName = as
				.checkMemberSimultaneousActivities(dni,
					Fecha.fromApptoDDBB(date), startHour,
					endHour);
			if (activityName.equals("")) {
			    String facilityName = as
				    .checkMemberSimultaneousReservations(dni,
					    Fecha.fromApptoDDBB(date),
					    startHour);
			    if (facilityName.equals("")) {
				if (!as.checkSimultaneousActivity(
					facility.getId(),
					Fecha.fromApptoDDBB(date), startHour)) {
				    if (!as.checkSimultaneousReservations(
					    facility.getId(),
					    Fecha.fromApptoDDBB(date),
					    startHour)) {
					hours.add(startHour);
				    } else {
					finish = true;
					JOptionPane.showMessageDialog(view,
						"Ya existe una reserva en esta instalación a las "
							+ startHour + " el día "
							+ date);
					break;
				    }
				} else {
				    finish = true;
				    JOptionPane.showMessageDialog(view,
					    "Ya existe una planificación de actividad en esta instalación a las "
						    + startHour + " el día "
						    + date);
				    break;
				}
			    } else {
				finish = true;
				JOptionPane.showMessageDialog(view,
					"El socio ya tiene una reserva simultánea de "
						+ facilityName + " a las "
						+ startHour + " el día "
						+ date);
				break;
			    }
			} else {
			    finish = true;
			    JOptionPane.showMessageDialog(view,
				    "El socio ya tiene una planificación simultánea de "
					    + activityName + " a las "
					    + startHour + " el día " + date);
			    break;
			}
			String back = startHour.substring(2);
			if (start + i < 10) {

			    startHour = "0" + String.valueOf(start + i) + back;
			} else {
			    startHour = String.valueOf(start + i) + back;
			}

		    }
		    if (!finish) {
			for (String h : hours) {
			    as.InsertMemberFacilitiesReservations(member_id,
				    facility.getId(), Fecha.fromApptoDDBB(date),
				    h);
			}
			JOptionPane.showMessageDialog(view,
				"Se ha reservado correctamente.\nPrecio a pagar:"
					+ facility.getPrecioPorHora()
						* (int) view.getSpinnerHours()
							.getValue());
			view.getFrame().dispose();
			mvc.initTable((InstalacionDto) mvc.getView()
				.getComboBoxFacilities().getSelectedItem());
		    }
		} else {
		    JOptionPane.showMessageDialog(view, "El socio no existe.");
		}

	    }
	});

	view.getSpinnerHours().addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		numberHours = (int) view.getSpinnerHours().getValue();
		end = start + numberHours;
		endHour = end + startHour.substring(2);
	    }
	});

	view.getTxtDNI().getDocument()
		.addDocumentListener(new DocumentListener() {
		    @Override
		    public void insertUpdate(DocumentEvent e) {
			changed();
		    }

		    @Override
		    public void removeUpdate(DocumentEvent e) {
			changed();
		    }

		    @Override
		    public void changedUpdate(DocumentEvent e) {
			changed();
		    }
		});

    }

    public void changed() {
	if (view.getTxtDNI().getText().equals("")) {
	    view.getBtnRservate().setEnabled(false);
	} else {
	    view.getBtnRservate().setEnabled(true);
	}

    }

}
