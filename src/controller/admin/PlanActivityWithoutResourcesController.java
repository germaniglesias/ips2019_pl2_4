package controller.admin;

import java.util.List;

import business.service.implementation.AdminServiceImpl;
import dto.ActividadDto;
import dto.PlanificacionActividadDto;
import dto.InstalacionDto;

public class PlanActivityWithoutResourcesController {

    public void planActivityWithoutResourcesController(PlanificacionActividadDto reserva) {
	AdminServiceImpl as = new AdminServiceImpl();
	as.insertarPlanificacionActividad(reserva);
    }

    public List<ActividadDto> getAllActividades() {
	AdminServiceImpl as = new AdminServiceImpl();
	List<ActividadDto> l = as.getListaActividades(false);
	l.addAll(as.getListaActividades(true));
	return l;
    }
    
    public List<ActividadDto> getActividades(boolean tieneRecursos) {
	AdminServiceImpl as = new AdminServiceImpl();
	return as.getListaActividades(tieneRecursos);
    }

    public List<InstalacionDto> getInstalaciones() {
	AdminServiceImpl as = new AdminServiceImpl();
	return as.getListaInstalaciones();
    }

}
