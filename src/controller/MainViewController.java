package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import business.service.AdminService;
import business.service.implementation.AdminServiceImpl;
import controller.admin.AdminMainController;
import controller.member.MemberMainController;
import dto.InstalacionDto;
import dto.PlanificacionActividadDto;
import dto.SocioReservaInstalacionDto;
import ui.Main;
import ui.monitor.MonitorMainView;
import utils.Fecha;

public class MainViewController {

    private AdminService as = new AdminServiceImpl();
    private JComboBox<InstalacionDto> facilities;
    private JSpinner date;
    private String startWeek;
    private String endWeek;
    private JTable table;
    List<String> columnTitle;
    private String today;
    private String startHour;
    private AdminMainController amc;
    private MemberMainController mmc;

    private Main main;
    private MainViewController instance = this;

    public MainViewController(Main main) {
	this.main = main;
	initLoginView();
	initView();
    }

    private void initLoginView() {
	main.getTxtPass().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		if (main.getTxtEmail().getText().equals("admin")) {
		    amc = new AdminMainController(instance);
		    amc.initController();
		} else if (main.getTxtEmail().getText().equals("socio")) {
		    mmc = new MemberMainController(
			    instance);
		    mmc.initController();
		} else if (main.getTxtEmail().getText().equals("monitor")) {
		    main.setVisible(false);
		    MonitorMainView frame = new MonitorMainView(main);
		    frame.setVisible(true);
		} else {
		    JOptionPane.showMessageDialog(null, "Sin implementar");
		}
	    }
	});
    }

    private void initView() {
	date = main.getSpinnerDate();
	setStartEndWeek();
	getCurrentDate();
	facilities = main.getComboBoxFacilities();
	facilities.setModel(
		new DefaultComboBoxModel<InstalacionDto>(getFacilitiesNames()));
	table = main.getTable();
	initTable((InstalacionDto) facilities.getSelectedItem());
    }

    private void getCurrentDate() {
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date d = new Date(System.currentTimeMillis());
	String[] s = format.format(d).split(" ");
	today = s[0];
	int time = Integer.parseInt(s[1].substring(0, 2));
	int timeAfter = time + 1;
	startHour = timeAfter + s[1].substring(2);
    }

    private InstalacionDto[] getFacilitiesNames() {
	List<InstalacionDto> lista = as.listFacilities();
	InstalacionDto[] names = new InstalacionDto[lista.size()];
	return names = lista.toArray(names);
    }

    public void initTable(InstalacionDto instalacion) {
	List<PlanificacionActividadDto> list = null;
	List<SocioReservaInstalacionDto> reservas = new ArrayList<SocioReservaInstalacionDto>();
	if (main.getBtnAsignarMonitor().isSelected()) {
	    list = as.listActivitiesWithoutMonitor(instalacion.getId(),
		    Fecha.fromApptoDDBB(endWeek), today, startHour);
	} else if (main.getBtnReservarPlaza().isSelected()) {
	    list = as.listActivitiesWithLimitSeat(instalacion.getId(), today,
		    startHour);
	} else {
	    list = as.listFacilitiesReservations(instalacion.getId(),
		    Fecha.fromApptoDDBB(startWeek),
		    Fecha.fromApptoDDBB(endWeek));
	    reservas = as.listReservasInstalacionSocio(instalacion.getId(),
		    Fecha.fromApptoDDBB(startWeek),
		    Fecha.fromApptoDDBB(endWeek));
	}

	DefaultTableModel tableModel = new DefaultTableModel(
		columnTitle.toArray(), 15);
	table.setModel(tableModel);
	for (PlanificacionActividadDto facilities : list) {
	    table.setValueAt(facilities, getRow(facilities.getHoraInicio()),
		    getColumn(facilities.getFecha()));
	}

	for (SocioReservaInstalacionDto s : reservas) {
	    table.setValueAt(s, getRow(s.getHoraInicio()),
		    getColumn(s.getFecha()));
	}

    }

    public void initController() {
	facilities.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		initTable((InstalacionDto) facilities.getSelectedItem());
	    }
	});

	date.addChangeListener(new ChangeListener() {
	    public void stateChanged(ChangeEvent e) {
		setStartEndWeek();
		initTable((InstalacionDto) facilities.getSelectedItem());
	    }
	});

	main.getBtnCerrarSesion().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		main.setTitle("CENTRO DEPORTIVO");
		main.card.show(main.getContentPane(), "login");
	    }
	});
    }

    private void setStartEndWeek() {
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	LocalDate pickedDate = Fecha.dateToLocalDate((Date) date.getValue());
	TemporalField fieldISO = WeekFields.of(Locale.FRANCE).dayOfWeek();
	startWeek = pickedDate.with(fieldISO, 1).format(formatter);
	endWeek = pickedDate.with(fieldISO, 7).format(formatter);
	columnTitle = Arrays.asList(startWeek,
		pickedDate.with(fieldISO, 2).format(formatter),
		pickedDate.with(fieldISO, 3).format(formatter),
		pickedDate.with(fieldISO, 4).format(formatter),
		pickedDate.with(fieldISO, 5).format(formatter),
		pickedDate.with(fieldISO, 6).format(formatter), endWeek);
    }

    private int getRow(String str) {
	for (int i = 0; i < main.getRowTitle().getSize(); i++) {
	    if (main.getRowTitle().getElementAt(i).equals(str)) {
		return i;
	    }
	}
	return 0;

    }

    private int getColumn(String str) {
	for (int i = 0; i < table.getColumnCount(); i++) {
	    if (main.getTable().getModel().getColumnName(i).equals(str)) {
		return i;
	    }
	}
	return 0;
    }

    public Main getView() {
	return main;
    }

    public AdminService getAS() {
	return as;
    }

}
