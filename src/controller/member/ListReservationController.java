package controller.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.ActividadSocioReservaDto;
import utils.Settings;

public class ListReservationController {
    
    List<ActividadSocioReservaDto> asrs = new ArrayList<ActividadSocioReservaDto>();
    private ActividadSocioReservaDto asr;
    
    public List<ActividadSocioReservaDto> listarReservas(String dni) {
	Connection conn = gestorDDBB.getConnection();
 	ResultSet rs = null;
 	PreparedStatement ps = null;

 	try {
 	    String consulta = Settings.get("SQL_LISTAR_RESERVAS_SOCIO");
 	    ps = conn.prepareStatement(consulta);
 	   ps.setString(1, dni);
 	    rs = ps.executeQuery();
 	    asrs = new ArrayList<ActividadSocioReservaDto>();

 	    while (rs.next()) {
 		asr = new ActividadSocioReservaDto();
 		asr.id_Actividad = rs.getString("id");
 		asr.nombre_Actividad = rs.getString("nombre");
 		asr.fecha = rs.getString("fecha");
 		asr.horario_Inicio = rs.getString("horaInicio");
 		asr.horario_Fin = rs.getString("horaFin");
 		asr.numero_Plazas = rs.getInt("numLimitePlazas");
 		asr.estado = rs.getString("estado");

 		asrs.add(asr);

 	    }

 	} catch (SQLException e) {
 	    e.printStackTrace();
 	} finally {
 	    gestorDDBB.close(conn);
 	    gestorDDBB.close(ps);
 	}
 	return asrs;
    }

}
