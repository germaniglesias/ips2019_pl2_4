package controller.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.ActividadSocioReservaDto;
import utils.Settings;

public class CancelReservationController {

    List<ActividadSocioReservaDto> asrs = new ArrayList<ActividadSocioReservaDto>();
    private ActividadSocioReservaDto asr;

    public List<ActividadSocioReservaDto> listarMatriculados() {

	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings
		    .get("SQL_LIST_MATRICULADOS_CANCEL_RESERVA_SOCIO");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    asrs = new ArrayList<ActividadSocioReservaDto>();

	    while (rs.next()) {
		asr = new ActividadSocioReservaDto();
		asr.dni_Socio = rs.getString("dni");
		asr.nombre_Actividad = rs.getString("nombre");
		asr.id_Actividad = rs.getString("id");
		asr.numero_Plazas = rs.getInt("numLimitePlazas");
		asr.fecha = rs.getString("fecha");
		asr.horario_Inicio = rs.getString("horaInicio");
		asr.horario_Fin = rs.getString("horaFin");
		asr.estado = rs.getString("estado");

		asrs.add(asr);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return asrs;
    }

    public void EliminarMatricula(String idSocio, String id) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_DELETE_RESERVA_SOCIO");
	    ps = conn.prepareStatement(consulta);
	    ps.setString(1, idSocio);
	    ps.setString(2, id);

	 ps.executeUpdate();
	 conn.commit();
	    

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	
    }

    public List<ActividadSocioReservaDto> comprobarEstado() {
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings
		    .get("SQL_LIST_ESTADOS_MATRICULADOS");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    asrs = new ArrayList<ActividadSocioReservaDto>();

	    while (rs.next()) {
		asr = new ActividadSocioReservaDto();
		asr.dni_Socio = rs.getString("dni");
		asr.id_Actividad = rs.getString("id");
		asr.estado = rs.getString("estado");

		asrs.add(asr);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return asrs;
    }


}
