package controller.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.ActividadSocioReservaDto;
import dto.PlanificacionActividadDto;
import dto.SocioDto;
import utils.Settings;

public class ReservatePlaceController {

    List<SocioDto> socios = new ArrayList<SocioDto>();
    private SocioDto socio;

    List<ActividadSocioReservaDto> asrs = new ArrayList<ActividadSocioReservaDto>();
    private ActividadSocioReservaDto asr;

    public List<SocioDto> listarSocios() {
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings.get("SQL_LIST_SOCIO");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    socios = new ArrayList<SocioDto>();

	    while (rs.next()) {
		socio = new SocioDto();
		socio.setNombre(rs.getString("nombre"));
		socio.setApellidos(rs.getString("apellidos"));
		socio.setDni(rs.getString("dni"));
		socio.setId(rs.getLong("id"));

		socios.add(socio);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return socios;
    }

    public List<ActividadSocioReservaDto> listarActividades() {
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings.get("SQL_LIST_ACTIVIDADES_SOCIO");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    asrs = new ArrayList<ActividadSocioReservaDto>();

	    while (rs.next()) {
		asr = new ActividadSocioReservaDto();

		asr.nombre_Actividad = rs.getString("nombre");
		asr.id_Actividad = rs.getString("id");
		asr.fecha = rs.getString("fecha");
		asr.horario_Inicio = rs.getString("horaInicio");
		asr.horario_Fin = rs.getString("horaFin");
		asr.numero_Plazas = rs.getInt("numLimitePlazas");
		asr.is_LimitePlazas = rs.getBoolean("limitePlazas");

		asrs.add(asr);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return asrs;
    }

    public void UpdateActividad(String idActividad, int plazas) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_UPDATE_ACTIVIDAD_SOCIO");
	    ps = conn.prepareStatement(consulta);
	   
		ps.setInt(1,plazas);
	 ps.setString(2,idActividad);

	 ps.executeUpdate();
	 conn.commit();
	    

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	
    }

    public void InsertarMatricula(String idSocio, String id, String estado) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_INSERT_RESERVA_SOCIO");
	    ps = conn.prepareStatement(consulta);
	    ps.setString(1, idSocio);
	    ps.setString(2, id);
	    ps.setString(3, estado);

	 ps.executeUpdate();
	 conn.commit();
	    

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	
    }

    public List<ActividadSocioReservaDto> listarMatriculados() {
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings.get("SQL_LIST_SOCIOS_MATRICULADOS");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    asrs = new ArrayList<ActividadSocioReservaDto>();

	    while (rs.next()) {
		asr = new ActividadSocioReservaDto();

		asr.dni_Socio = rs.getString("dni");
		asr.id_Actividad = rs.getString("id_PlanificacionActividad");

		asrs.add(asr);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return asrs;
    }

    public List<ActividadSocioReservaDto> listarReservas() {

 	Connection conn = gestorDDBB.getConnection();
 	ResultSet rs = null;
 	PreparedStatement ps = null;

 	try {
 	    String consulta = Settings.get("SQL_LIST_RESERVAS_SOCIO");
 	    ps = conn.prepareStatement(consulta);
 	    rs = ps.executeQuery();
 	    asrs = new ArrayList<ActividadSocioReservaDto>();

 	    while (rs.next()) {
 		asr = new ActividadSocioReservaDto();
 		asr.dni_Socio = rs.getString("dni");
 		asr.id_Actividad = rs.getString("id_PlanificacionActividad");
 		asr.fecha = rs.getString("fecha");
 		asr.horario_Inicio = rs.getString("horaInicio");
 		asr.horario_Fin = rs.getString("horaFin");

 		asrs.add(asr);

 	    }

 	} catch (SQLException e) {
 	    e.printStackTrace();
 	} finally {
 	    gestorDDBB.close(conn);
 	    gestorDDBB.close(ps);
 	}
 	return asrs;
     }

    public PlanificacionActividadDto comprobarColisionPlaza(Long idSocio, String fecha, String horaInicio) {
	PlanificacionActividadDto actividad = null;
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings.get("SQL_CHOQUE_RESERVA_ACTIVIDAD");
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, idSocio);
	    ps.setString(2, fecha);
	    ps.setString(3, horaInicio);
	    ps.setString(4, horaInicio);
	    rs = ps.executeQuery();

	    // Hay una reserva en actividad que colisiona
	    if (rs.next()) {
		actividad = new PlanificacionActividadDto();
		actividad.setId(rs.getLong("id_PlanificacionActividad"));
		actividad.setNombre(rs.getString("nombre_Actividad"));
		actividad.setNombreInstalacion(rs.getString("nombre_Instalacion"));
		actividad.setFecha(rs.getString("fecha"));
		actividad.setHoraInicio(rs.getString("horaInicio"));
		actividad.setHoraFin(rs.getString("horaFin"));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return actividad;
    }

   


}
