package controller.member;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;

import business.service.implementation.admin.FindMember;
import controller.MainViewController;
import dto.SocioDto;
import ui.Main;
import ui.member.ActivitiesScheduleView;
import ui.member.CancelReservationView;
import ui.member.CancellInstalationView;
import ui.member.ListReservateActivitiesView;
import ui.member.ReservateInstalationView;
import ui.member.ReservatePlaceView;
import ui.member.UndoCancelReservationView;

public class MemberMainController {

    private Main mainView;
    private SocioDto s;

    public MemberMainController(MainViewController instance) {
	this.mainView = instance.getView();
	// para cambiar de socio hasta que no se tenga el login
	s = new FindMember("45645645A").execute();
	initView();
    }

    public Main getView() {
	return mainView;
    }

    private void initView() {
	mainView.setTitle("CENTRO DEPORTIVO: SOCIO");
	mainView.getLblName().setText(s.getNombre() + " " + s.getApellidos());
	mainView.getPanelMenu().remove(mainView.getPanelBtn());
	mainView.getPanelMenu().add(mainView.getPanelMemberBtn());
	mainView.card.show(mainView.getContentPane(), "main");
    }

    public void initController() {

	// cambiar esto, no deberia lanzar tener que manejarse en un try catch

	mainView.getBtnActivitiesSchedule()
		.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			mainView.setVisible(false);
			ActivitiesScheduleView frame = null;
			try {
			    frame = new ActivitiesScheduleView(mainView);
			} catch (ClassNotFoundException | SQLException
				| ParseException e1) {
			    e1.printStackTrace();
			}
			frame.setVisible(true);
		    }
		});

	mainView.getBtnCancelReservation()
		.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			mainView.setVisible(false);
			CancelReservationView frame = new CancelReservationView(
				mainView);
			frame.setVisible(true);
		    }
		});

	// esto deberia verse en la tabla principal no?
	mainView.getBtnListaReservas().addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		mainView.setVisible(false);
		ListReservateActivitiesView frame = new ListReservateActivitiesView(
			mainView);
		frame.setVisible(true);
	    }
	});

	// cambiar esto, no deberia lanzar tener que manejarse en un try catch
	mainView.getBtnReservatePlace().addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {
		mainView.setVisible(false);
		ReservatePlaceView frame = null;
		try {
		    frame = new ReservatePlaceView(mainView);
		} catch (ClassNotFoundException | SQLException e1) {
		    e1.printStackTrace();
		}
		frame.setVisible(true);
	    }
	});

	mainView.getBtnUndoAnularReserva()
		.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
			mainView.setVisible(false);
			UndoCancelReservationView frame = new UndoCancelReservationView(
				mainView);
			frame.setVisible(true);
		    }
		});

	mainView.getBtnReservaInstalacion().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		mainView.setVisible(false);
		ReservateInstalationView frame = null;
		try {
		    frame = new ReservateInstalationView(mainView);
		} catch (ClassNotFoundException | SQLException e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
		frame.setVisible(true);
	    }
	});
	
	mainView.getBtnCancelReservaInstalacion().addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		mainView.setVisible(false);
		CancellInstalationView frame = null;
		frame = new CancellInstalationView(mainView);
		frame.setVisible(true);
	    }
	});
    }

}
