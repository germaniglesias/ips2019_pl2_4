package controller.member;

import java.sql.Connection;
import java.util.List;

import business.service.MemberService;
import business.service.implementation.MemberServiceImpl;
import database.gestorDDBB;
import dto.SocioReservaInstalacionDto;

public class ShowAccumulatedPaymentsController {
    
    private Connection conn = gestorDDBB.getConnection();
    private MemberService ms = new MemberServiceImpl();

    public List<SocioReservaInstalacionDto> listReservasSocio(Long id) {
	return ms.listReservasSocio(id, conn);
    }

}
