package controller.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.ActividadesSocioDto;
import utils.Settings;

public class ActivitiesScheduleController {

    List<ActividadesSocioDto> actividades = new ArrayList<ActividadesSocioDto>();
    private ActividadesSocioDto actividad;

    public List<ActividadesSocioDto> execute() {

	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings.get("SQL_ACTIVITY_SCHEDULES");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    actividades = new ArrayList<ActividadesSocioDto>();

	    while (rs.next()) {
		actividad = new ActividadesSocioDto();
		actividad.setNombre(rs.getString("nombre"));
		actividad.setFecha(rs.getString("fecha"));
		actividad.setHoraInicio(rs.getString("horaInicio"));
		actividad.setHoraFin(rs.getString("horaFin"));
		actividad.setTieneLimitePlazas(
			rs.getBoolean("limitePlazas"));
		actividad.setNumLimitePlazas(rs.getString("numLimitePlazas"));

		actividades.add(actividad);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return actividades;

    }
}