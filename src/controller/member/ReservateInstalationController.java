package controller.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.InstalacionSocioReservaDto;
import dto.SocioReservaInstalacionDto;
import utils.Settings;

public class ReservateInstalationController {

    
    List<InstalacionSocioReservaDto> isrs = new ArrayList<InstalacionSocioReservaDto>();
    private InstalacionSocioReservaDto isr;
    
    public List <InstalacionSocioReservaDto> listarInstalaciones() {
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings.get("SQL_LIST_INSTALACIONES_SOCIO");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    isrs = new ArrayList<InstalacionSocioReservaDto>();

	    while (rs.next()) {
		isr = new InstalacionSocioReservaDto();

		isr.id_Instalacion= rs.getString("id");
		isr.nombre_Instalacion = rs.getString("nombre");

		isrs.add(isr);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return isrs;
    }

    public List<InstalacionSocioReservaDto> listarReservas() {
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;
	try {
	    String consulta = Settings.get("SQL_LISTAR_RESERVAS_INSTALACION_SOCIO");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    isrs = new ArrayList<InstalacionSocioReservaDto>();

	    while (rs.next()) {
		isr = new InstalacionSocioReservaDto();
		isr.id_Reserva = rs.getString("id");
		isr.id_Instalacion = rs.getString("id_Instalacion");
		isr.nombre_Instalacion = rs.getString("nombre");
		isr.dni_Socio = rs.getString("dni");

		isr.fecha = rs.getString("fecha");
		isr.horaInicio = rs.getString("horaInicio");
		isr.precio = rs.getInt("precioPorHora");
		

		isrs.add(isr);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return isrs;
    }

    public void reservarInstalacion(String hi, String fecha,
	    String iDinstalacion, String iDsocio) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_RESERVA_INSTALACION_SOCIO");
	    ps = conn.prepareStatement(consulta);

	    ps.setString(1, iDsocio);
	    ps.setString(2, iDinstalacion);
	    ps.setString(3, fecha);
	    ps.setString(4, hi);

	    ps.executeUpdate();
	    conn.commit();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	
    }

    public List<InstalacionSocioReservaDto> listarMatriculados() {
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings.get("SQL_LIST_SOCIOS_MATRICULADOS_INSTALACIONES");
	    ps = conn.prepareStatement(consulta);
	    rs = ps.executeQuery();
	    isrs = new ArrayList<InstalacionSocioReservaDto>();

	    while (rs.next()) {
		isr = new InstalacionSocioReservaDto();
		
		isr.id_Reserva = rs.getString("id");
		isr.dni_Socio = rs.getString("dni");
		isr.nombre_Instalacion = rs.getString("nombre");
		isr.id_Instalacion = rs.getString("id_Instalacion");
		isr.fecha = rs.getString("fecha");
		isr.horaInicio = rs.getString("horaInicio");
		isr.precio = rs.getInt("precioPorHora");

		isrs.add(isr);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return isrs;
    }

    public void eliminarInstalacion(String id,String idSocio, String idActividad) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	try {
	    conn.setAutoCommit(false);
	    String consulta = Settings.get("SQL_DELETE_RESERVA_INSTALACION_SOCIO");
	    ps = conn.prepareStatement(consulta);
	    ps.setString(1, id);
	    ps.setString(2, idSocio);
	    ps.setString(3, idActividad);

	 ps.executeUpdate();
	 conn.commit();
	    

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	
    }

    public SocioReservaInstalacionDto comprobarColisionReserva(Long idSocio, String fecha, String horaInicio) {
	SocioReservaInstalacionDto reserva = null;
	Connection conn = gestorDDBB.getConnection();
	ResultSet rs = null;
	PreparedStatement ps = null;

	try {
	    String consulta = Settings.get("SQL_CHOQUE_RESERVA_INSTALACION");
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, idSocio);
	    ps.setString(2, fecha);
	    ps.setString(3, horaInicio);
	    rs = ps.executeQuery();

	    // Hay una reserva que colisiona
	    if (rs.next()) {
		reserva = new SocioReservaInstalacionDto();
		reserva.setId(rs.getLong("id"));
		reserva.setNombreInstalacion(rs.getString("nombre"));
		reserva.setId_Instalacion(rs.getLong("id_Instalacion"));
		reserva.setFecha(rs.getString("fecha"));
		reserva.setHoraInicio(rs.getString("horaInicio"));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(conn);
	    gestorDDBB.close(ps);
	}
	return reserva;
    }
}
