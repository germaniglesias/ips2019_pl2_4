package controller.monitor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.PlanificacionActividadDto;
import dto.SocioDto;
import exception.ApplicationException;
import utils.Settings;

public class AssistControlActivityController {

    public List<PlanificacionActividadDto> listActivitiesLimited(
	    int idMonitor) {
	List<PlanificacionActividadDto> actividadesLimitePlazas = new ArrayList<PlanificacionActividadDto>();

	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rsActivities = null;

	String consulta = Settings
		.get("SQL_LIST_ACTIVITIES_LIMITED_BY_MONITOR");

	try {

	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, idMonitor);
	    rsActivities = ps.executeQuery();

	    while (rsActivities.next()) {
		PlanificacionActividadDto act = new PlanificacionActividadDto();
		act.setId(rsActivities.getLong("id"));
		act.setNombre(rsActivities.getString("nombre"));
		act.setFecha(rsActivities.getString("fecha"));
		act.setHoraInicio(rsActivities.getString("horaInicio"));
		act.setHoraFin(rsActivities.getString("horaFin"));
		act.setLimitePlazas(rsActivities.getBoolean("limitePlazas"));
		act.setNumLimitePlazas(rsActivities.getInt("numLimitePlazas"));

		actividadesLimitePlazas.add(act);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rsActivities, ps, conn);
	}
	return actividadesLimitePlazas;
    }

    public SocioDto socioByDni(String dni) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	try {
	    String consulta = Settings.get("SQL_SOCIO_BY_DNI");
	    ps = conn.prepareStatement(consulta);
	    ps.setString(1, dni);
	    rs = ps.executeQuery();

	    rs.next();
	    // Si el monitor se encuentra en la base de datos
	    if (rs != null) {
		SocioDto socio = new SocioDto();
		socio.setDni(dni);
		socio.setId(rs.getLong("id"));
		socio.setNombre(rs.getString("nombre"));
		socio.setApellidos(rs.getString("apellidos"));
		return socio;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);

	}

	return null;

    }

    public int numeroPlazasById(int idActividad) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	try {
	    String consulta = Settings.get("SQL_NUM_PLAZAS_BY_ACTIVITY");
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, idActividad);
	    rs = ps.executeQuery();

	    rs.next();
	    if (rs != null)
		return rs.getInt("numLimitePlazas");

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);

	}

	return 0;
    }

    public void disminuirPlazasLibres(int sociosNoPresentes, int idActividad) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	try {
	    String consulta = Settings.get("SQL_UPDATE_NUM_PLAZAS");
	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, sociosNoPresentes);
	    ps.setInt(2, idActividad);

	    if (ps.executeUpdate() == 0) {
		throw new ApplicationException(
			"Error, no se han podido disminuir las plazas libres");
	    } else {
		System.out.println("Se generaron plazas libres");
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);

	}

    }

    public void eliminarSocioEnActividad(int idSocio, int idActividad) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	try {
	    String consulta = Settings.get("SQL_DELETE_SOCIO_IN_ACTIVITY");
	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, idActividad);
	    ps.setInt(2, idSocio);

	    ps.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);

	}

    }
}
