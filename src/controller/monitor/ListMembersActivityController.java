package controller.monitor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import java.util.List;

import database.gestorDDBB;
import dto.PlanificacionActividadDto;
import dto.SocioDto;
import utils.Settings;

public class ListMembersActivityController {

    public List<PlanificacionActividadDto> listOfActivitiesByMonitor(int id) {
	List<PlanificacionActividadDto> actividades = new ArrayList<PlanificacionActividadDto>();

	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rsActivities = null;

	try {
	    String consulta = Settings.get("SQL_LIST_ACTIVITIES_BY_MONITOR");
	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, id);

	    rsActivities = ps.executeQuery();

	    // Guardamos las actividades
	    while (rsActivities.next()) {
		PlanificacionActividadDto act = new PlanificacionActividadDto();
		act.setId(rsActivities.getLong("id"));
		act.setNombre(rsActivities.getString("nombre"));
		act.setFecha(rsActivities.getString("fecha"));
		act.setHoraInicio(rsActivities.getString("horaInicio"));
		act.setHoraFin(rsActivities.getString("horaFin"));
		act.setLimitePlazas(rsActivities.getBoolean("limitePlazas"));
		act.setNumLimitePlazas(rsActivities.getInt("numLimitePlazas"));

		actividades.add(act);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rsActivities, ps, conn);

	}
	return actividades;
    }

    public boolean isMonitorInDatabase(int id) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	try {
	    String consulta = Settings.get("SQL_IS_MONITOR");
	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, id);
	    rs = ps.executeQuery();

	    rs.next();
	    // Si el monitor se encuentra en la base de datos
	    if (rs.getInt(1) > 0) {
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);

	}

	return false;
    }

    public List<SocioDto> listSociosByActivity(int idActividad) {
	List<SocioDto> socios = new ArrayList<SocioDto>();

	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	try {
	    String consulta = Settings.get("SQL_LIST_SOCIOS_BY_ACTIVITY");
	    ps = conn.prepareStatement(consulta);

	    ps.setInt(1, idActividad);

	    rs = ps.executeQuery();

	    // Procesamos los socios sacados de la base de datos
	    while (rs.next()) {
		SocioDto socio = new SocioDto();
		socio.setId(rs.getLong("id"));
		socio.setNombre(rs.getString("nombre"));
		socio.setApellidos(rs.getString("apellidos"));
		socio.setDni(rs.getString("dni"));

		socios.add(socio);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return socios;
    }

}
