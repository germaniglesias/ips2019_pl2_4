package controller.monitor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.gestorDDBB;
import dto.MonitorDto;
import utils.Settings;

public class MonitorController {

    private Connection conn;

    public void setConnection(Connection conn) {
	this.conn = conn;
    }

    public MonitorDto buscarMonitor(int idMonitor) {
	MonitorDto monitor = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	try {
	    String consulta = Settings.get("SQL_MONITOR_BY_ID");
	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, idMonitor);

	    rs = ps.executeQuery();

	    if (rs.next()) {
		monitor = new MonitorDto();
		monitor.setId(rs.getLong("id"));
		monitor.setNombre(rs.getString("nombre"));
		monitor.setApellidos(rs.getString("apellidos"));
		monitor.setDni(rs.getString("dni"));
		monitor.setCorreo(rs.getString("correo"));
		monitor.setClave(rs.getString("clave"));
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);

	}
	return monitor;
    }

}
