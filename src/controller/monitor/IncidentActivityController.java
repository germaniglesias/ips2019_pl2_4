package controller.monitor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.gestorDDBB;
import dto.ActividadDto;
import dto.PlanificacionActividadDto;
import utils.Settings;

public class IncidentActivityController {

    public List<PlanificacionActividadDto> listFinishedActivities(
	    int id_monitor, String dia, String hora) {
	List<PlanificacionActividadDto> actFinalizadasSinParte = new ArrayList<PlanificacionActividadDto>();

	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rsActivities = null;

	try {
	    String consulta = Settings
		    .get("SQL_ACTIVIDADES_FINALIZADAS_SIN_PARTE_BY_MONITOR");

	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, id_monitor);
	    ps.setString(2, hora);

	    rsActivities = ps.executeQuery();

	    while (rsActivities.next()) {
		PlanificacionActividadDto act = new PlanificacionActividadDto();
		act.setId(rsActivities.getLong("id"));
		act.setId_Actividad(rsActivities.getLong("id_Actividad"));
		act.setNombre(rsActivities.getString("nombre"));
		act.setFecha(rsActivities.getString("fecha"));
		act.setHoraInicio(rsActivities.getString("horaInicio"));
		act.setHoraFin(rsActivities.getString("horaFin"));

		actFinalizadasSinParte.add(act);

	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rsActivities, ps, conn);
	}
	return actFinalizadasSinParte;
    }

    public ActividadDto findActivityByPlanificacionId(int idPlanificacionId) {
	ActividadDto act = new ActividadDto();

	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rsActivities = null;

	String consulta = Settings.get("SQL_FIND_ACTIVITY_BY_PLANIFICACION_ID");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, idPlanificacionId);
	    rsActivities = ps.executeQuery();

	    while (rsActivities.next()) {
		act = new ActividadDto();
		act.setId(rsActivities.getLong("id"));
		act.setNombre(rsActivities.getString("nombre"));
		act.setIntensidad(rsActivities.getString("intensidad"));
		act.setUsaRecursos(rsActivities.getBoolean("usoRecurso"));
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rsActivities, ps, conn);
	}
	return act;
    }

    public PlanificacionActividadDto findPlanificacionActivityById(
	    Long idPlanificacionActividad) {
	PlanificacionActividadDto act = new PlanificacionActividadDto();

	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	String consulta = Settings
		.get("SQL_FIND_PLANIFICACIONACTIVITY_BY_PLANIFICACION_ID");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, idPlanificacionActividad);
	    rs = ps.executeQuery();

	    while (rs.next()) {
		act = new PlanificacionActividadDto();
		act.setId(rs.getLong("id"));
		act.setId_Actividad(rs.getLong("id_Actividad"));
		act.setId_Monitor(rs.getLong("id_Monitor"));
		act.setId_Instalacion(rs.getLong("id_Instalacion"));
		act.setFecha(rs.getString("fecha"));
		act.setHoraInicio(rs.getString("horaInicio"));
		act.setHoraFin(rs.getString("horaFin"));
		act.setLimitePlazas(rs.getBoolean("limitePlazas"));
		act.setNumLimitePlazas(rs.getInt("numLimitePlazas"));
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}
	return act;
    }

    public void generarParteIncidencias(Long id, int numAproxAsistentes,
	    String incidencias) {

	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	String consulta = Settings
		.get("SQL_GENERAR_PARTE_INCIDENCIAS");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, id);
	    ps.setInt(2, numAproxAsistentes);
	    ps.setString(3, incidencias);
	    
	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(ps);
	    gestorDDBB.close(conn);
	}
    }
}
