package controller.monitor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.gestorDDBB;
import utils.Settings;

public class RegisterMemberController {

    public int getCantidadPersonasApuntadas(int idActividad) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	String consulta = Settings.get("SQL_NUM_PLAZAS_LLENAS_BY_ACTIVITY");

	try {

	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, idActividad);
	    rs = ps.executeQuery();

	    rs.next();
	    if (rs != null)
		return rs.getInt(1);

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return 0;
    }

    public boolean isDniCorrecto(String dni) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	String consulta = Settings.get("SQL_SOCIO_BY_DNI");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setString(1, dni);
	    rs = ps.executeQuery();

	    // Si el dni se encuentra en la base de datos
	    if (rs.next())
		return true;
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return false;
    }

    public void registrarSocioEnActividad(int idSocio, int idActividad) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	String consulta = Settings.get("SQL_REGISTER_SOCIO_IN_ACTIVITY");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, idSocio);
	    ps.setInt(2, idActividad);

	    ps.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}
    }

    public boolean isSocioInActivity(String dni, int idActividad) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	String consulta = Settings.get("SQL_IS_SOCIO_IN_ACTIVITY");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setString(1, dni);
	    ps.setInt(2, idActividad);
	    rs = ps.executeQuery();

	    rs.next();
	    if (rs.getInt(1) > 0)
		return true;
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return false;
    }

    public String getEstadoSocioActividad(int idSocio, int idActividad) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	String consulta = Settings.get("SQL_IS_RESERVA_SOCIO_CANCELADA");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, idSocio);
	    ps.setLong(2, idActividad);

	    rs = ps.executeQuery();
	    if (rs.next())
		return rs.getString("estado");

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(ps);
	    gestorDDBB.close(conn);
	}
	return "";
    }

    public void actualizarEstadoSocioActividad(int idSocio, int idPActividad) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;

	String consulta = Settings.get("SQL_UPDATE_SOCIO_IN_ACTIVITY");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setInt(1, idPActividad);
	    ps.setInt(2, idSocio);
	    ps.executeUpdate();
	    
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(ps);
	    gestorDDBB.close(conn);
	}	
    }

    public boolean isSocioInAnotherActivity(Long idSocio, String horaInicio) {
	Connection conn = gestorDDBB.getConnection();
	PreparedStatement ps = null;
	ResultSet rs = null;

	String consulta = Settings.get("SQL_IS_SOCIO_IN_ANOTHER_ACTIVITY");

	try {
	    ps = conn.prepareStatement(consulta);
	    ps.setLong(1, idSocio);
	    ps.setString(2, horaInicio);
	    rs = ps.executeQuery();

	   if (rs.next())
		return true;
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    gestorDDBB.close(rs, ps, conn);
	}

	return false;
    }

}
