package utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import dto.PlanificacionActividadDto;
import dto.SocioReservaInstalacionDto;

public class TableRender extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
	    boolean selected, boolean focused, int row, int column) {
	super.getTableCellRendererComponent(table, value, selected, focused,
		row, column);
	// se puede modificar el color cuando se requiera distinguir
	// cursos/actividades/reservas socios/ reservas no socios pasando un
	// parametro al constructor
	setForeground(Color.DARK_GRAY);
	setHorizontalAlignment(SwingConstants.CENTER);
	table.getTableHeader().setFont(new Font("MS Gothic", Font.BOLD, 20));
	if (table.getValueAt(row,
		column) instanceof SocioReservaInstalacionDto) {
	    setForeground(Color.WHITE);
	    setBackground(Color.DARK_GRAY);
	} else if (table.getValueAt(row,
		column) instanceof PlanificacionActividadDto) {
	    setBackground(new Color(240, 97, 79));
	} else
	    setBackground(Color.WHITE);
	return this;
    }
}
