package utils;

import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;

import dto.SocioReservaInstalacionDto;

public class ComparadorSocioReservaInstalacion
	implements Comparator<SocioReservaInstalacionDto> {

    @Override
    public int compare(SocioReservaInstalacionDto o1, SocioReservaInstalacionDto o2) {
	Date fecha1 = null;
	Date fecha2 = null;
	try {
	    fecha1 = Fecha.getDateFrom(o1.getFecha());
	    fecha2 = Fecha.getDateFrom(o2.getFecha());
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	return fecha1.compareTo(fecha2);
    }

}
