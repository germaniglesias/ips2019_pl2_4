package utils;

public class Hora {
    
    /**
     * Método para la clase PlanificacionActividad (para no modificar lo que no hice yo)
     * Las horas se expresan como decimales (aunque se guarden como string) de una o dos cifras
     * El método las convierte en horas aptas para realizar sentencias SQL con la BD
     * @param hora
     * @return hora con el formato de la BD (hh:MM:ss)
     */
    public static String crearHora(String hora) {
	int horaInt = Integer.parseInt(hora);
	if (horaInt < 10)
	    return "0" + hora + ":00:00";
	
	return hora + ":00:00";	
    }
}
