package utils;

import javax.swing.table.DefaultTableModel;

public class NonEditableModel extends DefaultTableModel {

    private static final long serialVersionUID = 1L;

    public NonEditableModel(Object[] columnNames, int rowCount) {
	super(columnNames, rowCount);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
	return false;
    }

}