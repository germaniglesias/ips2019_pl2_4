package utils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FechaTest {

    private static Date date;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
	date = new Date();
    }

    @BeforeEach
    void setUp() throws Exception {
    }

    @Test
    void testInicial() {
	LocalDate local = Fecha.dateToLocalDate(date);
	System.out.println("Local Date: " + local);

	System.out.println("Day of week: " + Fecha.stringOfDayOfWeek(date));

	for (int i = 0; i < 6; i++) {
	    local = local.plusDays(1);
	    DayOfWeek dow = local.getDayOfWeek();
	    String output = dow.getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
	    System.out.println("Day of week: " + output);
	}
    }
    
    @Test
    void testFechasBetween() {
	System.out.println("Test Fechas Between");
	Date ini = Fecha.localDateToDate(LocalDate.now());
	Date fin = Fecha.localDateToDate(LocalDate.now().plusDays(10));
	
	List<Date> list = Fecha.getDatesOfWeekdaysInRange(ini, fin, true, false, true, false, false, false);
	
	for (Date d : list) {
	    System.out.println(d);
	}
    }

}
