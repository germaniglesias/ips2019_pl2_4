package utils;

import java.util.Date;

import controller.admin.ActivityInDateController;
import dto.ActividadDto;

public class GestorFechas {
    private ActivityInDateController aidc = new ActivityInDateController();

    public boolean isFechaValida(ActividadDto actividad, Date date) {
	// Buscamos si la actividad se desarrolla ese día
	// Si lo hace la fecha es válida (true) y si no inválida (false)
	return aidc.isFechaValida(actividad, date);

    }
}
