package utils;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JToggleButton;

import controller.MainViewController;
import dto.InstalacionDto;

public class StateBackground implements ItemListener {
    
    private MainViewController mvc;
    
    public StateBackground(MainViewController main) {
	this.mvc=main;
    }
    
	@Override
	public void itemStateChanged(ItemEvent e) {
	    JToggleButton b = (JToggleButton) e.getSource();
	    if (e.getStateChange() == ItemEvent.SELECTED) {
		b.setSelected(true);
		b.setBackground(new Color(240, 97, 79));
	    } else {
		b.setSelected(false);
		b.setBackground(Color.BLACK);
	    }
	    mvc.initTable((InstalacionDto) mvc.getView().getComboBoxFacilities().getSelectedItem());
	}
   }