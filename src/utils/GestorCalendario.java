package utils;

import java.awt.Color;
import java.util.Date;

import com.toedter.calendar.IDateEvaluator;

import dto.ActividadDto;

public class GestorCalendario implements IDateEvaluator {
    private GestorFechas g = new GestorFechas();
    private ActividadDto actividad;
    
    public GestorCalendario(ActividadDto actividad) {
	this.actividad = actividad;
    }

    @Override
    public Color getInvalidBackroundColor() {
	return null;
    }

    @Override
    public Color getInvalidForegroundColor() {
	return null;
    }

    @Override
    public String getInvalidTooltip() {
	return "En este día no se desarrolla la actividad";
    }

    @Override
    public Color getSpecialBackroundColor() {
	return Color.green;
    }

    @Override
    public Color getSpecialForegroundColor() {
	return null;
    }

    @Override
    public String getSpecialTooltip() {
	return "Este día se lleva a cabo la actividad";
    }

    @Override
    public boolean isInvalid(Date date) {
	return !g.isFechaValida(actividad, date);
    }

    @Override
    public boolean isSpecial(Date date) {
	return g.isFechaValida(actividad, date);
    }

}
