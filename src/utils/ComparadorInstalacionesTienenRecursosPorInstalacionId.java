package utils;

import java.util.Comparator;

import dto.InstalacionesTienenRecursosDto;

public class ComparadorInstalacionesTienenRecursosPorInstalacionId
	implements Comparator<InstalacionesTienenRecursosDto> {

    @Override
    public int compare(InstalacionesTienenRecursosDto i,
	    InstalacionesTienenRecursosDto a) {
	if (i.getId_Instalacion() < a.getId_Instalacion())
	    return -1;
	else if (i.getId_Instalacion() > a.getId_Instalacion())
	    return 1;
	return 0;
    }

}
