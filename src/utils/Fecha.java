package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class Fecha {

    public static List<Date> getDatesOfWeekdaysInRange(Date startdate,
	    Date endDate, boolean mon, boolean tue, boolean wed, boolean thu,
	    boolean fri, boolean sat) {
	List<Date> dates = new ArrayList<Date>();
	Calendar calendar = new GregorianCalendar();
	calendar.setTime(startdate);

	while (calendar.getTime().getTime() <= endDate.getTime()) {
	    Date result = calendar.getTime();
	    if ((mon && stringOfDayOfWeek(result).equals("Mon")) // Si se
								 // quieren los
								 // lunes y es
								 // un lunes
		    || (tue && stringOfDayOfWeek(result).equals("Tue"))
		    || (wed && stringOfDayOfWeek(result).equals("Wed"))
		    || (thu && stringOfDayOfWeek(result).equals("Thu"))
		    || (fri && stringOfDayOfWeek(result).equals("Fri"))
		    || (sat && stringOfDayOfWeek(result).equals("Sat"))) {
		dates.add(result);
	    }
	    // Incrementa el dia del calendario
	    calendar.add(Calendar.DATE, 1);
	}

	return dates;
    }

    /**
     * returns the short string of the day of the week in english (Mon, Tue,
     * Wed, Thu, Fri, Sat, Sun)
     * 
     * @param date
     * @return
     */
    public static String stringOfDayOfWeek(Date date) {
	LocalDate d = dateToLocalDate(date);
	DayOfWeek dow = d.getDayOfWeek(); // Extracts a `DayOfWeek` enum object.
	String output = dow.getDisplayName(TextStyle.SHORT, Locale.ENGLISH); // String
									     // =
									     // Tue

	return output;
    }

    /**
     * Transforms the specified date into a LocalDate
     * 
     * @param date
     * @return
     */
    public static LocalDate dateToLocalDate(Date date) {
	return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date localDateToDate(LocalDate localDate) {
	return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault())
		.toInstant());
    }

    public static Month getMonth(Date d) {
	return dateToLocalDate(d).getMonth();
    }

    public static int getYear(Date d) {
	return dateToLocalDate(d).getYear();
    }

    /**
     * @param d
     * @return Devuelve un string con el nombre del mes en español
     */
    public static String getMonthName(Date d) {
	switch (getMonthEnglishName(d)) {
	case "January":
	    return "Enero";
	case "February":
	    return "Febrero";
	case "March":
	    return "Marzo";
	case "April":
	    return "Abril";
	case "May":
	    return "Mayo";
	case "June":
	    return "Junio";
	case "July":
	    return "Julio";
	case "August":
	    return "Agosto";
	case "September":
	    return "Septiembre";
	case "October":
	    return "Octubre";
	case "November":
	    return "Noviembre";
	case "December":
	    return "Diciembre";
	default:
	    return "no sé inglés";
	}
    }

    /**
     * Devuelve un string con el nombre completo del mes en inglés.
     * 
     * @param d
     * @return
     */
    public static String getMonthEnglishName(Date d) {
	return dateToLocalDate(d).getMonth().getDisplayName(TextStyle.FULL,
		new Locale("en"));
    }

    /**
     * Devuelve una lista con dos fechas (Date) que son la misma fecha pasada
     * como parametro con el dia igual a 20 y la fecha del mes siguiente con dia
     * igual a 20.
     * 
     * @param d, Date fecha actual
     * @return a List of two Date (s) siendo la primera la actual en dia 10 y la
     *         segunda la del mes siguiente en dia 20
     */
    public static List<Date> convertToLimitDate(Date d) {
	List<Date> dates = new ArrayList<Date>();
	LocalDate l = LocalDate.of(getYear(d), getMonth(d), 20);
	dates.add(localDateToDate(l));
	l = l.plusMonths(1);
	// l = LocalDate.of(getYear(d), getMonth(d).plus(1), 20);
	dates.add(localDateToDate(l));

	return dates;
    }

    public static Date getNextMonth20th(Date d) {
	LocalDate l = LocalDate.of(getYear(d), getMonth(d), 20);
	l = l.plusMonths(1);
	return localDateToDate(l);
    }

    public static Date getLastMonth20th(Date d) {
	LocalDate l = LocalDate.of(getYear(d), getMonth(d), 20);
	l = l.plusMonths(-1);
	return localDateToDate(l);
    }

    public static Date getThisMonth20th(Date d) {
	LocalDate l = LocalDate.of(getYear(d), getMonth(d), 20);
	return localDateToDate(l);
    }

    /**
     * 
     * @param str, String, DEBE ESTAR en formato dd-MM-yyyy.
     * @return
     * @throws ParseException si el string NO está en formato dd-MM-yyyy
     */
    public static Date getDateFrom(String str) throws ParseException {
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	Date date = formatter.parse(str);
	return date;
    }

    public static int getDay(String string) {
	Date d = null;
	try {
	    d = getDateFrom(string);
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	return dateToLocalDate(d).getDayOfMonth();
    }

    public static String fromDDBBtoApp(String date) {
	String day = date.substring(8);
	String month = date.substring(5, 7);
	String year = date.substring(0, 4);
	return day + "-" + month + "-" + year;
    }

    public static String fromApptoDDBB(String date) {
	String year = date.substring(6);
	String month = date.substring(3, 5);
	String day = date.substring(0, 2);
	return year + "-" + month + "-" + day;
    }
}
