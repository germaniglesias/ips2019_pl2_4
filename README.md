# IPS2019_PL2_4

### INSTALANDO GIT

En Eclipse, vais a **Help -> Install New Software...**
Una vez allí añadís la URL: http://download.eclipse.org/egit/updates y seleccionáis **Eclipse Git Team Provider**
y **JGit**.

---

### COMO ENLAZAR EL RESPOSITORIO BITBUCKET CON ECLIPSE

En Eclipse, Window -> Show View -> Other -> *buscais la carpeta git* -> **Git Repositories**.
Una vez dentro de esa vista, aparecen varios iconos arriba a la derecha. Le dais al que tiene una *flecha verde*
llamado **Clone a Git repository...**
Ahí poneis la url que sale cuando accedéis al proyecto en Bitbucket: 
git clone https://uo270318@bitbucket.org/germaniglesias/ips2019_pl2_4.git y en el siguiente paso seleccionáis 
vuestra rama. Una vez finalizado os sale el repositorio en la vista **Git Respositories** y le dais click derecho.
Con **Import Projects...** ya podréis usarlo.

**IMPORTANTE** : El primer paso es hacer click sobre el proyecto, ir a Team -> Pull (el segundo) y escribir
**develop** en *reference* para obtener la última versión 
de la rama principal y poder trabajar desde ahí. Una vez hecho esto no será necesario repetirlo hasta que no 
se cambie de historia y sea necesaria una nueva base.

---

### COMO AÑADIR FUTURAS RAMAS (HISTORIAS DE USUARIO)

Click derecho en el proyecto, Team-> **Fetch from Upstream**, o desde el repositorio en la vista git.

---

### SOBRE LA ESTRUCTURA DEL PROYECTO BASE
A modo resumen simplificado
 ~ Entity: Campos de la tabla de la base de datos
 ~ Dto: Campos que se enlazan con las vistas
 ~ Controller: Funcionalidad de las vistas


### SOBRE EL REPOSITORIO BITBUCKET

La rama develop está protegida. Por motivos de seguridad es necesario tener **2 revisiones aprobadas** para poder enlazar
la rama a la que hagais push con develop.
Para poder solicitarlo teneis que ir al apartado **Pull request**.

